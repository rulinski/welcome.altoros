<?php
/* Plugin Name: punchCodes
Plugin URI: http://www.themepunch.com
Description: Awesome Shortcodes!
Version: 1.0
Author: ThemePunch
Author URI: http://www.themepunch.com
License: GPLv2 or later
*/

define( 'TPSTRAP_PATH', plugin_dir_path(__FILE__) );
define( 'TPSTRAP_URL',str_replace("index.php","",plugins_url( 'index.php', __FILE__ )));

if ( ! defined( 'ABSPATH' ) )
	die( "Can't load this file directly" );

class ThunderCodes
{
	function __construct() {
		add_action( 'admin_init', array( $this, 'action_admin_init' ) );
	}
	
	function action_admin_init() {
		if ( current_user_can( 'edit_posts' ) && current_user_can( 'edit_pages' ) ) {
			add_filter( 'mce_buttons', array( $this, 'filter_mce_button' ) );
			add_filter( 'mce_external_plugins', array( $this, 'filter_mce_plugin' ) );
		}
	}
	
	function filter_mce_button( $buttons ) {
		array_push( $buttons, '|', 'thundercodes_button' );
		return $buttons;
	}
	
	function filter_mce_plugin( $plugins ) {
		$plugins['thundercodes'] = TPSTRAP_URL . 'editor/thundercodes_plugin.js';
		return $plugins;
	}
}
$thunderCode = new ThunderCodes();

function codesCSS() {
	wp_enqueue_style( 'thunderCodes',TPSTRAP_URL . 'editor/thundercodes.css',array('bootstrap'));
}
//add_action('wp_enqueue_scripts', 'codesCSS','99');

function google_shortcode_scripts() {
	global $post;
	if( has_shortcode( $post->post_content, 'tp_mapgyver') ) {
		wp_enqueue_script('maps_google_script', "http://maps.google.com/maps/api/js?sensor=true", array('jquery'),false,true);
		wp_enqueue_script('gmap3_script', TPSTRAP_URL.'js/gmap3.min.js', array('jquery'),false,true);
	}
}
add_action( 'wp_enqueue_scripts', 'google_shortcode_scripts');


//SHORTCODES
$template_uri_shortcodes = get_template_directory_uri();

/* !SERVICE */
if (!function_exists('tp_service_shortcodes')) {	
	function tp_service_shortcodes( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'image' => '',
			'title' => '',
			'button_url' => '',
			'button_text' => '',
			'button_target' => '_self',
			'button_color_text' => '#ffffff',
			'button_color' => '#65517c',
		), $atts));
		
		$title = empty($title) ? '' : '<h4 class="servicetitle txtshadow">'.$title.'</h4>';
		$button = empty($button_text) ? '' : '<div><a href="'.$button_url.'" target="'.$button_target.'"><button style="background-color:'.$button_color.';color:'.$button_color_text.'" class="decoredbutton fullwidth">'.$button_text.'</button></a></div>';
		$content = empty($content) ? '' : '<div class="servicecontent"><p>'.do_shortcode($content).'</p></div>';
		$image = empty($image) ? '' : '<div class="serviceimg "><img src="'.$image.'" alt="" /></div>';
		
	   return '<article class="tp_service slidemefrombottom">'.$image.$title.$content.$button.'</article>';
	}
	add_shortcode('tp_service', 'tp_service_shortcodes');
}

/* !TABS */
if (!function_exists('tp_tabs_shortcode')) {
	function tp_tabs_shortcode ( $atts, $content=null ) {
			extract (shortcode_atts( array(
			'title' => '',
			'bot_margin' => 25
		), $atts ));
		$content = nl2br($content);
		$title = explode('|', $title);
		$content = explode('|', $content);
		
		$uniq = uniqid("tp_tabs_");
		
		$html = '<article class="navigation-tabs">';
		if( count($title) ){
		   $html .= '<ul id="'.$uniq.'" class="nav nav-tabs">';
		
			if(is_array($title) && is_array($content)){
				for($i=0; $i<count($title); $i++) {
					$html .= '<li><a id="link-'.$uniq."-".str_replace("%","",sanitize_title( $title[$i] )) .'" href="#tab-'.$uniq."-". str_replace("%","",sanitize_title( $title[$i] )) .'"  data-toggle="tab">'. $title[$i] . '</a></li>';
				}
			}
		}
		
		$html .= '</ul><div class="tab-content" id="tab-content-'.$uniq.'">';
		
		if(is_array($content)){
			for($i=0; $i<count($content); $i++) {
				$html .= '<div id="tab-'.$uniq."-". str_replace("%","",sanitize_title( $title[$i] )) .'" class="tab-pane fade">'. do_shortcode( $content[$i] ) .'</div>';
			}
		}
		$html .= '</div></article><p style="display:none"><script>jQuery("#'.$uniq.' li:first , #tab-content-'.$uniq.' div:first").addClass("active in"); //trick for the frontendbuilder plugin  </script></p>';
		return $html;
	}
	add_shortcode( 'tp_tabs', 'tp_tabs_shortcode' );
}

/* !ACCORDIONS */
if (!function_exists('tp_accordion_shortcode')) {
	function tp_accordion_shortcode ( $atts, $content=null ) {
			extract (shortcode_atts( array(
			'title' => '',
			'style' => 'glass',
			'active' => '',
			'bot_margin' => 25
		), $atts ));
		$content = nl2br($content);
		$title = explode('|', $title);
		$active = explode('|', $active);
		$content = explode('|', $content);
		$uniq = uniqid("tp_acc_");
		
		$html =  '<article class="accordion accordion-'.$style.'" id="'.$uniq.'">';
		
		if(is_array($title) && is_array($content)){
			for($i=0; $i<count($title); $i++) {
				if(!empty($active[$i]) && $active[$i] == "true") {
					$active_now = "in";
					$active_tab = "";
				}
				else{
					$active_now = "";
					$active_tab = "collapsed";
				}
				$html .= '<div class="accordion-group "><div class="accordion-heading">
					      <a class="accordion-toggle '.$active_tab.'" data-toggle="collapse" data-parent="#'.$uniq.'" href="#acc_tab_'.$i.'_'.$uniq.'">'.$title[$i].'</a>
					    </div>
					    <div id="acc_tab_'.$i.'_'.$uniq.'" class="accordion-body collapse '.$active_now.'"><div class="accordion-inner">'.$content[$i].'</div></div></div>';
			}
		}
				
		$html .='</article>';
		
		
		$bot_margin = (int)$bot_margin;
		//$html = '<div class="'.$class.'" style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
		
		return $html;
	}
	add_shortcode( 'tp_accordion', 'tp_accordion_shortcode' );
}

/* !PROGRESS */
if (!function_exists('tp_progressbar_shortcode')) {
	function tp_progressbar_shortcode ( $atts, $content=null ) {
			extract (shortcode_atts( array(
			'percent' => '',
			'title' => '',
			'bot_margin' => 25
		), $atts ));
		$percent = explode('|', $percent);
		$title = explode('|', $title);
		$uniq = uniqid("tp_sg_");
		
		$html =  '<article class="skillgroup">';
		
		if(is_array($percent) && is_array($title)){
			for($i=0; $i<count($percent); $i++) {
				if ($i+1==count($percent)) $last = "last-skill";
				else $last = "";
				$html .= '<div class="skill-scale '.$last.'" data-scale="'.str_replace(" ","",$percent[$i]).'"><h5 class="skill-title">'.$title[$i].'</h5></div>';
			}
		}
				
		$html .='</article><p style="display:none"><script>jQuery(document).ready(function(){initSkills();}); //trick for the frontendbuilder plugin  </script></p>';
		
		
		$bot_margin = (int)$bot_margin;
		//$html = '<div class="'.$class.'" style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
		
		return $html;
	}
	add_shortcode( 'tp_progressbar', 'tp_progressbar_shortcode' );
}

/* !TEAM MEMBER */
if (!function_exists('tp_team_member_shortcode')) {
	function tp_team_member_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'image' => '',
			'name' => '',
			'position' => ''
		), $atts));
		
		$image = empty($image) ? '' : '<img src="'.$image.'" alt="" />';
		$name = empty($name) ? '' : '<h4 class="teamname txtshadow txt-center">'.$name.'</h4>';
		$position = empty($position) ? '' : '<p class="teamfunction txt-center">'.$position.'</p>';
		
		$html = '<article class="teamgroup">
					'.$image.'
					'.$name.'
					'.$position.'
					<p class="nobottommargin">'.$content.'</p>
					<div class="contentdivider-mini"></div>
					<ul class="centeredlist teamsocials">';
					
					
		
						if(!empty($atts["mail"]))				
							$html .= '<li><a href="mailto:'.$atts["mail"].'" target="_blank"><div class="icon-mail"></div></a></li>';
						if(!empty($atts["phone"]))				
							$html .= '<li><a href="callto:'.$atts["phone"].'" target="_blank"><div class="icon-phone"></div></a></li>';
						if(!empty($atts["facebook"]))				
							$html .= '<li><a href="'.$atts["facebook"].'" target="_blank"><div class="icon-s-facebook"></div></a></li>';
						if(!empty($atts["twitter"]))								
							$html .= '<li><a href="'.$atts["twitter"].'" target="_blank"><div class="icon-s-twitter"></div></a></li>';
						if(!empty($atts["gplus"]))								
							$html .= '<li><a href="'.$atts["gplus"].'" target="_blank"><div class="icon-s-gplus"></div></a></li>';
						if(!empty($atts["linkedin"]))								
							$html .= '<li><a href="'.$atts["linkedin"].'" target="_blank"><div class="icon-s-linkedin"></div></a></li>';
							
		
		return $html.'</ul></article>';

	}
	add_shortcode( 'tp_team_member', 'tp_team_member_shortcode' );
}
/* !TEAM WALL */
if (!function_exists('tp_teamwall_shortcode')) {
	function tp_teamwall_shortcode ( $atts, $content=null ) {
			extract (shortcode_atts( array(
			'image' => '',
			'name' => '',
			'position' => '',
			'columns' => 'threecolumn',
			'mail' => '',
			'phone' => '',
			'facebook' => '',
			'twitter' => '',
			'linkedin' => '',
			'gplus' => '',
			'bot_margin' => 25
		), $atts ));
		$image = explode('|', $image);
		$name = explode('|', $name);
		$position = explode('|', $position);
		$mail = explode('|', $mail);
		$phone = explode('|', $phone);
		$facebook = explode('|', $facebook);
		$twitter = explode('|', $twitter);
		$gplus = explode('|', $gplus);
		$linkedin = explode('|', $linkedin);
		
		$html =  '<div id="teamgroup1" class="mediawall"><article class="mediawall-gallery team-gallery" data-maxshownitems="99">';
		
		if(is_array($name) && is_array($image)){
			for($i=0; $i<count($name); $i++) {
				$image[$i] = empty($image[$i]) ? '' : '<div class="mediawall-mediacontainer"><img src="'.$image[$i].'" alt=""/><div class="mediawall-overlay"></div></div>';
				$name[$i] = empty($name[$i]) ? '' : '<h4 class="mediawall-teamname txtshadow">'.$name[$i].'</h4>';
				$position[$i] = empty($position[$i]) ? '' : '<p class="mediawall-teamfunction">'.$position[$i].'</p>';
				$html .= '<div class="'.$columns.' item">
							'.$image[$i].'
							<ul>';
			
							if(!empty($mail[$i]))				
								$html .= '	<li class="mediawall-link notalone"><a href="mailto:'.$mail[$i].'" target="_blank"><i class="icon-mail"></i></a></li>';
							if(!empty($phone[$i]))				
								$html .= '	<li class="mediawall-link notalone"><a href="callto:'.$phone[$i].'" target="_blank"><i class="icon-phone"></i></a></li>';
							if(!empty($facebook[$i]))				
								$html .= '	<li class="mediawall-link notalone"><a href="'.$facebook[$i].'" target="_blank"><i class="icon-s-facebook"></i></a></li>';
							if(!empty($twitter[$i]))								
								$html .= '	<li class="mediawall-link notalone"><a href="'.$twitter[$i].'" target="_blank"><i class="icon-s-twitter"></i></a></li>';
							if(!empty($gplus[$i]))								
								$html .= '	<li class="mediawall-link notalone"><a href="'.$gplus[$i].'" target="_blank"><i class="icon-s-gplus"></i></a></li>';
							if(!empty($linkedin[$i]))								
								$html .= '	<li class="mediawall-link notalone"><a href="'.$linkedin[$i].'" target="_blank"><i class="icon-s-linkedin"></i></a></li>';
	
				$html .= '   </ul>  
						   
						
						  <div class="mediawall-content">	
							'.$name[$i].'
							'.$position[$i].'
						  </div></div>';
			}
		}
		$bot_margin = (int)$bot_margin;
		//$html = '<div class="'.$class.'" style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
		
		return $html.'</article></div><p style="display:none"><script>jQuery(document).ready(function(){initMediaWall()}); //trick for the frontendbuilder plugin  </script></p>';
	}
	add_shortcode( 'tp_teamwall', 'tp_teamwall_shortcode' );
}

/* !PRICING TABLE */
if (!function_exists('tp_pricetable_column_shortcode')) {
	function tp_pricetable_column_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'width' => 'onethird',
			'style' => 'colored',
			'highlight' => '',
			'headline' => '',
			'subline' => '',
			'price' => '',
			'currency' => '',
			'price_subline' => '',
			'row1' => '',
			'row2' => '',
			'row3' => '',
			'row4' => '',
			'row5' => '',
			'row6' => '',
			'row7' => '',
			'row8' => '',
			'row9' => '',
			'row10' => '',
			'button_url' => '#',
			'button_text' => '',
			'button_color_text' => '#ffffff',
			'button_color' => '#65517c',
			'button_target' => '_self'
		), $atts));	
		
		$width = $width == 'onefourth' ? 'fourcolumn' : 'threecolumn';
		
		$highlight = explode('|', $highlight);
		$headline = explode('|', $headline);
		$subline = explode('|', $subline);
		$price = explode('|', $price);
		$currency = explode('|', $currency);
		$price_subline = explode('|', $price_subline);
		$button_url = explode('|', $button_url);
		$button_text = explode('|', $button_text);
		$button_color_text = explode('|', $button_color_text);
		$button_color = explode('|', $button_color);
		$button_target = explode('|', $button_target);
		$row1 = explode('|', $row1);
		$row2 = explode('|', $row2);
		$row3 = explode('|', $row3);
		$row4 = explode('|', $row4);
		$row5 = explode('|', $row5);
		$row6 = explode('|', $row6);
		$row7 = explode('|', $row7);
		$row8 = explode('|', $row8);
		$row9 = explode('|', $row9);
		$row10 = explode('|', $row10);
		
		$html = "";
		$hashighlight = "";
		
		if(is_array($price)){
			for($i=0; $i< sizeof($price); $i++) {
			
				$highlight_here = !empty($highlight[$i]) && $highlight[$i]=="true" ? 'highlight' : '';
				$hashighlight = "hashighlight";
				
				$html  .= '<ul class="pricingtable pt'.$style.' '.$width.' '.$highlight_here.'">';
				$html .= '<li><h5 class="tableheder">'.$headline[$i].'</h5><p class="tablesubheader">'.$subline[$i].'</p></li>';
				$html .= '<li class="pricerow"><span class="currency">'.$currency[$i].'</span><span class="price">'.$price[$i].'</span><span class="subprice">'.$price_subline[$i].'</span></li>';
				if(!empty($row1[$i])) $html .= '<li><p>'.$row1[$i].'</p></li>';
				if(!empty($row2[$i])) $html .= '<li><p>'.$row2[$i].'</p></li>';
				if(!empty($row3[$i])) $html .= '<li><p>'.$row3[$i].'</p></li>';
				if(!empty($row4[$i])) $html .= '<li><p>'.$row4[$i].'</p></li>';
				if(!empty($row5[$i])) $html .= '<li><p>'.$row5[$i].'</p></li>';
				if(!empty($row6[$i])) $html .= '<li><p>'.$row6[$i].'</p></li>';
				if(!empty($row7[$i])) $html .= '<li><p>'.$row7[$i].'</p></li>';
				if(!empty($row8[$i])) $html .= '<li><p>'.$row8[$i].'</p></li>';
				if(!empty($row9[$i])) $html .= '<li><p>'.$row9[$i].'</p></li>';
				if(!empty($row10[$i])) $html .= '<li><p>'.$row10[$i].'</p></li>';
				
				if(!empty($button_text[$i])){
					if($style=="glas" && (!empty($highlight_here) && $highlight_here=="highlight") ) $button_color[$i] = "rgba(255,255,255,0.05);";
					if($style=="glas" && ( empty($highlight_here) || $highlight_here=="") ) $button_color[$i] = "rgba(0,0,0,0.25);";
					$button_color_text[$i] = empty($button_color_text[$i]) ? "#ffffff" : $button_color_text[$i];
					if($style=="glas") $button_color_text[$i] = "";
					$html .= '<li><a href="'.$button_url[$i].'"  target="'.$button_target[$i].'"><button style="color:'.$button_color_text[$i].';background-color:'.$button_color[$i].'" class="decoredbutton fullwidth txtshadow" target="'.$button_target[$i].'">'.$button_text[$i].'</button></a></li>';
				}
				$html .= '</ul>';
			}
		}	
		return '<div class="'.$hashighlight.' pricetable_wrapper">'.$html.'<div class="clear"></div></div>';
	}
	add_shortcode( 'tp_pricetable', 'tp_pricetable_column_shortcode' );
}

// !CLEAR
if (!function_exists('tp_clear_shortcode')) {
	function tp_clear_shortcode( $atts, $content = null ) {
		return '<div style="clear:both"></div>';

	}
	add_shortcode( 'tp_clear', 'tp_clear_shortcode' );
}

// !DIVIDER
if (!function_exists('tp_divider_shortcode')) {
	function tp_divider_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'top_margin' => '0',
			'bot_margin' => '0'
		), $atts));	
		return '<div class="contentdivider" style="margin-bottom: '.str_replace(" px","",$bot_margin).'px; margin-top: '.str_replace(" px","",$top_margin).'px;" ></div>';

	}
	add_shortcode( 'tp_divider', 'tp_divider_shortcode' );
}


// !HEADLINE
if (!function_exists('tp_headline_shortcode')) {
	function tp_headline_shortcode( $atts, $content = null ) {
		return '<h4 class="headline-title ">'.$content.'</h4>';

	}
	add_shortcode( 'tp_headline', 'tp_headline_shortcode' );
}

// !VIDEO
if (!function_exists('tp_video_shortcode')) {
	function tp_video_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'fitvid' => 'false',
			'type' => 'iframe',
			'link' => 'http://video-js.zencoder.com/oceans-clip.mp4',
			'poster' => 'http://video-js.zencoder.com/oceans-clip.png'
		), $atts));
		$uniq = uniqid("tp_video_");
		if(!empty($atts["fitvid"]) && $atts["fitvid"]=="true"){
			 $fitvid = '<p style="display:none"><script>if(jQuery(".fbuilder_row_controls").length) jQuery("#'.$uniq.'").fitVids(); //trick for the frontendbuilder plugin  </script></p>';
			 $class = 'fitvideo';
		}
		else {
			$fitvid = "";
			$class = "";
		}

		if($type == "iframe"){		
			$video = '<div id="'.$uniq.'" class="fitvid '.$class.'">'.$content.'</div>'.$fitvid;
		}
		else {
			$video = '<div class="video-wrapper"><video width="100%" height="100%" class="video-js vjs-default-skin" controls="controls" preload="none" poster="'.$poster.'" data-setup="{}">
			
			<source src="'.$link.'" type="video/mp4" />
			
			<object width="100%" height="100%" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0">
							<param name="src" value="'.get_home_url().'/wp-includes/js/tinymce/plugins/media/moxieplayer.swf" /><param name="flashvars" value="url='.urlencode($link).'&amp;poster='.urlencode($poster).'" /><param name="allowfullscreen" value="true" />
							<param name="allowscriptaccess" value="true" />
							<embed width="100%" height="100%" type="application/x-shockwave-flash" src="'.get_home_url().'/wp-includes/js/tinymce/plugins/media/moxieplayer.swf" flashvars="url='.urlencode($link).'&amp;poster='.urlencode($poster).'" allowfullscreen="true" allowscriptaccess="true" />
					</object>
					<track kind="captions" src="demo.captions.vtt" srclang="en" label="English"></track> </video></div>';
		}
		return $video;

	}
	add_shortcode( 'tp_video', 'tp_video_shortcode' );
}

/* !BUTTON */
if (!function_exists('tp_button_shortcodes')) {	
	function tp_button_shortcodes( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'button_url' => '',
			'button_text' => '',
			'button_target' => '_self',
			'button_color' => '#65517c',
			'button_color_text' => '#ffffff',
			'fullwidth' => 'false',
			'decoredbutton' => 'false',
			'centered' => 'false'
		), $atts));
		
		$fullwidth = $fullwidth=="false" || empty($fullwidth) ? '' : 'fullwidth';
		$decoredbutton = $decoredbutton=="false" || empty($decoredbutton) ? '' : 'decoredbutton';
		$centered = $centered=="false" ? "noncentered" : "";

		$button = empty($button_text) ? '' : '<a href="'.$button_url.'" class="smoothscroll" target="'.$button_target.'"><button style="color:'.$button_color_text.';background-color:'.$button_color.'" class="btn '.$centered.' txtshadow '.$fullwidth.' '.$decoredbutton.'">'.$button_text.'</button></a>';
		return $button;
	}
	add_shortcode('tp_button', 'tp_button_shortcodes');
}

// !SPACER
if (!function_exists('tp_spacer_shortcode')) {
	function tp_spacer_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'height' => '',
			'visible_phone' => '',
			'visible_tablet' => '',
			'visible_desktop' => '',
			'hidden_phone' => '',
			'hidden_tablet' => '',
			'hidden_desktop' => '',
		), $atts));
		
		if($visible_phone) $visible_phone = "visible-phone";
		if($visible_tablet) $visible_tablet = "visible-tablet";
		if($visible_desktop) $visible_desktop = "visible-desktop";
		if($hidden_phone) $hidden_phone = "hidden-phone";
		if($hidden_desktop) $hidden_desktop = "hidden-desktop";
		if($hidden_tablet) $hidden_tablet = "hidden-tablet";		
		
		return '<div style="height:'.str_replace(" ","",$height).'" class="'.$visible_phone.' '.$visible_tablet.' '.$visible_desktop.' '.$hidden_phone.' '.$hidden_tablet.' '.$hidden_desktop.'"></div>';

	}
	add_shortcode( 'tp_spacer', 'tp_spacer_shortcode' );
}

// !TEXT
if (!function_exists('tp_text_shortcode')) {
	function tp_text_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'boxed' => 'false'
		), $atts));
	    if(isset($atts["boxed"]) && $atts["boxed"]!="false") $content = '<div class="boxedbg boxedpadding">'.$content.'</div>';
		return do_shortcode(wpautop($content));
	}
	add_shortcode( 'tp_text', 'tp_text_shortcode' );
	add_shortcode( 'fbuilder_text', 'tp_text_shortcode' );
}

// !SHORTCODE
if (!function_exists('tp_shortcode_shortcode')) {
	function tp_shortcode_shortcode( $atts, $content = null ) {
		$array = array (
	                '<p>[' => '[',
	                ']</p>' => ']',
	                ']<br />' => ']'
	        );
	    $content = strtr($content, $array);
		if(isset($atts["boxed"]) && $atts["boxed"]!="false") $content = '<div class="boxedbg boxedpadding">'.$content.'</div>';
		return $content;
	}
	add_shortcode( 'tp_shortcode', 'tp_shortcode_shortcode' );
}

// !IMAGE
if (!function_exists('tp_image_shortcodes')) {	
	function tp_image_shortcodes( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'text_align' => '',
			'link' => '',
			'link_type' => '',
			'title' => '',
			'meta' => ''
			
		), $atts));
		
		if(!empty($text_align)) $text_align = "align".$text_align;
		
		if(!empty($link)){
			switch($link_type){
				case 'new-tab': 
						$image = '<a href="'.$link.'" target="_blank"><img src="'.$content.'" alt="" class="'.$text_align.'  size-full" /></a>';
					break;
				case 'lightbox-image':
						$image = '<a href="'.$link.'" class="singlepunchbox" data-ref="'.$link.'" data-title="'.$title.'" data-meta="'.$meta.'"><img src="'.$content.'" alt="" class="'.$text_align.'  size-full" /></a>';
					break;
				case 'standard':
						$image = '<a href="'.$link.'"><img src="'.$content.'" alt="" class="'.$text_align.' size-full" /></a>';
					break;
				default:
						$image = '<img src="'.$content.'" alt="" class="'.$text_align.' class="size-full" />';
					break;
			}
		}
		else {
			$image = '<a class="'.$text_align.'" style="cursor:arrow"><img src="'.$content.'" alt="" class="size-full" /></a>';
		}
		if($link) $image .= '<p style="display:none"><script>jQuery("body").punchBox({
				// SELECTOR
				items:".singlepunchbox",

				// NAVIGATION SETTINGS
				navigation:{
					container:"insidemedia",		// Put Nav inside or outside the Container
					position:"dettached",			// Dettached, topleft, topright, top, bottomleft,bottom,bottomright
					autoplay:"disabled",					// Enable / Disable Navigation (disable)
					autoplaydelay:0,		// Timer after how many ms the Slide changes (5000)
				},

				// THE META INFO
				metaInfo:{
					showMeta:true,
					orderMarkup:"%n / %m",
					metaMarkup: "<div class=\"pb-metawrapper\"><div class=\"pb-title\">%title%</div><div class=\"pb-metadata\">%metadata%</div><div class=\"pb-order\">%ordermarkup%</div></div>",
				},

				// THE CALLBACKS
				callbacks:{
					ready:function(slide,oldslide) {
						//alert("ready"+slide.html());
					},
					beforeAnim:function(slide,oldslide) {
						//alert("beforeAnim");
					},
					afterAnim:function(slide,oldslide) {
						//alert("afterAnim");
					}
				}

             });</script></p>'; 
		return $image;
	}
	add_shortcode('tp_image', 'tp_image_shortcodes');
}

// !GOOGLE MAP
if (!function_exists('tp_mapgyver_shortcode')) {
	function tp_mapgyver_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
			'address' => 'Lüderichstraße 2 51105 Köln',
			'zoom' => '15',
			'height' => '400'
		), $atts));
		
		return '<div id="mapgyver_holder" data-address="'.$address.'" data-zoom="'.trim($zoom).'" data-height="'.trim($height).'" ><div class="content-behind-map">'.do_shortcode($content).'</div></div><p style="display:none"><script>if(jQuery(".fbuilder_row_controls").length) mapgyver();</script></p><div class="clear"></div>';

	}
	add_shortcode( 'tp_mapgyver', 'tp_mapgyver_shortcode' );
}

// !WIDGET AREA
if (!function_exists('tp_widgetarea_shortcode')) {
	function tp_widgetarea_shortcode( $atts, $content = null ) {
		$html = '<section class="tp_widgetarea">';	
		ob_start();
		dynamic_sidebar($content);
		$html .= ob_get_contents();
		ob_end_clean();
		$html .='</section>';
		return $html;
	}
	add_shortcode( 'tp_widget_area', 'tp_widgetarea_shortcode' );
}		

/* !MEDIA WALL */
if (!function_exists('tp_mediawall_shortcode')) {
	function tp_mediawall_shortcode ( $atts, $content=null ) {
			extract (shortcode_atts( array(
			'image' => '',
			'name' => '',
			'height' => '205',
			'autoplay' => 'false',
			'autoplaydelay' => '5',
			'lightbox_image' => '',
			'subtitle' => '',
			'lightbox_type' => '',
			'iframe' => '',
			'columns' => 'threecolumn',
			'description' => '',
			'nodescbox' => ''
		), $atts ));
		$image = explode('|', $image);
		$nodescbox = explode('|', $nodescbox);
		$content = explode('|', $content);
		$lightbox_image = explode('|', $lightbox_image);
		$name = explode('|', $name);
		$lightbox_type = explode('|', $lightbox_type);
		$subtitle = explode('|', $subtitle);
		$description = explode('|', $description);
		
		if($autoplay!="false"){
			$autoplay = 'data-autoplay="enabled" data-autoplaydelay="'.(trim($autoplaydelay)*1000).'"';
		}
		else $autoplay = "";
		
		$html =  '<div class="mediawall" '.$autoplay.'><article class="mediawall-gallery team-gallery" data-maxshownitems="99999">';
		if(is_array($name) && is_array($image)){
			for($i=0; $i<count($name); $i++) {
				$orig_image = $image[$i];
				$originalname = $name[$i];
				$image[$i] = $height>0 && aq_resize($image[$i],500,$height,true) ? aq_resize($image[$i],500,$height,true) : $image[$i];
				$image[$i] = empty($image[$i]) ? '' : '<div class="mediawall-mediacontainer"><img src="'.$image[$i].'" alt=""/><div class="mediawall-overlay"></div></div>';
				
				
				
				if($nodescbox[$i]=="true"){
					$originalname = "";
					$description[$i] = "";
				}
				
				switch($lightbox_type[$i]){
					case 'lightbox-image':
							$lightbox_image[$i] = empty($lightbox_image[$i]) ? $orig_image : $lightbox_image[$i];
							$lightbox = '<ul><li class="mediawall-lightbox notalone"><a class="punchbox" data-metas="'.$nodescbox[$i].'" data-ref="'.$lightbox_image[$i].'" data-rel="gallery" data-meta="'.$description[$i].'" data-title="'.$originalname.'"><i class="icon-search"></i></a></li></ul>';
							break;
					case 'video':
							$video_iframe = $content[$i];
							preg_match('/src="(.*?)"/', $video_iframe, $src); 
							$video_url = $src[1];
							preg_match('/height="(.*?)"/', $video_iframe, $src); 
							$video_height = $src[1];
							preg_match('/width="(.*?)"/', $video_iframe, $src); 
							$video_width = $src[1];
							$lightbox = '<ul><li class="mediawall-lightbox notalone"><a class="punchbox" data-metas="'.$nodescbox[$i].'" data-ref="'.$video_url.'" data-rel="gallery" data-title="'.$originalname.'" data-meta="'.$description[$i].'" data-width="'.$video_width.'" data-height="'.$video_height.'" data-type="iframe"><i class="icon-search"></i></a></li></ul>';
							break;
					default:
							$lightbox = '<ul><li class="mediawall-lightbox notalone"><a class="punchbox" data-metas="'.$nodescbox[$i].'" data-ref="'.$orig_image.'" data-rel="gallery" data-meta="'.$description[$i].'" data-title="'.$originalname.'"><i class="icon-search"></i></a></li></ul>';
							break;
				}
					
					
				$styletitle = empty($subtitle[$i]) ? 'style="border-bottom:none;"' : '';
				$name[$i] = empty($name[$i]) ? '' : '<h4 class="mediawall-teamname txtshadow" '.$styletitle.'>'.$name[$i].'</h4>';
				$subtitle[$i] = empty($subtitle[$i]) ? '' : '<p class="mediawall-teamfunction">'.$subtitle[$i].'</p>';
				
				$html .= '<div class="'.$columns.' item">'.$image[$i].' '.$lightbox.' <div class="mediawall-content">'.$name[$i].' '.$subtitle[$i].'</div></div>';
			}
		}
		return $html.'</article></div><p style="display:none"><script>jQuery(document).ready(function(){if(jQuery(".fbuilder_row_controls").length) initMediaWall()});</script></p>';
	}
	add_shortcode( 'tp_mediawall', 'tp_mediawall_shortcode' );
}


if(!function_exists('aq_resize')){
	/**
* Title		: Aqua Resizer
* Description	: Resizes WordPress images on the fly
* Version	: 1.1.4
* Author	: Syamil MJ
* Author URI	: http://aquagraphite.com
* License	: WTFPL - http://sam.zoy.org/wtfpl/
* Documentation	: https://github.com/sy4mil/Aqua-Resizer/
*
* @param string $url - (required) must be uploaded using wp media uploader
* @param int $width - (required)
* @param int $height - (optional)
* @param bool $crop - (optional) default to soft crop
* @param bool $single - (optional) returns an array if false
* @uses wp_upload_dir()
*
* @return str|array
*/

function aq_resize( $url, $width, $height = null, $crop = null, $single = true, $retina = false ) {
		
		 //validate inputs
		 if(!$url OR !$width ) return false;
		
		 //define upload path & dir
		 $upload_info = wp_upload_dir();
		 $upload_dir = $upload_info['basedir'];
		 $upload_url = $upload_info['baseurl'];
		
		 //check if $img_url is local
		 if(strpos( $url, $upload_url ) === false) return false;
		
		 //define path of image
		 $rel_path = str_replace( $upload_url, '', $url);
		 $img_path = $upload_dir . $rel_path;
		
		 //check if img path exists, and is an image indeed
		 if( !file_exists($img_path) OR !getimagesize($img_path) ) return false;
		
		 //get image info
		 $info = pathinfo($img_path);
		 $ext = $info['extension'];
		 list($orig_w,$orig_h) = getimagesize($img_path);
		
		 //get image size after cropping
		 $dims = image_resize_dimensions($orig_w, $orig_h, $width, $height, $crop);
		 $dst_w = $dims[4];
		 $dst_h = $dims[5];
		
		 //use this to check if cropped image already exists, so we can return that instead
		 $suffix = "{$dst_w}x{$dst_h}";
		 $dst_rel_path = str_replace( '.'.$ext, '', $rel_path);
		 $destfilename = "{$upload_dir}{$dst_rel_path}-{$suffix}.{$ext}";
		
		 //Retina Image
		 if($retina){
		  if(!$dst_h) {
		   //can't resize, so return original url
		   $img_url = $url;
		   $dst_w = $orig_w;
		   $dst_h = $orig_h;
		  }
		  //else check if cache exists
		  elseif(file_exists(str_replace(".".$ext,"@2x.".$ext,$destfilename)) && getimagesize(str_replace(".".$ext,"@2x.".$ext,$destfilename))) {
		   $img_url = "{$upload_url}{$dst_rel_path}-{$suffix}@2x.{$ext}";
		  } 
		  //else, we resize the image and return the new resized image url
		  else {
		   if(function_exists('wp_get_image_editor')) {
		    $editor = wp_get_image_editor($img_path);
		    if ( is_wp_error( $editor ) || is_wp_error( $editor->resize( $width*2, $height*2, $crop ) ) )
		     return false;
		 
		    $resized_img_path = $editor->save();
		 
		   } else {
		    	//$resized_img_path = image_resize( $img_path, $width*2, $height*2, $crop ); // Fallback foo
		    	return false;
		   }
		 
		   if(!is_wp_error($resized_img_path)) {
		    rename($resized_img_path["path"], str_replace(".".$ext,"@2x.".$ext,$destfilename));
		   } else {
		    return false;
		   }
		 
		  }
		 }
		 
		
		 if(!$dst_h) {
		  //can't resize, so return original url
		  $img_url = $url;
		  $dst_w = $orig_w;
		  $dst_h = $orig_h;
		 }
		 //else check if cache exists
		 elseif(file_exists($destfilename) && getimagesize($destfilename)) {
		  $img_url = "{$upload_url}{$dst_rel_path}-{$suffix}.{$ext}";
		 } 
		 //else, we resize the image and return the new resized image url
		 else {
		  if(function_exists('wp_get_image_editor')) {
		   $editor = wp_get_image_editor($img_path);
		
		   if ( is_wp_error( $editor ) || is_wp_error( $editor->resize( $width, $height, $crop ) ) )
		    return false;
		
		   $resized_img_path = $editor->save();
		
		  } else {
		   //$resized_img_path = image_resize( $img_path, $width, $height, $crop ); // Fallback foo
		   return false;
		  }
		
		  if(!is_wp_error($resized_img_path)) {
		   $resized_rel_path = str_replace( $upload_dir, '', $resized_img_path);
		   $img_url = $upload_url . $resized_rel_path;
		  } else {
		   return false;
		  }
		
		 }
		
		 //return the output
		 if($single) {
		  //str return
		  $image = $img_url;
		 } else {
		  //array return
		  $image = array (
		   0 => $img_url,
		   1 => $dst_w,
		   2 => $dst_h
		  );
		 }
		
		
		$image = $suffix == "x" ? "{$upload_url}{$dst_rel_path}.{$ext}" : "{$upload_url}{$dst_rel_path}-{$suffix}.{$ext}"; 
		
		 return $image;
		}
}

if(!function_exists('has_shortcode')){
  function has_shortcode($shortcode = '') {
	
	$post_to_check = get_post(get_the_ID());
	
	// false because we have to search through the post content first
	$found = false;
	
	// if no short code was provided, return false
	if (!$shortcode) {
		return $found;
	}
	// check the post content for the short code
	if ( stripos($post_to_check->post_content, '[' . $shortcode) !== false ) {
		// we have found the short code
		$found = true;
	}
	
	// return our final results
	return $found;
 }
}

?>