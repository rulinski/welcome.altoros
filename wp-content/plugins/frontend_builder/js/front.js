(function ($) {
    var fbuilder_shortcode_sw = false;
    var fbuilderRemoveTemplateFlag = false;

    $(document).ready(function () {
        if (fbuilder_sw == 'on' && fbuilder_user) {
            var html = '<div class="fbuilder_header">'
            html += '<a href="#" class="fbuilder_save"><img src="' + fbuilder_url + 'images/ajax-loader.gif" alt="" class="save_loader" /><img src="' + fbuilder_url + 'images/icons/save-page.png" alt="" /><span>Save Page</span></a>';
            html += '<a href="#" class="fbuilder_save_template"><img src="' + fbuilder_url + 'images/icons/save-page-template.png" alt="" /><span>Save/Export Template</span></a>';
            html += '<a href="#" class="fbuilder_load"><img src="' + fbuilder_url + 'images/icons/load-page-template.png" alt="" /><span>Load/Import Page</span></a>';
            html += '<a href="' + frbPagePermalink + '" class="fbuilder_permalink_preview_trigger" target="_blank"><i class="fa fa-link"></i><span>Preview Page</span></a>';
            html += '<a href="#" class="fbuilder_toggle"><img src="' + fbuilder_url + 'images/icons/save-hide-builder-controls.png" alt="" /><span>Show / Hide Builder Controls</span></a>';
            html += '<a href="#" class="fbuilder_toggle_zoom_trigger"><i class="fa fa-search"></i><span>Toggle Zoom</span></a>';

            html += '<a href="#" class="fbuilder_toggle_screen" data-width="1200"><i class="icon-desktop fawesome"></i></a>';
            html += '<a href="#" class="fbuilder_toggle_screen" data-width="960"><i class="icon-laptop fawesome"></i></a>';
            html += '<a href="#" class="fbuilder_toggle_screen" data-width="768"><i class="icon-tablet fawesome"></i></a>';
            html += '<a href="#" class=" fbuilder_toggle_screen" data-width="340"><i class="icon-mobile-phone fawesome"></i></a>';

            html += '<a href="#" class="fbuilder_add_row_popup_trigger"><i class="fa fa-plus"></i><span>Add new Row</span></a>';
            html += '</div>';
		
	    /* Removed Drag & Drop Sidebar  */
            /*html += '<div id="fbuilder_main_menu" class="fbuilder_controls_wrapper"><form autocomplete="off"><div class="fbuilder_menu_inner">';

            for (var x in fbuilder_main_menu) {
                if (x == 'fbuilder_layout') {
                    if (typeof fbuilder_items['sidebar'] != 'undefined' && fbuilder_items['sidebar']['active'] == true)
                        fbuilder_main_menu[x]['std'] = fbuilder_items['sidebar']['type'];
                }
                var newControl = new fbuilderControl(x, fbuilder_main_menu[x]);
                html += newControl.html();
            }
            html += '</div></form></div>';*/


            html += '<div id="fbuilder_add_shortcode_popup" class="fbuilder_popup">';
            for (var x in fbuilder_main_menu) {
                fbuilder_main_menu[x]['type'] = 'shortcode-popup';
                var newControl = new fbuilderControl(x, fbuilder_main_menu[x]);
                html += newControl.html();
            }
            html += '<a href="#" class="fbuilder_gradient fbuilder_button fbuilder_popup_close right">Close</a></div>';


            html += '<div id="fbuilder_add_row_popup" class="fbuilder_popup"><span class="frb_headline">Chose Row Type:</span><div class="clear"></div>';
            for (var x in fbuilder_rows) {
                var newRow = fbuilder_rows[x];
                html += '<a href="#' + x + '" class="fbuilder_row_button fbuilder_gradient" title="' + newRow.label + '"><img src="' + newRow.image + '" alt="" /></a>';
            }
            html += '<div class="clear"></div><a href="#" class="fbuilder_gradient fbuilder_button">Close</a></div>';

            $('#fbuilder_body').css({borderLeftWidth: 0, borderTopWidth: 37});
            $('body').append(html);

//			$('#fbuilder_editor-tmce').trigger('click');
        }
        else if (fbuilder_showall && fbuilder_sw == 'on') {
            var html = '<div class="fbuilder_header">'
            html += '<a href="#" class="fbuilder_disabled"><img src="' + fbuilder_url + 'images/icons/save-page.png" alt="" /><span>Save Page</span></a>';
            html += '<a href="#" class="fbuilder_disabled"><img src="' + fbuilder_url + 'images/icons/save-page-template.png" alt="" /><span>Save as Page Template</span></a>';
            html += '<a href="#" class="fbuilder_load"><img src="' + fbuilder_url + 'images/icons/load-page-template.png" alt="" /><span>Load Page</span></a>';
            html += '<a href="' + frbPagePermalink + '" class="fbuilder_permalink_preview_trigger" target="_blank"><i class="fa fa-link"></i><span>Preview Page</span></a>';
            html += '<a href="#" class="fbuilder_toggle"><img src="' + fbuilder_url + 'images/icons/save-hide-builder-controls.png" alt="" /><span>Show / Hide Builder Controls</span></a>';
            html += '<a href="#" class="fbuilder_toggle_zoom_trigger"><i class="fa fa-search"></i><span>Toggle Zoom</span></a>';

            html += '<a href="#" class="fbuilder_toggle_screen" data-width="1200"><i class="icon-desktop fawesome"></i></a>';
            html += '<a href="#" class="fbuilder_toggle_screen" data-width="960"><i class="icon-laptop fawesome"></i></a>';
            html += '<a href="#" class="fbuilder_toggle_screen" data-width="768"><i class="icon-tablet fawesome"></i></a>';
            html += '<a href="#" class="fbuilder_toggle_screen" data-width="340"><i class="icon-mobile-phone fawesome"></i></a>';

            html += '<a href="#" class="fbuilder_add_row_popup_trigger"><i class="fa fa-plus"></i><span>Add new Row</span></a>';
            html += '</div>';

	    /* Removed Drag & Drop Sidebar */
            /*html += '<div id="fbuilder_main_menu" class="fbuilder_controls_wrapper"><form autocomplete="off"><div class="fbuilder_menu_inner">';

            for (var x in fbuilder_main_menu) {
                if (x == 'fbuilder_layout') {
                    if (typeof fbuilder_items['sidebar'] != 'undefined' && fbuilder_items['sidebar']['active'] == true)
                        fbuilder_main_menu[x]['std'] = fbuilder_items['sidebar']['type'];
                }
                if (x == 'fbuilder_save_page') {
                    fbuilder_main_menu[x]['class'] = 'fbuilder_false_save left'
                }
                var newControl = new fbuilderControl(x, fbuilder_main_menu[x]);
                html += newControl.html();
            }

            html += '</div></form></div>';*/

            html += '<div id="fbuilder_add_shortcode_popup" class="fbuilder_popup">';
            for (var x in fbuilder_main_menu) {
                fbuilder_main_menu[x]['type'] = 'shortcode-popup';
                var newControl = new fbuilderControl(x, fbuilder_main_menu[x]);
                html += newControl.html();
            }
            html += '<a href="#" class="fbuilder_gradient fbuilder_button fbuilder_popup_close right">Close</a></div>';


            html += '<div id="fbuilder_add_row_popup" class="fbuilder_popup"><span class="frb_headline">Chose Row Type:</span><div class="clear"></div>';
            for (var x in fbuilder_rows) {
                var newRow = fbuilder_rows[x];
                html += '<a href="#' + x + '" class="fbuilder_row_button fbuilder_gradient" title="' + newRow.label + '"><img src="' + newRow.image + '" alt="" /></a>';
            }
            html += '<div class="clear"></div><a href="#" class="fbuilder_gradient fbuilder_button">Close</a></div>';


            $('#fbuilder_body').css({borderLeftWidth: 0, borderTopWidth: 37});
            $('body').append(html);
            $('#fbuilder_main_menu').css({left: -42});
        }
        $('#fbuilder_body_frame').ready(function () {
            if (fbuilder_sw == 'on' && (fbuilder_user || fbuilder_showall)) {
                fbuilderIframeInit(this);
            }
        });
    });
    function fbuilderIframeInit($this) {
        if (typeof $this.contentWindow == 'undefined' || typeof $this.contentWindow.jQuery === 'undefined' || typeof $this.contentWindow.jQuery.ui === 'undefined' || typeof $this.contentWindow.jQuery.ui.sortable === 'undefined' || typeof $this.contentWindow.jQuery.ui.draggable === 'undefined') {
            setTimeout(function () {
                fbuilderIframeInit($('#fbuilder_body_frame')[0])
            }, 1000);
        }
        else {
            var loc = '',
                win = $this.contentWindow,
                doc = win.document,
                body = doc.body;
            win.jQuery(doc).on('click', 'a', function () {
                if (typeof win.jQuery(this).attr('href') != 'undefined' && win.jQuery(this).attr('href') != '' && win.jQuery(this).attr('href').substr(0, 1) != '#') loc = win.jQuery(this).attr('href');
            });
            $(win).unload(function () {
                if (loc != '' && loc != '#') {
                    window.location = loc;
                }
            });


            var $iframe = $($this).contents();
            var $fbMainMenu = $('#fbuilder_main_menu');
            var $fbDraggable = $fbMainMenu.find('.fbuilder_draggable').parent();

            fbuilderFrameControls($iframe);
            fbuilderSortableInit(win.jQuery('#fbuilder_content .fbuilder_row'));
            fbuilderControlsInit(win.jQuery, doc);
            fbuilderRefreshDragg(win.jQuery);
            $('#fbuilder_frame_cover').hide();
            if (!fbuilder_user)
                $('.fbuilder_toggle').trigger('click');
        }


        //				Nav Away Failsafe


        // var iWindow = $('#fbuilder_body_frame')[0].contentWindow;
        // $(iWindow).on('beforeunload', function(e) {
        // 	var message = 'Are you sure you want to leave the page? Any unsaved data will be lost.';
        // 	 e.returnValue = message ;
        // 	return message;
        // });

        fbuilderBeforeunload('on');
    }


    function fbuilderBeforeunload(trig) {
        var iWindow = $('#fbuilder_body_frame')[0].contentWindow;
        if (trig === 'on') {
            $(iWindow).on('beforeunload', function (e) {
                var message = 'Are you sure you want to leave the page? Any unsaved data will be lost.';
                e.returnValue = message;
                return message;
            });
        } else if (trig === 'off') {
            $(iWindow).off('beforeunload');
        }
    }

    function fbuilderFrameControls($iframe) {
        $iframe.find('#fbuilder_wrapper .fbuilder_row').each(function () {
            var parentRow = $(this).parent().closest('.fbuilder_row');
            if (parentRow.length <= 0 || (parentRow.closest('#fbuilder_wrapper').length <= 0 && $(this).closest('#fbuilder_wrapper').length > 0)) {
                if (!$(this).hasClass('fbuilder_sidebar'))
                    $(this).prepend('<div class="fbuilder_row_controls fbuilder_gradient"><a href="#" class="fbuilder_edit" title="Edit"></a><a class="fbuilder_drag_handle" href="#" title="Move"></a><a class="fbuilder_clone" href="#" title="Clone"></a><a class="fbuilder_delete" href="#" title="delete"></a></div>');
                else
                    $(this).prepend('<div class="fbuilder_row_controls"><span class="fbuilder_sidebar_label">Sidebar</span></div>');
            }
        });

        $iframe.find('#fbuilder_wrapper .fbuilder_column').each(function () {
            var parentCol = $(this).parent().closest('.fbuilder_column');
            if (parentCol.length <= 0 ||
                (parentCol.closest('#fbuilder_wrapper').length <= 0 && $(this).closest('#fbuilder_wrapper').length > 0) ||
                (parentCol.length > 0 && parentCol.closest('#fbuilder_wrapper').length <= 0)) {
                $(this).append('<div class="fbuilder_drop_borders"><div class="fbuilder_empty_content"><div class="fbuilder_add_shortcode fbuilder_gradient">+</div><span>Add Shortcode</span></div></div>');
            }
        });

        $iframe.find('#fbuilder_wrapper .fbuilder_module').each(function () {
            if ($(this).parent().closest('.fbuilder_module').length <= 0) {
                $(this).wrapInner('<div class="fbuilder_module_content" />');

                $(this).prepend('<img class="fbuilder_module_loader" src="' + fbuilder_url + 'images/module-loader-new.gif" /><div class="fbuilder_module_controls fbuilder_gradient"><a href="#" class="fbuilder_dodaj" title="Add Shortcode"></a><a href="#" class="fbuilder_edit" title="Edit"></a><a href="#" class="fbuilder_drag" title="Drag"></a><a href="#" class="fbuilder_clone" title="Clone"></a><a href="#" class="fbuilder_delete" title="delete"></a></div>');
            }
        });

        var rows = '<div class="fbuilder_row_holder">' +
            '<a href="#" class="fbuilder_new_row fbuilder_gradient">+ Add new row</a>' +
            '<div class="fbuilder_row_holder_inner">';

        for (var x in fbuilder_rows) {
            var newRow = fbuilder_rows[x];
            rows += '<a href="#' + x + '" class="fbuilder_row_button fbuilder_gradient" title="' + newRow.label + '"><img src="' + newRow.image + '" alt="" /></a>';
        }

        rows += '<div style="clear:both;"></div></div></div>';
        $iframe.find('#fbuilder_wrapper').addClass('edit').children('#fbuilder_content_wrapper').append(rows);

        $iframe.find('#fbuilder_content').sortable({
            items: "> div",
            handle: '.fbuilder_row_controls .fbuilder_drag_handle',
            stop: function (event, ui) {
                fbuilder_items['rowOrder'] = [];
                $iframe.find('#fbuilder_content .fbuilder_row').each(function (index) {
                    fbuilder_items['rowOrder'][index] = parseInt($(this).attr('data-rowid'));
                });
            }
        });
    }

    // fbuilderControl Class
    function fbuilderControl(name, values) {

        this.name = name;
        this.values = values;

        this.html = function () {
            var hideCond = '';
            var halfControl = (typeof this.values['half_column'] != 'undefined' && this.values['half_column'] == 'true' ? ' fbuilder_half_control' : '');
            var labelWidth = 0.5;
            var controlWidth = 0.5;
            if (this.values.type == 'image' || this.values.type == 'textarea') {
                labelWidth = 1;
                controlWidth = 1;
            }
            labelWidth = (typeof this.values['label_width'] != 'undefined' ? this.values['label_width'] : labelWidth);
            controlWidth = (typeof this.values['control_width'] != 'undefined' ? this.values['control_width'] : controlWidth);

            var wrapper =
                '<div class="fbuilder_control' + (typeof this.values['hide_if'] != 'undefined' ? ' fbuilder_hidable' : '') + (typeof this.values['class'] != 'undefined' ? ' ' + this.values['class'] : '') + halfControl + '">' +
                (typeof(this.values.label) != 'undefined' ?
                '<div class="fbuilder_label" style="width:' + (labelWidth * 100) + '%">' +
                '<label for="' + this.name + '">' + this.values.label + ' </label>' +
                (typeof(this.values.desc) != 'undefined' ? '<span class="fbuilder_desc fbuilder_gradient">' + this.values.desc + '</span>' : '') +
                '</div>' : '') +
                '<div class="fbuilder_control_content" style="width:' + (controlWidth * 100) + '%">';

            var wrapperClose = '</div><div style="clear:both;"></div></div>';
            var html = '';

            switch (this.values.type) {
                case 'input' :
                    html += wrapper;
                    html += '<input class="fbuilder_input' + (typeof this.values['hide_if'] != 'undefined' ? ' fbuilder_hidable_control' : '') + '" name="' + this.name + '" id="fbuilder_' + this.values.type + '_' + this.name + '" ' + (typeof(this.values.std) != 'undefined' && this.values.std != '' ? 'value="' + this.values.std + '"' : '') + '/>';
                    html += wrapperClose;
                    break;

                case 'textarea' :
                    html += wrapper;
                    html += '<textarea class="fbuilder_textarea' + (typeof this.values['hide_if'] != 'undefined' ? ' fbuilder_hidable_control' : '') + '" name="' + this.name + '" id="fbuilder_' + this.values.type + '_' + this.name + '">' + (typeof(this.values.std) != 'undefined' && this.values.std != '' ? this.values.std : '') + '</textarea>';
                    html += '<a href="#" class="fbuilder_wp_editor_button fbuilder_button fbuilder_gradient">Open in WP Editor</a><div style="clear:both;"></div>';
                    html += wrapperClose;
                    break;

                case 'checkbox' :
                    html +=
                        '<div class="fbuilder_control' + halfControl + '">' +
                        '<div class="fbuilder_checkbox' + (typeof(this.values.std) != 'undefined' && this.values.std != '' && this.values.std == 'true' ? ' active' : '') + '"></div>' +
                        '<input class="fbuilder_checkbox_input' + (typeof this.values['hide_if'] != 'undefined' ? ' fbuilder_hidable_control' : '') + '" name="' + this.name + '" id="fbuilder_' + this.values.type + '_' + this.name + '" style="display:none;"' +
                        (typeof(this.values.std) != 'undefined' && this.values.std == 'true' ? ' value="' + this.values.std + '"' : ' value="false"') + ' />' +
                        '<div class="fbuilder_checkbox_label">' +
                        (typeof(this.values.label) != 'undefined' ? '<label for="' + this.name + '">' + this.values.label + ' </label>' : '') +
                        (typeof(this.values.desc) != 'undefined' ? '<span class="fbuilder_desc fbuilder_gradient">' + this.values.desc + '</span>' : '') +
                        '</div><div style="clear:both;"></div>' +
                        '</div>';
                    break;

                case 'select' :
                    var options = this.values.options;
                    html += wrapper;

                    html += '<input class="' + (typeof this.values['hide_if'] != 'undefined' ? 'fbuilder_hidable_control' : '') + (typeof this.values['input_class'] != 'undefined' ? ' ' + this.values['input_class'] : '') + '" name="' + this.name + '" id="fbuilder_' + this.values.type + '_' + this.name + '" style="display:none;"';
                    html += (typeof this.values.std != 'undefined' && this.values.std != '' ? ' value="' + this.values.std + '"' : '');

                    var visibleSelect = '<div class="fbuilder_select fbuilder_gradient' + (typeof this.values['search'] != 'undefined' && this.values['search'] == 'true' ? ' fbuilder_select_with_search' : '') + (typeof this.values['multiselect'] != 'undefined' && this.values['multiselect'] == 'true' ? ' fbuilder_select_multi' : '') + '" data-name="' + this.name + '">';

                    var count = 0;
                    if (typeof(this.values.multiselect) != 'undefined' && this.values.multiselect == 'true')
                        var explVal = this.values.std.split(',');
                    if (options.length <= 0) {
                        visibleSelect += '<span>No data found</span>';
                    } // fail-safe if no data fetch
                    for (var x in options) {
                        if (count == 0) {
                            html += (typeof this.values.std == 'undefined' || this.values.std != '' ? ' value="' + x + '"' : '');
                            if (typeof(this.values.multiselect) != 'undefined' && this.values.multiselect == 'true') {
                                if (typeof(this.values.std) != 'undefined' && this.values.std != '') {
                                    visibleSelect += '<span>';
                                    for (y in explVal) {
                                        if (y != 0)
                                            visibleSelect += ',';
                                        visibleSelect += options[explVal[y]];
                                    }
                                    visibleSelect += '</span>';
                                }
                                else {
                                    visibleSelect += '<span>' + options[x] + '</span>';
                                }
                            }
                            else {
                                visibleSelect +=
                                    '<span>' + (typeof(this.values.std) != 'undefined' && this.values.std != '' ? options[this.values.std] : options[x]) + '</span>';
                            }
                            visibleSelect +=
                                '<div class="drop_button"></div>' +
                                (typeof this.values['search'] != 'undefined' && this.values['search'] == 'true' ? '<input class="fbuilder_select_search" placeholder="Search..." value="" style="display:none" />' : '') +
                                '<ul style="display:none">';


                            if (typeof(this.values.multiselect) != 'undefined' && this.values.multiselect == 'true') {
                                visibleSelect +=
                                    '<li><a href="#" data-value="' + x + '"' + ((explVal.indexOf(x) != -1) ? ' class="selected"' : '') + '>' + options[x] + '</a></li>';
                            }
                            else {

                                visibleSelect +=
                                    '<li><a href="#" data-value="' + x + '"' + ((typeof this.values.std == 'undefined' || this.values.std == '' || this.values.std == x) ? ' class="selected"' : '') + '>' + options[x] + '</a></li>';
                            }
                        }
                        else {
                            if (typeof(this.values.multiselect) != 'undefined' && this.values.multiselect == 'true') {
                                visibleSelect +=
                                    '<li><a href="#" data-value="' + x + '"' + ((explVal.indexOf(x) != -1) ? ' class="selected"' : '') + '>' + options[x] + '</a></li>';
                            }
                            else {
                                visibleSelect += '<li><a href="#" data-value="' + x + '"' + (this.values.std == x ? ' class="selected"' : '') + '>' + options[x] + '</a></li>';
                            }
                        }
                        count++;
                    }


                    html += ' />';
                    visibleSelect +=
                        '</ul>' +
                        '<div class="clear"></div>' +
                        '</div>';
                    html += visibleSelect;
                    html += wrapperClose;

                    break;

                case 'icon' :
                    var dataMin = ((typeof this.values['notNull'] != 'undefined' && this.values['notNull'] == false) ? 'no-icon' : 'ba-adjust');
                    var current = ((typeof(this.values.std) != 'undefined' && this.values.std != '' && this.values.std != null ) ? this.values.std : dataMin);
                    var prefix = current.substr(0, 2);
                    var old = false;

                    if (current.substr(2, 1) != '-') {
                        old = true;
                        prefix = 'fa';
                    }

                    html += wrapper;
                    html += '<input class="' + (typeof this.values['hide_if'] != 'undefined' ? ' fbuilder_hidable_control' : '') + '" type="hidden" name="' + this.name + '" id="fbuilder_' + this.values.type + '_' + this.name + '" data-min="' + dataMin + '" value="' + current + '" /><div class="fbuilder_icon_holder"><i class="' + (old ? 'old_icon ' : '') + prefix + ' ' + current + ' frb_icon"></i></div><a href="#" class="fbuilder_gradient fbuilder_icon_pick">Change</a>';
                    html += '<div style="clear:both;"></div><span class="fbuilder_icon_drop_arrow"></span><div class="fbuilder_icon_dropdown"><div class="fbuilder_icon_dropdown_tabs">';
                    var icon_drop_content = '<div class="fbuilder_icon_dropdown_scroll">';
                    for (var x in fbuilder_icons) {
                        if (x == 'noicon') {
                            if (typeof this.values['notNull'] != 'undefined' && this.values['notNull'] == false) {
                                html += '<span class="fbuilder_icon_noicon' + (prefix == fbuilder_icons[x]['prefix'] ? ' active' : '') + '">' + fbuilder_icons[x]['label'] + '</span>';
                            }

                        }
                        else {
                            html += '<span data-tabid="' + fbuilder_icons[x]['prefix'] + '" class="fbuilder_icon_tab' + (prefix == fbuilder_icons[x]['prefix'] ? ' active' : '') + '">' + fbuilder_icons[x]['label'] + '</span>';
                            icon_drop_content += '<div class="fbuilder_icon_dropdown_content' + (prefix == fbuilder_icons[x]['prefix'] ? ' active' : '') + '" data-tabid="' + fbuilder_icons[x]['prefix'] + '">';
                            for (var y in  fbuilder_icons[x]['icons']) {
                                icon_drop_content += '<a href="' + fbuilder_icons[x]['icons'][y] + '"><i class="' + fbuilder_icons[x]['prefix'] + ' ' + fbuilder_icons[x]['icons'][y] + ' frb_icon"></i></a>';
                            }
                            icon_drop_content += '<div style="clear:both;"></div></div>';
                        }
                    }
                    icon_drop_content += '</div>';
                    html += '</div>' + icon_drop_content;
                    html += '<div style="clear:both;"></div></div><div style="clear:both;"></div>';
                    html += wrapperClose;
                    break;
                case 'image' :
                    html += wrapper;
                    html += '<a html="' + this.name + '" class="fbuilder_image_button fbuilder_button fbuilder_gradient" data-input="fbuilder_' + this.values.type + '_' + this.name + '">Upload</a>';
                    html += '<div class="fbuilder_image_input"><input class="fbuilder_input' + (typeof this.values['hide_if'] != 'undefined' ? ' fbuilder_hidable_control' : '') + '" name="' + this.name + '" id="fbuilder_' + this.values.type + '_' + this.name + '" value="' + (typeof(this.values.std) != 'undefined' && this.values.std != '' ? this.values.std + '" />' : '" /><span>Enter image url...</span>') + '</div>';

                    html += '<div style="clear:both;"></div>';
                    html += wrapperClose;
                    break;

                case 'media_select' :
                    html += wrapper;
                    html += '<a html="' + this.name + '" class="fbuilder_media_select_button fbuilder_button fbuilder_gradient" data-input="fbuilder_' + this.values.type + '_' + this.name + '">+ Add Media</a>';
                    html += '<div class="fbuilder_media_select_input"><input class="fbuilder_input' + (typeof this.values['hide_if'] != 'undefined' ? ' fbuilder_hidable_control' : '') + '" name="' + this.name + '" id="fbuilder_' + this.values.type + '_' + this.name + '" value="' + (typeof(this.values.std) != 'undefined' && this.values.std != '' ? this.values.std + '" />' : '" />') + '</div>';

                    html += '<div style="clear:both;"></div>';
                    html += wrapperClose;
                    break;

                case 'shortcode-popup' :
                    html += '<div class="fbuilder_add_shortcode_popup_inner fbuilder_controls_wrapper">';
                    if (typeof this.values.groups != 'undefined') {

                        var cntHtml = '<div class="fbuilder_shortcode_groups">';
                        var selectHtml = '';
                        var selectArray = {};
                        var cnt = 0;
                        for (var g in this.values.groups) {

                            /* select */
                            selectArray[this.values.groups[g]['id']] = this.values.groups[g]['label'];

                            /* content */
                            cntHtml += '<div class="fbuilder_shortcode_group" data-group="' + this.values.groups[g]['id'] + '">';
                            for (var x in fbuilder_shortcodes) {
                                if (typeof fbuilder_shortcodes[x]['group'] != 'undefined' && this.values.groups[g]['id'] == fbuilder_shortcodes[x]['group']) {
                                    fbuilder_shortcodes[x]['type'] = 'shortcode-popup-clickable';
                                    var newControl = new fbuilderControl(x, fbuilder_shortcodes[x]);
                                    cntHtml += newControl.html();
                                }
                            }
                            cntHtml += '<div style="clear:both;"></div></div>';


                            cnt++;
                        }
                        var selectOptions = {
                            'type': 'select',
                            'label': 'Shortcode group',
                            'label_width': 0.35,
                            'control_width': 0.65,
                            'options': selectArray
                        }
                        var selectCtrl = new fbuilderControl('fbuilder_add_shortcode_group', selectOptions);
                        selectHtml += '<div class="fbuilder_add_shortcode_popup_controls">' + selectCtrl.html() + '<div style="clear:both;"></div></div>';
                        tabsHtml += '</div>';
                        cntHtml += '</div>';

                        html += selectHtml + cntHtml;

                    }
                    else {

                    }
                    html += '</div>';
                    break;

                case 'shortcode-popup-clickable' :
                    html += '<div class="fbuilder_shortcode_block" data-shortcode="' + this.name + '"><span class="shortcode_icon">' + (typeof this.values.icon != 'undefined' && this.values.icon != '' ? '<img src="' + this.values.icon + '" alt="" />' : '<img src="' + fbuilder_url + 'images/icons/11.png" alt="" />') + '</span><span class="shortcode_name">' + this.values.text + '</span></div>';
                    break;

                case 'shortcode-holder' :
                    html += '<div class="fbuilder_shortcode_holder">';
                    if (typeof this.values.groups != 'undefined') {

                        var tabsHtml = '<div class="fbuilder_shortcode_tabs">';
                        var cntHtml = '<div class="fbuilder_shortcode_groups">';
                        var selectHtml = '<div class="fbuilder_shortcode_group_select">';
                        var cnt = 0;
                        for (var g in this.values.groups) {

                            /* select */
                            if (cnt == 0) {
                                selectHtml += '<div class="fbuilder_shortcode_tab_select">';
                                selectHtml += '<img src="' + this.values.groups[g]['img'] + '" alt="" />';
                                selectHtml += '</div>';
                            }

                            /* tabs */
                            tabsHtml += '<div class="fbuilder_shortcode_tab' + (cnt != 0 ? ' after' : '') + '" data-group="' + this.values.groups[g]['id'] + '" style="left:' + (cnt * 42) + 'px;">';
                            tabsHtml += '<img src="' + this.values.groups[g]['img'] + '" alt="" />';
                            tabsHtml += '</div>';

                            /* content */
                            cntHtml += '<div class="fbuilder_shortcode_group" data-group="' + this.values.groups[g]['id'] + '">';
                            for (var x in fbuilder_shortcodes) {
                                if (typeof fbuilder_shortcodes[x]['group'] != 'undefined' && this.values.groups[g]['id'] == fbuilder_shortcodes[x]['group']) {
                                    var newControl = new fbuilderControl(x, fbuilder_shortcodes[x]);
                                    cntHtml += newControl.html();
                                }
                            }
                            cntHtml += '</div>';


                            cnt++;
                        }
                        selectHtml += '<div class="fbuilder_shortcode_tab_select_name">Change shortcode group</div></div>';
                        tabsHtml += '</div>';
                        cntHtml += '</div>';

                        html += selectHtml + tabsHtml + cntHtml;

                    }
                    else {

                    }
                    html += '</div>';
                    break;

                case 'draggable' :
                    html += '<div class="fbuilder_draggable" data-shortcode="' + this.name + '"><span class="shortcode_icon">' + (typeof this.values.icon != 'undefined' && this.values.icon != '' ? '<img src="' + this.values.icon + '" alt="" />' : '<img src="' + fbuilder_url + 'images/icons/11.png" alt="" />') + '</span><span class="fbuilder_shortcode_name">' + this.values.text + '</span></div>';
                    break;

                case 'button' :
                    var cl = (typeof this.values['class'] != 'undefined' && this.values['class'] != '' ? this.values['class'] : '');
                    var href = (typeof this.values['href'] != 'undefined' && this.values['href'] != '' ? this.values['href'] : '#');
                    var id = (typeof this.values['id'] != 'undefined' && this.values['id'] != '' ? ' id="' + this.values['id'] + '"' : '');
                    var style = (this.values['style'] == 'primary' ? 'fbuilder_gradient_primary' : 'fbuilder_gradient');
                    var align = (this.values['align'] == 'right' ? ' style="float:right;"' : '');

                    html += '<a' + id + ' href="' + href + '" class="' + style + ' fbuilder_button ' + cl + '"' + align + '>' + this.values['label'] + '</a>' + (typeof this.values['loader'] != 'undefined' && this.values['loader'] == 'true' ? '<img src="' + fbuilder_url + 'images/save-loader.gif" class="fbuilder_save_loader" alt="" />' : '') + (this.values['clear'] != 'false' ? '<div style="clear:both;"></div>' : '');
                    break;

                case 'number' :
                    var min = (typeof this.values['min'] != 'undefined' && this.values['min'] != '' ? parseInt(this.values['min']) : 0);
                    var max = (typeof this.values['max'] != 'undefined' && this.values['max'] != '' ? parseInt(this.values['max']) : 100);
                    var std = (typeof this.values['std'] != 'undefined' && this.values['std'] != '' ? parseInt(this.values['std']) : 0);
                    var step = (typeof this.values['step'] != 'undefined' && this.values['step'] != '' ? parseInt(this.values['step']) : 1);
                    var unit = (typeof this.values['unit'] != 'undefined' && this.values['unit'] != '' ? this.values['unit'] : '');
                    var maxStr = '' + max;

                    html += wrapper;
                    html += '<div class="fbuilder_number_bar_wrapper"><div class="fbuilder_number_bar" data-min="' + min + '" data-max="' + max + '" data-std="' + std + '" data-step="' + step + '" data-unit="' + unit + '"></div></div><input class="fbuilder_number_amount fbuilder_input' + (typeof this.values['hide_if'] != 'undefined' ? ' fbuilder_hidable_control' : '') + '" name="' + this.name + '" id="fbuilder_' + this.values.type + '_' + this.name + '" value="' + std + unit + '"/><div class="fbuilder_number_button fbuilder_gradient"></div><div style="clear:both;"></div>';
                    html += wrapperClose;
                    break;

                case 'color' :
                    html += wrapper;
                    html += '<div class="fbuilder_color_wrapper">';
                    html += '<input class="fbuilder_color fbuilder_input' + (typeof this.values['hide_if'] != 'undefined' ? ' fbuilder_hidable_control' : '') + '" name="' + this.name + '" id="fbuilder_' + this.values.type + '_' + this.name + '" ' + (typeof(this.values.std) != 'undefined' && this.values.std != '' ? 'value="' + this.values.std + '"' : '') + '/>';
                    html += '<div class="fbuilder_color_display fbuilder_gradient"><span></span></div><div class="fbuilder_colorpicker"></div>';
                    html += '</div>';
                    html += wrapperClose;
                    break;

                case 'collapsible' :
                    var lab = (typeof(this.values.label) != 'undefined' ? '<label for="' + this.name + '">' + this.values.label + ' </label>' : '');
                    var open = (typeof this.values['open'] != 'undefined' && this.values['open'] == 'true');
                    html += '<div class="fbuilder_collapsible_big fbuilder_collapsible" data-name="' + this.name + '"><div class="fbuilder_collapsible_header">' + lab + '<span class="fbuilder_collapse_trigger fbuilder_gradient' + (open ? ' active' : '') + '">' + (open ? '-' : '+') + '</span></div><div class="fbuilder_collapsible_content"' + (open ? ' style="display:block"' : '') + '>';
                    var controlObj = $.extend(true, {}, this.values['options']);
                    for (var y in controlObj) {
                        var newControl = new fbuilderControl(y, controlObj[y]);
                        html += newControl.html();
                    }
                    html += '<div style="clear:both;"></div></div></div>';
                    break;
                case 'sortable' :
                    var item_name = (typeof this.values['item_name'] != 'undefined' && this.values['item_name'] != '' ? this.values['item_name'] : 'item');
                    html += wrapper;
                    html += '<div class="fbuilder_sortable_holder" data-name="' + this.name + '" data-iname="' + item_name + '" id="fbuilder_' + this.values.type + '_' + this.name + '">';
                    html += '<div class="fbuilder_sortable">';


                    if (typeof this.values['std'] != 'undefined' && this.values['std'] != '') {
                        if (typeof this.values['std']['order'] != 'undefined' && this.values['std']['order'] != '' && this.values['std']['order'] != {}) {
                            for (var x in this.values['std']['order']) {
                                var sortid = this.values['std']['order'][x];
                                html += '<div class="fbuilder_sortable_item fbuilder_collapsible" data-sortid="' + sortid + '" data-sortname="' + this.name + '"><div class="fbuilder_gradient fbuilder_sortable_handle fbuilder_collapsible_header">' + item_name + ' ' + sortid + ' - <span class="fbuilder_sortable_delete">delete</span>, <span class="fbuilder_sortable_clone">clone</span><span class="fbuilder_collapse_trigger">+</span></div><div class="fbuilder_collapsible_content">';

                                var controlObj = $.extend(true, {}, this.values['options']);
                                for (var y in controlObj) {
                                    if (typeof this.values['std']['items'][sortid][y] != 'undefined') {
                                        controlObj[y]['std'] = this.values['std']['items'][sortid][y];
                                    }
                                    var newControl = new fbuilderControl('fsort-' + sortid + '-' + y, controlObj[y]);
                                    html += newControl.html();
                                }
                                html += '<div style="clear:both"></div></div></div>';
                            }
                        }
                    }


                    html += '</div>';
                    html += '<a href="#" class="fbuilder_sortable_add fbuilder_gradient fbuilder_button">+ Add new ' + item_name + '</a>';
                    html += '<div style="clear:both;"></div>';
                    html += '</div>';
                    html += wrapperClose;
                    break;
            }

            return html;
        }

    }


    /*  Ajax shortcode gathering  */
    window.fbuilder_shajax = {}
    function fbuilderGetShortcode(f, holder, options) {
        holder.closest('.fbuilder_module').find('.fbuilder_module_loader').show();
        var data = {
            action: 'fbuilder_shortcode',
            f: f
        }
        if (typeof options !== 'undefined') {
            data.options = JSON.stringify(options);
        }
        var modid = holder.closest('.fbuilder_module').attr('data-modid');
        if (typeof window.fbuilder_shajax[modid] != 'undefined') window.fbuilder_shajax[modid].abort();
        window.fbuilder_shajax[modid] = $.post(ajaxurl, data, function (response) {
            holder.html(response);
            holder.closest('.fbuilder_module').trigger('refresh');
            holder.closest('.fbuilder_module').find('.fbuilder_module_loader').hide();
        });
    }

    var keyTimeout = {};

    function fbuilderContolChange($jq, $control, timeout) {

        var name = $control.attr('name');
        $menu = $('.fbuilder_shortcode_menu:first');
        if ($menu.length > 0) {
            var modid = parseInt($menu.attr('data-modid'));
            if (name.search('fsort') == -1) {
                if (!$menu.hasClass('fbuilder_rowedit_menu')) {
                    fbuilder_items['items'][modid]['options'][name] = $control.val();
                }
                else {
                    if (typeof fbuilder_items['rows'][modid]['options'] == 'undefined') fbuilder_items['rows'][modid]['options'] = {};
                    fbuilder_items['rows'][modid]['options'][name] = $control.val();
                }
            }
            else {
                var subname = name.substr(name.search('-') + 1);
                name = subname.substr(subname.search('-') + 1);
                var sortid = parseInt(subname.substr(0, subname.search('-')));
                var $parent = $control.closest('.fbuilder_sortable_item');
                sortname = $parent.attr('data-sortname');
                if (!$menu.hasClass('fbuilder_rowedit_menu')) {
                    fbuilder_items['items'][modid]['options'][sortname]['items'][sortid][name] = $control.val();
                }
                else {
                    if (typeof fbuilder_items['rows'][modid]['options'] == 'undefined') fbuilder_items['rows'][modid]['options'] = {};
                    fbuilder_items['rows'][modid]['options'][sortname]['items'][sortid][name] = $control.val();
                }

            }
            if (typeof timeout !== 'undefined') {
                window.clearTimeout(keyTimeout[modid]);
                keyTimeout[modid] = window.setTimeout(function () {
                    $menu.trigger('fchange');
                }, timeout);
            }
            else {
                $menu.trigger('fchange');
            }
        }
        fbuilderHideControls($control);
        fbuilderRefreshControls($jq, $control)
    }

    function fbuilderRowChange($row, options) {
        var rowback = '';
        var rowbackimage = '';
        var rowbackvideo = '';
        var rowbackrep = '';
        var rowbackpos = '';
        var rowbackcolor = '';
        for (var x in options) {
            switch (x) {
                case 'padding_top':
                    $row.css('padding-top', parseInt(options[x]) + 'px');
                    break;

                case 'padding_bot':
                    $row.css('padding-bottom', parseInt(options[x]) + 'px');
                    break;

                case 'full_width' :
                    if (options[x] == 'true') {
                        $row.addClass('fbuilder_row_full_width');
                        $row.trigger('row_width_change');
                    } else {
                        $row.removeClass('fbuilder_row_full_width');
                        $row.trigger('row_width_change');
                    }
                    break;

                case 'back_type' :
                    if (options[x] == 'parallax' || options[x] == 'video_fixed') {
                        rowbackpos = 'fixed';
                    }

                    if (options[x] == 'parallax_animated' || options[x] == 'video_parallax') {
                        rowbackpos = 'parallax';
                    }
                    if (options[x] == 'parallax_fade') {
                        rowbackpos = 'parallax_fade';
                    }
                    if (options[x] == 'parallax_scale') {
                        rowbackpos = 'parallax_scale';
                    }
                    if (options[x] == 'parallax_scale_fade') {
                        rowbackpos = 'parallax_scale_fade';
                    }

                    if (options[x] == 'video' || options[x] == 'video_fixed' || options[x] == 'video_parallax') {
                        if (typeof options['back_video_source'] != 'undefined') {
                            rowbackvideo = options['back_video_source'];
                        }
                        else {
                            rowbackvideo = 'youtube';
                        }

                    }
                    if (options[x] == 'parallax')
                        $row.addClass('fbuilder_row_parallax');
                    else
                        $row.removeClass('fbuilder_row_parallax');
                    break;

                case 'back_color' :
                    if (options[x] != '')
                        rowbackcolor = 'background-color:' + options[x] + ';';
                    break;

                case 'back_image' :
                    if (options[x] != '')
                        rowbackimage = 'background-image:url(' + options[x] + ');';
                    break;

                case 'back_repeat' :
                    if (options[x] == 'true')
                        rowbackrep = 'background-repeat:repeat;';
                    break;

                case 'column_padding' :
                    $row.children('div').children('.fbuilder_column').children('div:first-child').css('padding', parseInt(options[x]) + 'px');
                    break;

                case 'column_back' :
                    if (typeof options['column_back_opacity'] == 'undefined') {
                        if (options[x] == '')
                            $row.children('div').children('.fbuilder_column:not(".frb_column_specific_color")').children('div:first-child').css('background', 'transparent');
                        else
                            $row.children('div').children('.fbuilder_column:not(".frb_column_specific_color")').children('div:first-child').css('background', options[x]);
                    }
                    else {
                        var hex = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(options[x]);
                        if (hex) {
                            var r = parseInt(hex[1], 16);
                            var g = parseInt(hex[2], 16);
                            var b = parseInt(hex[3], 16);
                            $row.children('div').children('.fbuilder_column:not(".frb_column_specific_color")').children('div:first-child').css('background', 'rgba(' + r + ',' + g + ',' + b + ',' + (parseInt(options['column_back_opacity']) / 100) + ')');
                        }
                        else {
                            $row.children('div').children('.fbuilder_column:not(".frb_column_specific_color")').children('div:first-child').css('background', 'transparent');
                        }

                    }
                    break;

                default :
                    if (x.indexOf('column') > -1 && x !== 'column_back' && x !== 'column_back_opacity') {

                        for (cnt = 0; cnt < 12; cnt++) {

                            var $sel = $row.children('div').children('.fbuilder_column').eq(cnt);
                            var r = undefined, g = undefined, b = undefined;
                            var op;
                            if (('column' + cnt + '_back' in options)) {
                                if (typeof options['column' + cnt + '_back'] != 'undefined' && options['column' + cnt + '_back'] != '') {
                                    var hex = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(options['column' + cnt + '_back']);
                                    if (hex) {
                                        var r = parseInt(hex[1], 16);
                                        var g = parseInt(hex[2], 16);
                                        var b = parseInt(hex[3], 16);
                                    }
                                }

                                if (typeof options['column' + cnt + '_back_opacity'] != 'undefined' && options['column' + cnt + '_back_opacity'] != '') {
                                    op = (parseInt(options['column' + cnt + '_back_opacity']) / 100);
                                } else {
                                    op = '1';
                                }

                                if (typeof r != 'undefined' && typeof g != 'undefined' && typeof b != 'undefined') {
                                    $sel.addClass('frb_column_specific_color').children('div:first-child').css('background', 'rgba(' + r + ',' + g + ',' + b + ',' + op + ')');
                                } else {
                                    if (typeof options['column_back'] != 'undefined' && options['column_back'] != '' && options['column_back'] != 'transparent') {
                                        var hex2 = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(options['column_back']);
                                        $sel.removeClass('frb_column_specific_color').children('div:first-child').css('background', 'rgba(' + hex2[1] + ',' + hex2[2] + ',' + hex2[3] + ',' + (typeof options['column_back_opacity'] != 'undefined' ? (parseInt(options['column_back_opacity']) / 100) : '1') + ')');
                                    } else {
                                        $sel.removeClass('frb_column_specific_color').children('div:first-child').css('background', 'transparent');
                                    }
                                }
                            }
                        }
                    }

                    break;
            }
        }
        var out;
        if (rowbackvideo != '') {
            var loop = (typeof options['back_video_loop'] == 'undefined' || options['back_video_loop'] != 'false');
            if (rowbackvideo == 'youtube') {

                id = 'yt' + Math.floor((Math.random() * 100000) + 1);
                if (typeof options['back_video_youtube_id'] != 'undefined' && options['back_video_youtube_id'] != '') {
                    out = '<div class="fbuilder_row_video fbuilder_row_background' + (rowbackpos == 'fixed' ? ' fbuilder_row_background_fixed' : '') + (rowbackpos == 'parallax' ? ' fbuilder_row_background_parallax' : '') + '">' +
                    '<div id="' + id + '" class="YTPlayer" style="display:block; margin: auto; background: rgba(0,0,0,0.5)" data-property="{videoURL:\'http://youtu.be/' + options['back_video_youtube_id'] + '\',containment:\'self\',startAt:1,mute:true,autoPlay:true' + (loop ? ',loop:true' : ',loop:false') + ',opacity:1,showControls:true,quality:\'hd720\'}"></div>' +
                    '</div>';

                }
                else {
                    out = '';
                }
            }
            else if (rowbackvideo == 'vimeo') {
                out = '<div class="fbuilder_row_video fbuilder_row_video_vimeo fbuilder_row_background' + (rowbackpos == 'fixed' ? ' fbuilder_row_background_fixed' : '') + (rowbackpos == 'parallax' ? ' fbuilder_row_background_parallax' : '') + '">' +
                '<iframe src="//player.vimeo.com/video/' + options['back_video_vimeo_id'] + '?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff&amp;autoplay=1' + (loop ? '&amp;loop=1' : '') + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' +
                '</div>';
            }
            else {
                out = '<div class="fbuilder_row_video fbuilder_row_video_html5 fbuilder_row_background' + (rowbackpos == 'fixed' ? ' fbuilder_row_background_fixed' : '') + (rowbackpos == 'parallax' ? ' fbuilder_row_background_parallax' : '') + '">' +
                '<video muted autoplay' + (loop ? ' loop' : '') +
                (typeof options['back_video_html5_img'] != 'undefined' && options['back_video_html5_img'] != '' ? ' poster="' + options['back_video_html5_img'] + '"' : '') + ' >' +
                (typeof options['back_video_html5_mp4'] != 'undefined' && options['back_video_html5_mp4'] != '' ? '<source src="' + options['back_video_html5_mp4'] + '" type="video/mp4">' : '') +
                (typeof options['back_video_html5_webm'] != 'undefined' && options['back_video_html5_webm'] != '' ? '<source src="' + options['back_video_html5_webm'] + '" type="video/webm">' : '') +
                (typeof options['back_video_html5_ogv'] != 'undefined' && options['back_video_html5_ogv'] != '' ? '<source src="' + options['back_video_html5_ogv'] + '" type="video/ogg">' : '') +
                '</video></div>';
            }
        }
        else if (rowbackimage != '') {
            out = '<div class="fbuilder_row_background' + (rowbackpos == 'fixed' ? ' fbuilder_row_background_fixed' : '') + (rowbackpos == 'parallax' ? ' fbuilder_row_background_parallax' : '') + (rowbackpos == 'parallax_fade' ? ' fbuilder_row_background_parallax fbuilder_row_background_fade' : '') + (rowbackpos == 'parallax_scale' ? ' fbuilder_row_background_parallax fbuilder_row_background_scale' : '') + (rowbackpos == 'parallax_scale_fade' ? ' fbuilder_row_background_parallax fbuilder_row_background_scale fbuilder_row_background_fade' : '') + '" ><div class="fbuilder_row_back_image" style="' + rowbackimage + rowbackcolor + rowbackrep + '"></div></div>';
        }
        else if (rowbackcolor) {
            out = '<div class="fbuilder_row_background" style="' + rowbackcolor + '"></div>';
        }
        else {
            out = '';
        }
        $row.children('.fbuilder_row_background').remove();
        $row.prepend(out);
        $row.trigger('refresh');
    }

    function fbuilderCreateRowMenu(rowId, $item) {

        var rowJSON = $.extend(true, {}, fbuilder_row_controls);

        var columnColorsJSON = rowJSON['group_column_back'];

        for (cnt = 0; cnt < fbuilder_items['rows'][rowId]['columns'].length; cnt++) {
            var formatedOptions = {};
            formatedOptions['column' + cnt + '_back'] = {
                'label': 'Column ' + (cnt + 1) + ' Color',
                'std': '',
                'type': 'color'
            };
            formatedOptions['column' + cnt + '_back_opacity'] = {
                'label': 'Column ' + (cnt + 1) + ' Opacity',
                'std': 100,
                'type': 'number',
                'unit': '%'
            };
            columnColorsJSON['options'] = $.extend(true, columnColorsJSON['options'], formatedOptions);
        }

        // 		generate controls from rowJSON
        var html = '';
        for (var x in rowJSON) {
            if (rowJSON[x]['type'] == 'collapsible') {
                for (var y in rowJSON[x]['options']) {
                    if (typeof(fbuilder_items['rows'][rowId]['options']) != "undefined" && typeof(fbuilder_items['rows'][rowId]['options'][y]) != "undefined") {
                        rowJSON[x]['options'][y]['std'] = fbuilder_items['rows'][rowId]['options'][y];
                    }
                }
            }
            else if (typeof(fbuilder_items['rows'][rowId]['options']) != "undefined" && typeof(fbuilder_items['rows'][rowId]['options'][x]) != "undefined") {
                rowJSON[x]['std'] = fbuilder_items['rows'][rowId]['options'][x];
            }
            var newControl = new fbuilderControl(x, rowJSON[x]);
            html += newControl.html();
        }

        return html;
    }

    function fbuilderCreateShortcodeMenu(itemId, $item) {

        var shortcodeJSON = $.extend(true, {}, fbuilder_shortcodes[$item.attr('data-shortcode')]);
        var html = '';
        for (var x in shortcodeJSON['options']) {
            if (shortcodeJSON['options'][x]['type'] == 'collapsible') {
                for (var y in shortcodeJSON['options'][x]['options']) {
                    if (typeof(fbuilder_items['items'][itemId]['options'][y]) != "undefined") {
                        shortcodeJSON['options'][x]['options'][y]['std'] = fbuilder_items['items'][itemId]['options'][y];
                    }
                }
            }
            else if (typeof(fbuilder_items['items'][itemId]['options'][x]) != "undefined") {
                shortcodeJSON['options'][x]['std'] = fbuilder_items['items'][itemId]['options'][x];
            }
            var newControl = new fbuilderControl(x, shortcodeJSON['options'][x]);
            html += newControl.html();
        }

        return html;
    }

    function fbuilderLoadContent(content) {
        var items = $.extend(true, {}, content);
        var output = '';
        var html = '';
        if (!$.isEmptyObject(items)) {
            if (typeof items['sidebar'] != 'undefined' && items['sidebar']['active']) {
                var sidebar = items['sidebar']['type'];
                html = '<div class="fbuilder_sidebar fbuilder_' + sidebar + ' fbuilder_row" data-rowid="sidebar"><div class="fbuilder_column">';
                for (var x in items['sidebar']['items']) {
                    if (typeof items['items'][items['sidebar']['items'][x]] != 'undefined' && items['items'][items['sidebar']['items'][x]] != null) {
                        html += '<div class="fbuilder_module" data-shortcode="' + items['items'][items['sidebar']['items'][x]]['slug'] + '" data-modid="' + items['sidebar']['items'][x] + '">';
                        html += '</div>';
                    }
                }
                html += '</div><div style="clear:both;"></div></div>';
            }

        }
        output += html +
        '<div id="fbuilder_content_wrapper"' + (sidebar != false ? ' class="fbuilder_content_' + sidebar + '"' : '') + '>' +
        '<div id="fbuilder_content">';

        if (!$.isEmptyObject(items)) {

            for (var rowId = 0; rowId < items['rowCount']; rowId++) {
                if (typeof items['rowOrder'] != 'undefined')
                    var row = items['rowOrder'][rowId];
                else
                    var row = null;
                if (row != null) {
                    var current = items['rows'][row];
                    html = fbuilder_rows[current['type']]['html'];

                    html = html.replace('%1$s', row);
                    var rowInterface = '<div class="fbuilder_row_controls"><a href="#" class="fbuilder_edit" title="Edit"></a><a href="#" class="fbuilder_drag_handle" title="Move"></a><a href="#" class="fbuilder_clone" title="Clone"></a><a href="#" class="fbuilder_delete" title="delete"></a></div>';
                    html = html.replace('%2$s', rowInterface);


                    for (var colId in current['columns']) {
                        columnInterface = '<div class="fbuilder_droppable">';
                        for (var x in current['columns'][colId]) {
                            if (typeof items['items'][current['columns'][colId][x]] != 'undefined' && items['items'][current['columns'][colId][x]] != null) {

                                var shortcode_slug = items['items'][current['columns'][colId][x]]['slug'];

                                columnInterface += '<div class="fbuilder_module" data-shortcode="' + shortcode_slug + '" data-modid="' + current['columns'][colId][x] + '">';
                                columnInterface += '<div class="fbuilder_module_controls fbuilder_gradient"><span class="fbuilder_module_name">' + fbuilder_shortcodes[shortcode_slug]['text'] + '</span> <img class="fbuilder_module_loader" src="' + fbuilder_url + 'images/save-loader.gif" /><a href="#" class="fbuilder_edit" title="Edit"></a><a href="#" class="fbuilder_clone" title="Clone"></a><a href="#" class="fbuilder_delete" title="delete"></a></div>';
                                columnInterface += '<div class="fbuilder_module_content"></div></div>';

                            }
                        }

                        columnInterface += '</div><div class="fbuilder_drop_borders"><div class="fbuilder_empty_content"><div class="fbuilder_add_shortcode fbuilder_gradient">+</div><span>Add Shortcode</span></div></div>';
                        html = html.replace('%' + (parseInt(colId) + 3) + '$s', columnInterface);
                    }

                    output += html;
                }
            }
        }


        output += '</div>' +
        '<div style="clear:both"></div>' +
        '</div>' +
        '<div style="clear:both"></div>';
        return output;
    }

    function fbuilderCloneCanvas(newCanvas, oldCanvas) {
        var context = newCanvas.getContext('2d');

        //set dimensions
        newCanvas.width = oldCanvas.width;
        newCanvas.height = oldCanvas.height;

        //apply the old canvas to the new one
        context.drawImage(oldCanvas, 0, 0);
    }

    function fbuilderHideControls($src, init, $mainquery) {
        var $shmenu = $('.fbuilder_shortcode_menu:first');
        if ($shmenu.hasClass('fbuilder_rowedit_menu')) {
            var shortcode = 'rowcontrols';
        }
        else {
            var shortcode = $shmenu.attr('data-shortcode');
        }
        var name = $src.attr('name');
        var qquery = '';
        if (typeof $mainquery == 'undefined')
            $mainquery = $('body');
        if (typeof init == 'undefined') {
            if (name.substr(0, 5) != 'fsort') {
                if (typeof fbuilder_hideifs['parents'][shortcode] == 'object' && typeof fbuilder_hideifs['parents'][shortcode][name] != 'undefined') {
                    var objects = fbuilder_hideifs['parents'][shortcode][name];
                    for (var x in objects) {
                        if (typeof objects[x][0] == 'undefined') {
                            for (var y in objects[x]) {
                                if (qquery != '') qquery += ', ';
                                qquery += '[name$=' + y + ']';
                            }
                        }
                        else {
                            if (qquery != '') qquery += ', ';
                            qquery += '[name=' + x + ']';
                        }

                    }
                }
            }
            else {
                var sliceName = name.split('-');
                var sortName = sliceName.slice(2).join('-');
                var sortStart = sliceName.slice(0, 2).join('-');
                var $sortHolder = $src.closest('.fbuilder_sortable_holder')
                var sortHolderName = $sortHolder.attr('data-name');

                if (typeof fbuilder_hideifs['parents'][shortcode][sortHolderName] == 'object' && typeof fbuilder_hideifs['parents'][shortcode][sortHolderName][sortName] != 'undefined') {
                    for (x in fbuilder_hideifs['parents'][shortcode][sortHolderName][sortName][sortHolderName]) {
                        if (qquery != '') qquery += ', ';
                        qquery += '[name=' + sortStart + '-' + x + ']';
                    }
                }
            }
        }
        else {
            qquery = '.fbuilder_hidable_control';
        }
        $mainquery.find(qquery).each(function () {
            var hideBool = false;
            var hideName = $(this).attr('name');
            if (hideName.substr(0, 5) != 'fsort') {
                var hideArr = fbuilder_hideifs['children'][shortcode][hideName];
                for (var x in hideArr) {
                    var $hideObj = $('.fbuilder_shortcode_menu .fbuilder_control [name=' + x + ']:first');
                    if (hideArr[x].indexOf($hideObj.val()) != -1) {
                        hideBool = true;
                        break;
                    }
                }
            }
            else {
                var sliceName = hideName.split('-');
                var hideSName = sliceName.slice(2).join('-');
                var hideSStart = sliceName.slice(0, 2).join('-');
                var $hideSHolder = $(this).closest('.fbuilder_sortable_holder')
                var hideHName = $hideSHolder.attr('data-name');
                var hideArr = fbuilder_hideifs['children'][shortcode][hideHName][hideSName];
                for (var x in hideArr) {
                    if (!(hideArr[x] instanceof Array)) {
                        for (var y in hideArr[x]) {
                            if (hideArr[x][y] instanceof Array) {
                                var $hideObj = $hideSHolder.find('.fbuilder_control [name=' + hideSStart + '-' + y + ']:first');
                                if (hideArr[x][y].indexOf($hideObj.val()) != -1) {
                                    hideBool = true;
                                    break;
                                }
                            }
                            else {
                                var $hideObj = $hideSHolder.find('[name=' + x + ']:first');
                                if (hideArr[x][y] == $hideObj.val()) {
                                    hideBool = true;
                                    break;
                                }
                            }
                        }
                    }
                    else {
                        var $hideObj = $('.fbuilder_shortcode_menu .fbuilder_control [name=' + x + ']:first');
                        if (hideArr[x].indexOf($hideObj.val()) != -1) {
                            hideBool = true;
                            break;
                        }
                    }
                }
            }

            if (hideBool) $(this).closest('.fbuilder_control').addClass('fbuilder_control_hidden');
            else $(this).closest('.fbuilder_control').removeClass('fbuilder_control_hidden');
        });
    }

    /*  Refresh Frontend Builder Controls  */

    function fbuilderRefreshControls($jq, $location) {
        if (typeof $jq == 'undefined') $jq = jQuery;
        if (typeof $location == 'undefined') $location = $('body');

        /* UI slider for number controles */
        $location.find(".fbuilder_number_bar").each(function () {
            if (!$(this).hasClass('ui-slider')) {
                var min = parseInt($(this).attr('data-min'));
                var max = parseInt($(this).attr('data-max'));
                var std = parseInt($(this).attr('data-std'));
                var step = parseInt($(this).attr('data-step'));
                var unit = $(this).attr('data-unit');

                $(this).slider({
                    min: min,
                    max: max,
                    step: step,
                    value: std,
                    range: "min",
                    slide: function (event, ui) {
                        $(this).closest('.fbuilder_control').find(".fbuilder_number_amount").val(ui.value + unit);
                    },
                    change: function (event, ui) {
                        var $input = $(this).closest('.fbuilder_control').find(".fbuilder_number_amount");
                        fbuilderContolChange($jq, $input, 400);

                    }
                });
            }
        });

        /* Sortable init on new controles */
        $location.find('.fbuilder_sortable').each(function () {
            if (!$(this).hasClass('ui-sortable')) {
                $(this).sortable({
                    items: '.fbuilder_sortable_item',
                    handle: '.fbuilder_sortable_handle',
                    stop: function (event, ui) {
                        var name = $(this).parent().attr('data-name');
                        var itemId = parseInt($('.fbuilder_shortcode_menu').attr('data-modid'));
                        fbuilder_items['items'][itemId]['options'][name]['order'] = {};
                        $(this).children('.fbuilder_sortable_item').each(function (index) {
                            fbuilder_items['items'][itemId]['options'][name]['order'][index] = parseInt($(this).attr('data-sortid'));
                        });
                        $('.fbuilder_shortcode_menu').trigger('fchange');
                    }
                });
            }
        });


        /* Shortcode color control */
        var fbuilder_color_iris;
        $location.find('.fbuilder_color').each(function () {
            var $this = $(this);
            $(this).parent().find('.fbuilder_color_display span').css('background', $(this).val());
            $(this).fbiris({
                width: 228,
                target: $(this).parent().find('.fbuilder_colorpicker'),
                palettes: ['', '#1abc9c', '#16a085', '#3498db', '#2980b9', '#9b59b6', '#8e44ad', '#34495e', '#2c3e50', '#e67e22', '#d35400', '#e74c3c', '#c0392b', '#ecf0f1', '#bdc3c7', '#ffffff', '#000000'],
                change: function (event, ui) {
                    var $thisChange = $(this);
                    $(this).parent().find('.fbuilder_color_display span').css('background-color', ui.color.toString());
                    clearTimeout(fbuilder_color_iris);
                    fbuilder_color_iris = setTimeout(function () {
                        fbuilderContolChange($jq, $thisChange, true)
                    }, 400);
                }
            });
        });


        /*
         var modid = parseInt($('.fbuilder_shortcode_menu').attr('data-modid'));
         var $module = $jq('.fbuilder_module[data-modid='+modid+']');

         $hideLocation.find('.fbuilder_hidable').each(function(){
         var hideBool = false;
         var hideName = $(this).find('.fbuilder_hidable_control').attr('name');
         if(hideName.substr(0,5) != 'fsort') {
         var hideArr = fbuilder_shortcodes[$module.attr('data-shortcode')]['options'][hideName]['hide_if'];
         for (var x in hideArr) {
         var $hideObj = $('.fbuilder_shortcode_menu').find('[name='+x+']');
         if(hideArr[x].indexOf($hideObj.val()) != -1) {
         hideBool = true;
         break;
         }
         }
         }
         else {
         var sliceName = hideName.split('-');
         var hideSName = sliceName.slice(2).join('-');
         var hideSStart = sliceName.slice(0,2).join('-');
         var $hideSHolder = $(this).closest('.fbuilder_sortable_holder')
         var hideHName = $hideSHolder.attr('data-name');
         var hideArr = fbuilder_shortcodes[$module.attr('data-shortcode')]['options'][hideHName]['options'][hideSName]['hide_if'];

         for (var x in hideArr) {
         if(typeof hideArr[x] === 'object') {
         for(var y in hideArr[x]) {
         if(hideArr[x][y] instanceof Array) {
         var $hideObj = $hideSHolder.find('[name='+hideSStart+'-'+x+']');
         if(hideArr[x][y].indexOf($hideObj.val()) != -1){
         hideBool = true;
         break;
         }
         }
         else {
         var $hideObj = $hideSHolder.find('[name='+x+']');
         if(hideArr[x][y] == $hideObj.val()) {
         hideBool = true;
         break;
         }
         }
         }
         }
         else {
         var $hideObj = $hideSHolder.find('[name='+x+']');
         if(hideArr[x].indexOf($hideObj.val()) != -1){
         hideBool = true;
         break;
         }
         }
         }
         }

         if(hideBool) $(this).hide();
         else $(this).show();
         });
         */

        /* mCustomScrollbar when new items are created */

        $location.find('.fbuilder_select ul').each(function () {
            if (!$(this).hasClass('fmCustomScrollbar')) {
                $(this).fmCustomScrollbar({mouseWheelPixels: 150});
            }
        });
        $location.find('.fmCustomScrollbar').each(function () {
            $(this).fmCustomScrollbar('update');
        })

        $location.find('.fbuilder_icon_dropdown .fbuilder_icon_dropdown_scroll').each(function () {
            if (!$(this).hasClass('fmCustomScrollbar')) {
                $(this).fmCustomScrollbar();
            }
            else {
                $(this).fmCustomScrollbar('update');
            }
        });
        if (!$('.fbuilder_shortcode_menu').hasClass('fmCustomScrollbar')) {
            $('.fbuilder_shortcode_menu').fmCustomScrollbar({
                mouseWheelPixels: 150,
                advanced: {autoScrollOnFocus: false}
            });
        }
        else {
            $('.fbuilder_shortcode_menu').fmCustomScrollbar('update');
        }

    }


    function fbuilderSortableInit($column) {
        $column.find('.fbuilder_droppable').sortable({
            items: '.fbuilder_module',
            connectWith: '.fbuilder_droppable',
            handle: '.fbuilder_module_controls .fbuilder_drag',
            start: function (event, ui) {
                fbuilder_sender = ui.item.parent();
                fbuilder_sender.css('z-index', '10');
            },
            stop: function (event, ui) {
                if (!ui.item.hasClass('fbuilder_module')) {
                    var shortcode_slug = ui.item.attr('data-shortcode');
                    ui.item.removeClass('fbuilder_draggable fbuilder_gradient').addClass('fbuilder_module').css('z-index', '2');
                    var moduleInterface = '<img class="fbuilder_module_loader" src="' + fbuilder_url + 'images/module-loader-new.gif" /><div class="fbuilder_module_controls fbuilder_gradient"><a href="#" class="fbuilder_edit" title="Edit"></a><a class="fbuilder_drag" title="Drag" href="#"></a><a href="#" class="fbuilder_clone" title="Clone"></a><a href="#" class="fbuilder_delete" title="delete"></a></div>';
                    ui.item.html(moduleInterface + '<div class="fbuilder_module_content"></div>');
                    var sid = 0;
                    while (typeof fbuilder_items['items'][sid] != 'undefined') {
                        sid++;
                    }
                    ui.item.attr('data-modid', sid);

                    fbuilder_items['items'][sid] = {};
                    fbuilder_items['items'][sid]['f'] = fbuilder_shortcodes[shortcode_slug]['function'];
                    fbuilder_items['items'][sid]['slug'] = shortcode_slug;
                    fbuilder_items['items'][sid]['options'] = {};
                    for (var x in fbuilder_shortcodes[shortcode_slug]['options']) {
                        if (fbuilder_shortcodes[shortcode_slug]['options'][x]['type'] == 'sortable') {
                            fbuilder_items['items'][sid]['options'][x] = $.extend(true, {}, fbuilder_shortcodes[shortcode_slug]['options'][x]['std']);
                        }
                        else if (fbuilder_shortcodes[shortcode_slug]['options'][x]['type'] == 'collapsible') {
                            for (var y in fbuilder_shortcodes[shortcode_slug]['options'][x]['options']) {

                                if (fbuilder_shortcodes[shortcode_slug]['options'][x]['options'][y]['type'] == 'sortable') {
                                    fbuilder_items['items'][sid]['options'][y] = $.extend(true, {}, fbuilder_shortcodes[shortcode_slug]['options'][x]['options'][y]['std']);
                                }
                                else if (typeof fbuilder_shortcodes[shortcode_slug]['options'][x]['options'][y]['std'] != 'undefined') {
                                    fbuilder_items['items'][sid]['options'][y] = fbuilder_shortcodes[shortcode_slug]['options'][x]['options'][y]['std'];
                                }
                                else {
                                    fbuilder_items['items'][sid]['options'][y] = '';
                                }
                            }
                        }
                        else if (typeof fbuilder_shortcodes[shortcode_slug]['options'][x]['std'] != 'undefined') {
                            fbuilder_items['items'][sid]['options'][x] = fbuilder_shortcodes[shortcode_slug]['options'][x]['std'];
                        }
                        else {
                            fbuilder_items['items'][sid]['options'][x] = '';
                        }
                    }

                    fbuilderGetShortcode(fbuilder_items['items'][sid]['f'], ui.item.find('.fbuilder_module_content'), fbuilder_items['items'][sid]['options']);
                    ui.item.find('.fbuilder_edit').trigger('click');
                }
                else {
                    fbuilder_sender.css('z-index', 1);
                    if (fbuilder_sender.children('*').length == 0) {
                        fbuilder_sender.parent().addClass('empty');
                    }
                    var shortcode_slug = ui.item.attr('data-shortcode');
                    var sid = parseInt(ui.item.attr('data-modid'));
                    fbuilderGetShortcode(fbuilder_shortcodes[shortcode_slug]['function'], ui.item.find('.fbuilder_module_content'), fbuilder_items['items'][sid]['options']);
                    ui.item.find('.fbuilder_edit').trigger('click');
                }

                // update data
                for (var ii = 0; ii < 2; ii++) {
                    if (ii == 0) {
                        var $fbCol = ui.item.parent();
                    }
                    else {
                        if (fbuilder_sender[0] != ui.item.parent()[0]) {
                            var $fbCol = fbuilder_sender;
                        }
                        else {
                            break;
                        }

                    }

                    $fbCol = $fbCol.closest('.fbuilder_column');
                    if (ii == 0)
                        $fbCol.find('.fbuilder_droppable:first').parent().removeClass('empty');
                    var ind = parseInt($fbCol.attr('data-colnumber'));
                    var rowId = $fbCol.closest('[data-rowid]').attr('data-rowid');
                    if (rowId != 'sidebar') {
                        rowId = parseInt(rowId);
                        fbuilder_items['rows'][rowId]['columns'][ind] = new Array();
                        $fbCol.find('.fbuilder_module').each(function (index) {
                            fbuilder_items['rows'][rowId]['columns'][ind][index] = parseInt($(this).attr('data-modid'));
                        });
                    }
                    else {
                        if (typeof fbuilder_items['sidebar'] == 'undefined')
                            fbuilder_items['sidebar'] = {}
                        fbuilder_items['sidebar']['items'] = [];
                        $fbCol.find('.fbuilder_module').each(function (index) {
                            fbuilder_items['sidebar']['items'][index] = parseInt($(this).attr('data-modid'));
                        });
                    }

                }

            }
        });
    }

    /* Draggable function */
    function fbuilderRefreshDragg($jq) {
        var drElem = $jq('.fbuilder_droppable');
        drElem.each(function () {
            if ($jq(this).children('*').length == 0) {
                $jq(this).parent().addClass('empty');
            }
        });

        $jq(".fbuilder_draggable", document).draggable("option", "connectToSortable", drElem);
    }

    /*  Activate Frontend Builder Controls  */

    function fbuilderControlsInit($jq, iDocument) {


        if (typeof $jq == 'undefined') $jq = jQuery;
        $jq('#fbuilder_content').sortable({
            items: "> div",
            handle: '.fbuilder_row_controls .fbuilder_drag_handle',
            stop: function (event, ui) {
                fbuilder_items['rowOrder'] = [];
                $jq('#fbuilder_content .fbuilder_row').each(function (index) {
                    fbuilder_items['rowOrder'][index] = parseInt($(this).attr('data-rowid'));
                });
            }
        });

        $('.fbuilder_shortcode_tab_select').click(function () {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
                $(this).closest('.fbuilder_shortcode_holder').find('.fbuilder_shortcode_tabs').show();
            }
            else {
                $(this).removeClass('active');
                $(this).closest('.fbuilder_shortcode_holder').find('.fbuilder_shortcode_tabs').hide();
            }
        });

        $('.fbuilder_shortcode_tab:first, .fbuilder_shortcode_group:first').addClass('active');

        $('.fbuilder_shortcode_tab').click(function (e) {
            e.preventDefault();
            var ind = $(this).index();
            var $holder = $(this).closest('.fbuilder_shortcode_holder');
            $holder.find('.fbuilder_shortcode_tab.active').removeClass('active');
            $holder.find('.fbuilder_shortcode_tab.after').removeClass('after');
            $(this).addClass('active');
            $(this).nextAll().addClass('after');
            $holder.find('.fbuilder_shortcode_group.active').removeClass('active');
            $holder.find('.fbuilder_shortcode_group').eq(ind).addClass('active').fmCustomScrollbar('update');
            $holder.find('.fbuilder_shortcode_tab_select img').attr('src', $(this).find('img').attr('src'));
            $holder.find('.fbuilder_shortcode_tab_select').trigger('click');
        });

        $(".fbuilder_shortcode_group").fmCustomScrollbar();

        $jq(".fbuilder_draggable", document).draggable({
            appendTo: $jq('body'),
            helper: 'clone',
            connectToSortable: $jq('.fbuilder_droppable'),
            start: function (event, ui) {
                ui.helper.css({width: $jq(this).width()});
                window.fbuilder_drag = true;
            },
            drag: function (event, ui) {
                ui.helper.css({marginTop: ui.offset.top - ui.position.top});
            }
        });

        $('.fbuilder_toggle_wrapper').hover(function () {
            $(this).stop(true).animate({bottom: 0}, 300);
        }, function () {
            $(this).stop(true).animate({bottom: -54}, 300);
        });

        $jq(iDocument).on('click', '.fbuilder_toggle_ctrl .frb_button', function (e) {
            e.preventDefault();
            $('.fbuilder_toggle').trigger('click');
        });

        $('.fbuilder_toggle').click(function (e) {
            e.preventDefault();
            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
                $jq('.fbuilder_row_holder').hide();
                $jq('#fbuilder_wrapper').removeClass('edit');
                $('#fbuilder_main_menu').stop(true).animate({left: -42}, 300);
                $('.fbuilder_shortcode_menu_toggle').stop(true).animate({right: -47}, 300);
                $('.fbuilder_shortcode_menu').stop(true).animate({right: -260}, 300);
                $('#fbuilder_body').stop(true).animate({borderLeftWidth: 0, borderRightWidth: 0}, 300);
            }
            else {
                $(this).removeClass('active');
                $jq('.fbuilder_row_holder').show();
                $jq('#fbuilder_wrapper').addClass('edit');
                $('#fbuilder_main_menu').stop(true).animate({left: 0}, 300);
                if ($('.fbuilder_shortcode_menu').length > 0) {
                    $('.fbuilder_shortcode_menu_toggle').stop(true).animate({'right': 213}, 300);
                    $('.fbuilder_shortcode_menu').stop(true).animate({right: 0}, 300);
                    $('#fbuilder_body').stop(true).animate({borderLeftWidth: 42, borderRightWidth: 260}, 300);
                }
                else {
                    $('#fbuilder_body').stop(true).animate({borderLeftWidth: 42}, 300);
                }

            }
        });

        //			shortcode menu toggle

        $(document).on('click', '.fbuilder_shortcode_menu_toggle', function () {
            $(this).stop(true).animate({'right': -47}, 300);
            $('.fbuilder_shortcode_menu').stop(true).animate({right: -260}, 300, function () {
                $(this).remove();
                fbuilder_shortcode_sw = false;
            });
            $('#fbuilder_body').stop(true).animate({borderRightWidth: 0}, 300);
            var savedData = {};

            if ($jq('.fbuilder_row.child_selected').length <= 0) {
                savedData['refid'] = parseInt($jq('.fbuilder_row.selected').attr('data-rowid'));
                savedData['type'] = 'row';
            } else {
                savedData['refid'] = parseInt($jq('.fbuilder_module.selected').attr('data-modid'));
                savedData['type'] = 'module';
            }
            $(this).data('menu_toggle_options', savedData);

            $jq('.fbuilder_module_controls.fbuilder_gradient_primary').removeClass('fbuilder_gradient_primary');
            $jq('.fbuilder_row.selected, .fbuilder_module.selected').removeClass('selected');
            $jq('.fbuilder_row.child_selected').removeClass('child_selected');
        });

        //				control descriptions

        $(document).on('mouseenter', '.fbuilder_control [class*="_label"] label', function () {
            $(this).parent().addClass('hovered').find('.fbuilder_desc').show().stop(true).delay(500).animate({opacity: 1}, 200);
        });

        $(document).on('mouseleave', '.fbuilder_control [class*="_label"] label', function () {
            $(this).parent().removeClass('hovered').find('.fbuilder_desc').stop(true).animate({opacity: 0}, 200, function () {
                $(this).hide();
            });
        });

        $(document).on('mouseenter', '.fbuilder_control .fbuilder_checkbox', function () {
            $(this).siblings('.fbuilder_control [class*="_label"]').children('label').trigger('mouseenter');
        });

        $(document).on('mouseleave', '.fbuilder_control .fbuilder_checkbox', function () {
            $(this).siblings('.fbuilder_control [class*="_label"]').children('label').trigger('mouseleave');
        });


        //				add row popup
        $(document).on('click', '.fbuilder_add_row_popup_trigger', function (e) {
            e.preventDefault();
            $('#fbuilder_add_row_popup').show();
            $('#fbuilder_editor_popup_shadow').show();
        });
        $(document).on('click', '.fbuilder_popup#fbuilder_add_row_popup .fbuilder_button', function (e) {
            e.preventDefault();
            $('#fbuilder_add_row_popup').hide();
            $('#fbuilder_editor_popup_shadow').hide();
        });

        $(document).on('click', '.fbuilder_row_button', function (e) {
            e.preventDefault();
            var value = parseInt($jq(this).attr('href').substr(1));
            var html = fbuilder_rows[value]['html'];

            var id = 0;
            while ($jq('#fbuilder_content .fbuilder_row[data-rowid=' + id + ']').length > 0) id++;
            html = html.replace('%1$s', id + '');

            var rowInterface = '<div class="fbuilder_row_controls fbuilder_gradient"><a href="#" class="fbuilder_edit" title="Edit"></a><a href="#" class="fbuilder_drag_handle" title="Move"></a><a href="#" class="fbuilder_clone" title="Clone"></a><a href="#" class="fbuilder_delete" title="delete"></a></div>';
            html = html.replace('%2$s', rowInterface);

            var columnInterface = '<div class="fbuilder_droppable empty"></div><div class="fbuilder_drop_borders"><div class="fbuilder_empty_content"><div class="fbuilder_add_shortcode fbuilder_gradient">+</div><span>Add Shortcode<br />(or drag & drop one from the left bar)</span></div></div>';
            html = html.replace(/%[0-9]+\$s/g, columnInterface);

            if (typeof fbuilder_items.rows == 'undefined') {
                fbuilder_items.rows = new Array();
                fbuilder_items.rowCount = 0;
                fbuilder_items.rowOrder = new Array();
                fbuilder_items.items = new Array();
            }
            var columns = new Array();
            var count = html.match(/fbuilder_column /g);
            for (var x = 0; x < count.length; x++) {
                columns[x] = new Array();
            }
            fbuilder_items['rows'][id] = {type: value, columns: columns};

            if ($jq('#fbuilder_wrapper').hasClass('empty')) {
                $jq('#fbuilder_wrapper').removeClass('empty');
            }
            $('.fbuilder_popup#fbuilder_add_row_popup .fbuilder_button').trigger('click');

            if ($jq('.fbuilder_row.selected').length > 0) {
                $jq(html).insertAfter('.fbuilder_row.selected');
            } else if ($jq('.fbuilder_module.selected').length > 0) {
                $jq('.fbuilder_module.selected').closest('.fbuilder_row').each(function () {
                    $jq(html).insertAfter($(this));
                });

            } else {
                $jq('#fbuilder_content').append(html);
            }

            fbuilder_items['rowOrder'] = [];
            $jq('#fbuilder_content .fbuilder_row').each(function (index) {
                fbuilder_items['rowOrder'][index] = parseInt($jq(this).attr('data-rowid'));
            });
            fbuilder_items.rowCount = $jq('#fbuilder_content .fbuilder_row').length;

            fbuilderSortableInit($jq('#fbuilder_content .fbuilder_row[data-rowid=' + id + ']'));
            fbuilderRefreshDragg($jq);
            $jq('#fbuilder_content .fbuilder_row[data-rowid=' + id + '] .fbuilder_row_controls .fbuilder_edit').trigger('click');
            $jq('#fbuilder_wrapper').trigger('refresh');
            if ($('.fbuilder_toggle').hasClass('active')) {
                $('.fbuilder_toggle').trigger('click');
            }

            $('#fbuilder_body_frame').contents().find('html').stop(true).animate({scrollTop: $jq('#fbuilder_content .fbuilder_row[data-rowid=' + id + ']').offset().top - 150}, 1000);
        });


        $('.fbuilder_toggle_screen').click(function () {
            $('.fbuilder_toggle_screen.active').removeClass('active');
            $(this).addClass('active');
            if ($(this).find('.icon-desktop').length > 0)
                $('#fbuilder_body_frame').css({
                    'min-width': $(this).attr('data-width') + 'px',
                    'max-width': '100%',
                    'width': '100%'
                });
            else if ($(this).find('.icon-laptop').length > 0)
                $('#fbuilder_body_frame').css({
                    'width': '100%',
                    'min-width': $(this).attr('data-width') + 'px',
                    'max-width': (parseInt($(this).prev().attr('data-width')) - 1) + 'px'
                });
            else
                $('#fbuilder_body_frame').css({'width': $(this).attr('data-width') + 'px', 'min-width': '0'});
        });

        if ($('#fbuilder_body_frame').width() > parseInt($('.fbuilder_toggle_screen:first').attr('data-width'))) {
            $('.fbuilder_toggle_screen:first').trigger('click');
        }
        else {
            $('.fbuilder_toggle_screen').eq(1).trigger('click');
        }


        $('body').keydown(function (e) {
            var code = e.keyCode || e.which;
            if (code == '9' && $('input:focus, select:focus, textarea:focus').length <= 0) {
                e.preventDefault();
                $('.fbuilder_toggle').trigger('click');
            }
        });
        $jq('body').keydown(function (e) {
            var code = e.keyCode || e.which;
            if (code == '9' && $jq('input:focus, select:focus, textarea:focus').length <= 0) {
                e.preventDefault();
                $('.fbuilder_toggle').trigger('click');
            }
        });


        $('.fbuilder_layout').change(function () {
            var layout = $(this).val();
            if (layout != 'full-width') {
                if ($jq('.fbuilder_sidebar').length <= 0) {
                    var html = '<div class="fbuilder_sidebar fbuilder_' + layout + ' fbuilder_row" data-rowid="sidebar">' +
                        '<div class="fbuilder_row_controls"><span class="fbuilder_sidebar_label">Sidebar</span></div>' +
                        '<div class="fbuilder_column">' +
                        '<div class="fbuilder_droppable">';


                    html += '</div>' +
                    '<div class="fbuilder_drop_borders"><div class="fbuilder_empty_content"><div class="fbuilder_add_shortcode fbuilder_gradient">+</div><span>Add Shortcode<br />(or drag & drop one from the left bar)</span></div></div>' +
                    '</div></div>';
                    $jq('#fbuilder_wrapper').prepend(html);


                    if (typeof fbuilder_items['sidebar'] == 'undefined') {
                        fbuilder_items['sidebar'] = {active: true, type: layout, items: []};
                    }
                    else {
                        fbuilder_items['sidebar']['active'] = true;
                        fbuilder_items['sidebar']['type'] = layout;
                        for (var s in fbuilder_items['sidebar']['items']) {
                            var sid = fbuilder_items['sidebar']['items'][s];
                            if (typeof sid != 'undefined') {
                                shortcode_slug = fbuilder_items['items'][sid]['slug'];
                                var moduleInterface = '<img class="fbuilder_module_loader" src="' + fbuilder_url + 'images/module-loader-new.gif" /><div class="fbuilder_module_controls fbuilder_gradient"><a href="#" class="fbuilder_dodaj" title="Add Shortcode"></a><a href="#" class="fbuilder_edit" title="Edit"></a><a class="fbuilder_drag" title="Drag" href="#"></a><a href="#" class="fbuilder_clone" title="Clone"></a><a href="#" class="fbuilder_delete" title="delete"></a></div>';

                                $jq('.fbuilder_sidebar .fbuilder_droppable').append('<div class="fbuilder_module" data-modid="' + sid + '" data-shortcode="' + shortcode_slug + '">' + moduleInterface + '<div class="fbuilder_module_content"></div></div>');
                                fbuilderGetShortcode(fbuilder_items['items'][sid]['f'], $jq('.fbuilder_sidebar').find('.fbuilder_module_content:last'), fbuilder_items['items'][sid]['options']);
                            }
                        }
                    }
                    fbuilderSortableInit($jq('.fbuilder_sidebar .fbuilder_column'));
                    fbuilderRefreshDragg($jq);
                }
                else {
                    fbuilder_items['sidebar']['type'] = layout;
                    $jq('.fbuilder_sidebar').attr('class', 'fbuilder_sidebar fbuilder_' + layout);
                }
            }
            else {
                fbuilder_items['sidebar']['active'] = false;
                $jq('.fbuilder_sidebar').remove();
            }
            $jq('#fbuilder_wrapper').removeClass('fbuilder_wrapper_full-width fbuilder_wrapper_one-third-right-sidebar fbuilder_wrapper_one-third-left-sidebar fbuilder_wrapper_one-fourth-left-sidebar fbuilder_wrapper_one-fourth-right-sidebar').addClass('fbuilder_wrapper_' + layout + ' fbuilder_row');
        });

        function jsonMod(key, value) {
            if (typeof(value) == "string") {
                return value.replace(/"/g, '&quot;');
            }
            if (typeof(value) == "array") {
                for (var x in value) {
                    if (typeof(value[x]) == "string") {
                        value[x] = value[x].replace(/"/g, '&quot;');
                    }
                }
            }
            return value;
        }

        $('.fbuilder_disabled').click(function (e) {
            e.preventDefault();
        });
        $('.fbuilder_save').click(function (e) {
            e.preventDefault();
            var codedJSON = JSON.stringify(fbuilder_items, jsonMod);
            var data = {
                action: 'fbuilder_save',
                id: post_id,
                json: codedJSON
            }
            if (typeof window.fbuilder_saveajax != 'undefined') window.fbuilder_saveajax.abort();
            var $this = $(this);
            $this.find('.save_loader').show();
            window.fbuilder_saveajax = $.post(ajaxurl, data, function (response) {
                $this.find('.save_loader').hide();
            });
        });

        $(document).on('click', '.fbuilder_export_template', function (e) {
            e.preventDefault();
            fbuilderBeforeunload('off');
            window.location.href = $(this).attr('href');
            fbuilderBeforeunload('on');
        });

        $('.fbuilder_false_save').click(function (e) {
            alert('You can\'t save here!');
        });

        $('.fbuilder_save_template').click(function (e) {
            e.preventDefault();
            var html = '<div class="fbuilder_popup fbuilder_popup_template fbuilder_controls_wrapper"><div class="fbuilder_module_controls fbuilder_gradient"><span class="fbuilder_module_name">Save template</span> <a href="#" class="fbuilder_close" title="close"></a></div><div class="fbuilder_popup_content">';
            html += '<table><tr><td><p>';
            html += 'Template name';
            html += '</p></td><td>';
            var shJson = {
                type: 'input',
                label: '',
                label_width: 0,
                control_width: 1
            }

            var ctrl = new fbuilderControl('template_name', shJson);
            html += ctrl.html();
            html += '</td></tr></table>';
            html += '<a href="#" class="fbuilder_gradient fbuilder_button fbuilder_popup_close right">Close</a><img class="fbuilder_popup_button_loader right" alt="" src="' + fbuilder_url + 'images/save-loader.gif"></img><a href="#" class="fbuilder_gradient fbuilder_button fbuilder_popup_save right">Save</a><a href="' + ajaxurl + '?action=fbuilder_export_template&id=' + fbuilder_pageID + '" class="fbuilder_gradient fbuilder_button fbuilder_export_template right">Export</a>';
            html += '</div></div><div class="fbuilder_popup_shadow"></div>';
            $('#fbuilder_body').prepend(html);
        });


        $('.fbuilder_load').click(function (e) {
            e.preventDefault();
            var html = '<div class="fbuilder_popup fbuilder_popup_load fbuilder_controls_wrapper"><div class="fbuilder_module_controls fbuilder_gradient"><span class="fbuilder_module_name">Load</span> <a href="#" class="fbuilder_close" title="close"></a></div>';
            html += '<div class="fbuilder_popup_content"><img class="fbuilder_popup_loader" src="' + fbuilder_url + 'images/popup-loader.gif" /></div>';
            html += '</div><div class="fbuilder_popup_shadow"></div>';
            $('#fbuilder_body').prepend(html);

            var data = {
                action: 'fbuilder_pages'
            }
            window.fbuilder_popupajax = $.get(ajaxurl, data, function (response) {
                response = JSON.parse(response);
                var rHtml = '';

                rHtml += '<div class="fbuilder_popup_tabs"><ul><li><a href="#pages_popup_tab_content">Load page</a></li><li><a href="#templates_popup_tab_content">Load template</a></li><li><a href="#import_popup_tab_content">Import Template</a></li></ul>';

                rHtml += '<div id="pages_popup_tab_content"><table><tr><td><p>';
                rHtml += 'Select the post you want to load';
                rHtml += '</p></td><td>';
                var shJson = {
                    type: 'select',
                    label: '',
                    label_width: 0,
                    control_width: 1,
                    options: [],
                    search: 'true'
                }
                for (var x in response['pages']) {
                    shJson['options'][x] = response['pages'][x]['title'];
                }
                select = new fbuilderControl('loaded_pages', shJson);
                rHtml += select.html();
                rHtml += '</td></tr></table></div>';


                rHtml += '<div id="templates_popup_tab_content">';
                if (!$.isEmptyObject(response['templates'])) {
                    rHtml += '<table><tr><td><p>Select the template you want to load';
                    rHtml += '</p></td><td>';
                    shJson = {
                        type: 'select',
                        label: '',
                        label_width: 0,
                        control_width: 1,
                        options: [],
                        search: 'true'
                    }
                    for (var x in response['templates']) {
                        shJson['options'][x] = response['templates'][x];
                    }
                    select = new fbuilderControl('loaded_templates', shJson);
                    rHtml += select.html();
                    rHtml += '</td></tr></table></div>';


                }
                else {
                    rHtml += '<p>You don\'t have any templates yet.</p>';
                }


                rHtml += '<div id="import_popup_tab_content">';
                rHtml += '<table><tr><td>';


                rHtml += '<form action="#" type="post" class="fbuilder_file_input_form" enctype="multipart/form-data">';
                rHtml += '<input type="file" name="file" class="fbuilder_file_input" />';
                rHtml += '<input type="checkbox" checked="checked" name="save" class="fbuilder_import_save_check" />';
                rHtml += '<div class="fbuilder_file_input_replacement"><div class="fbuilder_gradient fbuilder_button fbuilder_import_button">Browse</div><div class="fbuilder_file_input_rep_content">No file selected.</div>';
                rHtml += '<div class="frb_clear"></div></div>';
                rHtml += '</form>';

                rHtml += '</td><td>';
                rHtml += '<div class="fbuilder_import_checkbox active"></div><div class="fbuilder_import_checkbox_label">Also save as page template</div>'
                rHtml += '</td></tr></table></div>';


                rHtml += '</div><a href="#" class="fbuilder_gradient fbuilder_button fbuilder_popup_close right">Close</a><img class="fbuilder_popup_button_loader right" alt="" src="' + fbuilder_url + 'images/save-loader.gif"></img><a href="#" class="fbuilder_gradient fbuilder_button fbuilder_popup_load right">Load</a>';
                $('.fbuilder_popup_content').html(rHtml);

                $(".fbuilder_popup_tabs > ul a:first").addClass("active");
                $(".fbuilder_popup_tabs > div").hide();
                $(".fbuilder_popup_tabs > div:first").show();
                $(".fbuilder_popup_tabs > ul a").click(function (e) {
                    e.preventDefault();
                    if (!$(this).hasClass('active')) {
                        $(this).closest('ul').find('a').removeClass("active");
                        $(this).addClass('active');

                        var tabId = $(this).attr('href');

                        $(this).closest('.fbuilder_popup_tabs').children('div').stop(true, true).hide();
                        $(tabId).fadeIn();
                    }
                });
                $('.fbuilder_popup_load .fbuilder_select[data-name="loaded_templates"]').find('ul li').each(function () {
                    $(this).append('<div class="fbuilder_remove_template">X</div>');
                });

                $(".tabs").each(function () {
                    $(this).find("a:first").trigger("click");
                });

                fbuilderRefreshControls($jq, $('.fbuilder_popup_content'));
            });
        });

        $(document).on('click', '.fbuilder_import_button', function () {
            $(this).parent().siblings('.fbuilder_file_input').trigger('click');
        });

        $(document).on('change', '.fbuilder_file_input', function (e) {
            $(this).siblings('.fbuilder_file_input_replacement').find('.fbuilder_file_input_rep_content:first').html($(this).val());
            var reader = new FileReader();

            var file = e.target.files['0'];
            var fileNameArray = file.name.split('.');
            if (fileNameArray.indexOf('frb') > 0) {
                reader.onloadend = function (e) {
                    $('.fbuilder_file_input').data('fbuilder_import', e.target.result);
                };
                reader.readAsText(file);
            } else {
                alert('Wrong type of file! Please upload a valid Fbuilder file.');
            }
        });

        $(document).on('click', '.fbuilder_import_checkbox', function () {
            $(this).toggleClass('active');
            $('.fbuilder_import_save_check').trigger('click');
        });

        $(document).on('submit', '.fbuilder_file_input_form', function (e) {
            e.preventDefault();
        });

        $(document).on('click', '.fbuilder_remove_template', function () {
            if (fbuilderRemoveTemplateFlag !== true) {
                fbuilderRemoveTemplateFlag = true;
                var $this = $(this);
                var ind = $this.siblings('a').attr('data-value');
                var data = {
                    action: 'fbuilder_template_remove',
                    id: ind
                }
                $.post(ajaxurl, data, function (response) {
                    $this.closest('li').remove();
                    fbuilderRemoveTemplateFlag = false;
                });
            }

        });

        $(document).on('click', '.fbuilder_popup .fbuilder_close, .fbuilder_popup_close', function (e) {
            if ($(this).closest('#fbuilder_add_shortcode_popup').length > 0 || $(this).closest('#fbuilder_editor_popup').length > 0) {
                $('.fbuilder_popup, #fbuilder_editor_popup, .fbuilder_popup_shadow, #fbuilder_editor_popup_shadow').hide();
            }
            else {
                $(this).closest('.fbuilder_popup').remove();
                $('.fbuilder_popup_shadow').remove();
            }

        });
        $(document).on('click', '.fbuilder_popup .fbuilder_popup_load', function (e) {
            e.preventDefault();
            $(this).animate({
                paddingRight: 30,
                marginRight: -10
            }, 200).prev('.fbuilder_popup_button_loader').animate({opacity: 1, marginRight: 10}, 200);

            var $popC = $(this).closest('.fbuilder_popup_content');
            var loadIndex = $popC.find('.fbuilder_control:visible #fbuilder_select_loaded_pages, .fbuilder_control:visible #fbuilder_select_loaded_templates').val();
            var pdata;
            var alsoSave = $('.fbuilder_import_checkbox').hasClass('active');
            if (typeof loadIndex == 'undefined') {
                loadIndex = 'import';
                pdata = $('.fbuilder_file_input').data('fbuilder_import');
            } else {
                pdata = '{}';
            }
            var data = {
                action: 'fbuilder_page_content',
                id: loadIndex,
                page_data: pdata
            }

            $.post(ajaxurl, data, function (response) {
                response = response.split('|+break+response+|');
                var loadJson = JSON.parse(response[0].replace(/\\(.)/mg, "$1"));
                var loadHtml = response[1];
                fbuilder_items = loadJson;
                $jq('#fbuilder_wrapper').replaceWith(loadHtml);
                fbuilderFrameControls($jq(iDocument));
                fbuilderSortableInit($jq('#fbuilder_content .fbuilder_row'));
                fbuilderRefreshDragg($jq);
                $jq('.fbuilder_module').trigger('refresh');
                if ($('.fbuilder_shortcode_menu').length > 0) {
                    $('.fbuilder_shortcode_menu').remove();
                    $('#fbuilder_body').css({borderRightWidth: 0});
                }
                var add_shortcode_popupHTML = '<div id="fbuilder_add_shortcode_popup" class="fbuilder_popup">';

                for (var x in fbuilder_main_menu) {
                    fbuilder_main_menu[x]['type'] = 'shortcode-popup';
                    var newControl = new fbuilderControl(x, fbuilder_main_menu[x]);
                    add_shortcode_popupHTML += newControl.html();
                }
                add_shortcode_popupHTML += '<a href="#" class="fbuilder_gradient fbuilder_button fbuilder_popup_close right">Close</a></div>';

                $jq('#fbuilder_wrapper').trigger('refresh');

                fbuilder_shortcode_sw = false;
                $('.fbuilder_popup_load, .fbuilder_popup_shadow').remove();
                if (loadIndex === 'import' && alsoSave) {
                    $('.fbuilder_save_template').trigger('click');
                }
            });
        });
        $(document).on('click', '.fbuilder_popup .fbuilder_popup_save', function (e) {
            e.preventDefault();
            $(this).animate({
                paddingRight: 30,
                marginRight: -10
            }, 200).prev('.fbuilder_popup_button_loader').animate({opacity: 1, marginRight: 10}, 200);

            var $popC = $(this).closest('.fbuilder_popup_content');
            var tmplName = $popC.find('#fbuilder_input_template_name').val();
            var itemsString = JSON.stringify(fbuilder_items, jsonMod);
            var data = {
                action: 'fbuilder_template_save',
                name: tmplName,
                items: itemsString
            }
            $.post(ajaxurl, data, function (response) {
                $('.fbuilder_popup, .fbuilder_popup_shadow').remove();
            });
        });


        /* Add new row button */

        $jq(iDocument).on('click', '.fbuilder_new_row', function (e) {
            e.preventDefault();
            var $holder = $jq(this).parent();
            var buttonHeight = $holder.children('.fbuilder_new_row').height() + parseInt($holder.children('.fbuilder_new_row').css('padding-top')) + parseInt($holder.children('.fbuilder_new_row').css('padding-bottom'));
            var innerHeight = $holder.children('.fbuilder_row_holder_inner').height() + parseInt($holder.children('.fbuilder_row_holder_inner').css('padding-top')) + parseInt($holder.children('.fbuilder_row_holder_inner').css('padding-bottom'));

            if (!$jq(this).hasClass('active')) {
                $jq(this).addClass('active fbuilder_gradient_primary').removeClass('fbuilder_gradient');
                $holder.stop(true).animate({height: (buttonHeight + innerHeight + 2) + 'px'}, 300);
            }
            else {
                $jq(this).removeClass('active').addClass('fbuilder_gradient').removeClass('fbuilder_gradient_primary');
                $holder.stop(true).animate({height: (buttonHeight + 2) + 'px'}, 300, function () {
                    $jq(this).trigger('refresh');
                });
            }
        });

        /* Row button click */

        $jq(iDocument).on('click', '.fbuilder_row_button', function (e) {
            e.preventDefault();
            var value = parseInt($jq(this).attr('href').substr(1));
            var html = fbuilder_rows[value]['html'];

            var id = 0;
            while ($jq('#fbuilder_content .fbuilder_row[data-rowid=' + id + ']').length > 0) id++;
            html = html.replace('%1$s', id + '');


            var rowInterface = '<div class="fbuilder_row_controls fbuilder_gradient"><a href="#" class="fbuilder_edit" title="Edit"></a><a href="#" class="fbuilder_drag_handle" title="Move"></a><a href="#" class="fbuilder_clone" title="Clone"></a><a href="#" class="fbuilder_delete" title="delete"></a></div>';
            html = html.replace('%2$s', rowInterface);

            var columnInterface = '<div class="fbuilder_droppable empty"></div><div class="fbuilder_drop_borders"><div class="fbuilder_empty_content"><div class="fbuilder_add_shortcode fbuilder_gradient">+</div><span>Add Shortcode<br />(or drag & drop one from the left bar)</span></div></div>';
            html = html.replace(/%[0-9]+\$s/g, columnInterface);

            if (typeof fbuilder_items.rows == 'undefined') {
                fbuilder_items.rows = new Array();
                fbuilder_items.rowCount = 0;
                fbuilder_items.rowOrder = new Array();
                fbuilder_items.items = new Array();
            }
            var columns = new Array();
            var count = html.match(/fbuilder_column /g);
            for (var x = 0; x < count.length; x++) {
                columns[x] = new Array();
            }
            fbuilder_items['rows'][id] = {type: value, columns: columns};

            if ($jq('#fbuilder_wrapper').hasClass('empty')) {
                $jq('#fbuilder_wrapper').removeClass('empty');
            }
            $jq('.fbuilder_new_row').trigger('click');
            $jq('#fbuilder_content').append(html);


            fbuilder_items['rowOrder'] = [];
            $jq('#fbuilder_content .fbuilder_row').each(function (index) {
                fbuilder_items['rowOrder'][index] = parseInt($jq(this).attr('data-rowid'));
            });
            fbuilder_items.rowCount = $jq('#fbuilder_content .fbuilder_row').length;

            fbuilderSortableInit($jq('#fbuilder_content .fbuilder_row:last'));
            fbuilderRefreshDragg($jq);
            $jq('#fbuilder_content .fbuilder_row:last .fbuilder_row_controls .fbuilder_edit').trigger('click');
            $jq('#fbuilder_wrapper').trigger('refresh');
        });


        /* Row controls */
        $jq(iDocument).on('mouseenter', '.fbuilder_row', function (e) {
            //$jq('.fbuilder_row.selected .fbuilder_row_controls:first').hide();
            $jq(this).find('.fbuilder_row_controls:first').addClass('visible');
        });
        $jq(iDocument).on('mouseleave', '.fbuilder_row', function (e) {
            $jq(this).find('.fbuilder_row_controls:first').removeClass('visible');
            //$jq('.fbuilder_row.selected .fbuilder_row_controls:first').show();
        });
        $jq(iDocument).on('click', '.fbuilder_row_controls .fbuilder_drag_handle', function (e) {
            e.preventDefault();
        });
        $jq(iDocument).on('click', '.fbuilder_row_controls .fbuilder_delete', function (e) {
            e.preventDefault();
            var $parent = $jq(this).closest('.fbuilder_row');
            var id = parseInt($parent.attr('data-rowid'));
            var found = false;

            if ($('.fbuilder_shortcode_menu').hasClass('fbuilder_rowedit_menu') && $('.fbuilder_shortcode_menu').attr('data-modid') == id) {

                $('.fbuilder_shortcode_menu_toggle').stop(true).animate({'right': -47}, 300);
                $('.fbuilder_shortcode_menu').animate({right: -300}, 300, function () {
                    $(this).remove();
                    fbuilder_shortcode_sw = false;
                });
                $('#fbuilder_body').stop(true).animate({borderRightWidth: 0}, 300);
            }

            $parent.find('.fbuilder_module .fbuilder_delete').trigger('click');
            $parent.remove();
            fbuilder_items['rowOrder'] = [];
            $jq('#fbuilder_content .fbuilder_row').each(function (index) {
                fbuilder_items['rowOrder'][index] = parseInt($jq(this).attr('data-rowid'));
            });
            fbuilder_items.rowCount = $jq('#fbuilder_content .fbuilder_row').length;
            $jq('#fbuilder_wrapper').trigger('refresh');
        });

        $jq(iDocument).on('click', '.fbuilder_row_controls .fbuilder_clone', function (e) {
            e.preventDefault();
            var $parent = $jq(this).closest('[data-rowid]');
            var id = parseInt($parent.attr('data-rowid'));
            var newId = 0;
            while ($jq('.fbuilder_row[data-rowid="' + newId + '"]').length > 0) {
                newId++;
            }
            var found = false;
            var i = fbuilder_items.rowCount;
            var idReplace = {};
            while (!found) {
                if (fbuilder_items['rowOrder'][i] == id) {
                    found = true;
                    fbuilder_items['rowOrder'][i + 1] = newId;
                    fbuilder_items['rows'][newId] = $.extend(true, {}, fbuilder_items['rows'][id]);
                    fbuilder_items['rows'][newId]['columns'] = [];

                    var ind = 0;
                    for (var x in fbuilder_items['rows'][id]['columns']) {
                        fbuilder_items['rows'][newId]['columns'][x] = [];
                        for (var y in fbuilder_items['rows'][id]['columns'][x]) {
                            var itemId = fbuilder_items['rows'][id]['columns'][x][y];
                            if (typeof itemId != 'undefined') {
                                while (typeof fbuilder_items['items'][ind] != 'undefined') {
                                    ind++;
                                }
                                fbuilder_items['items'][ind] = {};
                                fbuilder_items['items'][ind]['f'] = fbuilder_items['items'][itemId]['f'];
                                fbuilder_items['items'][ind]['slug'] = fbuilder_items['items'][itemId]['slug'];
                                fbuilder_items['items'][ind]['options'] = $.extend(true, {}, fbuilder_items['items'][itemId]['options']);
                                fbuilder_items['rows'][newId]['columns'][x][y] = ind;
                                idReplace[itemId] = ind;
                            }
                        }
                    }
                } else {
                    fbuilder_items['rowOrder'][i] = fbuilder_items['rowOrder'][i - 1];
                }
                i--;
            }
            $parent.clone().insertAfter($parent);
            $parent.next().attr('data-rowid', newId);
            $parent.next().find('.fbuilder_module').each(function (ind) {
                $jq(this).attr('data-modid', idReplace[parseInt($jq(this).attr('data-modid'))]);
                var id = parseInt($(this).attr('data-modid'));
                var $module = $jq('.fbuilder_module[data-modid=' + id + ']:first');
                var f = fbuilder_items['items'][id]['f'];
                var holder = $module.find('.fbuilder_module_content:first');
                var options = fbuilder_items['items'][id]['options'];
                fbuilderGetShortcode(f, holder, options);
            });
            $parent.next().find('.fbuilder_gradient_primary').removeClass('fbuilder_gradient_primary');
            $parent.next().removeClass('selected');
            fbuilderSortableInit($parent.next());
            fbuilder_items.rowCount++;
        });

        $jq(iDocument).on('click', '.fbuilder_row_controls .fbuilder_edit', function (e) {
            e.preventDefault();
            $controls = $jq(this).closest('.fbuilder_row_controls');
            $row = $controls.closest('.fbuilder_row');

            var id = parseInt($row.attr('data-rowid'));

            if (fbuilder_shortcode_sw) {
                var $menu = $('.fbuilder_shortcode_menu');
                if (!$menu.hasClass('fbuilder_rowedit_menu') || parseInt($menu.attr('data-modid')) != id) {
                    $menu.addClass('fbuilder_rowedit_menu');
                    $menu.attr('data-modid', id);
                    if (parseInt($menu.css('right')) != 0) {
                        $menu.stop(true).animate({right: 0}, 300);
                        $('#fbuilder_body').stop(true).animate({borderRightWidth: 260}, 300);
                    }
                    $jq('.fbuilder_module_controls.fbuilder_gradient_primary').removeClass('fbuilder_gradient_primary');
                    $jq('.fbuilder_row.selected, .fbuilder_module.selected').removeClass('selected');
                    $jq('.fbuilder_row.child_selected').removeClass('child_selected');
                    $row.addClass('selected');
                    $menu.find('.fbuilder_menu_inner').stop(true).animate({opacity: 0}, 200, function () {
                        var shHtml = fbuilderCreateRowMenu(id, $row);
                        $(this).html(shHtml).animate({opacity: 1}, 300);
                        fbuilderHideControls($('false'), true);
                        fbuilderRefreshControls($jq, $menu);
                    });
                }
            }
            else {
                fbuilder_shortcode_sw = true;
                $row.addClass('selected');
                var html = ($('.fbuilder_shortcode_menu_toggle').length <= 0 ? '<div class="fbuilder_shortcode_menu_toggle fbuilder_gradient">Close</div>' : '') + '<div style="left:auto; right:-250px;" class="fbuilder_shortcode_menu fbuilder_rowedit_menu fbuilder_controls_wrapper" data-modid="' + id + '"><form autocomplete="off"><div class="fbuilder_menu_inner">';
                html += fbuilderCreateRowMenu(id, $row);
                html += '</div></form></div>';
                $('body').append(html);
                var $menu = $('.fbuilder_shortcode_menu');
                fbuilderHideControls($('false'), true);
                fbuilderRefreshControls($jq, $menu);
                $menu.stop(true).animate({right: 0}, 300);
                $('.fbuilder_shortcode_menu_toggle').stop(true).animate({'right': 213}, 300);
                $('#fbuilder_body').stop(true).animate({borderRightWidth: 260}, 300);
            }

        });


        /* Module controls */


        var moduleDeleteFlag = false;
        $jq(iDocument).on('click', '.fbuilder_module_controls .fbuilder_edit', function (e) {
            e.preventDefault();
            $controls = $jq(this).closest('.fbuilder_module_controls');
            $module = $controls.closest('.fbuilder_module');
            $row = $module.closest('.fbuilder_row');

            var id = parseInt($module.attr('data-modid'));
            var shortcode = $module.attr('data-shortcode');
            if (fbuilder_shortcode_sw) {
                var $menu = $('.fbuilder_shortcode_menu');
                if ($menu.hasClass('fbuilder_rowedit_menu') || parseInt($menu.attr('data-modid')) != id) {
                    $menu.removeClass('fbuilder_rowedit_menu');
                    $jq('.fbuilder_row.selected, .fbuilder_module.selected').removeClass('selected');
                    $jq('.fbuilder_row.child_selected').removeClass('child_selected');
                    $row.addClass('child_selected');
                    $menu.attr('data-modid', id).attr('data-shortcode', shortcode);
                    if ($menu.css('right') != '0px') {
                        $menu.stop(true).animate({right: 0}, 300);
                        $('#fbuilder_body').stop(true).animate({borderRightWidth: 260}, 300);
                    }
                    $jq('.fbuilder_module_controls.fbuilder_gradient_primary').removeClass('fbuilder_gradient_primary');
                    $controls.addClass('fbuilder_gradient_primary');
                    $module.addClass('selected');
                    $menu.find('.fbuilder_menu_inner').stop(true).animate({opacity: 0}, 200, function () {
                        var shHtml = fbuilderCreateShortcodeMenu(id, $module);
                        $(this).html(shHtml).animate({opacity: 1}, 300);
                        fbuilderHideControls($('false'), true);
                        fbuilderRefreshControls($jq, $menu);
                    });
                }
            }
            else {
                fbuilder_shortcode_sw = true;
                $row.addClass('child_selected');
                $module.addClass('selected');
                $controls.addClass('fbuilder_gradient_primary');
                var html = ($('.fbuilder_shortcode_menu_toggle').length <= 0 ? '<div class="fbuilder_shortcode_menu_toggle fbuilder_gradient">Close</div>' : '') + '<div style="left:auto; right:-250px;" class="fbuilder_shortcode_menu fbuilder_controls_wrapper" data-modid="' + id + '" data-shortcode="' + shortcode + '"><form autocomplete="off"><div class="fbuilder_menu_inner">';
                html += fbuilderCreateShortcodeMenu(id, $module);
                html += '</div></form></div>';
                $('body').append(html);
                var $menu = $('.fbuilder_shortcode_menu');
                fbuilderHideControls($('false'), true);
                fbuilderRefreshControls($jq, $menu);
                $menu.stop(true).animate({right: 0}, 300);
                $('.fbuilder_shortcode_menu_toggle').stop(true).animate({'right': 213}, 300);
                $('#fbuilder_body').stop(true).animate({borderRightWidth: 260}, 300);
            }
        });

        $jq(iDocument).on('click', '.fbuilder_module_controls .fbuilder_drag', function (e) {
            e.preventDefault();
        });

        /* Add Shortcode in MODULE CONTROLS */
        $jq(iDocument).on('click', '.fbuilder_module_controls .fbuilder_dodaj', function (e) {
            e.preventDefault();

            $this = $jq(this);
            if ($jq(this).children('div').length <= 0) {
                $column = $this.closest('.fbuilder_column');
                $row = $column.closest('.fbuilder_row');
                $('#fbuilder_add_shortcode_popup').data('row', $row).data('column', $column).fadeIn();
                $('#fbuilder_editor_popup_shadow').show();
            }
        });

        $jq(iDocument).on('click', '.fbuilder_module_controls .fbuilder_delete', function (e) {
            e.preventDefault();
            moduleDeleteFlag = true;
            var $module = $jq(this).parent().parent();
            var modid = parseInt($module.attr('data-modid'));
            var $column = $module.parent().parent();
            var rowid = $column.closest('[data-rowid]').attr('data-rowid');

            $module.remove();
            if ($('.fbuilder_shortcode_menu').attr('data-modid') == modid) {

                $('.fbuilder_shortcode_menu_toggle').stop(true).animate({'right': -47}, 300);
                $('.fbuilder_shortcode_menu').animate({right: -300}, 300, function () {
                    $(this).remove();
                    fbuilder_shortcode_sw = false;
                });
                $('#fbuilder_body').stop(true).animate({borderRightWidth: 0}, 300);
            }

            if (rowid != 'sidebar') {
                rowid = parseInt(rowid);
                var colnum = parseInt($column.attr('data-colnumber'));
                fbuilder_items['rows'][rowid]['columns'][colnum] = [];
                $column.find('.fbuilder_module').each(function (index) {
                    fbuilder_items['rows'][rowid]['columns'][colnum][index] = parseInt($jq(this).attr('data-modid'));
                });
                delete fbuilder_items['items'][modid];

                if ($column.find('.fbuilder_module').length == 0) {
                    $column.addClass('empty');
                }

            }
            else {
                fbuilder_items['sidebar']['items'] = [];
                $column.find('.fbuilder_module').each(function (index) {
                    fbuilder_items['sidebar']['items'][index] = parseInt($jq(this).attr('data-modid'));
                });
                delete fbuilder_items['items'][modid];
            }
            $jq('#fbuilder_wrapper').trigger('refresh');
        });

        $jq(iDocument).on('click', '.fbuilder_module_controls .fbuilder_clone', function (e) {
            e.preventDefault();
            var $module = $jq(this).parent().parent();
            var $clone = $module.clone();
            var modid = parseInt($module.attr('data-modid'));
            var $column = $module.parent().parent();
            var colnum = parseInt($column.attr('data-colnumber'));
            var rowid = $column.closest('[data-rowid]').attr('data-rowid');

            var newid = 0;
            while (typeof fbuilder_items['items'][newid] != 'undefined') newid++;
            fbuilder_items['items'][newid] = {};
            fbuilder_items['items'][newid]['f'] = fbuilder_items['items'][modid]['f'];
            fbuilder_items['items'][newid]['slug'] = fbuilder_items['items'][modid]['slug'];
            fbuilder_items['items'][newid]['options'] = $.extend(true, {}, fbuilder_items['items'][modid]['options']);

            $clone.insertAfter($module);
            $module.next().attr('data-modid', newid);
            var $module = $jq('.fbuilder_module[data-modid=' + newid + ']:first');
            var f = fbuilder_items['items'][newid]['f'];
            var holder = $module.find('.fbuilder_module_content:first');
            var options = fbuilder_items['items'][newid]['options'];
            fbuilderGetShortcode(f, holder, options);

            if (rowid != 'sidebar') {
                rowid = parseInt(rowid);
                fbuilder_items['rows'][rowid]['columns'][colnum] = [];
                $column.find('.fbuilder_module').each(function (index) {
                    fbuilder_items['rows'][rowid]['columns'][colnum][index] = parseInt($jq(this).attr('data-modid'));
                });
            }
            else {
                fbuilder_items['sidebar']['items'] = [];
                $column.find('.fbuilder_module').each(function (index) {
                    fbuilder_items['sidebar']['items'][index] = parseInt($jq(this).attr('data-modid'));
                });
            }
        });


        /* Shortcode select control */

        $(document).on('mouseenter', '.fbuilder_select', function () {
            $(this).data('hover', true);
        });
        $(document).on('mouseleave', '.fbuilder_select', function () {
            $(this).data('hover', false);
        });

        $(document).on('click', '.fbuilder_select span, .fbuilder_select .drop_button', function (e) {
            e.preventDefault();
            $parent = $(this).parent();
            if (!$parent.hasClass('active')) {
                $parent.addClass('active').find('ul, input').show();
            }
            else {
                $parent.removeClass('active').find('ul, input').hide();
            }
            fbuilderRefreshControls($jq, $(this).closest('.fbuilder_control'));
        });
        $(document).on('click', '.fbuilder_select ul a', function (e) {
            e.preventDefault();
            var $parent = $(this).closest('.fbuilder_select');
            var multi = $parent.hasClass('fbuilder_select_multi');
            var $select = $('[name=' + $parent.attr('data-name') + ']');
            if (!multi || typeof window.shiftKey == 'undefined' || window.shiftKey == false) {
                $select.val($(this).attr('data-value'));
                $parent.find('span').html($(this).html());
                $parent.removeClass('active').find('ul, input').hide();
                $parent.find('ul a.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            else {
                var multiVal = $select.val();
                var multiHtml = $parent.find('span').html();

                if (!$(this).hasClass('selected')) {
                    $(this).addClass('selected');
                    if (multiVal != '') {
                        multiVal += ',';
                        multiHtml += ',';
                    }
                    multiVal += $(this).attr('data-value');
                    multiHtml += $(this).html();
                }
                else {
                    $(this).removeClass('selected');

                    var multiSplitHtml = multiHtml.split(',');
                    var multiSplitVal = multiVal.split(',');
                    multiHtml = '';
                    multiVal = '';
                    var flag = 0;
                    for (var x in multiSplitVal) {
                        if (multiSplitVal[x] != $(this).attr('data-value')) {
                            if (x != 0 && flag != 1) {
                                multiVal += ',';
                                multiHtml += ',';
                            }
                            multiVal += multiSplitVal[x];
                            multiHtml += multiSplitHtml[x];
                            flag = 0;
                        }
                        else if (x == 0) {
                            flag = 1;
                        }
                    }

                    //multiVal +=	$(this).attr('data-value');
                    //multiHtml += $(this).html();
                }

                $select.val(multiVal);
                $parent.find('span').html(multiHtml);
            }
            $select.trigger('change');
            fbuilderContolChange($jq, $select);
        });
        $('body').keydown(function (e) {
            var code = e.keyCode || e.which;
            if (e.ctrlKey) {
                window.shiftKey = true;
            }
        });
        $('body').keyup(function (e) {
            var code = e.keyCode || e.which;
            if (code == 17) {
                window.shiftKey = false;
            }
        });
        /*$('body').click(function () {
            $('.fbuilder_select.active').each(function () {
                if (!$(this).data('hover')) {
                    $(this).removeClass('active').find('ul, input').hide();
                }
            });
        });*/

        $(document).on('keyup', '.fbuilder_select input', function () {
            var inValue = $(this).val();
            if (inValue == '') {
                $(this).closest('.fbuilder_select').find('ul li').show();
            }
            else {
                $(this).closest('.fbuilder_select').find('ul li').each(function () {
                    if ($(this).html().toLowerCase().search(inValue.toLowerCase()) > -1) {
                        $(this).show();
                    }
                    else {
                        $(this).hide();
                    }
                });
            }
            $(this).closest('.fbuilder_select').find('ul').fmCustomScrollbar('update');
        });


        /* Shortcode input/textarea control */
        $(document).on('click', '.fbuilder_input_wrapper label', function () {
            var $input = $(this).parent().find('input');
            var val = $input.val();
            $input.trigger('focus').val('').val(val);
        })
        $(document).on('keyup', '.fbuilder_shortcode_menu input, .fbuilder_shortcode_menu textarea', function () {
            fbuilderContolChange($jq, $(this), 500);
        });
        var $fbuilder_editor_textarea;
        $(document).on('click', '.fbuilder_wp_editor_button', function (e) {
            e.preventDefault();
            $fbuilder_editor_textarea = $(this).siblings('.fbuilder_textarea');
            $('#fbuilder_editor_popup, #fbuilder_editor_popup_shadow').show();
            $('#fbuilder_editor-tmce').trigger('click');
            if (typeof tinymce.editors[0] != 'undefined') {
                tinymce.editors[0].setContent($fbuilder_editor_textarea.val());
            } else if (typeof tinymce.editors[1] != 'undefined') {
                tinymce.editors[1].setContent($fbuilder_editor_textarea.val());
            } else {
                tinymce.editors.fbuilder_editor.setContent($fbuilder_editor_textarea.val());
            }
        });


        $jq(iDocument).on('click', '.fbuilder_column.empty .fbuilder_droppable', function () {
            $this = $jq(this);
            if ($jq(this).children('div').length <= 0) {
                $column = $this.closest('.fbuilder_column');
                $row = $column.closest('.fbuilder_row');
                $('#fbuilder_add_shortcode_popup').data('row', $row).data('column', $column).fadeIn();
                $('#fbuilder_editor_popup_shadow').show();
            }
        });

        $('#fbuilder_select_fbuilder_add_shortcode_group').on('change', function () {
            var $popup = $('#fbuilder_add_shortcode_popup');
            $popup.find('.fbuilder_shortcode_group').hide();
            $popup.find('.fbuilder_shortcode_group[data-group="' + $(this).val() + '"]').show();
        });
        $('.fbuilder_shortcode_block').on('click', function () {
            var $popup = $('#fbuilder_add_shortcode_popup');
            var $fbCol = $popup.data('column');
            var $row = $popup.data('row');
            var shortcode_slug = $(this).attr('data-shortcode');
            var sid = 0;
            while (typeof fbuilder_items['items'][sid] != 'undefined') {
                sid++;
            }

            fbuilder_items['items'][sid] = {};
            fbuilder_items['items'][sid]['f'] = fbuilder_shortcodes[shortcode_slug]['function'];
            fbuilder_items['items'][sid]['slug'] = shortcode_slug;
            fbuilder_items['items'][sid]['options'] = {};

            for (var x in fbuilder_shortcodes[shortcode_slug]['options']) {
                if (fbuilder_shortcodes[shortcode_slug]['options'][x]['type'] == 'sortable') {
                    fbuilder_items['items'][sid]['options'][x] = $.extend(true, {}, fbuilder_shortcodes[shortcode_slug]['options'][x]['std']);
                }
                else if (fbuilder_shortcodes[shortcode_slug]['options'][x]['type'] == 'collapsible') {
                    for (var y in fbuilder_shortcodes[shortcode_slug]['options'][x]['options']) {

                        if (fbuilder_shortcodes[shortcode_slug]['options'][x]['options'][y]['type'] == 'sortable') {
                            fbuilder_items['items'][sid]['options'][y] = $.extend(true, {}, fbuilder_shortcodes[shortcode_slug]['options'][x]['options'][y]['std']);
                        }
                        else if (typeof fbuilder_shortcodes[shortcode_slug]['options'][x]['options'][y]['std'] != 'undefined') {
                            fbuilder_items['items'][sid]['options'][y] = fbuilder_shortcodes[shortcode_slug]['options'][x]['options'][y]['std'];
                        }
                        else {
                            fbuilder_items['items'][sid]['options'][y] = '';
                        }
                    }
                }
                else if (typeof fbuilder_shortcodes[shortcode_slug]['options'][x]['std'] != 'undefined') {
                    fbuilder_items['items'][sid]['options'][x] = fbuilder_shortcodes[shortcode_slug]['options'][x]['std'];
                }
                else {
                    fbuilder_items['items'][sid]['options'][x] = '';
                }
            }

            var html = '<div class="fbuilder_module" style="z-index:2" data-modid="' + sid + '" data-shortcode="' + shortcode_slug + '">';
            html += '<img class="fbuilder_module_loader" src="' + fbuilder_url + 'images/module-loader-new.gif" /><div class="fbuilder_module_controls fbuilder_gradient"><a href="#" class="fbuilder_dodaj" title="Add Shortcode"></a><a href="#" class="fbuilder_edit" title="Edit"></a><a class="fbuilder_drag" title="Drag" href="#"></a><a href="#" class="fbuilder_clone" title="Clone"></a><a href="#" class="fbuilder_delete" title="delete"></a></div>';
            html += '<div class="fbuilder_module_content">';
            html += '</div>';
            html += '</div>';

            $fbCol.find('.fbuilder_droppable:first').append(html);
            var $item = $fbCol.find('.fbuilder_droppable:first').children('.fbuilder_module:last');
            fbuilderGetShortcode(fbuilder_items['items'][sid]['f'], $item.find('.fbuilder_module_content'), fbuilder_items['items'][sid]['options']);
            $item.find('.fbuilder_edit').trigger('click');

            // update data


            $fbCol.find('.fbuilder_droppable:first').parent().removeClass('empty');
            var ind = parseInt($fbCol.attr('data-colnumber'));
            var rowId = $row.attr('data-rowid');
            if (rowId != 'sidebar') {
                rowId = parseInt(rowId);
                fbuilder_items['rows'][rowId]['columns'][ind] = new Array();
                $fbCol.find('.fbuilder_module').each(function (index) {
                    fbuilder_items['rows'][rowId]['columns'][ind][index] = parseInt($(this).attr('data-modid'));
                });
            }
            else {
                if (typeof fbuilder_items['sidebar'] == 'undefined')
                    fbuilder_items['sidebar'] = {}
                fbuilder_items['sidebar']['items'] = [];
                $fbCol.find('.fbuilder_module').each(function (index) {
                    fbuilder_items['sidebar']['items'][index] = parseInt($(this).attr('data-modid'));
                });
            }
            $popup.hide();
            $('.fbuilder_popup_shadow, #fbuilder_editor_popup_shadow').hide();

        });

        $(document).on('click', '.fbuilder_popup_edit_submit', function (e) {
            e.preventDefault();
            $('#fbuilder_editor_popup, #fbuilder_editor_popup_shadow').hide();
            $fbuilder_editor_textarea.val(tinymce.activeEditor.getContent()).trigger('keyup');
        });


        /* Shortcode checkbox control */
        $(document).on('click', '.fbuilder_checkbox', function () {
            var $input = $(this).parent().find('.fbuilder_checkbox_input');
            if ($(this).hasClass('active')) {
                $input.val('false');
                $(this).removeClass('active');
            }
            else {
                $input.val('true');
                $(this).addClass('active');
            }
            fbuilderContolChange($jq, $input);

        });

        /* Shortcode icon control */

        $(document).on('click', '.fbuilder_icon_pick', function (e) {
            e.preventDefault();
            var $drop = $(this).parent().find('.fbuilder_icon_dropdown');
            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
                $drop.show().addClass('active').fmCustomScrollbar('update');
                $(this).parent().find('.fbuilder_icon_drop_arrow').show();
            }
            else {
                $(this).removeClass('active');
                $drop.hide().removeClass('active');
                $(this).parent().find('.fbuilder_icon_drop_arrow').hide();
            }
            fbuilderRefreshControls($jq, $(this).closest('.fbuilder_control'));
        });
        $(document).on('click', '.fbuilder_icon_dropdown a', function (e) {
            e.preventDefault();
            var $parent = $(this).closest('.fbuilder_control');
            var $input = $parent.find('input:first');
            var val = $(this).attr('href');
            $input.val(val);
            $parent.find('.fbuilder_icon_holder i:first').attr('class', val + ' frb_icon ' + val.substr(0, 2));
            fbuilderContolChange($jq, $input);
        });
        $(document).on('click', '.fbuilder_icon_tab', function (e) {
            e.preventDefault();
            if (!$(this).hasClass('active')) {
                var $parent = $(this).closest('.fbuilder_control');
                var tabid = $(this).attr('data-tabid');
                $parent.find('.fbuilder_icon_tab.active, .fbuilder_icon_noicon.active').removeClass('active');
                $(this).addClass('active');
                $parent.find('.fbuilder_icon_dropdown_content.active').removeClass('active');
                $parent.find('.fbuilder_icon_dropdown_content[data-tabid=' + tabid + ']').addClass('active');
                fbuilderRefreshControls($jq, $parent);
            }
        });
        $(document).on('click', '.fbuilder_icon_noicon', function (e) {
            e.preventDefault();
            if (!$(this).hasClass('active')) {
                var $parent = $(this).closest('.fbuilder_control');
                var $input = $parent.find('input:first');
                var tabid = $(this).attr('data-tabid');
                $input.val('no-icon')
                $parent.find('.fbuilder_icon_holder i:first').attr('class', 'frb_icon no-icon');
                $parent.find('.fbuilder_icon_tab.active').removeClass('active');
                $(this).addClass('active');
                $parent.find('.fbuilder_icon_dropdown_content.active').removeClass('active');
                fbuilderRefreshControls($jq, $parent);
                fbuilderContolChange($jq, $input);
            }
        });

        $(document).on('mouseenter', '.fbuilder_icon_dropdown, .fbuilder_icon_pick', function () {
            $(this).data('hover', true);
        });
        $(document).on('mouseleave', '.fbuilder_icon_dropdown, .fbuilder_icon_pick', function () {
            $(this).data('hover', false);
        });

        $('body').click(function () {
            $('.fbuilder_icon_dropdown.active').each(function () {
                if (!$(this).data('hover') && !$(this).parent().find('.fbuilder_icon_pick').data('hover')) {
                    $(this).removeClass('active').hide();
                    $(this).parent().find('.fbuilder_icon_drop_arrow').hide();
                    $(this).parent().find('.fbuilder_icon_pick').removeClass('active');
                }
            });
        });

        /*	Shortcode media select control	*/

        $(document).on('click', '.fbuilder_media_select_button', function (e) {
            e.preventDefault();
            var media, $this = $(this);

            if (typeof media != 'undefined') {
                media.open();
            } else {
                media = wp.media({
                    button: {
                        close: false
                    },
                    multiple: true
                });
            }
            media.on('select', function () {
                var attachment = media.state().get('selection');
                media.close();
                var out = '';
                attachment.map(function (att) {
                    out = out + ',' + att.id;
                });

                while (out.substr(0, 1) === ',') {
                    out = out.substr(1);
                }

                $this.siblings('.fbuilder_media_select_input').find('input').val(out).trigger('keyup');
            });

            media.on('open', function () {
                var attachment, sel = media.state().get('selection');
                preselect = $this.siblings('.fbuilder_media_select_input').find('input').val().split(',');
                preselect.forEach(function (id) {
                    attachment = wp.media.attachment(id);
                    attachment.fetch();
                    sel.add(attachment ? [attachment] : []);
                });
            });

            media.open();
        });

        /* Shortcode image control */

        var thickboxId = '';
        $(document).on('click', '.fbuilder_image_button', function (e) {
            e.preventDefault();
            var frame, $this = $(this);

            if (typeof frame != 'undefined') {
                frame.open();
            } else {
                frame = wp.media({
                    button: {
                        close: false
                    }
                });
            }
            frame.on('select', function () {
                var attachment = frame.state().get('selection').first();
                frame.close();
                $this.siblings('.fbuilder_image_input').find('input').val(attachment.attributes.url).trigger('keyup').siblings('span').html('');
            });

            frame.open();
        });

        $(document).on('click', '.fbuilder_image_input span', function () {
            $(this).hide();
	    setTimeout(function () {
                $(this).parent().find('input').focus();
	    });
        });

        $(document).on('focusout', '.fbuilder_image_input input', function () {
            if ($(this).val() == '') {
                $(this).parent().find('span').show();
            }
        });
        $(document).on('keyup', '.fbuilder_image_input input', function () {
            thickboxId = '#' + $(this).attr('id') + '_holder';
            imgurl = $(this).val();
            var ww = $(thickboxId).width();
            var hh = $(thickboxId).height();
            if ($(thickboxId).hasClass('fbuilder_background_holder')) {
                $(thickboxId).css('background', 'url(' + imgurl + ') repeat');
            }
            else {
                $(thickboxId).html('<img style="max-width:' + ww + 'px; max-height:' + hh + 'px;" src="' + imgurl + '" alt="" />');
            }
            fbuilderContolChange($jq, $(this));
        });

        window.send_to_editor = function (html) {
            if (typeof formfield != 'undefined') {
                var img_pos = html.indexOf('<img');
                if (img_pos > 0) html = html.substring(img_pos);
                img_pos = html.indexOf('>');
                if (img_pos > 0) html = html.substring(0, img_pos + 1);
                while (html.indexOf('\\"') > -1) html = html.replace('\\"', '"');
                var $jhtml = $(html);
                var imgurl = $jhtml.attr('src');

                $('#' + formfield).parent().find('span').hide();
                $('#' + formfield).val(imgurl);
                var ww = $(thickboxId).width();
                var hh = $(thickboxId).height();
                if ($(thickboxId).hasClass('fbuilder_background_holder')) {
                    $(thickboxId).css('background', 'url(' + imgurl + ') repeat');
                }
                else {
                    $(thickboxId).html('<img style="max-width:' + ww + 'px; max-height:' + hh + 'px;" src="' + imgurl + '" alt="" />');
                }
                tb_remove();
                fbuilderContolChange($jq, $('#' + formfield));
            }
            else {
                // mce
                tinymce.editors[0].execCommand('mceInsertContent', false, html);
                tinymce.editors[1].execCommand('mceInsertContent', false, html);
                tinymce.editors.fbuilder_editor.execCommand('mceInsertContent', false, html);
            }
        }
//				Zoom toggle
        $('.fbuilder_toggle_zoom_trigger').click(function () {
            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                $('#fbuilder_body_frame').css({
                    'transform': 'scale(0.5,0.5)',
                    '-webkit-transform': 'scale(0.5,0.5)',
                    'height': '200%',
                    'top': '-50%'
                });
            } else {
                $('#fbuilder_body_frame').css({
                    'transform': 'scale(1,1)',
                    '-webkit-transform': 'scale(1,1)',
                    'height': '100%',
                    'top': '0'
                });
            }
        });

        /* Shortcode sortable control */

        $(document).on('click', '.fbuilder_sortable_add', function (e) {
            e.preventDefault();
            var html = '';
            var name = $(this).closest('.fbuilder_sortable_holder').attr('data-name');
            var item_name = $(this).closest('.fbuilder_sortable_holder').attr('data-iname');
            var $smenu = $(this).parent().parent();
            while (!$smenu.hasClass('fbuilder_shortcode_menu'))
                $smenu = $smenu.parent();
            var itemId = parseInt($smenu.attr('data-modid'));
            var itemSh = $smenu.attr('data-shortcode');

            var shortcodeJSON = {};

            if (typeof fbuilder_shortcodes[itemSh]['options'][name] == 'undefined') {
                for (var x in fbuilder_shortcodes[itemSh]['options']) {
                    if (typeof fbuilder_shortcodes[itemSh]['options'][x]['options'] != 'undefined' && typeof fbuilder_shortcodes[itemSh]['options'][x]['options'][name] != 'undefined') {
                        shortcodeJSON = $.extend(true, {}, fbuilder_shortcodes[itemSh]['options'][x]['options'][name]);
                    }
                }
            }
            else {
                shortcodeJSON = $.extend(true, {}, fbuilder_shortcodes[itemSh]['options'][name]);
            }


            if (typeof fbuilder_items['items'][itemId]['options'][name]['items'] == 'undefined') {
                fbuilder_items['items'][itemId]['options'][name]['items'] = {};
                fbuilder_items['items'][itemId]['options'][name]['order'] = {};
            }
            var count = 0;
            while (typeof fbuilder_items['items'][itemId]['options'][name]['items'][count] != 'undefined' && fbuilder_items['items'][itemId]['options'][name]['items'][count] != '')
                count++;

            var pos = 0;
            while (typeof fbuilder_items['items'][itemId]['options'][name]['order'][pos] != 'undefined')
                pos++;
            fbuilder_items['items'][itemId]['options'][name]['order'][pos] = count;

            html += '<div class="fbuilder_sortable_item fbuilder_collapsible" data-sortid="' + count + '" data-sortname="' + name + '"><div class="fbuilder_gradient fbuilder_sortable_handle fbuilder_collapsible_header">' + item_name + ' ' + count + ' - <span class="fbuilder_sortable_delete">delete</span>, <span class="fbuilder_sortable_clone">clone</span><span class="fbuilder_collapse_trigger">+</span></div><div class="fbuilder_collapsible_content">';
            fbuilder_items['items'][itemId]['options'][name]['items'][count] = {};
            for (var x in shortcodeJSON['options']) {
                var newControl = new fbuilderControl('fsort-' + count + '-' + x, shortcodeJSON['options'][x]);
                html += newControl.html();

                fbuilder_items['items'][itemId]['options'][name]['items'][count][x] = (typeof shortcodeJSON['options'][x]['std'] != 'undefined' ? shortcodeJSON['options'][x]['std'] : '');
            }
            html += '<div style="clear:both;"></div></div></div>';
            $(this).parent().find('.fbuilder_sortable').append(html);
            fbuilderRefreshControls($jq, $(this).parent());
            fbuilderHideControls($('false'), true, $(this).parent().find('.fbuilder_sortable_item'));
            $('.fbuilder_shortcode_menu').trigger('fchange');
        });


        //		Sortable Clone
        $(document).on('click', '.fbuilder_sortable_clone', function () {
            var $sortitem = $(this).parent().parent();
            var id = parseInt($sortitem.attr('data-sortid'));
            var name = $sortitem.attr('data-sortname');
            var itemId = parseInt($('.fbuilder_shortcode_menu').attr('data-modid'));
            var $sortable = $sortitem.parent();
            var newId = 0;
            while (typeof fbuilder_items['items'][itemId]['options'][name]['items'][newId] != 'undefined' && fbuilder_items['items'][itemId]['options'][name]['items'][newId] != '')
                newId++;
            fbuilder_items['items'][itemId]['options'][name]['items'][newId] = {};


            fbuilder_items['items'][itemId]['options'][name]['items'][newId] = $.extend(fbuilder_items['items'][itemId]['options'][name]['items'][newId], fbuilder_items['items'][itemId]['options'][name]['items'][id]);


            //		regenerate controls
            var html = '';
            var item_name = $(this).closest('.fbuilder_sortable_holder').attr('data-iname');
            var itemSh = $('.fbuilder_shortcode_menu').attr('data-shortcode');
            var shortcodeJSON = {};

            if (typeof fbuilder_shortcodes[itemSh]['options'][name] == 'undefined') {
                for (var x in fbuilder_shortcodes[itemSh]['options']) {
                    if (typeof fbuilder_shortcodes[itemSh]['options'][x]['options'] != 'undefined' && typeof fbuilder_shortcodes[itemSh]['options'][x]['options'][name] != 'undefined') {
                        shortcodeJSON = $.extend(true, {}, fbuilder_shortcodes[itemSh]['options'][x]['options'][name]);
                    }
                }
            }
            else {
                shortcodeJSON = $.extend(true, {}, fbuilder_shortcodes[itemSh]['options'][name]);
            }

            html += '<div class="fbuilder_sortable_item fbuilder_collapsible" data-sortid="' + newId + '" data-sortname="' + name + '"><div class="fbuilder_gradient fbuilder_sortable_handle fbuilder_collapsible_header">' + item_name + ' ' + newId + ' - <span class="fbuilder_sortable_delete">delete</span>, <span class="fbuilder_sortable_clone">clone</span><span class="fbuilder_collapse_trigger">+</span></div><div class="fbuilder_collapsible_content">';
            var bay = shortcodeJSON['options'];
            for (var x in bay) {
                bay[x]['std'] = fbuilder_items['items'][itemId]['options'][name]['items'][newId][x];
                var newControl = new fbuilderControl('fsort-' + newId + '-' + x, bay[x]);
                html += newControl.html();
            }
            html += '<div style="clear:both;"></div></div></div>';
            $(html).insertAfter($sortitem);

            // 		recalculate order

            var orderStr = '',
                insertPosition = 0;
            for (key in fbuilder_items['items'][itemId]['options'][name]['order']) {
                if (fbuilder_items['items'][itemId]['options'][name]['order'][key] == id) {
                    insertPosition = parseInt((key + '').replace('"', '')) + 1
                }
                orderStr += (fbuilder_items['items'][itemId]['options'][name]['order'][key] + ',');
            }
            orderStr = orderStr.substr(0, orderStr.length - 1).split(',');
            orderStr.splice(insertPosition, 0, parseInt((newId + '').replace('"', '')) + '');
            var tempObj = {};
            for (i = 0; i < orderStr.length; i++) {
                tempObj['"' + i + '"'] = parseInt(orderStr[i].replace('"', ''));
            }
            fbuilder_items['items'][itemId]['options'][name]['order'] = tempObj;

            //		trigger refreshes
            fbuilderRefreshControls($jq, $(this).parent());
            fbuilderHideControls($('false'), true, $(this).closest().find('.fbuilder_sortable_item'));
            $('.fbuilder_shortcode_menu').trigger('fchange');

        });


        $(document).on('click', '.fbuilder_sortable_delete', function () {
            var $sortitem = $(this).parent().parent();
            var id = parseInt($sortitem.attr('data-sortid'));
            var name = $sortitem.attr('data-sortname');
            var itemId = parseInt($('.fbuilder_shortcode_menu').attr('data-modid'));
            var $sortable = $sortitem.parent();
            $sortitem.remove();
            delete fbuilder_items['items'][itemId]['options'][name]['items'][id];
            delete fbuilder_items['items'][itemId]['options'][name]['order'];
            fbuilder_items['items'][itemId]['options'][name]['order'] = {};
            $sortable.children('.fbuilder_sortable_item').each(function (index) {
                fbuilder_items['items'][itemId]['options'][name]['order'][index] = parseInt($(this).attr('data-sortid'));
            });
            $('.fbuilder_shortcode_menu').trigger('fchange');
        });


        /* Shortcode collapsible control */

        $(document).on('click', '.fbuilder_collapse_trigger', function () {
            var $content = $(this).parent().parent().children('.fbuilder_collapsible_content');
            if (!$(this).hasClass('active')) {
                $(this).html('-').addClass('active');
                $content.show();
            }
            else {
                $(this).html('+').removeClass('active');
                $content.hide();
            }
            fbuilderRefreshControls($jq, $(this).closest('.fbuilder_control'));

        });

        /* Shortcode colorpicker control */
        $(document).on('click', '.fbuilder_color_display', function () {
            var $ctrl = $(this).closest('.fbuilder_color_wrapper');
            $ctrl.find('.fbuilder_colorpicker').css('margin-left', -$ctrl.position().left + 10).addClass('active').show();
            setTimeout(function () {
                fbuilderRefreshControls($jq, $(this).parent().find('.fbuilder_color'))
            }, 10);
            $(this).parent().find('.fbuilder_color').trigger('focus');
        });
        $(document).on('mouseenter', '.fbuilder_color_wrapper', function () {
            $(this).find('.fbuilder_colorpicker').data('hover', true);
        });
        $(document).on('mouseleave', '.fbuilder_color_wrapper', function () {
            $(this).find('.fbuilder_colorpicker').data('hover', false);
        });

        $(document).on('mouseenter', '.fbuilder_colorpicker, .fbuilder_number_bar_wrapper', function () {
            $(this).closest('.fbuilder_control').find('.fbuilder_number_button').data('hover', true);
        });
        $(document).on('mouseleave', '.fbuilder_colorpicker, .fbuilder_number_bar_wrapper', function () {
            $(this).closest('.fbuilder_control').find('.fbuilder_number_button').data('hover', false);
        });

        $('body').click(function () {
            $('.fbuilder_colorpicker.active').each(function () {
                if (!$(this).data('hover')) {
                    $(this).removeClass('active').hide();
                    fbuilderRefreshControls($jq, $('false'));
                }
            });
            $('.fbuilder_number_button.active').each(function () {
                if (!$(this).data('hover')) {
                    $(this).removeClass('active').closest('.fbuilder_control').find('.fbuilder_number_bar_wrapper').hide();
                    fbuilderRefreshControls($jq, $('false'));
                }
            });
        });
        $jq('body').on('mouseup', function () {
            $('body').trigger('mouseup');
        });
        $jq('body').on('click', function () {
            $('body').trigger('click');
        });

        /* Shortcode number control */
        $(document).on('keyup', '.fbuilder_number_amount', function () {
            var $this = $(this);
            $this.closest('.fbuilder_control').find('.fbuilder_number_bar').slider('value', parseInt($this.val()));
        });
        $(document).on('click', '.fbuilder_number_button', function () {
            var $this = $(this);
            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
                var $ctrl = $this.closest('.fbuilder_control');
                $ctrl.find('.fbuilder_number_bar_wrapper').css('margin-left', -$ctrl.position().left + 10).show();
            }
            else {
                $(this).removeClass('active');
                $this.closest('.fbuilder_control').find('.fbuilder_number_bar_wrapper').hide();
            }
        });


        /* Shortcode change */

        $(document).on('fchange', '.fbuilder_shortcode_menu', function () {
            if (!$('.fbuilder_shortcode_menu:first').hasClass('fbuilder_rowedit_menu')) {
                var id = parseInt($(this).attr('data-modid'));
                var $module = $jq('.fbuilder_module[data-modid=' + id + ']:first');
                var f = fbuilder_items['items'][id]['f'];
                var holder = $module.find('.fbuilder_module_content:first');
                var options = fbuilder_items['items'][id]['options'];
                fbuilderGetShortcode(f, holder, options);
            }
            else {
                var id = parseInt($(this).attr('data-modid'));
                var $row = $jq('.fbuilder_row[data-rowid=' + id + ']:first');
                var options = fbuilder_items['rows'][id]['options'];
                fbuilderRowChange($row, options);
            }
        });

        $(document).on('click', '.ui-draggable', function (e) {
            e.preventDefault();
        });


        $(iDocument).on('mousedown', '.fbuilder_drag_handle, .fbuilder_drag', function (e) {
            $(iDocument).data('FRBdragHandleSwitch', true);
            if ($('#fbuilder_body_frame').contents().find('body:first > .frb_drag_placeholder_element').length > 0) {
                $jq('.frb_drag_placeholder_element:first').css('display', 'block');
            } else {
                $jq('body:first').append('<div class="frb_drag_placeholder_element"></div>');
            }
            if ($(this).hasClass('fbuilder_drag_handle')) {
                $jq('.frb_drag_placeholder_element:first').html('"Row ID = ' + $(this).closest('.fbuilder_row').attr('data-rowid') + '"');
            } else {
                $jq('.frb_drag_placeholder_element:first').html('"' + $(this).closest('.fbuilder_module').attr('data-shortcode') + '"');
            }
            var xy = FRBpointerEventToXY(e);
            $jq('.frb_drag_placeholder_element:first').css({'left': xy.x, 'top': xy.y});
        });
        $(iDocument).on('mouseup', function (e) {
            $(iDocument).data('FRBdragHandleSwitch', false);
            $jq('.frb_drag_placeholder_element:first').css('display', 'none');
        });

        $(iDocument).on('mousemove', function (e) {
            if ($(iDocument).data('FRBdragHandleSwitch') === true) {
                var xy = FRBpointerEventToXY(e);
                $jq('.frb_drag_placeholder_element:first').css({'left': xy.x, 'top': xy.y - 20});
            }
        });


    }


    var FRBpointerEventToXY = function (e) {
        var out = {x: 0, y: 0};
        if (e.type == 'touchstart' || e.type == 'touchmove' || e.type == 'touchend' || e.type == 'touchcancel') {
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            out.x = touch.pageX;
            out.y = touch.pageY;
        } else if (e.type == 'mousedown' || e.type == 'mouseup' || e.type == 'mousemove' || e.type == 'mouseover' || e.type == 'mouseout' || e.type == 'mouseenter' || e.type == 'mouseleave') {
            out.x = e.pageX;
            out.y = e.pageY;
        } else if (e.type == 'MSPointerDown' || e.type == 'MSPointerMove' || e.type == 'MSPointerUp') {
            var touch = e.originalEvent;
            out.x = touch.pageX;
            out.y = touch.pageY;
        }
        return out;
    };

})(jQuery);