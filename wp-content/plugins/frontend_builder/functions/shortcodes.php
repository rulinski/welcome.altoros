<?php

/* ------------------ */
/* fbuilder_gallery */
/* ------------------ */


function fbuilder_gallery($atts){
	extract(shortcode_atts(array(
		'bot_margin' => 24,
		'shortcode_id' => '',
		'class' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => '',
		'animation_speed' => 1000,
		'column_number' => '3',
		'item_padding' => '10',
		'image_size' => 'full',
		'aspect_ratio' => '16:9',
		'enable_categories' => 'false',
		'media_files' => '',
		'on_image_click' => 'none',
		'bckg_color' => 'transparent',
		'bckg_hover_color' => 'transparent',
		'text_color' => '#232323',
		'text_hover_color' => '#232323',
		'border_color' => 'transparent',
		'border_hover_color' => 'transparent',
		'border_thickness' => '',
		'show_all_category' => 'false',
		'category_name' => 'Images',
		'active' => 'false',
		'category_media_files' => '',
		'hover_icon' => 'ba-search',
		'hover_icon_size' => '30px',
		'initial_shade_color' => '#000000',
		'initial_shade_opacity' => '0',
		'hover_shade_color' => '#000000',
		'hover_shade_opacity' => '0.3',
		'hover_content' => 'icon',
		'hover_title_color' => '#ffffff',
		'merge_galleries' => 'false'
	), $atts));

	global $fbuilder;
	
	$bot_margin = (int)$bot_margin.'px';
	$randomId = $shortcode_id == '' ? 'frb_gallery_'.rand() : $shortcode_id;	
	
	$hover_icon_size = (int)$hover_icon_size.'px';
	$hover_shade_opacity = (float)$hover_shade_opacity;
	$hover_shade_opacity = $hover_shade_opacity > 1 ? 1 : $hover_shade_opacity;
	$hover_shade_opacity = $hover_shade_opacity < 0 ? 0 : $hover_shade_opacity;
	$merge_galleries = $merge_galleries === 'true' ? true : false;
	$column_number = (int)$column_number;
	$column_width = 100/$column_number;
	$item_padding = (int)$item_padding.'px';
	$border_thickness = (int)$border_thickness.'px';
	$on_image_click = in_array($on_image_click, array('none', 'pretty_photo', 'new_tab'), true) ? $on_image_click : 'none';
	$hover_content = in_array($hover_content, array('icon', 'title'), true) ? $hover_content : 'icon';
	$media_files = explode(',',$media_files);
	$category_name = explode('|',$category_name);
	$active = explode('|',$active);
	$category_media_files = explode('|',$category_media_files);
	$category_array=array();

	if($bckg_color == ''){$bckg_color = 'transparent';}
	if($bckg_hover_color == ''){$bckg_hover_color = 'transparent';}
	if($text_color == ''){$text_color = 'transparent';}
	if($text_hover_color == ''){$text_hover_color = 'transparent';}
	if($border_color == ''){$border_color = 'transparent';}
	if($border_hover_color == ''){$border_hover_color = 'transparent';}
	
	foreach ($category_media_files as $cat_array) {
		array_push($category_array,explode(',',$cat_array));
	}
	
	
	$all_category_media_files = '';
	
	foreach($category_media_files as $single_cat_media) {
		$all_category_media_files.=$single_cat_media.',';
	}
	
	$all_category_media_files = explode(',',$all_category_media_files);
	array_pop($all_category_media_files);
	$all_category_media_files=array_unique($all_category_media_files);
	
	$html = '<style type="text/css">
		#'.$randomId.' .frb_gallery_inner {
				margin-right:-'.$item_padding.';
			}
		
		
		#'.$randomId.' .frb_gallery_inner .frb_media_file {
			width:'.$column_width.'%;
			padding-right:'.$item_padding.';
			padding-top:'.$item_padding.';
		}

		#'.$randomId.' .frb_gallery_categories .frb_gallery_cat {
			background-color:'.$bckg_color.';
			color:'.$text_color.';
			border:'.$border_thickness.' solid '.$border_color.';
		}

		#'.$randomId.' .frb_gallery_categories .frb_gallery_cat:hover, #'.$randomId.' .frb_gallery_categories .frb_gallery_cat.frb_cat_active {
			background-color:'.$bckg_hover_color.';
			color:'.$text_hover_color.';
			border:'.$border_thickness.' solid '.$border_hover_color.';
		}

		#'.$randomId.' .frb_gallery_hover .frb_gallery_image_title {
			color: '.$hover_title_color.';
		}

		#'.$randomId.' .frb_gallery_hover {
			background-color: rgba('.$fbuilder ->hex2rgb($initial_shade_color).', '.$initial_shade_opacity.');
		}
		#'.$randomId.' .frb_gallery_hover:hover {
			background-color: rgba('.$fbuilder ->hex2rgb($hover_shade_color).', '.$hover_shade_opacity.');
		}
	
		
		
	</style>';
	

	$iconInsert = '';
	if(substr($hover_icon,0,4) == 'icon') {
		$iconInsert .= '<i class="fawesome '.$hover_icon.'" style="line-height:'.$hover_icon_size.'; font-size:'.$hover_icon_size.'; height:'.$hover_icon_size.'; width:'.$hover_icon_size.'; margin:'.(-$hover_icon_size/2).' "></i>';
	}
	else {
		$iconInsert .= '<i class="frb_icon '.substr($hover_icon,0,2).' '.$hover_icon.'" style="line-height:'.$hover_icon_size.'; font-size:'.$hover_icon_size.'; height:'.$hover_icon_size.'; width:'.$hover_icon_size.'; margin-top:'.(-$hover_icon_size/2).'px; margin-left:'.(-$hover_icon_size/2).'px "></i>';
	}


	
	 $html .= '<div class="frb_gallery_container" data-frb_aspect_ratio="'.$aspect_ratio.'" data-frb_media_column="'.$column_number.'">';
	 
	 if ($enable_categories=='true') {
	 	$html.='<div class="frb_gallery_categories">';
	 	
	 	
	 		if ($show_all_category=='true')
	 			$html.='<a class="frb_gallery_cat frb_cat_active" href="All">'.__('All','frontend-builder').'</a>';
	 		
	 		$media_files = $all_category_media_files;
	 		
			for ($i=0; $i<count($category_name); $i++) {
				$html.='<a class="frb_gallery_cat'.($active[$i]=='true' ? ' frb_cat_active' : '').'" href="'.$category_name[$i].'">'.$category_name[$i].'</a>';
			}
		
	 	
	 	$html.='</div><div style="clear:both;"></div>';
	 	
	 	}
	 	
	 	
	 	$html .= '<div class="frb_gallery_outer">
	 		<div class="frb_gallery_inner">';
	 		
	 		for($i=0; $i<count($media_files); $i++) { 
	 		
	 		if ($enable_categories=='true') {
	 			
	 			$html.='<div class="frb_media_file';
	 			
	 			for ($j=0;$j<count($category_array);$j++) {
	 				if (in_array($media_files[$i],$category_array[$j]))
						$html.=' frb_gallery_cat_'.$j;
				}
	 			
	 			$html.='">';
	 			}
	 			else {
					$html.='<div class="frb_media_file '.(($i<$column_number) ? 'frb_gallery_no_top_padding' : '').'">';
				}
	 		
	 				$html.='<div class="frb_media_file_inner">';
	 		
	 		if (wp_attachment_is_image($media_files[$i])) {
	 			
	 		$image = wp_get_attachment_image_src($media_files[$i],'full');
	 		

	 		$imageThumb = wp_get_attachment_image_src($media_files[$i],$image_size);
	 						
	 						$html.=($on_image_click=='new_tab' ? '<a class="frb_gallery_new_tab_link" href="'.$image[0].'" target="_blank">' : ($on_image_click=='pretty_photo' ? '<a href="'.$image[0].'" rel="frbprettyphoto'.($merge_galleries ? '[gallery]' : '').'">' : '')).'
	 					<img src="'.$imageThumb[0].'" width="'.$imageThumb[1].'" height="'.$imageThumb[2].'"></img>
	 					'.($on_image_click=='none' ? '' : '</a>');
	 					
			} else {
				
				$media_file = wp_get_attachment_url( $media_files[$i] );
				$media_file_type = get_post_mime_type( $media_files[$i] );
				$media_file_types = array('audio/mpeg','audio/wav','audio/wma','audio/x-matroska','audio/midi','audio/ogg','audio/x-realaudio');
				
				$html.=($on_image_click=='new_tab' ? '<a class="frb_gallery_new_tab_link" href="'.$media_file.'" target="_blank">' : ($on_image_click=='pretty_photo' ? '<a href="'.$media_file.'?iframe=true" rel="frbprettyphoto'.($merge_galleries ? '[gallery]' : '').'">' : '')).'
	 					<video'.($on_image_click=='None' ? ' controls ' : ' ');
	 					
	 					
	 					if (in_array($media_file_type,$media_file_types))
	 				 $html.=' poster="'.plugins_url( '\frontend_builder\images\audio.jpg' ).'" ';
	 				 
	 			$html.=		'>
  							<source src="'.$media_file.'">
 						 Your browser does not support the video tag.
						</video> 
	 					'.($on_image_click=='none' ? '' : '</a>');
			}
	 
	 			
	 			$html.= $on_image_click !== 'none' ? '<div class="frb_gallery_hover">'.($hover_content === 'icon' ? $iconInsert : '<span class="frb_centering_system"><span><span><span class="frb_gallery_image_title">'.(get_post((int)$media_files[$i]) -> post_title).'</span></span></span></span>').'</div>' : '';
	 			$html.= '</div>
	 			</div>';
	 			
	 		}
	 			
	 	 $html .=	'<div class="frb_clear"></div></div></div></div>';
	 
	
	
	
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
	
		$animSpeedSet = '<style>'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
	
	
	
	
	$html = $animSpeedSet.'
		<div id="'.$randomId.'" style="padding-bottom:'.$bot_margin.';" class="'.$class.$animate.'>'.$html.'</div>';

	return $html;
}
add_shortcode('fbuilder_gallery', 'fbuilder_gallery');





/* ------------------ */
/* fbuilder_audio */
/* ------------------ */


function fbuilder_audio($atts){
	extract(shortcode_atts(array(
		'bot_margin' => 24,
		'shortcode_id' => '',
		'class' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => '',
		'animation_speed' => 1000,
		'content_mp3' => 'http://media.w3.org/2010/07/bunny/04-Death_Becomes_Fur.mp4',
		'content_ogg' => 'http://media.w3.org/2010/07/bunny/04-Death_Becomes_Fur.oga',
		'background_color' => '#464646',
		'bar_color' => '#21CDEC',
		'icon_type' => 'default',
		'start_at' => '0',
		'autoplay' => 'false',
		'loop' => 'false',
		'mute' => 'false',
		'hide_controls'=> 'false'
		
	), $atts));
	
	$bot_margin = (int)$bot_margin.'px';
	$randomId =$shortcode_id == '' ? 'frb_audio_'.rand() : $shortcode_id;	
	
	
	$html='<style type="text/css" scoped="scoped">
		#'.$randomId.' .frb_audio_player {
			background-color:'.$background_color.';
		}
		
		#'.$randomId.' .frb_audio_player .ui-slider-range {
			background-color:'.$bar_color.';
			border:1px solid '.$bar_color.';
		}
		
	</style>';	
	
	$html .=  '<div class="frb_audio_player '.($icon_type=='default' ? '' : 'frb_audio_'.$icon_type ).'" data-frb_options="'.$autoplay.'|'.$loop.'|'.$mute.'|'.$hide_controls.'|'.$content_mp3.'|'.$content_ogg.'|'.$start_at.'">
 
            <a class="frb_play_button frb_image_button" href="" title=""></a>
            <a class="frb_stop_button frb_image_button" href="" title=""></a>
         	<div class="frb_current_time">00:00</div>
                <div class="frb_time_slider"></div>
     	<div class="frb_full_time"></div>
            
            <a class="frb_mute_button frb_image_button" href="" title=""></a> 
            <div class="frb_volume_slider"></div>
            <div class="frb_clear"></div>
            <audio>
 				 <source src="'.$content_ogg.'" type="audio/ogg">
  				<source src="'.$content_mp3.'" type="audio/mpeg">
  				Your browser does not support the audio element.
			</audio>
        </div><!-- / player -->';
	
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
	
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
	
	
	
	
	$html = $animSpeedSet.'
		<div id="'.$randomId.'" style="padding-bottom:'.$bot_margin.';" class="'.$class.$animate.'>'.$html.'</div>';

	return $html;
}
add_shortcode('fbuilder_audio', 'fbuilder_audio');


/* ------------------ */
/* fbuilder_percentage_bars */
/* ------------------ */

function fbuilder_percentage_bars($atts){
	extract(shortcode_atts(array(
		'bot_margin' => 24,
		'shortcode_id' => '',
		'class' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => '',
		'animation_speed' => 1000,
		'bar_style' => 'sharp',
		'element_spacing' => '20px',
		'percent_pin' => 'true',
		'headline_inside' => 'false',
		'headline_top_margin' => '0px',
		'headline_color' => '#232323',
		'line_background' => '#eeeeee',
		'custom_height' => '5px',
		'direction' => 'ltr',
		'headline_content' => __('Percentage Bar','frontend-builder'),
		'line_color' => '#000',
		'pin_color' => '#222',
		'pin_text_color' => '#fff',
		'percentage' => 100,
		'an_speed' => 400,
		'pattern_url' => ' '
	), $atts));
	
	$bot_margin = (int)$bot_margin.'px';
	$randomId = $shortcode_id == '' ? 'frb_percentage_bar'.rand() : $shortcode_id;
	$element_spacing = (int)$element_spacing.'px';
	$bar_style = $bar_style == 'round' ? ' frb_pbar_round' : '';
	$percent_pin = $percent_pin == 'false' ? false : true;
	$headline_inside = $headline_inside == 'false' ? false : true;
	$headline_top_margin = (int)$headline_top_margin.'px';
	$custom_height = (int)$custom_height.'px';
	
	
	$directionExp = explode('|', $direction);
	$headline_contentExp = explode('|', $headline_content);
	$line_colorExp = explode('|', $line_color);
	$pin_colorExp = explode('|', $pin_color);
	$pin_text_colorExp = explode('|', $pin_text_color);
	$pattern_urlExp = explode('|', $pattern_url);
	
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';

	$style = '<style type="text/css" scoped="scoped">
		#'.$randomId.'.frb_percentage_bar h5 {color:'.$headline_color.'; '.($headline_inside ? 'position:absolute; top:0; display:block; white-space: nowrap; margin-top:'.$headline_top_margin.';' : '').'}
		#'.$randomId.'.frb_percentage_bar .frb_pbar_line_wrapper {background-color:'.$line_background.'; margin-bottom:'.$element_spacing.'; height: '.$custom_height.';'.($headline_inside && $percent_pin ? 'margin-top:32px;' : '').'}
		#'.$randomId.'.frb_percentage_bar .frb_pbar_line {height: '.$custom_height.';}
		#'.$randomId.'.frb_percentage_bar .frb_pbar_pin {bottom: '.((int)$custom_height+13).'px;}';
		
	for($i = 0; $i<count($directionExp); $i++){
		$pattern_urlExp[$i] = ($pattern_urlExp[$i] === ' ' ? 'none' : 'url('.$pattern_urlExp[$i].')');
		$style .=
			'#'.$randomId.'.frb_percentage_bar .frb_pbar_single_bar_wrapper'.$i.' h5 {text-align:'.(($directionExp[$i] == 'rtl' ? ' rtl' : '') == ' rtl' ? 'right' : 'left').'; '.(($directionExp[$i] == 'rtl' ? ' rtl' : '') == ' rtl' ? 'right:5px;' : 'left:5px;').'}
			#'.$randomId.'.frb_percentage_bar .frb_pbar_single_bar_wrapper'.$i.' .frb_pbar_line {background-color:'.$line_colorExp[$i].';'.($pattern_urlExp[$i] == 'none' ? '' : 'background-image:'.$pattern_urlExp[$i].';').'}
			#'.$randomId.'.frb_percentage_bar .frb_pbar_single_bar_wrapper'.$i.' .frb_pbar_pin {background-color:'.$pin_colorExp[$i].'; color:'.$pin_text_colorExp[$i].';}
			#'.$randomId.'.frb_percentage_bar .frb_pbar_single_bar_wrapper'.$i.' .frb_pbar_pin:after {border-top-color:'.$pin_colorExp[$i].';}';
	}
		
		
	$style .= '</style>';

	$html = $animSpeedSet.$style.'<div class="frb_percentage_bar frb_animated'.($percent_pin ? '' : ' no-pin').$bar_style.$class.$animate.' id="'.$randomId.'" data-percentage="'.$percentage.'" data-aspd="'.$an_speed.'" data-dir="'.$direction.'" style="padding-bottom :'.$bot_margin.';">';
	
	for($i = 0; $i<count($directionExp); $i++){
		$H_tag = '<h5>'.$headline_contentExp[$i].'</h5>';
		$html .='<div class="frb_pbar_single_bar_wrapper frb_pbar_single_bar_wrapper'.$i.'">
				'.($headline_inside ? '' : $H_tag).'
				<div class="frb_pbar_line_wrapper'.($directionExp[$i] == 'rtl' ? ' rtl' : '').'">
					<div class="frb_pbar_line">'.($headline_inside ? $H_tag : '').'</div>
					<div class="frb_pbar_pin"><span>0%</span></div>
				</div>
			</div>';	
	}		
			
	$html .=	'</div>';
	
	
	return $html;
}
add_shortcode('fbuilder_percentage_bars', 'fbuilder_percentage_bars');

/* ------------------ */
/* fbuilder_creative_post_slider */
/* ------------------ */

function fbuilder_creative_post_slider($atts){
	extract(shortcode_atts(array(
		'bot_margin' => 24,
		'shortcode_id' => '',
		'class' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => '',
		'animation_speed' => 1000,
		'slides_per_view' => '3',
		'hover_background_color' => '#eee',
		'hover_text_color' =>'#222',
		'hover_icon' => 'ba-search',
		'hover_icon_size' => '30px',
		'title_size' => '20px',
		'title_line_height' => '24px',
		'excerpt_size' => '14px',
		'excerpt_line_height' => '18px',
		'excerpt_lenght' => 30,
		'categories' => '1',
		'category_order' => 'desc',
		'number_of_posts' => 5,
		'cat_line_height' => '18px',
		'cat_size' => '14px',
		'category_show' => 'true',
		'excerpt_show' => 'true',
		'image_size' => 'full',
		'aspect_ratio' => '16:9',
		'enable_custom_height' => 'false',
		'custom_slider_height' => '300px',
		'resize_reference' => '200px',
		'link_type' => 'prettyphoto',
		'open_link_in' => 'default',
		'order_by' => 'ID',
		'enable_icon' => 'true'
	), $atts));
	
	$bot_margin = (int)$bot_margin.'px';
	$title_size = (int)$title_size.'px';
	$title_line_height = (int)$title_line_height.'px';
	$cat_size = (int)$cat_size.'px';
	$cat_line_height = (int)$cat_line_height.'px';
	$excerpt_size = (int)$excerpt_size.'px';
	$excerpt_line_height = (int)$excerpt_line_height.'px';
	$excerpt_lenght = (int)$excerpt_lenght;
	$randomId = $shortcode_id == '' ? 'frb_creative_post_slider_'.rand() : $shortcode_id;
	$slides_per_view = (int)$slides_per_view;
	$number_of_posts = (int)$number_of_posts;
	$category_show = $category_show == 'true' ? true : false;
	$enable_custom_height = $enable_custom_height == 'true' ? true : false;
	$excerpt_show = $excerpt_show == 'true' ? true : false;
	$image_size = in_array($image_size, array('full', 'large', 'medium', 'creative_post_slider_medium', 'thumbnail'), true) ? $image_size : 'full';
	$aspect_ratio = in_array($aspect_ratio, array('1:1', '4:3', '16:9', '16:10', '1:2'), true) ? $aspect_ratio : '16:9';
	$custom_slider_height = (int)$custom_slider_height.'px';
	$resize_reference = (int)$resize_reference;
	$hover_icon_size = (int)$hover_icon_size.'px';
	$link_type = $link_type != 'prettyphoto' ? 'post' : 'prettyphoto';
	$open_link_in != 'tab' ? 'default' : 'tab';
	$enable_icon = $enable_icon != 'true' ? false : true;
	
	$html = '';
	$current_excerpt ='';
	$args = array('post_type'=>'post', 'post_status'=>'publish','order' => $category_order, 'orderby'=> $order_by, 'category__in'=> explode(',', $categories), 'posts_per_page'=> $number_of_posts);
	$post_query = new WP_Query($args);
	
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
		if((int)$animation_speed != 0) {
			$animate .= ' data-aspeed="'.$animation_speed.'"';
		}
	}
	else
		$animate = '"';

	$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';

	
	$style = '<style type="text/css" scoped="scoped">'.
		($enable_custom_height ? ('#'.$randomId.'.frb_creative_post_slider_container {height:'.$custom_slider_height.' !important;}') : '').'
		#'.$randomId.' .frb_creative_post_slider_hover {background: '.$hover_background_color.'; color:'.$hover_text_color.'; }
		#'.$randomId.' .frb_creative_post_slider_hover > h3 {font-size: '.$title_size.'; line-height: '.$title_line_height.'; margin: 0;}
		#'.$randomId.' .frb_creative_post_slider_hover > div {font-size: '.$excerpt_size.'; line-height: '.$excerpt_line_height.'; display:'.($excerpt_show ? 'block' : 'none').';}
		#'.$randomId.' .frb_creative_post_slider_hover > span {width:100%; display:'.($category_show ? 'block' : 'none').';}
		#'.$randomId.' .frb_creative_post_slider_hover > span > a {display:inline-block; font-size:'.$cat_size.'; line-height:'.$cat_line_height.';}
		
		</style>';

	$html .=  $animSpeedSet.$style.'<div id="'.$randomId.'" class="frb_creative_post_slider_container'.$class.$animate.' data-spv="'.$slides_per_view.'" data-asr="'.$aspect_ratio.'" data-rref="'.$resize_reference.'" style="padding-bottom:'.$bot_margin.'" data-icnh="'.$hover_icon_size.'"><div class="frb_creative_post_slider_wrapper">';
	
	if($post_query -> have_posts()) {
		while ($post_query->have_posts()) {
			$post_query->the_post();
			$current_excerpt = get_the_excerpt();
			$current_excerpt = mb_strlen($current_excerpt) >= $excerpt_lenght ? mb_substr($current_excerpt , 0, $excerpt_lenght - mb_strlen($current_excerpt)).'...' : $current_excerpt;
			$cat_storage = array();
			$cat_storage = get_the_category();
			$cat_to_print = '';
			for ($i =0; $i < count($cat_storage); $i++) {
				$cat_to_print .='<a href="'.get_category_link(get_cat_ID($cat_storage[$i] -> name)).'">'.($cat_storage[$i] -> name).'</a>, ';
			}
			$cat_to_print = substr($cat_to_print, 0,-2);
			
			$html .= '<div class="frb_creative_post_slide"> 
					<div class="frb_creative_post_slide_inner">
						<a href="'.($link_type == 'prettyphoto' ? (wp_get_attachment_url(get_post_thumbnail_id()) != '' ? wp_get_attachment_url(get_post_thumbnail_id()) : 'javascript:void(0);') : get_permalink()).'" '.($link_type == 'prettyphoto' ? (get_the_post_thumbnail(get_the_id(), $image_size) != '' ? 'rel="frbprettyphoto" ' : 'style="cursor:default;"') : ($open_link_in == 'tab' ? 'target="_blank" ' : '')).' class="frb_creative_post_slider_img_wrapper">
						
							'.(get_the_post_thumbnail(get_the_id(), $image_size) != '' ? get_the_post_thumbnail(get_the_id(), $image_size) : '<span class="frb_cps_no_image">No Image</span>').'
						</a>
						'.($enable_icon ? (get_the_post_thumbnail(get_the_id(), $image_size) != '' ? '<div class="frb_creative_link_icon"><i class="frb_icon '.substr($hover_icon,0,2).' '.$hover_icon.'" style="font-size:'.$hover_icon_size.'; line-height:'.$hover_icon_size.'; color:'.$hover_background_color.';"></i></div>' : '') : '').'
						<div class="frb_creative_post_slider_hover">
							<span>'.$cat_to_print.'</span>
							<h3><a href="'.get_permalink().'" '.($open_link_in == 'tab' ? 'target="_blank" ' : '').'>'.get_the_title().'</a></h3>
							<div>'.$current_excerpt.'</div>
						</div>
					</div>
				</div>';
		}
	}
	$html .= '</div></div>';
	 wp_reset_postdata();
	 
	
	return $html;
}
add_shortcode('fbuilder_creative_post_slider', 'fbuilder_creative_post_slider');
add_image_size('creative_post_slider_medium', 768, 9999);

/* ------------------ */
/* fbuilder_contact_form */
/* ------------------ */

function fbuilder_contact_form($atts) {
	extract(shortcode_atts(array(
		'bot_margin' => 24,
		'shortcode_id' => '',
		'class' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => '',
		'animation_speed' => 1000,
		'text_color' => '#333333',
		'background_color' => 'transparent',
		'border_color' => '#e7e7e7',
		'active_text_color' => '#cccccc',
		'active_background_color' => 'transparent',
		'active_border_color' => '#cccccc',
		'custom_field' => 'false',
		'custom_ph' => __('Custom','frontend-builder'),
		'email_ph' => __('E-mail Address','frontend-builder'),
		'name_ph' => __('Name','frontend-builder'),
		'website_ph' => __('Website','frontend-builder'),
		'textarea_ph' => __('Message goes here','frontend-builder'),
		'submit_ph' => __('Submit','frontend-builder'),
		'button_text_color' => '#ffffff',
		'button_background_color' => '#555555',
		'button_border_color' => '#555555',
		'active_button_text_color' => '#ffffff',
		'active_button_background_color' => '#222222',
		'active_button_border_color' => '#222222',
		'button_align' => 'left',
		'required' => 'name,email,textarea',
		'response_message' =>  __('Message Successfully Sent!','frontend-builder'),
		'button_fullwidth' => 'false',
		'recipient_email' => 'testemail@somewhere.com',
		'recipient_name' => 'Recipient',
		'send_response_delay' => 1500,
		'datepicker' => 'false'
	), $atts));
	$bot_margin = (int)$bot_margin.'px';
	$randomId =$shortcode_id == '' ? 'frb_contact_form_'.rand() : $shortcode_id;
	$custom_field = ($custom_field == 'false' || $custom_field == false) ? false : true;
	$button_fullwidth = ($button_fullwidth == 'false' || $button_fullwidth == false) ? false : true;
	$custom_ph = $custom_ph == '' ? 'Custom' : $custom_ph;
	$recipient_email = $recipient_email =='' ? 'testemail@somewhere.com' : $recipient_email;
	$recipient_name = $recipient_name =='' ? 'Recipient' : $recipient_name;
	$send_response_delay = (int)$send_response_delay;
	$datepicker = $datepicker == 'false' ? false : true;

	$submit_proc = __('Sending...','frontend-builder');
	
	
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
	$required_exp = explode(',', $required);
		
	$resp = array('name' => '', 'email' => '', 'website' => '', 'custom' => '', 'textarea' => '');

	foreach($required_exp as $val) {
		$resp[$val] = 'class="frb_required"';
	}	
	
	$custom_field_html = $custom_field ? '<div class="frb_input_wrapper"><input class="'.$resp['custom'].($datepicker ? ' frb_contact_datepicker' : '').'" type="text" name="custom" value="" placeholder="'.$custom_ph.'" />'.($datepicker ? '<i class="ba ba-calendar frb_icon"></i>' : '').'</div>' : '';
	$style = '<style type="text/css" scoped="scoped">
		#'.$randomId.'.frb_contact_form .frb_input_wrapper input[type="text"], #'.$randomId.'.frb_contact_form .frb_textarea_wrapper textarea {color:'.$text_color.'; background:'.$background_color.'; border: 3px solid '.$border_color.'; transition: background-color 300ms, border-color 300ms;}
		#'.$randomId.'.frb_contact_form .frb_input_wrapper input[type="text"]:focus, .frb_contact_form .frb_textarea_wrapper textarea:focus {color:'.$active_text_color.'; background:'.$active_background_color.'; border: 3px solid '.$active_border_color.';}
		#'.$randomId.'.frb_contact_form .frb_contact_submit input[type="submit"] {color:'.$button_text_color.'; background:'.$button_background_color.'; border: 3px solid '.$button_border_color.'; text-align:center; display:'.($button_fullwidth ? 'block; width:100%;' : 'inline-block').'; transition: background-color 300ms, border-color 300ms;} 
		#'.$randomId.'.frb_contact_form .frb_contact_submit input[type="submit"]:hover {color:'.$active_button_text_color.'; background:'.$active_button_background_color.'; border: 3px solid '.$active_button_border_color.';} 
		#'.$randomId.'.frb_contact_form .frb_contact_submit {text-align:'.$button_align.';}
		#'.$randomId.'.frb_contact_form {padding-bottom:'.$bot_margin.';}
		#'.$randomId.'.frb_contact_form .frb_contact_form_overlay > div > div > div > div {color:'.$text_color.';}
		#'.$randomId.'.frb_contact_form .frb_input_wrapper .frb_req_error, .frb_contact_form .frb_textarea_wrapper .frb_req_error {border-color:'.$active_border_color.' !important; }
		#'.$randomId.'.frb_contact_form .frb_input_wrapper i {color:'.$text_color.';}
	</style>';
	
	$jscript = '<script>
		(function($){
			$(document).ready(function(){
				$("#'.$randomId.'.frb_contact_form > form").data({"email" : "'.$recipient_email.'", "name" : "'.$recipient_name.'", "responseDelay" : '.$send_response_delay.'});
				'.($datepicker ? '$("#'.$randomId.'.frb_contact_form .frb_contact_datepicker").datepicker();' : '').' 
			});
		})(jQuery);
	</script>';
	
	$html = $animSpeedSet.$style.$jscript.'
		<div class="frb_contact_form'.$class.$animate.' id="'.$randomId.'">
			<form action="#" method="post">
				<div class="frb_input_row">
					<div class="frb_input_wrapper"><input '.$resp['email'].' type="text" name="e-mail" value="" placeholder="'.$email_ph.'" /></div>
					<div class="frb_input_wrapper"><input '.$resp['name'].' type="text" name="name" value="" placeholder="'.$name_ph.'" /></div>
					<div class="frb_input_wrapper"><input '.$resp['website'].' type="text" name="website" value="" placeholder="'.$website_ph.'" /></div>
					'.$custom_field_html.'
				</div>
				<div class="frb_textarea_wrapper"><textarea '.$resp['textarea'].' name="message" placeholder="'.$textarea_ph.'"></textarea></div>
				<div class="frb_contact_submit"><input type="submit" name="submit" data-proc-val="'.$submit_proc.'" value="'.$submit_ph.'" /></div>
				<div class="frb_contact_form_overlay"><div class="frb_centering_system"><div><div><div class="frb_contact_response" data-text="'.$response_message.'">'.$response_message.'</div></div></div></div></div>
			</form>
		</div>';

	return $html;
	
}
add_shortcode('fbuilder_contact_form', 'fbuilder_contact_form');

/* ------------------ */
/* fbuilder_graph */
/* ------------------ */

function fbuilder_graph($atts){
	extract(shortcode_atts(array(
		'bot_margin' => 24,
		'shortcode_id' => '',
		'class' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => '',
		'animation_speed' => 1000,
		'graph_style' => 'bar', //  line or bar
		'item_align' => 'left',  //  left, right, center
		'item_height' => 300,
		'item_width' => 1000,
		'labels' => 'January,February,March,April,May,June,July',
		'data_value' =>'15,20,25,30,50,60,80',
		'data_caption' => 'Lorem Ipsum',
		'data_color' => '#44bdd6',
		'fill' => 'true',
		'curve' => 'true',
		'bar_stroke' => 'true',
		'scale_font_color' => '#666666',
		'legend_font_color' => '#222222',
		'legend_font_size'=> 14,
		'legend_position' => 'bottom', // right, left, bottom
		'legend_shape' => 'circle' // square, round, circle
	), $atts));
	$bot_margin = (int)$bot_margin.'px';
	$randomId =$shortcode_id == '' ? 'frb_graph_'.rand() : $shortcode_id;
	$legend_font_size = (int)$legend_font_size.'px';
	$item_height = (int)$item_height;
	$item_width = (int)$item_width;
	
	if($graph_style !== 'bar' && $graph_style !== 'line') {$graph_style = 'line';}
	if($fill !== 'true' && $fill !== 'false') {$fill = 'true';}
	if($curve !== 'true' && $curve !== 'false') {$curve = 'true';}
	if($bar_stroke !== 'true' && $bar_stroke !== 'false') {$bar_stroke = 'true';}
	
	$dataValueExp = explode('|', $data_value);
	$dataColorExp = explode('|', $data_color);
	$dataCaptionExp = explode('|', $data_caption);
	
	global $fbuilder;
	$obj = '';
	
	$labelsReformat = explode(',', $labels);
	$labelsFormated = '';
	foreach($labelsReformat as $value) {
		$labelsFormated .= '"'.$value.'",';
	}
	$labelsFormated = substr($labelsFormated, 0, -1);
	
	
	for($i=0; $i<count($dataValueExp); $i++) {
		if (substr($dataColorExp[$i], 0, 1) == '#') {
			$rgbtemp = $fbuilder->hex2rgb($dataColorExp[$i]);
			$rgba = 'rgba('.$rgbtemp;
		} else {
			$rgbtemp = explode(',', $dataColorExp[$i]);
			$rgba = 'rgba'.substr($rgbtemp[0], 3).','.$rgbtemp[1].','.substr($rgbtemp[2],0, -1);
		}
		
		$obj .= '{fillColor:"'.$rgba.',0.5)",strokeColor:"'.$rgba.',1)",pointColor:"'.$rgba.',1)",pointStrokeColor: "#fff",data:['.$dataValueExp[$i].'],caption:"'.$dataCaptionExp[$i].'"},';
	}					
	$obj = substr($obj, 0, -1);
	
	$graphData ='{labels:['.$labelsFormated.'], datasets:['.$obj.']}';
	
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
	
	switch ($legend_shape) {
	    case 'square':
	       $legend_shape_val = '0px';
	        break;
	    case 'round':
	       $legend_shape_val = '5px';
	        break;
	    case 'circle':
	        $legend_shape_val = '50%';
	        break;
	}
		
	if($legend_position == 'left'){
		$firstFLoat = 'right'; $secFloat = 'left'; $secMargin = 'right'; $legFloat = 'right'; $padL = '10px';  $padR = ($legend_font_size+10).'px'; $legTextAlign = 'right'; $marL = '0px';
	} else if($legend_position == 'right'){
		$firstFLoat = 'left'; $secFloat = 'right'; $secMargin = 'left'; $legFloat = 'left'; $padL =($legend_font_size+10).'px';  $padR = '10px'; $legTextAlign = 'left'; $marL = '0px';
	} else if($legend_position == 'bottom'){
		$firstFLoat = 'none'; $secFloat = 'none'; $secMargin = 'top'; $legFloat = 'left'; $padL = ($legend_font_size+10).'px';  $padR = '10px'; $legTextAlign = 'left'; $marL = '30px';
	}
		
	$script = '<script>
		(function($){
			$(document).ready(function(){
				$(document).on("onscreen", "#'.$randomId.' .frb_graph_wrapper", function(){
					var '.$randomId.' = '.$graphData.';
					$("#'.$randomId.'").data({"graphData" : '.$graphData.', "legendFloat" : "'.$secFloat.'", graph_style : "'.$graph_style.'", fill : "'.$fill.'", curve : "'.$curve.'", barStroke : "'.$bar_stroke.'",scale_font_color : "'.$scale_font_color.'", itemWidth : "'.$item_width.'", legend_font_size:"'.$legend_font_size.'"});
					$("#'.$randomId.'").frbChartsLegendSetup('.$randomId.');
					$("#'.$randomId.'").frbGraphDraw();
				});	
			});
		})(jQuery);
	</script>';	
	
	
	$style = '<style type="text/css" scoped="scoped">
		#'.$randomId.' .frb_graph_wrapper canvas {float: '.$firstFLoat.';}
		#'.$randomId.' .frb_charts_legend {float: '.$secFloat.';margin-left:'.$marL.'; margin-'.$secMargin.':20px; color:'.$legend_font_color.';}
		#'.$randomId.' .frb_charts_legend_row {text-align:'.$legTextAlign.';}
		#'.$randomId.' .frb_charts_legend_row > div {float:'.$legFloat.'; border-radius:'.$legend_shape_val.'; height:'.$legend_font_size.'; width:'.$legend_font_size.';}
		#'.$randomId.' .frb_charts_legend_row > span {padding-left:'.$padL.'; padding-right:'.$padR.'; display:block; font-size:'.$legend_font_size.'; line-height:'.$legend_font_size.';}
		#'.$randomId.'.frb_chart_resp .frb_charts_legend_row > span {padding-left:'.($legend_font_size+10).'px !important;}
	</style>';
	$html = $animSpeedSet.$style.'
		<div id="'.$randomId.'" style="text-align:'.$item_align.';width:100%; padding-bottom:'.$bot_margin.';" class="'.$class.$animate.'>
			<div style="display:inline-block">
				<div class="frb_graph_wrapper frb_animated">
					<canvas class="frb_graph_canvas" height="'.$item_height.'" width="'.$item_width.'" data-rel="'.$randomId.'"></canvas>
				</div>
			</div>
		</div>'.$script;

	return $html;
}
add_shortcode('fbuilder_graph', 'fbuilder_graph');


/* ------------------ */
/* fbuilder_gauge */
/* ------------------ */

function fbuilder_gauge_chart($atts){
	extract(shortcode_atts(array(
		'bot_margin' => 24,
		'shortcode_id' => '',
		'class' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => '',
		'animation_speed' => 1000,
		'item_align' => 'center',
		'radius' => 200,
		'value' => 80,
		'min_value' => 0,
		'max_value' => 100,
		'unit' => '',	
		'show_min_max' => 'true',	
		'show_inner_shadow' => 'false',	
		'animation_length' => 700,	
		'value_color' => '#000000',	
		'unit_color' => '#000000',	
		'gauge_color' => '#000000',	
		'gauge_thickness' => 2,	
		'shadow_opacity' => 5,	
		'shadow_size' => 20,	
		'shadow_v_offset' => 5,	
		'gauge_gradient_1' => '#000000',	
		'gauge_gradient_2' => '#000000',	
		'gauge_gradient_3' => '#000000',	
		'gauge_gradient_4' => '#000000',	
		'gauge_gradient_5' => '#000000'	
	), $atts));
	
	$bot_margin = (int)$bot_margin.'px';
	$randomId =$shortcode_id == '' ? 'frb_gauge_'.rand() : $shortcode_id;	
	$radius = (int)$radius;
	$value = (int)$value;
	$min_value = (int)$min_value;
	$max_value = (int)$max_value;
	$animation_length = (int)$animation_length;
	$shadow_size = (int)$shadow_size;
	$shadow_v_offset = (int)$shadow_v_offset;
	$gauge_thickness = floatval($gauge_thickness)/10;
	$shadow_opacity = floatval($shadow_opacity)/10;
	$gauge_args = '{
          id: "'.$randomId.'_inner",
          value: 0, 
          min: '.$min_value.',
          max: '.$max_value.',
          title: " ",
          label: "'.$unit.'",
          valueFontColor: "'.$value_color.'",
          titleFontColor: "'.$value_color.'",
          labelFontColor: "'.$unit_color.'",
          gaugeColor: "'.$gauge_color.'",
          showMinMax: '.$show_min_max.',
          gaugeWidthScale: '.$gauge_thickness.',
          showInnerShadow: '.$show_inner_shadow.',
          shadowOpacity: '.$shadow_opacity.',
          shadowSize: '.$shadow_size.',
          shadowVerticalOffset: '.$shadow_v_offset.',
          levelColors: ["'.$gauge_gradient_1.'","'.$gauge_gradient_2.'","'.$gauge_gradient_3.'","'.$gauge_gradient_4.'","'.$gauge_gradient_5.'"],
          levelColorsGradient : true,
          refreshAnimationTime: '.$animation_length.',
          startAnimationTime:0
        }';
	 $html = '<div id="'.$randomId.'_inner" class="frb_gauge_shortcode"></div>
	 <script>
	      jQuery(\'#'.$randomId.'_inner\').data(\'gauge_init\','.$gauge_args.');
	      jQuery(\'#'.$randomId.'_inner\').data(\'gauge_value\','.$value.');
	      jQuery(\'#'.$randomId.'_inner\').data(\'gauge_width\','.$radius.');
	</script>';
	 
	
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
	
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}
	 					</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
	
	
	
	$style= '<style type="text/css" scoped="scoped">	
				#'.$randomId.' {text-align:'.$item_align.'; overlow:hidden;}
	 			#'.$randomId.'_inner {width:'.$radius.'px; height:'.(0.6*$radius).'px; display:inline-block;}
	 		</style>';

	$html = $animSpeedSet.$style.'
		<div id="'.$randomId.'" style="padding-bottom:'.$bot_margin.';" class="frb_gauge_chart frb_animated '.$class.$animate.'>'.$html.'</div>';

	return $html;
}
add_shortcode('fbuilder_gauge_chart', 'fbuilder_gauge_chart');

/* ------------------ */
/* fbuilder_piecharts */
/* ------------------ */

function fbuilder_piechart($atts){
	extract(shortcode_atts(array(
		'bot_margin' => 24,
		'shortcode_id' => '',
		'class' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => '',
		'animation_speed' => 1000,
		'color' => '#808080',
		'radius' => 220,
		'font_size'=> 16,
		'item_align' => 'left',
		'legend_position' => 'bottom',
		'inner_cut' => 0, 
		'stroke_width' => 15,
		'stroke_color' => "#ffffff",
		'data_value' => '15|14|13',
		'data_color' => '#6b58cd|#8677d4|#9c8ddc',
		'data_caption' => 'Lorem ipsum|Lorem ipsum|Lorem ipsum',
		'legend_shape' => 'square'
	), $atts));
	$bot_margin = (int)$bot_margin.'px';
	$inner_cut = (int)$inner_cut;
	$inner_cut = ($inner_cut>100) ? 100 : $inner_cut;
	$inner_cut = ($inner_cut<0) ? 0 : $inner_cut;
	$stroke_width = (int)$stroke_width;
	$stroke_width = ($stroke_width>10) ? 10 : $stroke_width;
	$stroke_width = ($stroke_width<0) ? 0 : $stroke_width;
	$font_size = (int)$font_size.'px';
	$stroke_color =( $stroke_color == "") ? '#ffffff' : $stroke_color;
	$randomId =$shortcode_id == '' ? 'frb_piechart_'.rand() : $shortcode_id;	
	
	$dataValueExp = explode('|', $data_value);
	$dataColorExp = explode('|', $data_color);
	$dataCaptionExp = explode('|', $data_caption);
	
	$obj = '{value:'.$dataValueExp[0].', color:"'.$dataColorExp[0].'", caption:"'.$dataCaptionExp[0].'"}';
	for($i=1; $i<count($dataValueExp);$i++) {
		$obj .=',{value:'.$dataValueExp[$i].', color:"'.$dataColorExp[$i].'", caption:"'.$dataCaptionExp[$i].'"}';
	}
	
	
	$strokeShow = ($stroke_width == 0) ? 'false' : 'true';
	
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
	
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
	
	$firstFLoat = 'left'; $secFloat = 'right'; $secMargin = 'left'; $legFloat = 'left'; $padL = '40px'; $padR = '10px'; $legTextAlign = 'left';
	if (!in_array($legend_position, array('left', 'right', 'bottom'), true)) {$legend_position = "right";}	
	if (!in_array($legend_shape, array('square', 'round', 'circle'), true)) {$legend_shape = "square";}	
	
	switch ($legend_shape) {
	    case 'square':
	       $legend_shape_val = '0px';
	        break;
	    case 'round':
	       $legend_shape_val = '5px';
	        break;
	    case 'circle':
	        $legend_shape_val = '50%';
	        break;
	}
		
	if($legend_position == 'left'){
		$firstFLoat = 'right'; $secFloat = 'left'; $secMargin = 'right'; $legFloat = 'right'; $padL = '10px';  $padR = ($font_size+10).'px'; $legTextAlign = 'right';
	} else if($legend_position == 'right'){
		$firstFLoat = 'left'; $secFloat = 'right'; $secMargin = 'left'; $legFloat = 'left'; $padL = ($font_size+10).'px';  $padR = '10px'; $legTextAlign = 'left';
	} else if($legend_position == 'bottom'){
		$firstFLoat = 'none'; $secFloat = 'none'; $secMargin = 'top'; $legFloat = 'left'; $padL = ($font_size+10).'px';  $padR = '10px'; $legTextAlign = 'left';
	}
		
	$script = '<script>
		(function($){
			$(document).ready(function(){
				$(document).on("onscreen", "#'.$randomId.' .frb_charts_wrapper", function(){
					var '.$randomId.' = ['.$obj.'];
					$("#'.$randomId.'").data({"obj" : ['.$obj.'], "font-size" : '.(int)$font_size.',"radius" : '.$radius.', "legendFloat" : "'.$secFloat.'", percentageInnerCutout : '.$inner_cut.', segmentShowStroke : '.$strokeShow.', segmentStrokeWidth : "'.$stroke_width.'", segmentStrokeColor : "'.$stroke_color.'",});
					$("#'.$randomId.'").frbChartsLegendSetup('.$randomId.');
					$("#'.$randomId.'").frbChartsDraw();	
				});	
			});
		})(jQuery);
	</script>';	
	
	$style = '<style type="text/css" scoped="scoped">
		#'.$randomId.' .frb_charts_wrapper canvas {float: '.$firstFLoat.';}
		#'.$randomId.' .frb_charts_legend {float: '.$secFloat.'; margin-'.$secMargin.':20px;}
		#'.$randomId.' .frb_charts_legend_row {text-align:'.$legTextAlign.';}
		#'.$randomId.' .frb_charts_legend_row > div {float:'.$legFloat.'; border-radius:'.$legend_shape_val.';  height:'.$font_size.'; width:'.$font_size.';}
		#'.$randomId.' .frb_charts_legend_row > span {padding-left:'.$padL.'; padding-right:'.$padR.'; font-size:'.$font_size.'; line-height:'.$font_size.';}
		#'.$randomId.'.frb_chart_resp .frb_charts_legend_row > span {padding-left:'.($font_size+10).'px !important;}
	</style>';
	$html = $animSpeedSet.$style.'
		<div id="'.$randomId.'" style="text-align:'.$item_align.';width:100%; padding-bottom:'.$bot_margin.';" class="'.$class.$animate.'>
			<div style="display:inline-block">
				<div class="frb_charts_wrapper frb_animated">
					<canvas class="frb_piechart_canvas" height="'.$radius.'" width="'.$radius.'" data-rel="'.$randomId.'"></canvas>
				</div>
			</div>
		</div>'.$script;

	return $html;
}
add_shortcode('fbuilder_piechart', 'fbuilder_piechart');


/* ------------------------- */
/* fbuilder_percentage_chart */
/* ------------------------- */

function fbuilder_percentage_chart($atts){
	extract(shortcode_atts(array(
		'bot_margin' => 24,
		'shortcode_id' => '',
		'color' => '#808080',
		'class' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => '',
		'animation_speed' => 1000,
		'percentage' => 73,
		'font_size' => 36,
		'line_color' => '#27a8e1',
		'background_line_color' => '#f2f2f2',
		'radius' => 200,
		'line_style' => 'square',
		'line_width' => 3,
		'item_align' => 'center',
		'percent_char' => 'true'
	), $atts));
	$bot_margin = (int)$bot_margin.'px';
	$font_size = (int)$font_size.'px';
	$radius = (int)$radius.'px';
	$line_width = (int)$line_width.'px';
	
	$randomId =$shortcode_id == '' ? 'frb_percentage_chart_'.rand() : $shortcode_id;
	if($percent_char == 'true'){$percent_char = true;}else {$percent_char = false;}
	if($percent_char != true && $percent_char != false){$percent_char = true;}
	if($percent_char){$percent_char_set = '0.7em';} else {$percent_char_set = '0';}
	
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	$style = '<style type="text/css" scoped="scoped">
		#'.$randomId.' .frb_perchart_percent {color:'.$color.';font-size:'.$font_size.';line-height:'.$font_size.';}
		#'.$randomId.' .frb_percentage_chart {width:'.$radius.'; height: '.$radius.';}
		#'.$randomId.' {text-align:'.$item_align.'; padding-bottom:'.$bot_margin.';}
		#'.$randomId.' .frb_perchart_percent > span {margin-top:-'.((int)$font_size/2).'px; color:'.$color.';}
		#'.$randomId.' .frb_perchart_percent > span:after { font-size: '.$percent_char_set.'; color:'.$color.';}
	</style>';
	
	$html = $animSpeedSet.$style.'<div id="'.$randomId.'">
					<span class="frb_percentage_chart frb_animated '.$class.$animate.' data-percent="'.$percentage.'" data-radius="'.(int)$radius.'" data-linewidth="'.$line_width.'" data-barcolor="'.$line_color.'" data-bgcolor="'.$background_line_color.'">
						<canvas class="frb_perchart_canvas" height="'.(int)$radius.'" width="'.(int)$radius.'" data-rel="'.$randomId.'"></canvas>
						<canvas class="frb_perchart_bg" height="'.(int)$radius.'" width="'.(int)$radius.'" data-rel="'.$randomId.'"></canvas>
						<span class="frb_perchart_percent">
							<span>'.$percentage.'</span>
						</span>
					</span>
				</div>';

	return $html;
}
add_shortcode('fbuilder_percentage_chart', 'fbuilder_percentage_chart');

/* ---------------- */
/* fbuilder_counter */
/* ---------------- */

function fbuilder_counter ($atts){
	extract(shortcode_atts(array(
		'bot_margin' => 24,
		'shortcode_id' => '',
		'color' => '#16a085',
		'class' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => '',
		'animation_speed' => 1000,
		'start_val' => 9999,
		'end_val' => 8847,
		'font_size' => 60,
		'direction' => 'auto',
		'item_align' => 'center'
	), $atts));
	$bot_margin = (int)$bot_margin.'px';
	$font_size = (int)$font_size.'px';
	$randomId =$shortcode_id == '' ? 'frb_counter_'.rand() : $shortcode_id;
	
	if($direction != 'upward' && $direction != 'downward') {$direction = 'auto';}
	
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	$style = '<style type="text/css" scoped="scoped">
		#'.$randomId.' {padding-bottom:'.$bot_margin.';}
		#'.$randomId.' .frb_scrolling_counter {color:'.$color.';font-size:'.$font_size.' !important; min-height: '.$font_size.';}
		#'.$randomId.' .frb_scrolling_counter  > .frb_scrl_count_digit_wrap > div {font-size:'.$font_size.' !important;}
	</style>';
	$html = $animSpeedSet.$style.'<div id="'.$randomId.'" class="'.$class.$animate.' style="width:100%; text-align:'.$item_align.';"><div class="frb_scrolling_counter frb_animated" data-startval="'.$start_val.'" data-result="'.$end_val.'" data-direction="'.$direction.'"></div></div>';
	
	
	return $html;
}
add_shortcode('fbuilder_counter', 'fbuilder_counter');

/* ------------- */
/* fbuilder_list */
/* ------------- */

function fbuilder_list ($atts, $content=null){
	extract(shortcode_atts(array(
		'icon' => 'ba-plus',
		'radius' => false,
		'color' => '#000000',
		'bot_margin' => 24,
		'shortcode_id' => '',
		'class' => '',
		'icon_color' => '#e95623',
		'background' => 'transparent',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => '',
		'animation_speed' => 1000
	), $atts));
	$bot_margin = (int)$bot_margin.'px';
	$randomId =$shortcode_id == '' ? 'frb_bullets_'.rand() : $shortcode_id;
	if($radius){$rad = '4px';}else {$rad = '0px';}
	if($background == ''){$background = 'transparent';}
	if($icon_color == ''){$icon_color = 'transparent';}
	if($color == ''){$color = 'transparent';}
	$style = '
	<style type="text/css" scoped="scoped">
		#'.$randomId.' {padding-bottom:'.$bot_margin.';}
		#'.$randomId.' li {color: '.$color.'; background-color: '.$background.'; border-radius: '.$rad.';}
		#'.$randomId.' li i {color: '.$icon_color.';}
	</style>
	';
	
	
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
	
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
	
	
	$html = $animSpeedSet.$style.'<ul class="frb_bullets_wrapper '.$class.$animate.' id="'.$randomId.'">';
	$cont = explode("\n", $content);
	foreach($cont as $textline) {
		if(substr($icon,0,4) == 'icon') {
			$html .= '<li><i class="fawesome '.$icon.'"></i>'.$textline.'</li>';
		}
		else {
			$html .= '<li><i class="frb_icon '.substr($icon,0,2).' '.$icon.'"></i>'.$textline.'</li>';
		}
	}
	$html .= '</ul>';
	return $html;
}
add_shortcode('fbuilder_list', 'fbuilder_list');


/* ------------------ */
/* fbuilder_separator */
/* ------------------ */

function fbuilder_separator ($atts, $content=null) {
	extract (shortcode_atts( array(
		'width' => 2,
		'style' => 'solid',
		'color' => '#27a8e1',
		'bot_margin' => 24,
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts));
	
	$randomId =$shortcode_id == '' ? 'frb_separator_'.rand() : $shortcode_id;
	$styleArray = array('solid', 'dashed', 'dotted', 'double');
	if(!in_array($style,$styleArray)) $style = 'solid';
	$width = (int)$width.'px';
	$bot_margin = (int)$bot_margin.'px';
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
	
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
	
	$html = $animSpeedSet.'<div id="'.$randomId.'" class="frb_separator '.$class.$animate.' style="border-top:'.$width.' '.$style.' '.$color.'; padding-bottom:'.$bot_margin.';"></div>';
	return $html;
}
add_shortcode( 'fbuilder_separator', 'fbuilder_separator' );



/* --------------- */
/* fbuilder_slider */
/* --------------- */



function fbuilder_slider ( $atts, $content=null ) {
		extract (shortcode_atts( array(
		'ctype' => 'image',
		'image' => '',
		'image_link' => '',
		'image_link_type' => '',
		'iframe_width' => '600',
		'iframe_height' => '300',
		'html' => '',
		'text_align' => '',
		'back_color' => '',
		'text_color' => '',
		'responsive_layout' => 'false',
		'min_slide_width' => '200px',
		'bot_margin' => 24,
		'mode' => 'horizontal',
		'pagination' => 'true',
		'navigation' => 'none',
		'navigation_color' => '#ffffff',
		'slides_per_view' =>  1,
		'auto_play' => 'true',
		'auto_delay' => 5,
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts ));
	$sliderId =($shortcode_id != '' ? $shortcode_id : 'frb_slider_'.rand()) ;
	$content = nl2br($content);
	$modeArray = array('horizontal', 'vertical');
	if(!in_array($mode,$modeArray)) $mode = 'horizontal';
	$ctype = explode('|', $ctype);//image or html
	$content = explode('|', $content);
	$image = explode('|', $image);
	$image_link = explode('|', $image_link);
	$image_link_type = explode('|', $image_link_type);
	$text_align = explode('|', $text_align);
	$back_color = explode('|', $back_color);
	$text_color = explode('|', $text_color);	
	$auto_delay = (int) $auto_delay;
	$slides_per_view = (int)$slides_per_view;
	$iframe_width = (int)$iframe_width;
	$iframe_height = (int)$iframe_height;
	$min_slide_width = (int)$min_slide_width;
	$responsive_layout = $responsive_layout == 'true' ? true : false;
	
	if($navigation != 'none') {
		$nav_class = ' frb-swiper-nav-'.$navigation;
	}
	else {
		$nav_class = ' ';
	}

	$html ='
<style type="text/css" scoped="scoped">
	'.($pagination != 'true' ? '#'.$sliderId.' .frb-swiper-pagination {display:none;}': '').'
	#'.$sliderId.' .frb-swiper-nav-squared .frb-swiper-nav-left:before,
	#'.$sliderId.' .frb-swiper-nav-squared .frb-swiper-nav-right:before {
		background: '.$navigation_color.';
	}
	#'.$sliderId.' .frb-swiper-nav-round .frb-swiper-nav-left:before,
	#'.$sliderId.' .frb-swiper-nav-round .frb-swiper-nav-right:before {
		border-color: '.$navigation_color.';
		color: '.$navigation_color.';
	}
	
	
</style>
	
	
	    <div class="frb-swiper-container'.$nav_class.'" data-autoPlay="'.($auto_play == 'true' ? $auto_delay*1000 : '' ).'" data-slidesPerView="'.$slides_per_view.'" data-mode="'.$mode.'" '.($responsive_layout ? 'data-min-res-width="'.$min_slide_width.'"' : '').'>
	      <div class="swiper-wrapper">';

	
	
	if(is_array($ctype))
		foreach($ctype as $ind => $type) {
			if($type == 'image') {
				switch($image_link_type[$ind]) {
					case 'new-tab' : $lightbox = '" target="_blank'; break;
					case 'lightbox-image' : $lightbox = ' frb_lightbox_link" rel="frbprettyphoto'; break;
					case 'lightbox-iframe' : $lightbox = ' frb_lightbox_link"  rel="frbprettyphoto'; $image_link[$ind] .= '?iframe=true&width='.$iframe_width.'&height='.$iframe_height; /* &width=500&height=500 */ break;
					default : $lightbox = '';
				}
	        	$html .='
			<div class="swiper-slide">'.(isset($image_link[$ind]) && $image_link[$ind] != '' ? '<a class="'.$lightbox.'" href="'.$image_link[$ind].'"><img class="swiper-image" src="'.$image[$ind].'" alt=""></a>'  : '<img class="swiper-image" src="'.$image[$ind].'" alt="">').'</div>';
			}
			else {
				$html .='
	        <div class="swiper-slide" style="background:'.$back_color[$ind].'; color:'.$text_color[$ind].'; text-align:'.(isset($text_align[$ind]) ? $text_align[$ind] : 'left').';">
	          <div class="content-slide">
	            '.$content[$ind].'
	          </div>
	        </div>';
				
			}
		}
			
			
	$html .= '
	      </div>
		  <div class="frb-swiper-nav-left"></div><div class="frb-swiper-nav-right"></div>
	    	</div>
	    <div class="frb-swiper-pagination"></div>
	';
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$sliderId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$sliderId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
	$html = $animSpeedSet.'<div id="'.$sliderId.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_slider', 'fbuilder_slider' );



/* ------------- */
/* fbuilder_code */
/* ------------- */

function fbuilder_code ($atts, $content=null) {
	extract (shortcode_atts( array(
		'bot_margin' => 24,
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts));
	$content = nl2br($content);
	$randomId =$shortcode_id == '' ? 'frb_code_'.rand() : $shortcode_id;
	$bot_margin = (int)$bot_margin.'px';
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
	
	$html = $animSpeedSet.'<div id="'.$randomId.'" class="frb_code '.$class.$animate.' style="padding-bottom:'.$bot_margin.';"><pre><code>'.$content.'</code></pre></div>';
	return $html;
}
add_shortcode( 'fbuilder_code', 'fbuilder_code' );


/* --------------- */
/* fbuilder_button */
/* --------------- */

function fbuilder_button ( $atts, $content=null ) {
	extract (shortcode_atts( array(
		'text' => 'Read more',
		'url' => '',
		'icon' => 'no-icon',
		'type' => 'standard',
		'iframe_width' => '600',
		'iframe_height' => '300',
		'h_padding' => 35,
		'v_padding' => 20,
		'bot_margin' => 24,
		'font_size' => 16,
		'icon_size' => 16,
		'text_align' => 'left',
		'icon_align' => 'left',
		'fullwidth' => 'false',
		'round' => 'false',
		'fill' => 'false',
		'border_thickness' => '1px',
		'text_color' => '#222222',
		'back_color' => '#222222',
		'hover_text_color' => '#57bce8',
		'hover_back_color' => '#57bce8',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts ));
	

	$border_thickness = (int)$border_thickness;
	$alignArray = array('center', 'left', 'right');
	if(!in_array($text_align,$alignArray)) $text_align = 'left';
	$typeArray = array( 'standard', 'new-tab', 'lightbox-image', 'lightbox-iframe');
	if(!in_array($type,$typeArray)) $type = 'standard';
	$icon_alignArray = array( 'left','right', 'center');
	if(!in_array($icon_align,$icon_alignArray)) $icon_align = 'right';
	$font_size = (int)$font_size .'px';
	$icon_size = (int)$icon_size .'px';
	$h_padding = (int)$h_padding .'px';
	$v_padding = (int)$v_padding .'px';
	$iframe_width = (int)$iframe_width;
	$iframe_height = (int)$iframe_height;
	$randomId = $shortcode_id == '' ? 'frb_button_'.rand() : $shortcode_id;
	$content = nl2br($content);
	$style = 'style="'.
		'font-size:'.$font_size.'; '.
		'line-height:'.$font_size.'; '. 
		'padding:'.$v_padding.' '.$h_padding.'; '. 
		'color:'.($text_color == '' ? 'transparent' : $text_color).'; '.
		'background:'.($back_color == '' ? 'transparent' : $back_color).'; '.
		'border: '.($fill != 'true' ? $border_thickness : '0').'px solid '.($back_color == '' ? 'transparent' : $back_color).';" '.
		'data-textcolor="'.$text_color.'" '.
		'data-backcolor="'.$back_color.'" '.
		'data-hovertextcolor="'.$hover_text_color.'" '.
		'data-hoverbackcolor="'.$hover_back_color.'"';
	
	switch($type) {
		case 'new-tab' : $lightbox = '" target="_blank"'; break;
		case 'lightbox-image' : $lightbox = ' frb_lightbox_link" rel="frbprettyphoto'; break;
		case 'lightbox-iframe' : $lightbox = ' frb_lightbox_link"  rel="frbprettyphoto'; $url .= '?iframe=true&width='.$iframe_width.'&height='.$iframe_height; /* &width=500&height=500 */ break;
		default : $lightbox = '';
	}
	
	$align = ' frb_'.$text_align;
	$round = ($round == 'true' ? ' frb_round' : '');
	$no_fill = ($fill != 'true' ? ' frb_nofill' : '');
	$fullwidth = ($fullwidth == 'true' ? ' frb_fullwidth' : '');
	switch($icon_align) {
		case 'right' : $icon_style = 'padding-left:8px; float:right; font-size:'.$icon_size.'; color:'.($text_color == '' ? 'transparent' : $text_color).';'; break; 
		case 'left' : $icon_style = 'padding-right:8px; float:left; font-size:'.$icon_size.'; color:'.($text_color == '' ? 'transparent' : $text_color).';'; break; 
		case 'inline' : $icon_style = 'padding-right:8px; font-size:'.$icon_size.'; float:none; color:'.($text_color == '' ? 'transparent' : $text_color).';'; break;
	}
	
	if($icon != '' && $icon != 'no-icon') {
		if(substr($icon,0,4) == 'icon') {
			$icon = '<span class="frb_button_icon" style="'.$icon_style.'" data-hovertextcolor="'.$hover_text_color.'"><i class="'.$icon.' fawesome"></i></span>';
		}
		else {
			$icon = '<span class="frb_button_icon" style="'.$icon_style.'" data-hovertextcolor="'.$hover_text_color.'"><i class="'.substr($icon,0,2).' '.$icon.' frb_icon"></i></span>';
		}
	}
	else {
		$icon = '';
	}
	
	$html = '<a class="frb_button'.$round.' '.$align.$fullwidth.$no_fill.$lightbox.'" href="'.$url.'" '.$style.'>'.$icon.$text.'</a>';
	
	if($align == ' frb_center') $html = '<div class="frb_textcenter">'.$html.'</div>';
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
	$html = $animSpeedSet.'<div id="'.$randomId.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'<div style="clear:both;"></div></div>';
	
	return $html;
}
add_shortcode( 'fbuilder_button', 'fbuilder_button' );

function fbuilder_testimonials ( $atts, $content=null ) {
		extract (shortcode_atts( array(
		'name' => 'John Dough',
		'profession' => 'photographer / fashion interactive',
		'quote' => 'lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
		'italic' => 'true',
		'url' => '',
		'image' =>  '',
		'style' => 'default',
		'bot_margin' => 24,
		'name_color' => '#376a6e',
		'quote_color' => '#376a6e',
		'main_color' => '#27a8e1',
		'back_color' => '',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts ));
	
	$styleArray = array('default', 'clean', 'squared', 'rounded');
	if(!in_array($style,$styleArray)) $style = 'default';

	$randomId =$shortcode_id == '' ? 'frb_testimonials_'.rand() : $shortcode_id;
	$content = nl2br($content);

	$styled = '
		<style type="text/css" scoped="scoped">
			#'.$randomId.' .frb_testimonials_default:after {
				border-top-color:'.($back_color != '' ? $back_color : 'transparent' ).' !important;
			}
			'.($italic=='true' ? '#'.$randomId.' .frb_testimonials_quote {font-style:italic !important;}' : '#'.$randomId.' .frb_testimonials_quote {font-style:normal !important;}' ).'
		</style>';


	$name_block = '<span class="frb_testimonials_name"><b'.($name_color != '' ? ' style="color:'.$name_color.'"' : '').'>'.$name.'</b>'.'<span'.($name_color != '' ? ' style="color:'.$name_color.'"' : '').'>'.$profession.'</span></span>';
	if($image != ''){
		$quote_block = '<div class="frb_testimonials_quote" '.($quote_color != '' ? 'style="color:'.$quote_color.'"' : '').'>'.$quote.'</div>';
		$image = ($url != '' ? '<a href="'.$url.'"><img class="frb_testimonials_img" src="'.$image.'" alt=""/></a>' : '<img class="frb_testimonials_img" src="'.$image.'" alt=""/>');
		$main_block = '<div class="frb_testimonials_main_block" style="'.(($main_color != '' && $style!='default') ? 'background:'.$main_color.'; border-color:'.$main_color.';' : '').'">'.$image.'</div>';
		if ($style!='default') {
			$html = $styled.$name_block.'<div class="frb_testimonials frb_testimonials_'.$style.'" style="'.($back_color != '' ? 'background:'.$back_color.';' : '').($main_color != '' ? ' border-color:'.$main_color.';' : '').'">'.$main_block.$quote_block.'</div>';
		}
		else {
			$html =  $styled.'<div  class="frb_testimonials frb_testimonials_'.$style.'" style="'.($back_color != '' ? 'background:'.$back_color.';' : '').'">'.$quote_block.'</div>'.$main_block.$name_block;
		}
		
	}
	else {
		$quote_block = '<div class="frb_testimonials_quote'.($style == 'clean' ? ' frb_testimonials_quote_border' : '').'" style="'.($quote_color != '' ? 'color:'.$quote_color.';' : '').($main_color != '' ? ' border-color:'.$main_color.';' : '').'">'.$quote.'</div>';
		$name_block = ($url != '' ? '<a href="'.$url.'">'.$name_block.'</a>' : $name_block);
		$main_block = '<div class="frb_testimonials_main_block" style="'.($main_color != '' ? 'background:'.$main_color.'; border-color:'.$main_color.';' : '').'">'.$name_block.'</div>';
		if ($style!='default') {
			$html = $styled.'<div class="frb_testimonials frb_testimonials_'.$style.'"  style="'.($back_color != '' ? 'background:'.$back_color.';' : '').($main_color != '' ? ' border-color:'.$main_color.';' : '').'">'.$main_block.$quote_block.'</div>';
		}
		else {
			$html =  $styled.'<div  class="frb_testimonials frb_testimonials_'.$style.'" style="'.($back_color != '' ? 'background:'.$back_color.';' : '').'">'.$quote_block.'</div>'.$name_block;
		}
		
	}
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	$html = $animSpeedSet.'<div id="'.$randomId.'" class="frb_testimonial_style_'.$style.' '.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
	return $html;
}
add_shortcode( 'fbuilder_testimonials', 'fbuilder_testimonials' );

function fbuilder_alert ( $atts, $content=null ) {
	extract (shortcode_atts( array(
		'type' => 'info',
		'text' => 'This is an alert',
		'icon' => 'ba-warning',
		'style' => 'clean',
		'bot_margin' => 24,
		'main_color' => '#27a8e1',
		'text_color' => '#376a6e',
		'icon_color' => '#27a8e1',
		'back_color' => '',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts ));


	
	$randomId =$shortcode_id == '' ? 'frb_alert_'.rand() : $shortcode_id;
	$content = nl2br($content);
	$typeArray = array('info', 'success', 'notice', 'warning', 'custom');
	if(!in_array($type,$typeArray)) $type = 'info';
	$styleArray = array('info', 'success', 'notice', 'warning', 'custom');
	if(!in_array($type,$typeArray)) $type = 'info';
	if($type != 'custom') {
		$html = '<div class="frb_alert frb_alert_'.$type.' frb_alert_'.$style.'"><div class="frb_alert_icon"></div><div class="frb_alert_text">'.$text.'</div></div>';
	}
	else {
		$html = '<div class="frb_alert frb_alert_'.$type.' frb_alert_'.$style.'" style="border-color:'.$main_color.'; background-color:'.$back_color.';"><div class="frb_alert_icon" style="background-color:'.$main_color.';">';
		
		if($icon != '' && $icon != 'no-icon') {
			if(substr($icon,0,4) == 'icon') {
				$html .='<i class="'.$icon.' fawesome" style="color:'.$icon_color.';"></i>';
			}
			else {
				$html .='<i class="'.substr($icon,0,2).' '.$icon.' frb_icon" style="color:'.$icon_color.';"></i>';
			}
		}
		
		$html .='</div><div class="frb_alert_text" style="color:'.$text_color.';">'.$text.'</div></div>';
	}
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	$html = $animSpeedSet.'<div id="'.$randomId.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_alert', 'fbuilder_alert' );


function fbuilder_accordion ( $atts, $content=null ) {
		extract (shortcode_atts( array(
		'active' => '',
		'title' => '',
		'image' => '',
		'style' => 'default',
		'fixed_height' => 'true',
		'bot_margin' => 24,
		'title_color' => '#376a6e',
		'text_color' => '#376a6e',
		'trigger_color' => '#376a6e',
		'title_active_color' => '#376a6e',
		'trigger_active_color' => '#376a6e',
		'main_color' => '#27a8e1',
		'border_color' => '#376a6e',
		'back_color' => '',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => ''
	), $atts ));
	$content = nl2br($content);
	$styled = $style;
	$styleArray = array('default', 'clean-right', 'squared-right', 'rounded-right', 'clean-left', 'squared-left', 'rounded-left');
	if(!in_array($style,$styleArray)) $style = 'default';
	$title = explode('|', $title);
	$content = explode('|', $content);
	$active = explode('|', $active);
	$image = explode('|', $image);
	
	if($border_color == '') $border_color = 'transparent';
	if($back_color == '') $back_color = 'transparent';
	$randomId = rand();
	
	if ( $styled !== 'default' ) :
		$html = '
		<style type="text/css" scoped="scoped">
			#frb_accordion_'.$randomId.' {border-bottom-color:'.$border_color.';}
			#frb_accordion_'.$randomId.' h3 {color:'.$title_color.'; background:'.$back_color.'; border-top-color:'.$border_color.'; border-left-color:'.$border_color.';}
			#frb_accordion_'.$randomId.' h3 .frb_accordion_trigger{color:'.$trigger_color.'; background:'.$back_color.';}
			#frb_accordion_'.$randomId.' h3.ui-state-active {color:'.$title_active_color.';}
			#frb_accordion_'.$randomId.' h3.ui-state-active .frb_accordion_trigger{color:'.$trigger_active_color.';}
			#frb_accordion_'.$randomId.' div {color:'.$text_color.'; background:'.$back_color.';}
			#frb_accordion_'.$randomId.' h3.ui-accordion-header-active{'.($style == 'squared-right' || $style == 'rounded-right' ? 'background:'.$main_color.';' : 'color:'.$main_color.';').' border-left-color:'.$main_color.';}
			#frb_accordion_'.$randomId.' h3.ui-accordion-header-active .frb_accordion_trigger{'.($style == 'squared-left' || $style == 'rounded-left' ? 'background:'.$main_color.';':'').'}
			#frb_accordion_'.$randomId.' div.ui-accordion-content-active{'.($style == 'squared-right' || $style == 'rounded-right' ? 'background:'.$main_color.';' : '').' border-left-color:'.$main_color.';}
		';
	else :
		$html = '
		<style type="text/css" scoped="scoped">
			#frb_accordion_'.$randomId.' {border-bottom-color:'.$border_color.';}
			#frb_accordion_'.$randomId.' h3 {color:'.$title_color.'; background:'.$back_color.'; border-top-color:'.$border_color.'; border-left-color:'.$border_color.';}
			#frb_accordion_'.$randomId.' h3 .frb_accordion_trigger:after {background:'.$trigger_color.';}
			#frb_accordion_'.$randomId.' h3.ui-state-active {color:'.$title_active_color.';}
			#frb_accordion_'.$randomId.' div {color:'.$text_color.'; background:'.$back_color.';}
			#frb_accordion_'.$randomId.' h3.ui-state-active .frb_accordion_trigger:after {background:'.$trigger_active_color.';}
		';
	endif;

	if ( $styled == 'squared-left' ||  $styled == 'rounded-left' || $styled == 'clean-left' ||  $styled == 'clean-right') {
		$html.= 
		'#frb_accordion_'.$randomId.' h3.ui-state-active, #frb_accordion_'.$randomId.' div.ui-accordion-content-active {border-color:'.$main_color.';}
		#frb_accordion_'.$randomId.' div.ui-accordion-content {border-top-width:0px;}
		#frb_accordion_'.$randomId.' h3 {border-color:'.$border_color.';}';
	} else if ( $styled == 'clean-left' ||  $styled == 'clean-right') {
		$html.= 
		'#frb_accordion_'.$randomId.' h3.ui-state-active, #frb_accordion_'.$randomId.' div.ui-accordion-content-active {border-color:'.$main_color.';}
		#frb_accordion_'.$randomId.' div.ui-accordion-content {border-top-width:0px;}
		#frb_accordion_'.$randomId.' h3 {border-color:'.$border_color.';margin-top:1px;}';
	}

	$html.='</style>';
	
	$html .= '<div id="frb_accordion_'.$randomId.'" class="frb_accordion frb_accordion_'.$style.'" data-fixedheight="'.$fixed_height.'">';
	
	if(is_array($title) && is_array($content)){
		for($i=0; $i<count($title); $i++) {
			$html .= '<h3'.($active[$i] == 'true' ? ' class="ui-state-active"' : '').' >'.$title[$i].'<span class="frb_accordion_trigger"></span></h3>';
			$image[$i] = ($image[$i] != '' ? '<img style="float:left; margin-right:10px;" src="'.$image[$i].'" alt="" />' : '');
			$html .= '<div style="">'.$image[$i].$content[$i].'<div style="clear:both;"></div></div>';
		}
	}
			
	$html .='</div>';
	
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
	$html = '<div '.($shortcode_id != '' ? 'id="'.$shortcode_id.'"' : '').' class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.(do_shortcode($html)).'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_accordion', 'fbuilder_accordion' );

//			fbuilder_toggle

function fbuilder_toggle ( $atts, $content=null ) {
		extract (shortcode_atts( array(
		'active' => '',
		'title' => '',
		'image' => '',
		'style' => 'clean-right',
		'fixed_height' => 'true',
		'bot_margin' => 24,
		'title_color' => '#376a6e',
		'text_color' => '#376a6e',
		'trigger_color' => '#376a6e',
		'title_active_color' => '#376a6e',
		'trigger_active_color' => '#376a6e',
		'main_color' => '#27a8e1',
		'border_color' => '#376a6e',
		'active_border_color' => '',
		'back_color' => '',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts ));
	$content = nl2br($content);
	$styleArray = array('clean-right', 'squared-right', 'rounded-right', 'clean-left', 'squared-left', 'rounded-left');
	if(!in_array($style,$styleArray)) $style = 'clean-right';
	$title = explode('|', $title);
	$content = explode('|', $content);
	$active = explode('|', $active);
	$image = explode('|', $image);
	
	if($border_color == '') $border_color = 'transparent';
	if($active_border_color == '') $active_border_color = 'transparent';
	if($back_color == '') $back_color = 'transparent';
	$randomId = rand();
	
	$left = (($style == 'squared-left' || $style == 'rounded-left' || $style == 'clean-left' ) ? true : false);
	$rounded = (($style == 'rounded-left' || $style == 'rounded-right' ) ? true : false);
	
	$html = '
	<style type="text/css" scoped="scoped"> 

	'.(!$left ? '.frb_toggle input + label > h3 {padding-left:10px;}' : '').'
	
	#frb_toggle'.$randomId.' .frb_toggle_item_content {
	color:'.$text_color.';
	}
			
	#frb_toggle'.$randomId.' .frb_toggle_item > label h3  {
	color:'.$title_color.';
	border:1px solid '.$border_color.';
	'.($left ? '' : 'padding-top: 10px;padding-bottom: 10px;').'
	}
	
	#frb_toggle'.$randomId.' .frb_toggle_item > label i {
	'.($left ? '' : 'float:right;').'
	color:'.$trigger_color.';
	}
	
	#frb_toggle'.$randomId.' .frb_toggle_item input:checked + label i {
	color:'.$trigger_active_color.';
	}
	
	#frb_toggle'.$randomId.' .frb_toggle_item input:checked + label h3, #frb_toggle'.$randomId.' .frb_toggle_item input:checked ~ .frb_toggle_item_content {
		border:1px solid '.$active_border_color.';
	}
	
	'.(($style == 'squared-right' || $style == 'rounded-right') ? 
	'#frb_toggle'.$randomId.' .frb_toggle_item input:checked + label h3 {
	color:'.$title_active_color.';
	}
	#frb_toggle'.$randomId.' .frb_toggle_item input:checked + label h3, #frb_toggle'.$randomId.' .frb_toggle_item input:checked ~ .frb_toggle_item_content {
	background-color:'.$main_color.';
	
	}
	' 
	: '
	#frb_toggle'.$randomId.' .frb_toggle_item input:checked + label h3 {
	color:'.$main_color.';
	}
	' ).'
	
	#frb_toggle'.$randomId.' .frb_toggle_item input + label h3, #frb_toggle'.$randomId.' .frb_toggle_item_content {
	background-color:'.$back_color.';
	
	}
	
	'.($rounded ? 
	'#frb_toggle'.$randomId.' .frb_toggle_item > label h3, #frb_toggle'.$randomId.' .frb_toggle_item > .frb_toggle_item_content {
	border-radius:5px;
	}' 
	: '' ).
	(($style == 'squared-left' || $style == 'rounded-left') ? 
	'#frb_toggle'.$randomId.' .frb_toggle_item input:checked + label i, #frb_toggle'.$randomId.' .frb_toggle_item input:checked ~ .frb_toggle_item_content .frb_toggle_content_left {
	background-color:'.$main_color.';
	}
	
	#frb_toggle'.$randomId.' .frb_toggle_item input + label i, #frb_toggle'.$randomId.' .frb_toggle_item .frb_toggle_item_content .frb_toggle_content_left {
	background-color:'.$border_color.';
	'.($rounded ? '-moz-border-radius: 0px;
	-webkit-border-radius: 5px 0px 0px 5px;
	border-radius: 5px 0px 0px 5px;' : '').'
	}
	
	#frb_toggle'.$randomId.' .frb_toggle_item .frb_toggle_item_content .frb_toggle_content_left {
	width:29px;
	height: 100%;
	margin-right: 15px;
	}' : '' ).'
		
	</style>';
	
	
	$fixed_height = $fixed_height == 'false' ? false : true;
	
	$html .= '<div id="frb_toggle'.$randomId.'" class="frb_toggle '.($fixed_height ? 'frb_fixed_h' : '').'">';
	
	if(is_array($title) && is_array($content)){
		for($i=0; $i<count($title); $i++) {
			$image[$i] = ($image[$i] != '' ? '<img style="float:left; margin-right:10px;margin-left:-5px;" src="'.$image[$i].'" alt="" />' : '');
			$html .= '<div class="frb_toggle_item"><input id="fb_toggle-'.$randomId.$i.'" name="fb_toggle_'.$i.'" type="checkbox" '.($active[$i] == 'true' ? 'checked' : '').' /><label for="fb_toggle-'.$randomId.$i.'"><h3>'.($left ? '<i class="ba"></i>' : '').$title[$i].($left ? '' : '<i class="ba"></i>').'</h3></label>
		
		<div class="frb_toggle_item_content"><div class="frb_toggle_content_left"></div><div class="frb_toggle_content_right">'.$image[$i].$content[$i].'</div></div>
		<div style="clear:both;"></div></div>';
		}
	}
	
	$html.= '</div>';
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'.frb_toggle_anim_'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'.frb_toggle_anim_'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
	$html = '<div '.($shortcode_id != '' ? 'id="'.$shortcode_id.'"' : '').' class="frb_toggle_anim_'.$randomId.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.(do_shortcode($html)).'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_toggle', 'fbuilder_toggle' );

//			fbuilder_tour

function fbuilder_tour ( $atts, $content=null ) {
		extract (shortcode_atts( array(
		'active' => '',
		'title' => '',
		'image' => '',
		'tab_position' => 'left',
		'tab_text_align' => 'left',
		'round' => 'false',
		'border_position' => 'false',
		'bot_margin' => 24,
		'title_color' => '#376a6e',
		'text_color' => '#376a6e',
		'active_tab_title_color' => '#376a6e',
		'tab_border_color' => '#376a6e',
		'active_tab_border_color' => '#27a8e1',
		'border_color' => '#ebecee',
		'tab_back_color' => '#376a6e',
		'back_color' => '#f4f4f4',
		'class' => '',
		'custom_id' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts ));
	$content = nl2br($content);
	$title = explode('|', $title);
	$content = explode('|', $content);
	$active = explode('|', $active);
	$image = explode('|', $image);
	$custom_id = explode('|', $custom_id);
	
	$round = $round == 'false' ? false : true;
	$border_position = $border_position == 'false' ? false : true;
	
	if($border_color == '') $border_color = 'transparent';
	if($back_color == '') $back_color = 'transparent';
	if($title_color == '') $title_color = 'transparent';
	if($text_color == '') $text_color = 'transparent';
	if($tab_back_color == '') $tab_back_color = 'transparent';
	if($active_tab_border_color == '') $active_tab_border_color = 'transparent';
	if($tab_border_color == '') $tab_border_color = 'transparent';
	if($active_tab_title_color == '') $active_tab_title_color = 'transparent';
	
	$randomId = rand();
	
	$html = '
	<style type="text/css" scoped="scoped">
		'.($shortcode_id != '' ? $shortcode_id : '#frb_tour_'.$randomId).' .frb_tour-content {
			color:'.$text_color.';
			border:2px solid '.$border_color.';
			background:'.$back_color.';
		}
		
		'.($shortcode_id != '' ? $shortcode_id : '#frb_tour_'.$randomId).' ul:first-child {
			float:'.$tab_position.';
		}
		
		'.($shortcode_id != '' ? $shortcode_id : '#frb_tour_'.$randomId).' ul:first-child li a{
			color:'.$title_color.';
			text-align:'.$tab_text_align.';
			background:'.$tab_back_color.';
			border-top:2px solid '.($border_position ? $tab_border_color : 'rgba(255,255,255,0)').';
			border-'.($tab_position=='left' ? 'left' : 'right').':2px solid '.$tab_border_color.';
			border-bottom:2px solid rgba(255,255,255,0);
		}
		
		'.($shortcode_id != '' ? $shortcode_id : '#frb_tour_'.$randomId).' ul:first-child li:last-child a {
			border-bottom:2px solid '.($border_position ? $tab_border_color : 'rgba(255,255,255,0)').';
			padding-bottom:8px !important;
		}
		
		
		'.($shortcode_id != '' ? $shortcode_id : '#frb_tour_'.$randomId).' ul:first-child li a.active{
			'.($tab_position=='left' ? 'margin-right:-2px;padding-right:12px;border-left: 2px solid '.$active_tab_border_color.';' : 'padding-left:12px;margin-left:-2px;border-right: 2px solid'.$active_tab_border_color.';').'
			background:'.$back_color.';
			color:'.$active_tab_title_color.';
			border-top:2px solid '.($border_position ? $active_tab_border_color : 'rgba(255,255,255,0)').';
			padding-bottom:8px !important;
			border-bottom:2px solid '.($border_position ? $active_tab_border_color : 'rgba(255,255,255,0)').';
		}
		
		'.($shortcode_id != '' ? $shortcode_id : '#frb_tour_'.$randomId).' ul:first-child li a:hover{
				'.($tab_position=='left' ? 'margin-right:-2px;padding-right:12px;border-left: 2px solid '.$active_tab_border_color.';' : 'padding-left:12px;margin-left:-2px;border-right: 2px solid'.$active_tab_border_color.';').'
			background-color:'.$back_color.';
			color:'.$active_tab_title_color.';
			border-top:2px solid '.($border_position ? $active_tab_border_color : 'rgba(255,255,255,0)').';
			transition: border-top-color 300ms, background-color 300ms;
			-webkit-transition: border-top-color 300ms, background-color 300ms;
			padding-bottom:8px !important;
			border-bottom:2px solid '.($border_position ? $active_tab_border_color : 'rgba(255,255,255,0)').';
		}'
		.($round ? ($shortcode_id != '' ? $shortcode_id : '#frb_tour_'.$randomId).' ul:first-child li:first-child a {
				border-radius:'.($tab_position=='left' ? '5px 0' : '0 5px').' 0 0;
			}
			'.($shortcode_id != '' ? $shortcode_id : '#frb_tour_'.$randomId).' ul:first-child li:last-child a {
				border-radius:0 0 '.($tab_position=='left' ? '0 5px' : '5px 0').';
			}
			
			'.($shortcode_id != '' ? $shortcode_id : '#frb_tour_'.$randomId).' .frb_tour-content {
				border-radius:'.($tab_position=='left' ? '0 5px 5px 0' : '5px 0 0 5px').';
			}' : '').'
	</style>';
	
	$html .= '<div id="frb_tour_'.$randomId.'" class="frb_tour"><ul>';
	
	if(is_array($title) && is_array($content)){
		for($i=0; $i<count($title); $i++) {
			$html .='<li><a href="'.(isset($custom_id[$i]) && $custom_id[$i] != '' ? '#'.$custom_id[$i] : '#frb_tour_'.$randomId.'_'.$i).'"'.($active[$i] == 'true' ? ' class="active"' : '').'>'.$title[$i].'</a></li>';
			
		}
	}
			
	$html .='</ul>';
	
	if(is_array($title) && is_array($content)){
		for($i=0; $i<count($title); $i++) {
			$image[$i] = ($image[$i] != '' ? '<img style="float:left; margin-right:10px;" src="'.$image[$i].'" alt="" />' : '');
			$html .= '<div id="'.(isset($custom_id[$i]) && $custom_id[$i] != '' ? $custom_id[$i] : 'frb_tour_'.$randomId.'_'.$i).'" class="frb_tour-content">'.$image[$i].$content[$i].'<div style="clear:both;"></div></div>';
		}
	}
	
	$html .='</div>';
	
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'.frb_tour_anim_'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'.frb_tour_anim_'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	$html = $animSpeedSet.'<div '.($shortcode_id != '' ? 'id="'.$shortcode_id.'"' : '').' class="frb_tour_anim_'.$randomId.$class.$animate.' style="clear:both;width:100%;padding-bottom:'.$bot_margin.'px !important;">'.(do_shortcode($html)).'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_tour', 'fbuilder_tour' );


//			fbuilder_tabs
function fbuilder_tabs ( $atts, $content=null ) {
		extract (shortcode_atts( array(
		'active' => '',
		'title' => '',
		'image' => '',
		'style' => 'default',
		'bot_margin' => 24,
		'title_color' => '#376a6e',
		'text_color' => '#376a6e',
		'active_tab_title_color' => '#376a6e',
		'active_tab_border_color' => '#27a8e1',
		'border_color' => '#ebecee',
		'tab_back_color' => '#376a6e',
		'back_color' => '#f4f4f4',
		'class' => '',
		'custom_id' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_group' => ''
	), $atts ));
	$content = nl2br($content);
	$styled = $style;
	$styleArray = array('default', 'clean', 'squared', 'rounded');
	if(!in_array($style,$styleArray)) $style = 'default';
	$title = explode('|', $title);
	$content = explode('|', $content);
	$active = explode('|', $active);
	$image = explode('|', $image);
	$custom_id = explode('|', $custom_id);
	
	if($border_color == '') $border_color = 'transparent';
	if($back_color == '') $back_color = 'transparent';
	$randomId = rand();

	if ( $styled !== 'default' ) :
	$html = '
	<style type="text/css" scoped="scoped">
		#frb_tabs_'.$randomId.' .frb_tabs-content {
			color:'.$text_color.';
			border:2px solid '.$border_color.';
			'.($style != 'clean' ? 'background:'.$back_color.';' :'').'
		}
		#frb_tabs_'.$randomId.' ul:first-child a {
			color:'.$title_color.';
			'.($style != 'clean' ? '
			background:'.$tab_back_color.';':'').'
		}
		#frb_tabs_'.$randomId.' ul:first-child a.active{
			'.($style != 'clean' ? '
			background:'.$back_color.';
			color:'.$active_tab_title_color.';
			border-top:2px solid '.$active_tab_border_color.';
			padding-bottom:10px !important;
			margin-top:-2px !important' : '
			padding-bottom:10px !important;
			border-bottom:2px solid '.$active_tab_border_color.';').'
		}
		#frb_tabs_'.$randomId.' ul:first-child a:hover{
			'.($style != 'clean' ? '
			background-color:'.$back_color.';
			color:'.$active_tab_title_color.';
			border-top:2px solid '.$active_tab_border_color.';
			padding-bottom:10px !important;
			margin-top:-2px !important;
			transition: border-top-color 300ms, background-color 300ms;
			-webkit-transition: border-top-color 300ms, background-color 300ms;' : '
			padding-bottom:10px !important;
			border-bottom:2px solid '.$active_tab_border_color.';
			transition: border-bottom-color 300ms;
			-webkit-transition: border-bottom-color 300ms;').'
		}
		'.($style == 'rounded' ? '
			#frb_tabs_'.$randomId.' ul:first-child li:first-child a {
				border-radius:5px 0 0 0;
			}
			#frb_tabs_'.$randomId.' ul:first-child li:last-child a {
				border-radius:0 5px 0 0;
			}
		': '').'
	</style>';
	else :
	$html = '
	<style type="text/css" scoped="scoped">
		#frb_tabs_'.$randomId.' .frb_tabs-content {
			color:'.$text_color.';
			background: '.$tab_back_color.';
		}
		#frb_tabs_'.$randomId.' ul:first-child a {
			color:'.$title_color.';
		}
		#frb_tabs_'.$randomId.' ul:first-child a.active{
			color:'.$active_tab_title_color.';
		}
		#frb_tabs_'.$randomId.' ul:first-child a.active:after {
			border-top-color:'.$active_tab_border_color.' !important;
		}
		#frb_tabs_'.$randomId.' ul:first-child a.active {
			background:'.$active_tab_border_color.';
		}
		#frb_tabs_'.$randomId.' ul:first-child a {
			background:'.$tab_back_color.';
			
		}

	</style>';
	endif;
	
	$html .= '<div id="frb_tabs_'.$randomId.'" class="frb_tabs frb_tabs_'.$styled.'"><ul>';
	
	if(is_array($title) && is_array($content)){
		for($i=0; $i<count($title); $i++) {
			$html .='<li><a href="'.(isset($custom_id[$i]) && $custom_id[$i] != '' ? '#'.$custom_id[$i] : '#frb_tabs_'.$randomId.'_'.$i).'"'.($active[$i] == 'true' ? ' class="active"' : '').'>'.$title[$i].'</a></li>';
			
		}
	}
			
	$html .='</ul><div style="clear:both;"></div>';
	
	if(is_array($title) && is_array($content)){
		for($i=0; $i<count($title); $i++) {
			$image[$i] = ($image[$i] != '' ? '<img style="float:left; margin-right:10px;" src="'.$image[$i].'" alt="" />' : '');
			$html .= '<div id="'.(isset($custom_id[$i]) && $custom_id[$i] != '' ? $custom_id[$i] : 'frb_tabs_'.$randomId.'_'.$i).'" class="frb_tabs-content">'.$image[$i].$content[$i].'<div style="clear:both;"></div></div>';
		}
	}
	
	$html .='</div>';
	
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
	$html = '<div '.($shortcode_id != '' ? 'id="'.$shortcode_id.'"' : '').' class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.(do_shortcode($html)).'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_tabs', 'fbuilder_tabs' );


//			fbuilder_features

function fbuilder_features ( $atts, $content=null ) {
		extract (shortcode_atts( array(
		'title' => 'Lorem ipsum',
		'icon' => 'na-svg23',
		'link' => '',
		'order' => 'icon-after-title',
		'style' => 'clean',
		'bot_margin' => 24,
		'icon_size' => 40,
		'title_color' => '#ffffff',
		'icon_color' => '#27a8e1',
		'icon_padding' => 0,
		'icon_border' => 'false',
		'text_color' => '#808080',
		'back_color' => '#376a6e',
		'title_hover_color' => '#ffffff',
		'icon_hover_color' => '#27a8e1',
		'text_hover_color' => '#808080',
		'back_hover_color' => '#376a6e',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts ));
	if($shortcode_id == '') $shortcode_id = 'frb_features_'.rand();
	$content = nl2br($content);
	$styleArray = array('clean', 'squared', 'rounded', 'icon-squared', 'icon-rounded');
	if(!in_array($style,$styleArray)) $style = 'clean-right';
	$orderArray = array('icon-left', 'icon-right', 'icon-after-title', 'icon-before-title');
	if(!in_array($order,$orderArray)) $order = 'icon-after-title';
	$margin = (int)$icon_size + (int)$icon_padding*2 + 20;
	$icon_size = (int)$icon_size .'px';
	$icon_padding = (int)$icon_padding .'px';
	
	$sty = '
	<style type="text/css" scoped="scoped">
		#'.$shortcode_id.' .frb_features{
			'.($style == 'squared' || $style == 'rounded' ? 'background:'.$back_color.';"' : '').'
		}
		#'.$shortcode_id.' .frb_features:hover {
			'.($style == 'squared' || $style == 'rounded' ? 'background:'.$back_hover_color.';"' : '').'
		}
	
		#'.$shortcode_id.' .frb_features_title {
			color:'.$title_color.';
			'.($order != 'icon-before-title' ? 'margin-top:0; padding-top:10px;':'').'
			'.($order == 'icon-left' ? ' margin-left:'.($margin+2).'px;' : '').'
			'.($order == 'icon-right' ? ' margin-right:'.($margin+2).'px;' : '').'
			transition: color 300ms;
			-webkit-transition: color 300ms;
		}
		
		#'.$shortcode_id.' .frb_features:hover .frb_features_title {
			color: '.$title_hover_color.';
		}
		
		#'.$shortcode_id.' .frb_features_icon {
			font-size:'.$icon_size.'; 
			line-height:'.$icon_size.'; 
			color:'.$icon_color.';
			padding:'.$icon_padding.';
			'.($order == 'icon-left' ? 'margin-left:0; margin-right:20px;' : '').'
			'.($order == 'icon-right' ? 'margin-left:20px; margin-right:0;' : '').'
			'.($style == 'icon-squared' || $style == 'icon-rounded' ? 'background:'.$back_color.';' : '').'
			'.($style == 'icon-rounded' ? 'border-radius:50%;' : '').'
			'.($icon_border == 'true' ? 'border:1px solid '.$icon_color.';' : '').'
			transition: color 300ms;
			-webkit-transition: color 300ms, border-color 300ms;
		}
		
		#'.$shortcode_id.' .frb_features:hover .frb_features_icon {
			color: '.$icon_hover_color.';
			border-color: '.$icon_hover_color.';
			'.($style == 'icon-squared' || $style == 'icon-rounded' ? 'background:'.$back_hover_color.';"' : '').'
		}
		
		#'.$shortcode_id.' .frb_features_content {
			color:'.$text_color.';
			'.($order == 'icon-left' ? 'margin-left:'.($margin+2).'px;' : '').'
			'.($order == 'icon-right' ? 'margin-right:'.($margin+2).'px;' : '').'
			transition: color 300ms;
			-webkit-transition: color 300ms;
		}
		
		#'.$shortcode_id.' .frb_features:hover .frb_features_content {
			color:'.$text_hover_color.';
		}
	</style>
	';
	
	
	$title = '<h3 class="frb_features_title">'.$title.'</h3>';
	
	if($icon != '' && $icon != 'no-icon') {
		if(substr($icon,0,4) == 'icon') {
			$icon = '<i class="frb_features_icon frb_features_'.$order.' '.$icon.' fawesome" ></i>';
		}
		else {
			$icon = '<i class="frb_features_icon frb_features_'.$order.' '.substr($icon,0,2).' '.$icon.' frb_icon" ></i>';
		}
	}
	else {
		$icon = '';
	}
	$content = '<span class="frb_features_content">'.$content.'</span>';
	
	$html = '<div class="frb_features frb_features_'.$style.' frb_features_'.$order.'">';
	if($link != '') $html .= '<a href="'.$link.'">';
	if($order != 'icon-after-title') $html .= $icon.$title.$content;
	else  $html .= $title.$icon.$content;
	$html .= '<div style="clear:both;"></div>'.($link != '' ? '</a>' : '').'</div>';
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$shortcode_id.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$shortcode_id.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	$html = $animSpeedSet.$sty.'<div id="'.$shortcode_id.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_features', 'fbuilder_features' );

function fbuilder_icon_menu ( $atts, $content=null ) {
		extract (shortcode_atts( array(
		'icon' => '',
		'url' => '',
		'align' => 'left',
		'icon_padding' => '5px',
		'link_type' => 'standard',
		'iframe_width' => '600',
		'iframe_height' => '300',
		'bot_margin' => 24,
		'icon_size' => 24,
		'round' => 'false',
		'icon_color' => '#376a6e',
		'back_color' => '',
		'icon_hover_color' => '#27a8e1',
		'back_hover_color' => '',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts ));
	$alignArray = array('left', 'right', 'center');
	if(!in_array($align, $alignArray)) $align = 'left';
	if($back_hover_color == '') $back_hovar_color = 'transparent';
	if($back_color == '') $back_color = 'transparent';
	$icon_size = (int)$icon_size;
	$iframe_width = (int)$iframe_width;
	$iframe_height = (int)$iframe_height;
	$randomId =$shortcode_id == '' ? 'frb_icon_menu_'.rand() : $shortcode_id;
	
	$icon_padding = (int)$icon_padding/2;
	
	$html = '<style type="text/css" scoped="scoped">
			div.frb_iconmenu a.frb_iconmenu_link {
				padding:10px '.$icon_padding.'px;
			}
			</style>';
	
	$html .= '<div class="frb_iconmenu'.($round == 'true' ? ' frb_iconmenu_round' : '').' frb_iconmenu_'.$align.'" style="background:'.$back_color.';">';
	
	$icon = explode('|', $icon);
	$link_type = explode('|', $link_type);
	$url = explode('|', $url);
	if(is_array($icon)){
		for($i=0; $i<count($icon); $i++) {
			
			if(substr($icon[$i],0,4) == 'icon') {
				$ii = '<i class="fawesome '.$icon[$i].'" style="color:'.$icon_color.'; width:'.($icon_size+10).'px; font-size:'.$icon_size.'px; line-height:'.$icon_size.'px;" data-color="'.$icon_color.'" data-hovercolor="'.$icon_hover_color.'"></i>';
			}
			else {
				$ii = '<i class="frb_icon '.substr($icon[$i],0,2).' '.$icon[$i].'" style="color:'.$icon_color.'; width:'.($icon_size+10).'px; font-size:'.$icon_size.'px; line-height:'.$icon_size.'px;" data-color="'.$icon_color.'" data-hovercolor="'.$icon_hover_color.'"></i>';
			}
			
			switch($link_type[$i]) {
				case 'new-tab' : $lightbox = '" target="_blank'; break;
				case 'lightbox-image' : $lightbox = ' frb_lightbox_link" rel="frbprettyphoto'; break;
				case 'lightbox-iframe' : $lightbox = ' frb_lightbox_link"  rel="frbprettyphoto'; $url[$i] .= '?iframe=true&width='.$iframe_width.'&height='.$iframe_height; /* &width=500&height=500 */ break;
				default : $lightbox = '';
			}
			$html .= '<a href="'.$url[$i].'" style="background:'.$back_color.'; color:'.$icon_color.';" data-backcolor="'.$back_color.'" data-backhover="'.$back_hover_color.'" class="frb_iconmenu_link'.$lightbox.'">'.$ii.'</a>';
		}
	}
	$html .= '<div style="clear:both;"></div></div>';
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	$html = $animSpeedSet.'<div id="'.$randomId.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_icon_menu', 'fbuilder_icon_menu' );

function fbuilder_search ( $atts, $content=null ) {
		extract (shortcode_atts( array(
		'text' => 'Search',
		'bot_margin' => 24,
		'round' => 'flase',
		'text_color' => '#376a6e',
		'border_color' => '#ebecee',
		'back_color' => '',
		'text_focus_color' => '#376a6e',
		'border_focus_color' => '#376a6e',
		'back_focus_color' => '',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts ));
	
	if($back_color == '') $back_color = 'transparent';
	if($border_color == '') $border_color = 'transparent';
	if($text_color == '') $text_color = 'transparent';
	if($text_focus_color == '') $text_focus_color = 'transparent';
	if($border_focus_color == '') $border_focus_color = 'transparent';
	if($back_focus_color == '') $back_focus_color = 'transparent';
	$randomId =$shortcode_id == '' ? 'frb_search_'.rand() : $shortcode_id;
	$html = '
<form method="get" style="background:'.$back_color.'; border-color:'.$border_color.';"  data-backcolor="'.$back_color.'" data-bordercolor="'.$border_color.'"  data-backfocus="'.$back_focus_color.'" data-borderfocus="'.$border_focus_color.'" class="frb_searchform'.($round == 'true' ? ' frb_searchform_round' : '').'" action="'.home_url( '/' ).'">
	<div class="frb_searchleft">
		<div class="frb_searchleft_inner">
			<input type="text" style="color:'.$text_color.';"  data-color="'.$text_color.'" data-focuscolor="'.$text_focus_color.'" data-value="'.$text.'" class="frb_searchinput" value="'.$text.'" name="s" />
		</div>
	</div>
	<div class="frb_searchright">
		<i style="color:'.$text_color.';" class="frb_searchsubmit fawesome ba ba-search"></i>
	</div>
	<div class="frb_clear"></div>
</form>';

	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	$html = $animSpeedSet.'<div id="'.$randomId.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_search', 'fbuilder_search' );


function fbuilder_h( $atts, $content=null ) {
	extract (shortcode_atts( array(
		'type' => 'h1',
		'bot_margin' => 24,
		'custom_font_size' => 'false',
		'font_size' => 36,
		'line_height' => 40,
		'align' => 'left',
		'text_color' => '#232323',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts));
	
	$alignArray = array('left', 'right', 'center');
	if(!in_array($align, $alignArray)) $align = 'left';
	$randomId =$shortcode_id == '' ? 'frb_h_'.rand() : $shortcode_id;
	$bot_margin = (int)$bot_margin;
	$font_size = (int)$font_size;
	$line_height = (int)$line_height;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	return ($content != '' && $content != null ? $animSpeedSet.'<'.$type.' id="'.$randomId.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important; margin-top:0 !important; text-align:'.$align.'; color:'.$text_color.';'.($custom_font_size == 'true' ? ' font-size:'.$font_size.'px; line-height:'.$line_height.'px;' : '').'">'.$content.'</'.$type.'>' : '');
}
add_shortcode( 'fbuilder_h', 'fbuilder_h' );

function fbuilder_more($atts) {
	return '<!--more--><div class="frb_more_tag"></div>';
}
add_shortcode( 'fbuilder_more', 'fbuilder_more' );

function fbuilder_text( $atts, $content=null ) {
	extract (shortcode_atts( array(
		'autop' => 'true',
		'bot_margin' => 24,
		'custom_font_size' => 'false',
		'font_size' => 12,
		'line_height' => 14,
		'align' => 'left',
		'text_color' => '#232323',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts));
	
	$randomId =$shortcode_id == '' ? 'frb_text_'.rand() : $shortcode_id;
	$content2 = '';
	$scriptTag = false;
	if($autop == 'true') {
		$scriptpos = mb_strpos($content, '<script');
		while($scriptpos != false) {
			$content2 .= nl2br(mb_substr($content, 0, $scriptpos));
			$content = mb_substr($content, $scriptpos);
			
			$scrclosepos = mb_strpos($content, '/script>');
			if($scrclosepos != false) {
				$content2 .= mb_substr($content, 0, $scrclosepos+8);
				$content = mb_substr($content, $scrclosepos+8);
			}
			else {
				$content2 .= $content;
				$content = '';
			}
			$scriptpos = mb_strpos($content, '<script');
		}
		$content = $content2 . nl2br($content);
	}
	
	$alignArray = array('left', 'right', 'center');
	if(!in_array($align, $alignArray)) $align = 'left';
	
	$html = '<div class="frb_text" style="color:'.$text_color.';">'.do_shortcode($content).'</div>';
	
	$bot_margin = (int)$bot_margin;
	$font_size = (int)$font_size;
	$line_height = (int)$line_height;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	$html = $animSpeedSet.'<div id="'.$randomId.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important; text-align:'.$align.';'.($custom_font_size == 'true' ? ' font-size:'.$font_size.'px; line-height:'.$line_height.'px;' : '').'">'.$html.'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_text', 'fbuilder_text' );


function fbuilder_image ( $atts, $content=null ) {
		extract (shortcode_atts( array(
		'desc' => '',
		'text_align' => 'center',
		'link' => '',
		'link_type' => 'lightbox-image',
		'iframe_width' => '600',
		'iframe_height' => '300',
		'hover_icon' => 'ba-search',
		'hover_icon_size' => 30,
		'hover_shade_color' => '#000000',
		'hover_shade_opacity' => '0.4',
		'bot_margin' => 24,
		'round' => 'false',
		'image_width' => '600px',
		'image_height' => '300px',
		'custom_dimensions' => 'false',
		'border' => 'false',
		'border_color' => '#376a6e',
		'desc_color' => '#376a6e',
		'back_color' => '#ebecee',
		'border_hover_color' => '#376a6e',
		'desc_hover_color' => '#376a6e',
		'back_hover_color' => '#ebecee',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts ));
	$desc = nl2br($desc);
	$alignArray = array('left', 'right', 'center');
	$hover_shade_opacity = (float)$hover_shade_opacity;
	$hover_shade_opacity = $hover_shade_opacity > 1 ? 1 : $hover_shade_opacity;
	$hover_shade_opacity = $hover_shade_opacity < 0 ? 0 : $hover_shade_opacity;
	if(!in_array($text_align, $alignArray)) $text_align = 'center';
	if($border_color == '') $border_color = 'transparent';
	if($back_color == '') $back_color = 'transparent';
	if($desc_color == '') $desc_color = 'transparent';
	if($border_hover_color == '') $border_hover_color = 'transparent';
	if($desc_hover_color == '') $desc_hover_color = 'transparent';
	if($back_hover_color == '') $back_hover_color = 'transparent';
	$hover_icon_size = (int)$hover_icon_size;
	$iframe_width = (int)$iframe_width;
	$iframe_height = (int)$iframe_height;
	$image_width = (int)$image_width;
	$image_height = (int)$image_height;
	$custom_dimensions = $custom_dimensions =='true' ? true : false;
	$randomId =$shortcode_id == '' ? 'frb_image_'.rand() : $shortcode_id;
	
	$width = $custom_dimensions ? ('width:'.$image_width.'px; ') : '';
	$height = $custom_dimensions ? ('height:'.$image_height.'px !important;') : '';
	
	$html = '<span class="frb_image_inner'.($border != 'false' ? ' frb_image_border' : '').($round != 'false' ? ' frb_image_round' : '').'"  style="border-color:'.$border_color.';" data-bordercolor="'.$border_color.'" data-borderhover="'.$border_hover_color.'">
		<img'.($round != 'false' ? ' class="frb_image_round' : ' class="frb_image_flat').'" src="'.$content.'" alt="" '.($text_align == 'center' ? 'style="border-color:'.$border_color.'; margin:0 auto;'.$width.$height.'"' : ($text_align == 'right' ? 'style="border-color:'.$border_color.'; '.$width.$height.' float:right;"':'style="border-color:'.$border_color.'; '.$width.$height.'"')).' />
		<span style="clear:both; display:block;"></span>';
		if($link != '') {$html .= '<span class="frb_image_hover" data-transparency="'.$hover_shade_opacity.'"></span>';}
	
	
	
	if(substr($hover_icon,0,4) == 'icon') {
		$html .= '<i class="fawesome '.$hover_icon.'" style="line-height:'.$hover_icon_size.'px; font-size:'.$hover_icon_size.'px; height:'.$hover_icon_size.'px; width:'.$hover_icon_size.'px; margin:'.(-$hover_icon_size/2).'px "></i>';
	}
	else {
		$html .= '<i class="frb_icon '.substr($hover_icon,0,2).' '.$hover_icon.'" style="line-height:'.$hover_icon_size.'px; font-size:'.$hover_icon_size.'px; height:'.$hover_icon_size.'px; width:'.$hover_icon_size.'px; margin:'.(-$hover_icon_size/2).'px "></i>';
	}
	
	$html .= '</span>';
	
	if($desc != '') $html .= '<span class="frb_image_desc'.($round != 'false' ? ' frb_image_round' : '').'" style="color:'.$desc_color.'; text-align:'.$text_align.'; background:'.$back_color.'" data-color="'.$desc_color.'" data-hovercolor="'.$desc_hover_color.'" data-backcolor="'.$back_color.'" data-backhover="'.$back_hover_color.'">'.$desc.'</span>';
	
	if($link != '') {	
		switch($link_type) {
			case 'new-tab' : $lightbox = '" target="_blank'; break;
			case 'lightbox-image' : $lightbox = ' frb_lightbox_link" rel="frbprettyphoto'; break;
			case 'lightbox-iframe' : $lightbox = ' frb_lightbox_link" rel="frbprettyphoto'; $link .= '?iframe=true&width='.$iframe_width.'&height='.$iframe_height;
			break;
			default : $lightbox = '';
		}
		$html = '<a class="'.$lightbox.'" style="display:inline-block;" href='.$link.'>'.$html.'</a>';
	}
	
		$html = '<span style="text-align:'.$text_align.'; width:100%; display:block; line-height:0;">'.$html.'</span>';
	
	
	$html = '<div class="frb_image" style="'.$height.'">'.$html.'</div>';
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
	$style = '<style type="text/css" scoped="scoped">'.
				'#'.$randomId.' .frb_image_hover {background:'.$hover_shade_color.';}'.
			'</style>';
		
		
	$html = $animSpeedSet.$style.'<div id="'.$randomId.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_image', 'fbuilder_image' );


function fbuilder_video($atts) {
    extract(shortcode_atts(array(
        'url' => '',
		'auto_width' => 'true',
		'bot_margin' => 24,
		'width' => '620',
		'height' => '310',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
    ), $atts));

	$randomId =$shortcode_id == '' ? 'frb_video_'.rand() : $shortcode_id;
	$width = (int)$width;
	$height = (int)$height;
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		

	$html = $animSpeedSet.'<div id="'.$randomId.'" class="'.$class.' frb_video_wrapper'.($auto_width == 'true' ? ' frb_auto_width' : '').$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.fbuilder_get_video_iframe($url, $width, $height).'</div>';
	
    return $html;
}
add_shortcode('fbuilder_video', 'fbuilder_video');

function fbuilder_get_video_iframe($url, $width, $height){
	preg_match('/^(https?:\/\/)(www\.)?([^\/]+)(\.com)/i', $url, $matches);
	
	if ($matches[3] == 'youtube'){
		preg_match('/^(https?:\/\/)?(www\.)?youtube\.com\/(watch\?v=)?(v\/)?([^&]+)/i', $url, $matches);
		$match = $matches[5];
		
		$html = '<iframe src="http://www.youtube.com/embed/'.$match.'?rel=0&amp;hd=1" frameborder="0"';
		if ($width){
			$html .= ' width="'.$width.'"';
		} else {
			$html .= ' width="620"';
		}
		if ($height){
			$html .= ' height="'.$height.'"';
		} else {
			$html .= ' height="400"';
		}
		$html .= '></iframe>';
		
		return $html;
	} elseif ($matches[3] == 'vimeo'){
		preg_match('/^(https?:\/\/)?(www\.)?vimeo\.com\/([^\/]+)/i', $url, $matches);
		$match = $matches[3];
		
		$html = '<iframe src="http://player.vimeo.com/video/'.$match.'?title=0&amp;byline=0&amp;portrait=0" frameborder="0"';
		if ($width){
			$html .= ' width="'.$width.'"';
		} else {
			$html .= ' width="620"';
		}
		if ($height){
			$html .= ' height="'.$height.'"';
		} else {
			$html .= ' height="400"';
		}
		$html .= '></iframe>';
		
		return $html;
	} elseif ($matches[3] == 'dailymotion'){
		preg_match('/^(https?:\/\/)?(www\.)?dailymotion\.com\/(video\/)?([^_]+)/i', $url, $matches);
		$match = $matches[4];
		
		$html .= '<iframe src="http://www.dailymotion.com/embed/video/'.$match.'?hideInfos=1" frameborder="0"';
		if ($width){
			$html .= ' width="'.$width.'"';
		} else {
			$html .= ' width="620"';
		}
		if ($height){
			$html .= ' height="'.$height.'"';
		} else {
			$html .= ' height="400"';
		}
		$html .= '></iframe>';
		
		return $html;
	} elseif ($matches[3] == 'screenr'){
		preg_match('/^(https?:\/\/)?(www\.)?screenr\.com\/([^\/]+)/i', $url, $matches);
		$match = $matches[3];
		
		$html .= '<iframe src="http://www.screenr.com/embed/'.$match.'" frameborder="0"';
		if ($width){
			$html .= ' width="'.$width.'"';
		} else {
			$html .= ' width="620"';
		}
		if ($height){
			$html .= ' height="'.$height.'"';
		} else {
			$html .= ' height="400"';
		}
		$html .= '></iframe>';
		
		return $html;
	} else {
		return '<br /><h2 style="text-align:center;">Unknown type of the video. Check your video link.</h2><br />';
	}
}


function fbuilder_sidebar( $atts ) {
	extract (shortcode_atts( array( 
		'name' => '1',
		'bot_margin' => 0,
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts ));
	
	$html =  '<div id="' . str_replace( " ", "_", $name ) . '" class="frb_sidebar">';
	ob_start();
	if ( ! function_exists('dynamic_sidebar') || ! dynamic_sidebar($name) ) {}
	$html .= ob_get_contents();
	ob_end_clean();
	$randomId =$shortcode_id == '' ? 'frb_sidebar_'.rand() : $shortcode_id;
	$html .= '</div>';
		
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	$html = $animSpeedSet.'<div id="'.$randomId.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_sidebar', 'fbuilder_sidebar' );


function fbuilder_nav_menu( $atts, $content=null ) {
	extract (shortcode_atts( array(
		'wp_menu' => '',
		'type' => 'horizontal-clean',
		'menu_title' => 'Nav menu',
		'bot_margin' => 24,
		'text_color' => '#232323',
		'hover_color' => '#27a8e1',
		'hover_text_color' => '#ffffff',
		'back_color' => '',
		'sub_back_color' => '#f4f4f4',
		'sub_text_color' => '#232323',
		'separator_color' => '#ebecee',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts));
	
	$randomId = rand();
	$navArgs = array(
		'menu'            => $wp_menu,
		'container'       => '',
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => 'frb_menu frb_menu_'.$type,
		'menu_id'         => '',
		'echo'            => false,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul id="%1$s" class="%2$s" data-textcolor="'.$text_color.'" data-hovercolor="'.$hover_color.'" data-hovertextcolor="'.$hover_text_color.'"  data-subtextcolor="'.$sub_text_color.'">%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
	);

	$html = '
	<style type="text/css" scoped="scoped">
		#frb_menu'.$randomId.' {
			background:'.$back_color.';
		}
		#frb_menu'.$randomId.' ul.frb_menu a {
			color:'.$text_color.';
		}
		#frb_menu'.$randomId.' ul.frb_menu ul.sub-menu a {
			color:'.$sub_text_color.';
		}
		#frb_menu'.$randomId.' .frb_menu_header {
			color:'.$text_color.';
			border-bottom:1px solid '.$hover_color.';
		}
		#frb_menu'.$randomId.' ul.frb_menu ul.sub-menu li {
			background:'.$sub_back_color.';
		}
		
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_horizontal-clean ul.sub-menu:before {
			border-bottom-color:'.$hover_color.';
		}
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_horizontal-clean ul.sub-menu:after {
			border-bottom-color:'.$sub_back_color.';
		}
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_horizontal-clean ul.sub-menu a {
			border-top:1px solid '.$separator_color.';
		}
		
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_horizontal-clean ul.sub-menu li:first-child a {
			border-top:1px solid '.$hover_color.';
		}
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_horizontal-squared ul.sub-menu:before,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_horizontal-squared ul.sub-menu:after,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_horizontal-rounded ul.sub-menu:before,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_horizontal-rounded ul.sub-menu:after {
			border-bottom-color:'.$sub_back_color.';
		}
		
		#frb_menu'.$randomId.'.frb_menu_container_vertical-clean .frb_menu_header,
		#frb_menu'.$randomId.'.frb_menu_container_vertical-squared .frb_menu_header,
		#frb_menu'.$randomId.'.frb_menu_container_vertical-rounded .frb_menu_header{
			color:'.$text_color.';
		}
		#frb_menu'.$randomId.'.frb_menu_container_vertical-clean ul.frb_menu,
		#frb_menu'.$randomId.'.frb_menu_container_vertical-squared ul.frb_menu,
		#frb_menu'.$randomId.'.frb_menu_container_vertical-rounded ul.frb_menu {
			background:'.$sub_back_color.';
		}
		#frb_menu'.$randomId.'.frb_menu_container_vertical-clean ul.frb_menu a,
		#frb_menu'.$randomId.'.frb_menu_container_vertical-squared ul.frb_menu a,
		#frb_menu'.$randomId.'.frb_menu_container_vertical-rounded ul.frb_menu a,
		#frb_menu'.$randomId.'.frb_menu_container_vertical-clean ul.frb_menu ul.sub-menu a,
		#frb_menu'.$randomId.'.frb_menu_container_vertical-squared ul.frb_menu ul.sub-menu a,
		#frb_menu'.$randomId.'.frb_menu_container_vertical-rounded ul.frb_menu ul.sub-menu a  {
			color:'.$sub_text_color.';
		}
		
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_vertical-clean a,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_vertical-squared a,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_vertical-rounded a,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_vertical-clean > li > ul.sub-menu,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_vertical-squared > li > ul.sub-menu,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_vertical-rounded > li > ul.sub-menu {
			border-top:1px solid '.$separator_color.';
		}
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_vertical-clean li:first-child a,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_vertical-squared li:first-child a,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_vertical-rounded li:first-child a,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_vertical-clean ul.sub-menu a,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_vertical-squared ul.sub-menu a,
		#frb_menu'.$randomId.' ul.frb_menu.frb_menu_vertical-rounded ul.sub-menu a {
			border-top:0;
		}
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-clean .frb_menu_header,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-squared .frb_menu_header,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-rounded .frb_menu_header {
			color:'.$text_color.';
			border:0;
		}
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-clean .frb_menu_header:before,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-squared .frb_menu_header:before,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-rounded .frb_menu_header:before {
			background:'.$text_color.';
		}
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-clean .frb_menu_header:after,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-squared .frb_menu_header:after,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-rounded .frb_menu_header:after {
			border-top-color:'.$text_color.';
		}
		
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-clean ul.frb_menu:before {
			border-bottom-color:'.$hover_color.';
		}
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-clean ul.frb_menu:after {
			border-bottom-color:'.$sub_back_color.';
		}
		
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-squared ul.frb_menu:before,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-squared ul.frb_menu:after,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-rounded ul.frb_menu:before,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-rounded ul.frb_menu:after {
			border-bottom-color:'.$sub_back_color.';
			
		}
		
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-clean ul.frb_menu li,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-squared ul.frb_menu li,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-rounded ul.frb_menu li {
			background:'.$sub_back_color.';
		}
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-clean ul.frb_menu li a {
			color:'.$sub_text_color.';
			border-top:1px solid '.$separator_color.';
		}
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-squared ul.frb_menu li a,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-rounded ul.frb_menu li a {
			color:'.$sub_text_color.';
		}
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-squared ul.frb_menu li a,
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-rounded ul.frb_menu li a {
			color:'.$sub_text_color.';
		}
		#frb_menu'.$randomId.'.frb_menu_container_dropdown-clean ul.frb_menu li:first-child a {
			border-top:1px solid '.$hover_color.';
		}
		
		
	</style>
	';
	$html .= '<div class="frb_menu_container frb_menu_container_'.$type.'" id="frb_menu'.$randomId.'">';
	if($type != 'horizontal-clean' && $type != 'horizontal-squared' && $type != 'horizontal-rounded' && $menu_title != '') {
		$html .= '<div class="frb_menu_header">'.$menu_title.'</div>';
	}
	
	$html .= wp_nav_menu( $navArgs ).'</div>';
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#frb_menu'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#frb_menu'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
	$html = $animSpeedSet.'<div id="#frb_menu'.$randomId.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
	
	return $html;
}
add_shortcode( 'fbuilder_nav_menu', 'fbuilder_nav_menu' );


//			fbuilder post

function fbuilder_post( $atts, $content=null) {
	extract (shortcode_atts( array(
		'id' => '1',
		'hover_icon' => 'ba-search',
		'button_text' => 'Read more',
		'style' => 'clean',
		'bot_margin' => 24,
		'back_color' => '',
		'border_color' => '#27a8e1',
		'button_color' => '#27a8e1',
		'button_text_color' => '#ffffff',
		'button_hover_color' => '#57bce8',
		'button_text_hover_color' => '#ffffff',
		'head_color' => '#232323',
		'meta_visible' => true,
		'meta_color' => '#232323',
		'meta_hover_color' => '#27a8e1',
		'text_color' => '#232323',
		'excerpt_lenght' => 150,
		'link_type' => 'post',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts));
	
	$color_array = array(
		'back_color',
		'border_color',
		'button_color',
		'button_text_color',
		'button_hover_color',
		'button_text_hover_color',
		'head_color',
		'meta_color',
		'meta_hover_color',
		'text_color'
	);
	foreach($color_array as $color) {
		if($$color == '') $$color = 'transparent';
	}
	global $fbuilder;
	$randomId = rand();
	$link_type = ($link_type == 'prettyphoto') ? 'prettyphoto' : 'post';
	$meta_visible = $meta_visible === 'true' ? true : false;
	$id = (int)$id;
	$post = get_post($id);
	$thumb = get_the_post_thumbnail($id,'full');
	$url = $link_type == 'post' ? get_permalink($id) : wp_get_attachment_url(get_post_thumbnail_id($id));
	$author_id = $post->post_author;
	$comments = $post->comment_count;
	
	$content = $fbuilder->strip_html_tags($post->post_content);
	
	$content = mb_strlen($content) >= $excerpt_lenght ? mb_substr($content , 0, $excerpt_lenght - mb_strlen($content)).'...' : $content;
	
	if($comments == 1) $comments .= ' comment';
	else $comments .= ' comments';
	
	$html = '<span class="frb_image_inner">'.$thumb.'<span class="frb_image_hover"></span>';
	
	if(substr($hover_icon,0,4) == 'icon') {
		$html .= '<i class="fawesome '.$hover_icon.'"></i>';
	}
	else {
		$html .= '<i class="frb_icon '.substr($hover_icon,0,2).' '.$hover_icon.'"></i>';
	}
	$html .= '</span>';
	
	$html = '<a class="lightbox-image" href="'.$url.'" '.($link_type == 'prettyphoto' ? 'rel="frbprettyphoto"' : '').'>'.$html.'</a>';
	
	$html .= '<a class="frb_post_title" href="'.get_permalink($id).'"><h3>'.$post->post_title.'</h3></a>';
	
	$html .= $meta_visible ? '<div class="frb_post_meta"><span class="frb_date">'.get_the_time('m. d. Y.', $id).'</span> | '.'<a class="frb_author" href="mailto:'.get_the_author_meta( 'user_email' , $author_id ).'">'.get_the_author_meta( 'user_nicename' , $author_id ).'</a> | '.'<a href="'.$url.'">'.$comments.'</a></div>' : '';
	
	$html .= '<div class="frb_post_content">'.$content.'</div>';
		
	$button_style = 'style="'.
		'color:'.$button_text_color.'; '.
		'background:'.$button_color.'; '.
		'border-color:'.$button_color.'" '.
		
		'data-textcolor="'.$button_text_color.'" '.
		'data-backcolor="'.$button_color.'" '.
		'data-hovertextcolor="'.$button_text_hover_color.'" '.
		'data-hoverbackcolor="'.$button_hover_color.'"';

	$round = ($style == 'rounded' ? ' frb_round' : '');

	$html .= '<a class="frb_button'.$round.'" href="'.$url.'" '.$button_style.'>'.$button_text.'</a>';
	
	
	$html = '<div id="frb_post'.$randomId.'" class="frb_post frb_post_'.$style.' frb_image'.($border_color != 'transparent' ? ' frb_image_border' : '').'">'.$html.'</div>';
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#frb_post'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#frb_post'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	$post_style = '
	<style type="text/css" scoped="scoped">
		#frb_post'.$randomId.' {
			color: '.$text_color.';
			border-color:'.$border_color.'; 
			'.($style != 'clean' ? 'background:'.$back_color : '').'
		}
		
		#frb_post'.$randomId.' h3 {
			color:'.$head_color.';
		}
		
		#frb_post'.$randomId.' .frb_post_meta,
		#frb_post'.$randomId.' .frb_post_meta a{
			color: '.$meta_color.';
		}
		#frb_post'.$randomId.' .frb_post_meta a:hover {
			color: '.$meta_hover_color.'
		}
	</style>';
	
	$html = $animSpeedSet.$post_style.'<div id="#frb_post'.$randomId.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
	
	
	return $html;
}
add_shortcode( 'fbuilder_post', 'fbuilder_post' );



/* ------------------ */
/* fbuilder_recent_post */
/* ------------------ */
function fbuilder_recent_post( $atts, $content=null) {
	extract (shortcode_atts( array(
		'offset' => '0',
		'hover_icon' => 'ba-search',
		'button_text' => 'Read more',
		'style' => 'clean',
		'bot_margin' => 24,
		'back_color' => '',
		'border_color' => '#27a8e1',
		'button_color' => '#27a8e1',
		'button_text_color' => '#ffffff',
		'button_hover_color' => '#57bce8',
		'button_text_hover_color' => '#ffffff',
		'head_color' => '#232323',
		'meta_visible' => true,
		'meta_color' => '#232323',
		'meta_hover_color' => '#27a8e1',
		'text_color' => '#232323',
		'excerpt_lenght' => 150,
		'link_type' => 'post',
		'class' => '',
		'shortcode_id' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts));
	
	$color_array = array(
		'back_color',
		'border_color',
		'button_color',
		'button_text_color',
		'button_hover_color',
		'button_text_hover_color',
		'head_color',
		'meta_color',
		'meta_hover_color',
		'text_color'
	);
	foreach($color_array as $color) {
		if($$color == '') $$color = 'transparent';
	}
	global $fbuilder;
	$randomId = rand();
	$link_type = ($link_type == 'prettyphoto') ? 'prettyphoto' : 'post';
	$meta_visible = $meta_visible === 'true' ? true : false;

	$offset = (int)$offset;

	$args = array(
	    'numberposts' => 1,
	    'orderby' => 'post_date',
	    'offset' => $offset,
	    'order' => 'DESC',
	    'post_type' => 'post',
	    'post_status' => 'publish' 
	);

	$recent_post = wp_get_recent_posts($args, OBJECT);
	
	if (!$recent_post)
		{
			$thumb = '';
			$url = '#';
			$author_id = __('Sample author','frontend-builder');
			$comments = 0;
			$content = __('Post with that offset isn\'t found','frontend-builder');
		} else {
			$recent_post=$recent_post[0];
			$id = (int)($recent_post->ID);
			$post = $recent_post;
			$thumb = get_the_post_thumbnail($id,'full');
			$url = $link_type == 'post' ? get_permalink($id) : wp_get_attachment_url(get_post_thumbnail_id($id));
			$author_id = $post->post_author;
			$comments = $post->comment_count;
			
			$content = $fbuilder->strip_html_tags($post->post_content);
		}

	$content = mb_strlen($content) >= $excerpt_lenght ? mb_substr($content , 0, $excerpt_lenght - mb_strlen($content)).'...' : $content;
	
	if($comments == 1) $comments .= ' comment';
	else $comments .= ' comments';
	
	$html = '<span class="frb_image_inner">'.$thumb.'<span class="frb_image_hover"></span>';
	
	if(substr($hover_icon,0,4) == 'icon') {
		$html .= '<i class="fawesome '.$hover_icon.'"></i>';
	}
	else {
		$html .= '<i class="frb_icon '.substr($hover_icon,0,2).' '.$hover_icon.'"></i>';
	}
	$html .= '</span>';
	
	$html = '<a class="lightbox-image" href="'.$url.'" '.($link_type == 'prettyphoto' ? 'rel="frbprettyphoto"' : '').'>'.$html.'</a>';
	
	if (!$recent_post) {
		$html .= '<a class="frb_post_title" href="#"><h3>'.__('No such post','frontend-builder').'</h3></a>';
		$html .= $meta_visible === true ? '<div class="frb_post_meta"><span class="frb_date">'.current_time('m. d. Y.').'</span> | '.'<a class="frb_author" href="mailto:#">'.__('Sample author','frontend-builder').'</a> | '.'<a href="'.$url.'">'.$comments.'</a></div>' : '';
	} else {
		$html .= '<a class="frb_post_title" href="'.get_permalink($id).'"><h3>'.$post->post_title.'</h3></a>';
		$html .= $meta_visible === true ? '<div class="frb_post_meta"><span class="frb_date">'.get_the_time('m. d. Y.', $id).'</span> | '.'<a class="frb_author" href="mailto:'.get_the_author_meta( 'user_email' , $author_id ).'">'.get_the_author_meta( 'user_nicename' , $author_id ).'</a> | '.'<a href="'.$url.'">'.$comments.'</a></div>' : '';
	}
	
	$html .= '<div class="frb_post_content">'.$content.'</div>';
		
	$button_style = 'style="'.
		'color:'.$button_text_color.'; '.
		'background:'.$button_color.'; '.
		'border-color:'.$button_color.'" '.
		
		'data-textcolor="'.$button_text_color.'" '.
		'data-backcolor="'.$button_color.'" '.
		'data-hovertextcolor="'.$button_text_hover_color.'" '.
		'data-hoverbackcolor="'.$button_hover_color.'"';

	$round = ($style == 'rounded' ? ' frb_round' : '');

	$html .= '<a class="frb_button'.$round.'" href="'.$url.'" '.$button_style.'>'.$button_text.'</a>';
	
	
	$html = '<div class="frb_post frb_post_'.$style.' frb_image'.($border_color != 'transparent' ? ' frb_image_border' : '').'">'.$html.'</div>';
	
	$bot_margin = (int)$bot_margin;
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style type="text/css" scoped="scoped">'.
							'#frb_post'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#frb_post'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
		
		
	$post_style = '
	<style type="text/css" scoped="scoped">
		#frb_post'.$randomId.' {
			color: '.$text_color.';
			border-color:'.$border_color.'; 
			'.($style != 'clean' ? 'background:'.$back_color : '').'
		}
		
		#frb_post'.$randomId.' h3 {
			color:'.$head_color.';
		}
		
		#frb_post'.$randomId.' .frb_post_meta,
		#frb_post'.$randomId.' .frb_post_meta a{
			color: '.$meta_color.';
		}
		#frb_post'.$randomId.' .frb_post_meta a:hover {
			color: '.$meta_hover_color.'
		}
	</style>';
	
	$html = $animSpeedSet.$post_style.'<div id="frb_post'.$randomId.'" class="'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;">'.$html.'</div>';
	
	
	return $html;
}
add_shortcode( 'fbuilder_recent_post', 'fbuilder_recent_post' );


function fbuilder_pricing ( $atts, $content=null ) {
		extract (shortcode_atts( array(
		'bot_margin' => '24',
		'currency' => '',
		'bot_border' => '',
		'class' => '',
		'shortcode_id' => '',
		'colnum' => '1',
		'services_sidebar' => 'true',
		'row_type' => '',
		'service_label' => '',
		'service_icon' => '',
		'column_1_icon' => '',
		'column_1_text' => '',
		'column_1_price' => '',
		'column_1_interval' => '',
		'column_1_button_text' => '',
		'column_1_button_link' => '',
		'column_2_icon' => '',
		'column_2_text' => '',
		'column_2_price' => '',
		'column_2_interval' => '',
		'column_2_button_text' => '',
		'column_2_button_link' => '',
		'column_3_icon' => '',
		'column_3_text' => '',
		'column_3_price' => '',
		'column_3_interval' => '',
		'column_3_button_text' => '',
		'column_3_button_link' => '',
		'column_4_icon' => '',
		'column_4_text' => '',
		'column_4_price' => '',
		'column_4_interval' => '',
		'column_4_button_text' => '',
		'column_4_button_link' => '',
		'column_5_icon' => '',
		'column_5_text' => '',
		'column_5_price' => '',
		'column_5_interval' => '',
		'column_5_button_text' => '',
		'column_5_button_link' => '',
		'text_color' => '',
		'back_color' => '',
		'column_1_main_color' => '',
		'column_1_hover_color' => '',
		'column_1_button_text_color' => '',
		'column_2_main_color' => '',
		'column_2_hover_color' => '',
		'column_2_button_text_color' => '',
		'column_3_main_color' => '',
		'column_3_hover_color' => '',
		'column_3_button_text_color' => '',
		'column_4_main_color' => '',
		'column_4_hover_color' => '',
		'column_4_button_text_color' => '',
		'column_5_main_color' => '',
		'column_5_hover_color' => '',
		'column_5_button_text_color' => '',
		'animate' => 'none',
		'animation_delay' => 0,
		'animation_speed' => 1000,
		'animation_group' => ''
	), $atts ));
	
	$colnum = (int)$colnum;
	if($services_sidebar == 'true') $colnum++;
	$bot_border = explode('|', $bot_border);
	$row_type = explode('|', $row_type);
	$service_label = explode('|', $service_label);
	$service_icon = explode('|', $service_icon);
	$column_1_icon = explode('|', $column_1_icon);
	$column_1_text = explode('|', $column_1_text);
	$column_1_price = explode('|', $column_1_price);
	$column_1_interval = explode('|', $column_1_interval);
	$column_1_button_text = explode('|', $column_1_button_text);
	$column_1_button_link = explode('|', $column_1_button_link);
	$column_2_icon = explode('|', $column_2_icon);
	$column_2_text = explode('|', $column_2_text);
	$column_2_price = explode('|', $column_2_price);
	$column_2_interval = explode('|', $column_2_interval);
	$column_2_button_text = explode('|', $column_2_button_text);
	$column_2_button_link = explode('|', $column_2_button_link);
	$column_3_icon = explode('|', $column_3_icon);
	$column_3_text = explode('|', $column_3_text);
	$column_3_price = explode('|', $column_3_price);
	$column_3_interval = explode('|', $column_3_interval);
	$column_3_button_text = explode('|', $column_3_button_text);
	$column_3_button_link = explode('|', $column_3_button_link);
	$column_4_icon = explode('|', $column_4_icon);
	$column_4_text = explode('|', $column_4_text);
	$column_4_price = explode('|', $column_4_price);
	$column_4_interval = explode('|', $column_4_interval);
	$column_4_button_text = explode('|', $column_4_button_text);
	$column_4_button_link = explode('|', $column_4_button_link);
	$column_5_icon = explode('|', $column_5_icon);
	$column_5_text = explode('|', $column_5_text);
	$column_5_price = explode('|', $column_5_price);
	$column_5_interval = explode('|', $column_5_interval);
	$column_5_button_text = explode('|', $column_5_button_text);
	$column_5_button_link = explode('|', $column_5_button_link);
	
	$randomId = rand();
	
	$colIdMod = ($services_sidebar == 'true' ? 1 : 0);
	
	if($animate != 'none') {
		$animate = ' frb_animated frb_'.$animate.'"';
		
		if($animation_delay != 0) {
			$animation_delay = (int)$animation_delay;
			$animate .= ' data-adelay="'.$animation_delay.'"';
		}
		if($animation_group != '') {
			$animate .= ' data-agroup="'.$animation_group.'"';
		}
	}
	else
		$animate = '"';
		
		$animSpeedSet = '<style>'.
							'#frb_pricing_anim_'.$randomId.'.frb_onScreen.frb_animated {-webkit-animation-duration: '.(int)$animation_speed.'ms; animation-duration: '.(int)$animation_speed.'ms;}'.
							'#frb_pricing_anim_'.$randomId.'.frb_onScreen.frb_hinge {-webkit-animation-duration: '.((int)$animation_speed*2).'ms; animation-duration: '.((int)$animation_speed*2).'ms;}'.
					'</style>';
	$animSpeedSet = (int)$animation_speed != 0 ? $animSpeedSet : '';
	
	$html = $animSpeedSet.'
	<style>
		'.($text_color != '' ? 'table#frb_pricing_'.$randomId.'.frb_pricing_table {color: '.$text_color.';}' : '').'
		#frb_pricing_'.$randomId.'.frb_pricing_table td {border-right-color:#ffffff;} /* 	cell spacing color				*/
		'.($back_color != '' ? '#frb_pricing_'.$randomId.' .frb_pricing_pale_background {background-color: '.$back_color.';}' : '').'
		#frb_pricing_'.$randomId.'.frb_pricing_table .frb_pricing_row_separator td {background: #dedee0;}
		'.($services_sidebar == 'true' ? '' : '#frb_pricing_'.$randomId.'.frb_pricing_table .frb_pricing_section_responsive {display:none !important;}').'
		'.(($colnum == 1 && $services_sidebar != 'true') || ($colnum == 2 && $services_sidebar == 'true') ? '#frb_pricing_controls_'.$randomId.'.frb_pricing_controls {display:none !important;}': '').'
		';
		
	for($i=1; $i<=($colnum-$colIdMod); $i++) {
		$main_color = 'column_'.$i.'_main_color';
		$hover_color = 'column_'.$i.'_hover_color';
		$button_text_color = 'column_'.$i.'_button_text_color';
		
		$html .= '
		#frb_pricing_'.$randomId.' .frb_pricing_main_color'.($i+$colIdMod).' {background: '.$$main_color.'; color: '.$$button_text_color.'; transition:background-color 300ms;}
		#frb_pricing_'.$randomId.' .frb_pricing_main_color'.($i+$colIdMod).'-hover:hover {background: '.$$hover_color.'; color: '.$$button_text_color.'; transition:background-color 300ms;}
		';
	}
	$html .= '
	</style>';
	
	$html .= '
	<table cellspacing="0" class="frb_pricing_table frb_pricing_table_'.$colnum.'col" id="frb_pricing_'.$randomId.'">';
	
	
	if(is_array($row_type)){
		for($i=0; $i<count($row_type); $i++) {
			
		
		$html.= '
		<tr class="'.($row_type[$i] == 'text-button' ? 'frb_pricing_row_text_button' : '').($row_type[$i] == 'heading' || $row_type[$i] == 'border' ? 'frb_pricing_row_no_padding' : '').($row_type[$i] == 'section' ? 'frb_pricing_row_section' : '').($row_type[$i] == 'heading' ? ' frb_pricing_row_heading' : '').'">';
		
		for($j=0; $j<$colnum; $j++) {
			$ind = ($services_sidebar == 'true' ? $j : $j+1);
			$var_names = array(
				'icon' => 'column_'.$ind.'_icon',
				'text' => 'column_'.$ind.'_text',
				'price' => 'column_'.$ind.'_price',
				'interval' => 'column_'.$ind.'_interval',
				'button_text' => 'column_'.$ind.'_button_text',
				'button_link' => 'column_'.$ind.'_button_link'
			);
			
			if($j == 0 && $services_sidebar == 'true') {
				$slabel_flag = ($row_type[$i] != 'heading' && $row_type[$i] != 'price' && $row_type[$i] != 'button' && $row_type[$i] != 'border');
				
				$html .= '
			<td class="frb_pricing_column1 frb_pricing_column_label'.($slabel_flag && $service_label[$i] != '' ? ' frb_pricing_pale_background' : '').'">
				<div'.($row_type[$i] == 'text-button' ? ' class="frb_pricing_large_font"' : '').'>';
				if($slabel_flag && $service_label[$i] != '') {
					if($row_type[$i] == 'section' ) {
						if(substr($service_icon[$i],0,4) == 'icon') {
							$html .='<i class="'.$service_icon[$i].' fawesome frb_pricing_fawesome"></i> ';
						}
						else {
							$html .='<i class="'.$service_icon[$i].' '.substr($service_icon[$i],0,2).' frb_icon frb_pricing_fawesome"></i> ';
						}
						
					}	
					$html .= $service_label[$i];
				}
				$html .= '</div>
			</td>';
			}
			else {
				$html .= '	
			<td class="frb_pricing_column'.($j+1).' frb_pricing_pale_background">';
				switch($row_type[$i]){
					case 'heading' : 
					$textj = $$var_names['text'];
					$html .= '
				<div class="frb_pricing_table_category_tag frb_pricing_main_color'.($j+1).'">'.$textj[$i].'</div>';
					break;
					
					case 'price' :
					$pricej = $$var_names['price'];
					$intervalj = $$var_names['interval'];
					$html .= '
				<div class="frb_pricing_table_price" style="clear:both;"><div>'.$currency.'</div><span><div>'.$pricej[$i].'</div><span>'.$intervalj[$i].'</span></span></div>
					';
					break;
					
					case 'button' :
					$button_textj = $$var_names['button_text'];
					$button_linkj = $$var_names['button_link'];
					$html .= '
				<a href="'.$button_linkj[$i].'" class="frb_pricing_table_button frb_pricing_main_color'.($j+1).' frb_pricing_main_color'.($j+1).'-hover">'.$button_textj[$i].'</a>';
					break;
					
					case 'text-button' :
					$textj = $$var_names['text'];
					$button_textj = $$var_names['button_text'];
					$button_linkj = $$var_names['button_link'];
					$html .= '
				<div>'.$textj[$i].'</div>
				<a href="'.$button_linkj[$i].'" class="frb_pricing_table_button frb_pricing_main_color'.($j+1).' frb_pricing_main_color'.($j+1).'-hover">'.$button_textj[$i].'</a>';
					break;
					
					case 'border' :
					$textj = $$var_names['text'];
					$iconj = $$var_names['icon'];
					$html .= '
				<div class="frb_pricing_main_color'.($j+1).' frb_pricing_colored_line"></div>';
					break;
					
					case 'service' :
					$textj = $$var_names['text'];
					$iconj = $$var_names['icon'];
					$html .= '
				<div><strong class="frb_pricing_label_responsive">'.($services_sidebar =='true' ? $service_label[$i] : '').'</strong>';
				
				if($iconj[$i] != '' && $iconj[$i] != 'no-icon') {
					if(substr($iconj[$i],0,4) == 'icon') {
						$html .= '<i class="'.$iconj[$i].' fawesome frb_pricing_fawesome"></i>';
					}
					else {
						$html .= '<i class="'.$iconj[$i].' '.substr($iconj[$i],0,2).' frb_icon frb_pricing_fawesome"></i>';
					}
				}
				else {
					$html .= $textj[$i];
				}
				$html .= '
				</div>';
					break;
					
					case 'section' :
					if(substr($service_icon[$i],0,4) == 'icon') {
						$html .= '
				<div class="frb_pricing_section_responsive"><i class="'.$service_icon[$i].' fawesome frb_pricing_fawesome"></i> '.$service_label[$i].'</div>';
					}
					else {
						$html .= '
				<div class="frb_pricing_section_responsive"><i class="'.$service_icon[$i].' '.substr($service_icon[$i],0,2).' frb_icon frb_pricing_fawesome"></i> '.$service_label[$i].'</div>';
					}
					break;
				}
				$html .= '	
			</td>';
			}
		}
		
		
		$html .= '
		</tr>';
		
		if(isset($bot_border[$i]) && $bot_border[$i] == 'true') {
			$html .= '
				<tr class="frb_pricing_row_separator">';
				for($j=0; $j<$colnum; $j++) {
					$html .= '
					<td class="frb_pricing_column'.$j.' '.($j == 0 && $services_sidebar == 'true' ? 'frb_pricing_column_label' : '').'"></td>';
				}
			$html .= '
				</tr>';
		}
		
		}
	}
	
	$html .= '	
		
	</table>';

	$bot_margin = (int)$bot_margin;
	
		
		
	if($services_sidebar == 'true') $colnum--;
	$html = '<div data-colnum="'.$colnum.'" class="frb_pricing_container frb_pricing_container_'.$colnum.'col'.$class.$animate.' style="padding-bottom:'.$bot_margin.'px !important;" id="frb_pricing_anim_'.$randomId.'"><div id="frb_pricing_controls_'.$randomId.'" class="frb_pricing_controls"><a href="#" class="frb_pricing_left"><i class="ba ba-chevron-left fawesome" ></i></a><a href="#" class="frb_pricing_right"><i class="ba ba-chevron-right fawesome" ></i></a></div>'.(do_shortcode($html)).'</div>';
	return $html;
}
add_shortcode( 'fbuilder_pricing', 'fbuilder_pricing' );



?>