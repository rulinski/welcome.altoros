<?php


$fbuilder_controls = array(

	'group_general' => array(
		'type' => 'collapsible',
		'label' => __('Dimensions','frontend-builder'),
		'open' => 'true',
		'options' => array(
			'full_width' => array(
				'type' => 'checkbox',
				'label' => __('Full width row','frontend-builder'),
				'std' => 'false'
				
			),
			'padding_top' => array(
				'type' => 'number',
				'label' => __('Top padding','frontend-builder'),
				'std' => 36,
				'max' => 300,
				'unit' => 'px'
			),
			'padding_bot' => array(
				'type' => 'number',
				'label' => __('Bottom padding','frontend-builder'),
				'std' => 36,
				'max' => 300,
				'unit' => 'px'
			),
			'column_padding' => array(
				'type' => 'number',
				'label' => __('Column padding','frontend-builder'),
				'std' => 0,
				'unit' => 'px'
			)
		)
	),
	'group_background' => array(
		'type' => 'collapsible',
		'label' => __('Background','frontend-builder'),
		'open' => 'true',
		'options' => array(
			'back_type' => array(
				'type' => 'select',
				'label' => __('Type','frontend-builder'),
				'label_width' => 0.25,
				'control_width' => 0.75,
				'std' => 'static',
				'options' => array(
					'static' => __('Static','frontend-builder'),
					'parallax' => __('Fixed','frontend-builder'),
					'parallax_animated' => __('Parallax','frontend-builder'),
					'parallax_fade' => __('Fade Parallax','frontend-builder'),
					'parallax_scale' => __('Scale Parallax','frontend-builder'),
					'parallax_scale_fade' => __('Scale & Fade Parallax','frontend-builder'),
					'video' => __('Video','frontend-builder')
					)
			),
			'back_color' => array(
				'type' => 'color',
				'label' => __('Color','frontend-builder'),
				'label_width' => 0.25,
				'control_width' => 0.75,
				'std' => $this->option('row_back_color')->value,
				'hide_if' => array(
					'back_type' => array('video', 'video_fixed', 'video_parallax')
				)
				
			),
			'back_image' => array(
				'type' => 'image',
				'label' => __('Image','frontend-builder'),
				'label_width' => 0.25,
				'control_width' => 0.75,
				'std' => '',
				'hide_if' => array(
					'back_type' => array('video', 'video_fixed', 'video_parallax')
				)
			),
			'back_repeat' => array(
				'type' => 'checkbox',
				'label' => __('Repeat image','frontend-builder'),
				'std' => 'true',
				'hide_if' => array(
					'back_type' => array('video', 'video_fixed', 'video_parallax')
				)
			)
		)
	),
	'group_column_back' => array(
		'type' => 'collapsible',
		'label' => __('Column Background','frontend-builder'),
		'open' => 'true',
		'options' => array(
			'column_back' => array(
				'type' => 'color',
				'label' => __('Color','frontend-builder'),
				'std' => $this->option('column_back_color')->value
			),
			'column_back_opacity' => array(
				'type' => 'number',
				'label' => __('Opacity','frontend-builder'),
				'std' => 100,
				'unit' => '%'
			)
		)
	),
	'group_video_back' => array(
		'type' => 'collapsible',
		'label' => __('Video Background','frontend-builder'),
		'options' => array(
			'back_video_source' => array(
				'type' => 'select',
				'label' => __('Source','frontend-builder'),
				'std' => 'youtube',
				'options' => array(
					'youtube' => __('Youtube','frontend-builder'),
					'vimeo' => __('Vimeo','frontend-builder'),
					'html5' => __('HTML5','frontend-builder')
				),
				'hide_if' => array(
					'back_type' => array('static', 'parallax', 'parallax_animated')
				)
			),
			'back_video_youtube_id' => array(
				'type' => 'input',
				'label' => __('Youtube video ID','frontend-builder'),
				'desc' => 'example: tDvBwPzJ7dY',
				'std' => '',
				'hide_if' => array(
					'back_type' => array('static', 'parallax', 'parallax_animated'),
					'back_video_source' => array('vimeo', 'html5')
				)
			),
			'back_video_vimeo_id' => array(
				'type' => 'input',
				'label' => __('Vimeo video ID','frontend-builder'),
				'desc' => 'example: 30300114',
				'std' => '',
				'hide_if' => array(
					'back_type' => array('static', 'parallax', 'parallax_animated'),
					'back_video_source' => array('youtube', 'html5')
				)
			),
			'back_video_html5_img' => array(
				'type' => 'input',
				'label' => __('Image poster url','frontend-builder'),
				'desc' => 'If no video is supported',
				'std' => '',
				'hide_if' => array(
					'back_type' => array('static', 'parallax', 'parallax_animated'),
					'back_video_source' => array('youtube', 'vimeo')
				)
			),
			'back_video_html5_mp4' => array(
				'type' => 'input',
				'label' => __('MP4 url','frontend-builder'),
				'std' => '',
				'hide_if' => array(
					'back_type' => array('static', 'parallax', 'parallax_animated'),
					'back_video_source' => array('youtube', 'vimeo')
				)
			),
			'back_video_html5_webm' => array(
				'type' => 'input',
				'label' => __('WEBM url','frontend-builder'),
				'std' => '',
				'hide_if' => array(
					'back_type' => array('static', 'parallax', 'parallax_animated'),
					'back_video_source' => array('youtube', 'vimeo')
				)
			),
			'back_video_html5_ogv' => array(
				'type' => 'input',
				'label' => __('OGV url','frontend-builder'),
				'std' => '',
				'hide_if' => array(
					'back_type' => array('static', 'parallax', 'parallax_animated'),
					'back_video_source' => array('youtube', 'vimeo')
				)
			),
			'back_video_loop' => array(
				'type' => 'checkbox',
				'label' => __('Loop video','frontend-builder'),
				'std' => 'true',
				'hide_if' => array(
					'back_type' => array('static', 'parallax', 'parallax_animated')
				)
			)
		)
	),
	'group_css' => array(
		'type' => 'collapsible',
		'label' => __('ID & Custom CSS','frontend-builder'),
		'options' => array(
			'id' => array(
				'type' => 'input',
				'label' => __('ID','frontend-builder'),
				'desc' => __('For linking via hashtags','frontend-builder'),
				'std' => ''
			),
			'class' => array(
				'type' => 'input',
				'label' => __('Class','frontend-builder'),
				'desc' => __('For custom css','frontend-builder'),
				'std' => ''
			)
		)
	)

	
);


if($this->option('css_classes')->value == 'true') {
	$classControl = array(

	);
	$fbuilder_controls = array_merge($classControl, $fbuilder_controls);
	
}
$output = $fbuilder_controls;
?>