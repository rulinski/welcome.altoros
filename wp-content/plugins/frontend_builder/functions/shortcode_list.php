<?php
/* Gather wordpress menus */



$nav_menus = get_terms( 'nav_menu', array( 'hide_empty' => true ));
$fbuilder_menus = array();
$fbuilder_menu_std = '';
if(is_array($nav_menus)) 
	foreach($nav_menus as $menu) {
		if($fbuilder_menu_std == '') $fbuilder_menu_std = $menu->slug;
		$fbuilder_menus[$menu->slug] = $menu->name; 
	}

/* Gather wordpress sidebars (Must be done from the wp_head)

$fbuilder_sidebars = array();
$fbuilder_sidebar_std = '';
foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
	if($fbuilder_sidebar_std == '') $fbuilder_sidebar_std = $sidebar['id'];
	$fbuilder_sidebars[$sidebar['id']] = ucwords( $sidebar['name'] );
} 
*/

/* Gather wordpress posts */

global $wpdb;

 $querystr = "
    SELECT $wpdb->posts.ID, $wpdb->posts.post_title
    FROM $wpdb->posts
	WHERE $wpdb->posts.post_status = 'publish'
	AND $wpdb->posts.post_type = 'post'
    ORDER BY $wpdb->posts.post_date DESC
 ";
$posts_array = $wpdb->get_results($querystr, OBJECT);

$fbuilder_wp_posts = array();
$first_post = '';
foreach($posts_array as $key => $obj) {
	if($first_post == '') $first_post = $key;
	$fbuilder_wp_posts[$obj->ID] = $obj->post_title;
}
$admin_optionsDB = $this->option();
$opts = array();
foreach($admin_optionsDB as $opt) {
	if(isset($opt->name) && isset($opt->value))
		$opts[$opt->name] = $opt->value;
}

$animationList = array(
	'none' => __('None', 'frontend-builder'),
	'flipInX' => __('Flip in X', 'frontend-builder'),
	'flipInY' => __('Flip in Y', 'frontend-builder'),
	'fadeIn' => __('Fade in', 'frontend-builder'),
	'fadeInDown' => __('Fade in from top', 'frontend-builder'),
	'fadeInUp' => __('Fade in from bottom', 'frontend-builder'),
	'fadeInLeft' => __('Fade in from left', 'frontend-builder'),
	'fadeInRight' => __('Fade in from right', 'frontend-builder'),
	'fadeInDownBig' => __('Slide in from top', 'frontend-builder'),
	'fadeInUpBig' => __('Slide in from bottom', 'frontend-builder'),
	'fadeInLeftBig' => __('Slide in from left', 'frontend-builder'),
	'fadeInRightBig' => __('Slide in from right', 'frontend-builder'),
	'bounceIn' => __('Bounce in', 'frontend-builder'),
	'bounceInDown' => __('Bounce in from top', 'frontend-builder'),
	'bounceInUp' => __('Bounce in from bottom', 'frontend-builder'),
	'bounceInLeft' => __('Bounce in from left', 'frontend-builder'),
	'bounceInRight' => __('Bounce in from right', 'frontend-builder'),
	'rotateIn' => __('Rotate in', 'frontend-builder'),
	'rotateInDownLeft' => __('Rotate in from top-left', 'frontend-builder'),
	'rotateInDownRight' => __('Rotate in from top-right', 'frontend-builder'),
	'rotateInUpLeft' => __('Rotate in from bottom-left', 'frontend-builder'),
	'rotateInUpRight' => __('Rotate in from bottom-right', 'frontend-builder'),
	'lightSpeedIn' => __('Lightning speed', 'frontend-builder'),
	'rollIn' => __('Roll in', 'frontend-builder')
);

$animationControl = array(
	'group_animate' => array(
		'type' => 'collapsible',
		'label' => __('Animation','frontend-builder'),
		'options' => array(
			'animate' => array(
				'type' => 'select',
				'label' => __('Type:','frontend-builder'),
				'std' => 'none',
				'label_width' => 0.25,
				'control_width' => 0.75,
				'options' => $animationList
			),
			'animation_delay' => array(
				'type' => 'number',
				'label' => __('Delay:','frontend-builder'),
				'std' => 0,
				'unit' => 'ms',
				'min' => 0,
				'step' => 50,
				'max' => 10000,
				'half_column' => 'true'
			),
			'animation_speed' => array(
				'type' => 'number',
				'label' => __('Speed:','frontend-builder'),
				'std' => 1000,
				'unit' => 'ms',
				'min' => 100,
				'step' => 50,
				'max' => 3000,
				'half_column' => 'true'
			),
			'animation_group' => array(
				'type' => 'input',
				'label_width' => 0.25,
				'control_width' => 0.75,
				'label' => __('Group:','frontend-builder'),
				'std' => '',
			)
		)
	)
);



if(isset($opts['css_classes']) && $opts['css_classes'] == 'true') {
	$classControl = array(
		'group_css' => array(
			'type' => 'collapsible',
			'label' => __('ID & Custom CSS','frontend-builder'),
			'options' => array(
				'shortcode_id' => array(
					'type' => 'input',
					'label' => __('ID:','frontend-builder'),
					'desc' => __('For linking via hashtags','frontend-builder'),
					'label_width' => 0.25,
					'control_width' => 0.75,
					'std' => ''
				),
				'class' => array(
					'type' => 'input',
					'label' => __('Class:','frontend-builder'),
					'desc' => __('For custom css','frontend-builder'),
					'label_width' => 0.25,
					'control_width' => 0.75,
					'std' => ''
				)
			)
		)
	);
	$tabsId = array(
		'custom_id' => array(
			'type' => 'input',
			'label' => __('Tab ID:','frontend-builder'),
			'desc' => __('For use of anchor in url. Make sure that this ID is unique on the page.','frontend-builder'),
			'label_width' => 0.25,
			'std' => ''
		)
	);
}
else {
	$classControl = array();
	$tabsId = array();
}

/* -------------------------------------------------------------------------------- */
/* HEADING */
/* -------------------------------------------------------------------------------- */

$heading = array(

	'heading' => array(
		'type' => 'draggable',
		'text' => __('Heading','frontend-builder'),
		'icon' => $this->url.'images/icons/heading.png',
		'function' => 'fbuilder_h',
		'group' => __('Basic', 'frontent-builder'),
		'options' => array_merge(
		
		array(
			'group_heading' => array(
				'type' => 'collapsible',
				'label' => __('Heading','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'content' => array(
						'type' => 'textarea',
						'std' => 'Lorem ipsum',
						
					),
					'type' => array(
						'type' => 'select',
						'label' => __('Type:','frontend-builder'),
						'std' => 'h1',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'options' => array(
							'h1' => 'H1',
							'h2' => 'H2',
							'h3' => 'H3',
							'h4' => 'H4',
							'h5' => 'H5',
							'h6' => 'H6'
						)
					),
					'text_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['title_color'],
						'label_width' => 0.25,
						'control_width' => 0.75
					),
					
					'custom_font_size' => array(
						'type' => 'checkbox',
						'label' => __('Use custom font size','frontend-builder'),
						'std' => 'false'
					),
					'font_size' => array(
						'type' => 'number',
						'label' => __('Size:','frontend-builder'),
						'std' => 36,
						'half_column' => 'true',
						'unit' => 'px',
						'hide_if' => array(
							'custom_font_size' => array('false')
						)
					),
					'line_height' => array(
						'type' => 'number',
						'label' => __('Line:','frontend-builder'),
						'std' => 40,
						'half_column' => 'true',
						'unit' => 'px',
						'hide_if' => array(
							'custom_font_size' => array('false')
						)
					),
					
					'align' => array(
						'type' => 'select',
						'label' => __('Text alignment:','frontend-builder'),
						'options' => array(
							'left' => 'Left',
							'right' => 'Right',
							'center' => 'Center'
						),
						'std' => 'left'
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);


/* -------------------------------------------------------------------------------- */
/* TEXT */
/* -------------------------------------------------------------------------------- */

$text = array(
	'text' => array(
		'type' => 'draggable',
		'text' => __('Text / HTML','frontend-builder'),
		'icon' => $this->url.'images/icons/text-html.png',
		'function' => 'fbuilder_text',
		'group' => 'Basic',
		'options'  => array_merge(
		 array(
		 	'group_text' => array(
				'type' => 'collapsible',
				'label' => __('Text / HTML','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'content' => array(
						'type' => 'textarea',
						'label' => __('Content:','frontend-builder'),
						'desc' => 'You can use text, html and/or wordpress shortcodes',
						'std' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
					),
					'autop' => array(
						'type' => 'checkbox',
						'label' => __('Format new lines','frontend-builder'),
						'std' => 'true',
						'desc' => '"Enter" key is a new line'
					),
					
					'text_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['text_color'],
						'label_width' => 0.25,
						'control_width' => 0.50
					),
					'custom_font_size' => array(
						'type' => 'checkbox',
						'label' => __('Use custom font size','frontend-builder'),
						'std' => 'false'
					),
					'font_size' => array(
						'type' => 'number',
						'label' => __('Size:','frontend-builder'),
						'std' => 12,
						'half_column' => 'true',
						'unit' => 'px',
						'hide_if' => array(
							'custom_font_size' => array('false')
						)
					),
					'line_height' => array(
						'type' => 'number',
						'label' => __('Line:','frontend-builder'),
						'std' => 15,
						'half_column' => 'true',
						'unit' => 'px',
						'hide_if' => array(
							'custom_font_size' => array('false')
						)
					),
					'align' => array(
						'type' => 'select',
						'label' => __('Text alignment:','frontend-builder'),
						'options' => array(
							'left' => __('Left','frontend-builder'),
							'right' => __('Right','frontend-builder'),
							'center' => __('Center','frontend-builder')
						),
						'std' => 'left'
					),
				),	
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);


/* -------------------------------------------------------------------------------- */
/* BULLET LIST */
/* -------------------------------------------------------------------------------- */


$bulletlist = array(
	'bulletlist' => array(
		'type' => 'draggable',
		'text' => __('Bullet List','frontend-builder'),
		'icon' => $this->url.'images/icons/bullet-list.png',
		'function' => 'fbuilder_list',
		'group' =>  'Basic',
		'options'  => array_merge(
		 array(
		 	'group_text' => array(
				'type' => 'collapsible',
				'label' => __('Text','frontend-builder'),
				'desc' => __('new row - new bullet','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'content' => array(
						'type' => 'textarea',
						'std' => 'Lorem ipsum'
					),
					'color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['text_color'],
						'label_width' => 0.25,
						'control_width' => 0.50
					),
					
				),
			),
			'group_icon' => array(
				'type' => 'collapsible',
				'label' => __('Icon','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'icon' => array(
						'type' => 'icon',
						'label' => __('Icon:','frontend-builder'),
						'std' => 'ba-plus',
						'label_width' => 0.25,
						'control_width' => 0.75
					),
					'icon_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['main_color'],
						'label_width' => 0.25,
						'control_width' => 0.50
					),
				)
			),
			'group_background' => array(
				'type' => 'collapsible',
				'label' => __('Background','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'radius' => array(
						'type' => 'checkbox',
						'label' => __('Border radius','frontend-builder'),
						'std' => false
					),
					'background' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'std' => '',
						'label_width' => 0.25,
						'control_width' => 0.50
					),
				)
				
			)
			
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);



/* -------------------------------------------------------------------------------- */
/* BUTTON */
/* -------------------------------------------------------------------------------- */

$button = array(
	'button' => array(
		'type' => 'draggable',
		'text' => __('Button','frontend-builder'),
		'icon' => $this->url.'images/icons/button.png',
		'function' => 'fbuilder_button',
		'group' => 'Basic',
		'options'  => array_merge(
		 array(
		 	'group_text' => array(
				'type' => 'collapsible',
				'label' => __('Text','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'text' => array(
						'type' => 'input',
						'label' => __('Text:','frontend-builder'),
						'std' => __('Read more','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75
					),
					'url' => array(
						'type' => 'input',
						'label' => __('URL:','frontend-builder'),
						'desc' => __('ex. http://yoursite.com/','frontend-builder'),
						'std' => '',
						'label_width' => 0.25,
						'control_width' => 0.75
					),
					'font_size' => array(
						'type' => 'number',
						'label' => __('Size:','frontend-builder'),
						'std' => 16,
						'half_column' => 'true',
						'unit' => 'px'
					),
					'text_align' => array(
						'type' => 'select',
						'label' => __('Text alignment:','frontend-builder'),
						'std' => 'left',
						'options' => array(
							'left' => __('Left','frontend-builder'),
							'center' => __('Centered','frontend-builder'),
							'right' => __('Right','frontend-builder')
						)
					),
					'text_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['main_color'],
						'label_width' => 0.25,
						'control_width' => 0.50
					),
					'hover_text_color' => array(
						'type' => 'color',
						'label' => __('Hover:','frontend-builder'),
						'std' => $opts['light_main_color'],
						'label_width' => 0.25,
						'control_width' => 0.50
					)
				)
			),
			'group_icon' => array(
				'type' => 'collapsible',
				'label' => __('Icon','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'icon' => array(
						'type' => 'icon',
						'label' => __('Icon Type:','frontend-builder'),
						'notNull' => false,
						'std' => 'no-icon',
						'label_width' => 0.25,
						'control_width' => 0.75
					),
					'icon_align' => array(
						'type' => 'select',
						'label' => __('Icon alignment:','frontend-builder'),
						'std' => 'left',
						'options' => array(
							'left' => __('Left','frontend-builder'),
							'right' => __('Right','frontend-builder'),
							'inline' => __('Inline','frontend-builder')
						)
					),
					'icon_size' => array(
						'type' => 'number',
						'label' => __('Size:','frontend-builder'),
						'std' => 16,
						'unit' => 'px',
						'half_column' => 'true'
					)
					
				)
			),
			'group_background' => array(
				'type' => 'collapsible',
				'label' => __('Background','frontend-builder'),
				'open' => 'true',
				'options' => array(	
					'back_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['main_color'],
						'label_width' => 0.25,
						'control_width' => 0.50
					),
					'hover_back_color' => array(
						'type' => 'color',
						'label' => __('Hover:','frontend-builder'),
						'std' => $opts['light_main_color'],
						'label_width' => 0.25,
						'control_width' => 0.50
					),
					'fullwidth' => array(
						'type' => 'checkbox',
						'label' => __('Full width','frontend-builder'),
						'std' => 'false',
						'half_column' => 'true'
					),
					'round' => array(
						'type' => 'checkbox',
						'label' => 'Round',
						'std' => 'false',
						'half_column' => 'true'
					),
					'fill' => array(
						'type' => 'checkbox',
						'label' => __('Fill','frontend-builder'),
						'std' => 'false',
						'desc' => __('turn off to get a button with border','frontend-builder')
					),
					'border_thickness' => array(
						'type' => 'number',
						'label' => __('Border thickness:','frontend-builder'),
						'std' => 1,
						'min' => 0,
						'max' => 20,
						'unit' => 'px',
						'hide_if' => array(
							'fill' => array('true')
						)
					)
				)
			),
			'group_link' => array(
				'type' => 'collapsible',
				'label' => __('Link','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'type' => array(
						'type' => 'select',
						'label' => __('Type','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'std' => 'standard',
						'options' => array(
							'standard' => __('Standard','frontend-builder'),
							'new-tab' => __('Open in new tab','frontend-builder'),
							'lightbox-image' => __('Lightbox image','frontend-builder'),
							'lightbox-iframe' => __('Lightbox iframe','frontend-builder')
						)
					),
					'iframe_width' => array(
						'type' => 'number',
						'label' => __('Width:','frontend-builder'),
						'std' => 600,
						'min' => 0,
						'max' => 1200,
						'unit' => 'px',
						'half_column' => 'true',
						'hide_if' => array(
							'type' => array('standard', 'new-tab', 'lightbx-image')
						)
					),
					'iframe_height' => array(
						'type' => 'number',
						'label' => __('Height:','frontend-builder'),
						'std' => 300,
						'min' => 0,
						'max' => 1200,
						'unit' => 'px',
						'half_column' => 'true',
						'hide_if' => array(
							'type' => array('standard', 'new-tab', 'lightbx-image')
						)
					)
				)
			),
			'group_advanced' => array(
				'type' => 'collapsible',
				'label' => __('Advanced','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'v_padding' => array(
						'type' => 'number',
						'label' => __('Vertical padding:','frontend-builder'),
						'std' => 20,
						'unit' => 'px'
					),
					'h_padding' => array(
						'type' => 'number',
						'label' => __('Horizontal padding:','frontend-builder'),
						'std' => 35,
						'unit' => 'px'
					)
				)
			)		
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* IMAGE */
/* -------------------------------------------------------------------------------- */

$image = array(
	'image' => array(
		'type' => 'draggable',
		'text' => __('Image','frontend-builder'),
		'icon' => $this->url.'images/icons/image.png',
		'function' => 'fbuilder_image',
		'group' => 'Basic',
		'options'  => array_merge(
		 array(
		 	'group_image' => array(
				'type' => 'collapsible',
				'label' => __('Image','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'content' => array(
						'type' => 'image',
						'std' => $this->url.'images/image-default.jpg'
					),
					'custom_dimensions' => array(
						'type' => 'checkbox',
						'label' => __('Custom Dimensions','frontend-builder'),
						'std' => 'false'
					),
					'image_width' => array(
						'type' => 'number',
						'label' => __('Width:','frontend-builder'),
						'std' => 600,
						'min' => 0,
						'max' => 1200,
						'unit' => 'px',
						'half_column' => 'true',
						'hide_if' => array(
							'custom_dimensions' => array('false')
						)
					),
					'image_height' => array(
						'type' => 'number',
						'label' => __('Height:','frontend-builder'),
						'std' => 300,
						'min' => 0,
						'max' => 1200,
						'unit' => 'px',
						'half_column' => 'true',
						'hide_if' => array(
							'custom_dimensions' => array('false')
						)
					),
					'text_align' => array(
						'type' => 'select',
						'label' => __('Image alignment:','frontend-builder'),
						'std' => 'center',
						'options' => array(
							'left' => __('Left','frontend-builder'),
							'center' => __('Center','frontend-builder'),
							'right' => __('Right','frontend-builder')
						)
					),
					'round' => array(
						'type' => 'checkbox',
						'label' => __('Round edges','frontend-builder'),
						'std' => 'false',
						'half_column' => 'true'
					),
					'border' => array(
						'type' => 'checkbox',
						'label' => __('Bottom border','frontend-builder'),
						'std' => 'false',
						'half_column' => 'true'
					),
					'border_color' => array(
						'type' => 'color',
						'label' => __('Border color:','frontend-builder'),
						'std' => $opts['dark_border_color'],
						'label_width' => 0.5,
						'control_width' => 0.50,
						'hide_if' => array(
							'border' => array('false')
						)
					),
					'border_hover_color' => array(
						'type' => 'color',
						'label' => __('Border hover color:','frontend-builder'),
						'std' => $opts['main_color'],
						'label_width' => 0.5,
						'control_width' => 0.50,
						'hide_if' => array(
							'border' => array('false'),
							'link' => array('')
						)
					)
				)
			),
			'group_image_link' => array(
				'type' => 'collapsible',
				'label' => __('Image Link','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'link' => array(
						'type' => 'input',
						'label' => __('URL:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'std' => ''
					),
					'link_type' => array(
						'type' => 'select',
						'label' => __('Link type:','frontend-builder'),
						'std' => 'lightbox-image',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'options' => array(
							'standard' => __('Standard','frontend-builder'),
							'new-tab' => __('Open in new tab','frontend-builder'),
							'lightbox-image' => __('Lightbox image','frontend-builder'),
							'lightbox-iframe' => __('Lightbox iframe','frontend-builder')
						),
						'hide_if' => array(
							'link' => array('')
						)
					),
					'iframe_width' => array(
						'type' => 'number',
						'label' => __('Width:','frontend-builder'),
						'std' => 600,
						'min' => 0,
						'max' => 1200,
						'unit' => 'px',
						'half_column' => 'true',
						'hide_if' => array(
							'link' => array(''),
							'link_type' => array('standard', 'new-tab', 'lightbox-image')
						)
					),
					'iframe_height' => array(
						'type' => 'number',
						'label' => __('Height:','frontend-builder'),
						'std' => 300,
						'min' => 0,
						'max' => 1200,
						'unit' => 'px',
						'half_column' => 'true',
						'hide_if' => array(
							'link' => array(''),
							'link_type' => array('standard', 'new-tab', 'lightbox-image')
						)
					)
				)
			),
			'group_hover_icon' => array(
				'type' => 'collapsible',
				'label' => __('Hover Icon','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'hover_icon' => array(
						'type' => 'icon',
						'label' => __('Hover icon:','frontend-builder'),
						'std' => 'ba-search',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'link' => array('')
						)
					),
					'hover_icon_size' => array(
						'type' => 'number',
						'label' => __('Hover icon size:','frontend-builder'),
						'std' => '30',
						'unit' => 'px',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'link' => array('')
						)
					)	
				)
			),
			'group_hover_shade' => array(
				'type' => 'collapsible',
				'label' => __('Hover Shade','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'hover_shade_color' => array(
						'type' => 'color',
						'label' => __('Hover Color:','frontend-builder'),
						'std' => '#000000',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'link' => array('')
						)
					),
					'hover_shade_opacity' => array(
						'type' => 'select',
						'label' => __('Hover Opacity:','frontend-builder'),
						'std' => '0.4',
						'options' => array(
							'0.0' => '0',
							'0.1' => '0.1',
							'0.2' => '0.2',
							'0.3' => '0.3',
							'0.4' => '0.4',
							'0.5' => '0.5',
							'0.6' => '0.6',
							'0.7' => '0.7',
							'0.8' => '0.8',
							'0.9' => '0.9',
							'1.0' => '1'
						),
						'hide_if' => array(
							'link' => array('')
						)
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* VIDEO */
/* -------------------------------------------------------------------------------- */

$video = array(
	'video' => array(
		'type' => 'draggable',
		'text' => __('Video','frontend-builder'),
		'icon' => $this->url.'images/icons/video.png',
		'function' => 'fbuilder_video',
		'group' => 'Content',
		'options' => array_merge(
		array(
			'group_video' => array(
				'type' => 'collapsible',
				'label' => __('Video','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'url' => array(
						'type' => 'input',
						'label' => __('URL','frontend-builder'),
						'std' => 'http://www.youtube.com/watch?v=YE7VzlLtp-4',
						'label_width' => 0.25,
						'control_width' => 0.75
					),
					'width' => array(
						'type' => 'number',
						'label' => __('Width','frontend-builder'),
						'half_column' => 'true',
						'min' => 100,
						'max' => 1200,
						'std' => 620,
						'unit' => 'px',
						'hide_if' => array(
							'auto_width' => array('true')
						)
					),
					'height' => array(
						'type' => 'number',
						'label' => __('Height','frontend-builder'),
						'half_column' => 'true',
						'min' => 100,
						'max' => 1200,
						'std' => 310,
						'unit' => 'px'
					),
					'auto_width' => array(
						'type' => 'checkbox',
						'label' => __('Full width','frontend-builder'),
						'desc' => __('Use the width of the column','frontend-builder'),
						'std' => 'true'
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);


/* -------------------------------------------------------------------------------- */
/* AUDIO */
/* -------------------------------------------------------------------------------- */

$audio = array(

	'audio' => array(
		'type' => 'draggable',
		'text' => __('Audio','frontend-builder'),
		'icon' => $this->url.'images/icons/audio.png',
		'function' => 'fbuilder_audio',
		'group' => 'Content',
		'options' => array_merge(
		
		array(
			'group_heading' => array(
				'type' => 'collapsible',
				'label' => __('Audio','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'content_mp3' => array(
						'label' => __('Source - .mp3','frontend-builder'),
						'desc' =>__('URL to the audio file in .mp3 format','frontend-builder'),
						'type' => 'input',
						'std' => 'http://media.w3.org/2010/07/bunny/04-Death_Becomes_Fur.mp4',
						'label_width' => 0.5,
						'control_width' => 0.5
					),
					'content_ogg' => array(
						'label' => __('Source - .ogg','frontend-builder'),
						'desc' =>__('URL to the audio file in .ogg format','frontend-builder'),
						'type' => 'input',
						'std' => 'http://media.w3.org/2010/07/bunny/04-Death_Becomes_Fur.oga',
						'label_width' => 0.5,
						'control_width' => 0.5
					),
					'background_color' => array(
						'type' => 'color',
						'label' => __('Background color:','frontend-builder'),
						'std' => '#464646',
						'label_width' => 0.5,
						'control_width' => 0.5
					),
					'bar_color' => array(
						'type' => 'color',
						'label' => __('Progress bar color:','frontend-builder'),
						'std' => '#21CDEC',
						'label_width' => 0.5,
						'control_width' => 0.5
					),
					'icon_type' => array(
						'type' => 'select',
						'label' => __('Icon style:','frontend-builder'),
						'std' => 'default',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'default' => __('Default','frontend-builder'),
							'light' => __('Light','frontend-builder'),
							'dark' => __('Dark','frontend-builder'),
							'light_transparent' => __('Light Transparent','frontend-builder'),
							'dark_transparent' => __('Dark Transparent','frontend-builder')
						)
					),
					'start_at' => array(
						'type' => 'number',
						'label' => __('Starting time','frontend-builder'),
						'desc' => __('Starting time of the audio file in seconds','frontend-builder'),
						'std' => 0,
						'min' => 0,
						'max' => 5000,
						'unit' => ' sec'
					),
					
					'autoplay' => array(
						'type' => 'checkbox',
						'label' => __('Autoplay','frontend-builder'),
						'std' => 'false',
						'half_column' => 'true'
					),
					'loop' => array(
						'type' => 'checkbox',
						'label' => __('Loop','frontend-builder'),
						'std' => 'false',
						'half_column' => 'true'					
						),
					'mute' => array(
						'type' => 'checkbox',
						'label' => __('Mute','frontend-builder'),
						'std' => 'false',
						'half_column' => 'true'
					),
					'hide_controls' => array(
						'type' => 'checkbox',
						'label' => __('Hide controls','frontend-builder'),
						'std' => 'false',
						'half_column' => 'true'
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);


/* -------------------------------------------------------------------------------- */
/* FEATURED POST */
/* -------------------------------------------------------------------------------- */

$post = array(
	'post' => array(
		'type' => 'draggable',
		'text' => __('Featured post','frontend-builder'),
		'icon' => $this->url.'images/icons/featured-post.png',
		'function' => 'fbuilder_post',
		'group' => 'Content',
		'options' => array_merge(
		array(
			'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'id' => array(
						'type' => 'select',
						'label' => __('Post id:','frontend-builder'),
						'std' => $first_post,
						'desc' => __('You must have at leest one wordpress post','frontend-builder'),
						'options' => $fbuilder_wp_posts
					),
					'style' => array(
						'type' => 'select',
						'label' => __('Style:','frontend-builder'),
						'std' => 'clean',
						'options' => array(
							'clean' => __('Clean','frontend-builder'),
							'squared' => __('Squared','frontend-builder'),
							'rounded' => __('Rounded','frontend-builder')
						)
					),
					'link_type' => array(
						'type' => 'select',
						'label' => __('Img Link Type:','frontend-builder'),
						'std' => 'post',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'post' => __('Post','frontend-builder'),
							'prettyphoto' => __('Lightbox','frontend-builder')
						)
					),
					'excerpt_lenght' => array(
						'type' => 'number',
						'label' => __('Excerpt Length','frontend-builder'),
						'std' => 150,
						'max' => 300,
						'unit' => ''
					),					
					'hover_icon' => array(
						'type' => 'icon',
						'label' => __('Image hover icon:','frontend-builder'),
						'std' => 'ba-plus'
					)
					
				)
			),
			'group_button' => array(
				'type' => 'collapsible',
				'label' => __('Button','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'button_text' => array(
						'type' => 'input',
						'label' => __('Text:','frontend-builder'),
						'std' => 'Read more'
					),
					'button_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['main_color']
					),
					'button_text_color' => array(
						'type' => 'color',
						'label' => __('Text color:','frontend-builder'),
						'std' => $opts['main_back_text_color']
					),
					'button_hover_color' => array(
						'type' => 'color',
						'label' => __('Hover color:','frontend-builder'),
						'std' => $opts['light_main_color']
					),
					'button_text_hover_color' => array(
						'type' => 'color',
						'label' => __('Text hover color:','frontend-builder'),
						'std' => $opts['main_back_text_color']
					)
				)
			),
			'group_colors' => array(
				'type' => 'collapsible',
				'label' => __('Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					
					'head_color' => array(
						'type' => 'color',
						'label' => __('Heading color:','frontend-builder'),
						'std' => $opts['title_color']
					),
					'meta_visible' => array(
						'type' => 'checkbox',
						'std' => 'true',
						'label' => __('Meta Tags','frontend-builder'),
					),
					'meta_color' => array(
						'type' => 'color',
						'label' => __('Meta links color:','frontend-builder'),
						'desc' => __('color of the meta links - Date, Author, Comments','frontend-builder'),
						'std' => $opts['text_color'],
						'hide_if' => array(
							'meta_visible' => array('false')
						)
					),
					'meta_hover_color' => array(
						'type' => 'color',
						'label' => __('Meta hover color:','frontend-builder'),
						'std' => $opts['main_color'],
						'hide_if' => array(
							'meta_visible' => array('false')
						)
					),
					'text_color' => array(
						'type' => 'color',
						'label' => __('Text color:','frontend-builder'),
						'std' => $opts['text_color']
					),
					'back_color' => array(
						'type' => 'color',
						'label' => __('Background color:','frontend-builder'),
						'std' => $opts['light_back_color'],
						'hide_if' => array(
							'style' => array('clean')
						)
					),	
					'border_color' => array(
						'type' => 'color',
						'label' => __('Border color:','frontend-builder'),
						'std' => $opts['main_color']
					)
				)
			),
			
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);


/* -------------------------------------------------------------------------------- */
/* RECENT POST */
/* -------------------------------------------------------------------------------- */
$recent_post = array(
	'recent_post' => array(
		'type' => 'draggable',
		'text' => __('Recent post','frontend-builder'),
		'icon' => $this->url.'images/icons/recent-post.png',
		'function' => 'fbuilder_recent_post',
		'group' => 'Content',
		'options' => array_merge(
		array(
			'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'offset' => array(
						'type' => 'number',
						'label' => __('Offset:','frontend-builder'),
						'std' => 0,
						'desc' => __('Number of post to displace or pass over','frontend-builder'),
						'min' => 0,
						'max' => 100
					),
					'style' => array(
						'type' => 'select',
						'label' => __('Style:','frontend-builder'),
						'std' => 'clean',
						'options' => array(
							'clean' => __('Clean','frontend-builder'),
							'squared' => __('Squared','frontend-builder'),
							'rounded' => __('Rounded','frontend-builder')
						)
					),
					'link_type' => array(
						'type' => 'select',
						'label' => __('Img Link Type:','frontend-builder'),
						'std' => 'post',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'post' => __('Post','frontend-builder'),
							'prettyphoto' => __('Lightbox','frontend-builder')
						)
					),
					'excerpt_lenght' => array(
						'type' => 'number',
						'label' => __('Excerpt Length','frontend-builder'),
						'std' => 150,
						'max' => 300,
						'unit' => ''
					),					
					'hover_icon' => array(
						'type' => 'icon',
						'label' => __('Image hover icon:','frontend-builder'),
						'std' => 'ba-plus'
					)
				)
			),
			'group_button' => array(
				'type' => 'collapsible',
				'label' => __('Button','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'button_text' => array(
						'type' => 'input',
						'label' => __('Text:','frontend-builder'),
						'std' => 'Read more'
					),
					'button_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['main_color']
					),
					'button_text_color' => array(
						'type' => 'color',
						'label' => __('Text color:','frontend-builder'),
						'std' => $opts['main_back_text_color']
					),
					'button_hover_color' => array(
						'type' => 'color',
						'label' => __('Hover color:','frontend-builder'),
						'std' => $opts['light_main_color']
					),
					'button_text_hover_color' => array(
						'type' => 'color',
						'label' => __('Text hover color:','frontend-builder'),
						'std' => $opts['main_back_text_color']
					)
				)
			),
			'group_colors' => array(
				'type' => 'collapsible',
				'label' => __('Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'head_color' => array(
						'type' => 'color',
						'label' => __('Heading color:','frontend-builder'),
						'std' => $opts['title_color']
					),
					'meta_visible' => array(
						'type' => 'checkbox',
						'std' => 'true',
						'label' => __('Meta Tags','frontend-builder'),
					),
					'meta_color' => array(
						'type' => 'color',
						'label' => __('Meta links color:','frontend-builder'),
						'desc' => __('color of the meta links - Date, Author, Comments','frontend-builder'),
						'std' => $opts['text_color'],
						'hide_if' => array(
							'meta_visible' => array('false')
						)
					),
					'meta_hover_color' => array(
						'type' => 'color',
						'label' => __('Meta hover color:','frontend-builder'),
						'std' => $opts['main_color'],
						'hide_if' => array(
							'meta_visible' => array('false')
						)
					),
					'text_color' => array(
						'type' => 'color',
						'label' => __('Text color:','frontend-builder'),
						'std' => $opts['text_color']
					),
					'back_color' => array(
						'type' => 'color',
						'label' => __('Background color:','frontend-builder'),
						'std' => $opts['light_back_color'],
						'hide_if' => array(
							'style' => array('clean')
						)
					),	
					'border_color' => array(
						'type' => 'color',
						'label' => __('Border color:','frontend-builder'),
						'std' => $opts['main_color']
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* GALLERY */
/* -------------------------------------------------------------------------------- */

$gallery = array(
	'gallery' => array(
		'type' => 'draggable',
		'text' => __('Gallery','frontend-builder'),
		'icon' => $this->url.'images/icons/gallery-shortcode.png',
		'function' => 'fbuilder_gallery',
		'group' => 'Content',
		'options'  => array_merge(
		 array(
		 	'group_chart' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
						'column_number' => array(
						'type' => 'number',
						'label' => __('No. of columns:','frontend-builder'),
						'min' => 1,
						'max' => 5,
						'std' => 3
					),
					'item_padding' => array(
						'type' => 'number',
						'label' => __('Item padding:','frontend-builder'),
						'min' => 0,
						'max' => 100,
						'std' => 10,
						'unit' => 'px'
					),
					'image_size' => array(
						'type' => 'select',
						'label' => __('Image Size:','frontend-builder'),
						'std' => 'full',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'full' => 'Full',
							'large' => 'Large',
							'creative_post_slider_medium' => 'Medium',
							'medium' => 'Small',
							'thumbnail' => 'Thumbnail'
						)
					),
					'aspect_ratio' => array(
						'type' => 'select',
						'label' => __('Aspect Ratio:','frontend-builder'),
						'std' => '16:9',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'1:1' => '1:1',
							'4:3' => '4:3',
							'16:9' => '16:9',
							'16:10' => '16:10',
							'1:2' => '1:2'
						)
					),
					'media_files' => array(
						'type' => 'media_select',
						'label' => __('Select media files:','frontend-builder'),
						'label_width' => 1,
						'control_width' => 1,
						'hide_if' => array(
							'enable_categories' => array('true')
						)
					),
					'on_image_click' => array(
						'type' => 'select',
						'label' => __('Action on click:','frontend-builder'),
						'std' => 'none',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'none' => 'None',
							'pretty_photo' => 'prettyPhoto',
							'new_tab' => 'Open in new tab'
						)
					),
					'merge_galleries' => array(
						'type' => 'checkbox',
						'std' => 'false',
						'label' => __('Merge Lightbox Galleries','frontend-builder'),
						'desc' => __('Merge all gallery lightboxes into a single lightbox'),
						'hide_if' => array(
							'on_image_click' => array('none', 'new_tab')
						)
					)
				)
			),
			'group_hover_element' => array(
				'type' => 'collapsible',
				'label' => __('Hover Element','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'hover_content' => array(
						'type' => 'select',
						'label' => __('Overlay content:','frontend-builder'),
						'std' => 'icon',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'icon' => 'Icon',
							'title' => 'Title'
						),
						'hide_if' => array(
							'on_image_click' => array('none')
						)
					),
					'hover_icon' => array(
						'type' => 'icon',
						'label' => __('Hover icon:','frontend-builder'),
						'std' => 'ba-search',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'on_image_click' => array('none'),
							'hover_content' => array('title')
						)
					),
					'hover_icon_size' => array(
						'type' => 'number',
						'label' => __('Hover icon size:','frontend-builder'),
						'std' => '30',
						'unit' => 'px',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'on_image_click' => array('none'),
							'hover_content' => array('title')
						)
					),
					'hover_title_color' => array(
						'type' => 'color',
						'label' => __('Title Color:','frontend-builder'),
						'std' => '#ffffff',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'on_image_click' => array('none'),
							'hover_content' => array('icon')
						)
					)	
				)
			),
			'group_hover_shade' => array(
				'type' => 'collapsible',
				'label' => __('Hover Shade','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'initial_shade_color' => array(
						'type' => 'color',
						'label' => __('Initial Color:','frontend-builder'),
						'std' => '#000000',
						'label_width' => 0.5,
						'control_width' => 0.5
					),
					'initial_shade_opacity' => array(
						'type' => 'select',
						'label' => __('Initial Opacity:','frontend-builder'),
						'std' => '0.0',
						'options' => array(
							'0.0' => '0',
							'0.1' => '0.1',
							'0.2' => '0.2',
							'0.3' => '0.3',
							'0.4' => '0.4',
							'0.5' => '0.5',
							'0.6' => '0.6',
							'0.7' => '0.7',
							'0.8' => '0.8',
							'0.9' => '0.9',
							'1.0' => '1'
						)
					),
					'hover_shade_color' => array(
						'type' => 'color',
						'label' => __('Hover Color:','frontend-builder'),
						'std' => '#000000',
						'label_width' => 0.5,
						'control_width' => 0.5
					),
					'hover_shade_opacity' => array(
						'type' => 'select',
						'label' => __('Hover Opacity:','frontend-builder'),
						'std' => '0.3',
						'options' => array(
							'0.0' => '0',
							'0.1' => '0.1',
							'0.2' => '0.2',
							'0.3' => '0.3',
							'0.4' => '0.4',
							'0.5' => '0.5',
							'0.6' => '0.6',
							'0.7' => '0.7',
							'0.8' => '0.8',
							'0.9' => '0.9',
							'1.0' => '1'
						)
					)
				)
			),
			'group_categories' => array(
				'type' => 'collapsible',
				'label' => __('Categories','frontend-builder'),
				'open' => 'true',
				
				'options' => array(
					'enable_categories' => array(
						'type' => 'checkbox',
						'half_column' => 'true',
						'std' => 'false',
						'label' => __('Categories','frontend-builder')
					),
					'show_all_category' => array(
						'type' => 'checkbox',
						'std' => 'false',
						'half_column' => 'true',
						'label' => __('"All" category','frontend-builder'),
							'hide_if' => array(
							'enable_categories' => array('false')
						)
					),
					'sortable' => array(
						'type' => 'sortable',
						'label_width' => 0,
						'control_width' => 1,
						
						'desc' => __('Elements are sortable','frontend-builder'),
						'item_name' => __('Category','frontend-builder'),
						'std' => array(
							'items' => array(
								0 => array(
									'category_name' => 'Images',
									'active' => 'true',
									'category_media_files' => ''
								)
							),
							'order' => array(
								0 => 0
							)
						),
						'options'=> array(
							'category_name' => array(
								'type' => 'input',
								'std' => 'Images',
								'label' => __('Category name','frontend-builder'),
								'hide_if' => array(
								'enable_categories' => array('false')
									)
							),
							'active' => array(
							'type' => 'checkbox',
							'std' => 'false',
							'label' => __('Set this category as default','frontend-builder'),
							'hide_if' => array(
							'enable_categories' => array('false')
								)
							),
							'category_media_files' => array(
								'type' => 'media_select',
								'label' => __('Select media files:','frontend-builder'),
								'label_width' => 1,
								'control_width' => 1,
								'hide_if' => array(
									'enable_categories' => array('false')
								)
							)
						)
					)
				)
			),
			'style_gallery' => array(
				'type' => 'collapsible',
				'label' => __('Category styling','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'bckg_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => 'transparent',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'enable_categories' => array('false')
						)
					),
					'bckg_hover_color' => array(
						'type' => 'color',
						'label' => __('Background hover:','frontend-builder'),
						'std' => 'transparent',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'enable_categories' => array('false')
						)
					),
					/*'bckg_active_color' => array(
						'type' => 'color',
						'label' => __('Background active:','frontend-builder'),
						'std' => 'transparent',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'enable_categories' => array('false')
						)
					),*/
					'text_color' => array(
						'type' => 'color',
						'label' => __('Text:','frontend-builder'),
						'std' => '#232323',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'enable_categories' => array('false')
						)
					),
					'text_hover_color' => array(
						'type' => 'color',
						'label' => __('Text hover:','frontend-builder'),
						'std' => '#232323',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'enable_categories' => array('false')
						)
					),
				/*	'text_active_color' => array(
						'type' => 'color',
						'label' => __('Text active:','frontend-builder'),
						'std' => '#232323',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'enable_categories' => array('false')
						)
					),*/
					'border_color' => array(
						'type' => 'color',
						'label' => __('Border:','frontend-builder'),
						'std' => 'transparent',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'enable_categories' => array('false')
						)
					),
					'border_hover_color' => array(
						'type' => 'color',
						'label' => __('Border hover:','frontend-builder'),
						'std' => 'transparent',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'enable_categories' => array('false')
						)
					),
					/*'border_active_color' => array(
						'type' => 'color',
						'label' => __('Border active:','frontend-builder'),
						'std' => 'transparent',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'enable_categories' => array('false')
						)
					),*/
					'border_thickness' => array(
						'type' => 'number',
						'label' => __('Border width:','frontend-builder'),
						'min' => 0,
						'max' => 100,
						'std' => 0,
						'unit' => 'px',
						'hide_if' => array(
							'enable_categories' => array('false')
						)
					)
						
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* SLIDER */
/* -------------------------------------------------------------------------------- */
$slider = array(
	'slider' => array(
		'type' => 'draggable',
		'text' => __('Slider','frontend-builder'),
		'icon' => $this->url.'images/icons/slider.png',
		'function' => 'fbuilder_slider',
		'group' => 'Content',
		'options' => array_merge(
		array(
			'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'mode' => array(
						'type' => 'select',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'label' => __('Mode:','frontend-builder'),
						'std' => 'horizontal',
						'options' => array(
							'horizontal' => __('Hortizontal','frontend-builder'),
							'vertical' => __('Vertical','frontend-builder'),
						)
					),
					'slides_per_view' => array(
						'type' => 'number',
						'label_width' => 0.75,
						'control_width' => 0.25,
						'min' => 1,
						'label' => __('Slides per view:','frontend-builder'),
						'max' => 10,
						'std' => 1,
						'unit' => ''
					),
					'auto_delay' => array(
						'type' => 'number',
						'std' => 5,
						'label_width' => 0.75,
						'control_width' => 0.25,
						'label' => __('Transition delay time:','frontend-builder'),
						'unit' => 's',
						'hide_if' => array(
							'auto_play' => 'false'
						)
					),
					'auto_play' => array(
						'type' => 'checkbox',
						'std' => 'true',
						'label' => __('Auto play','frontend-builder')
					)
				)
			),
			'group_navigation' => array(
				'type' => 'collapsible',
				'label' => __('Navigation','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'navigation' => array(
						'type' => 'select',
						'std' => 'none',
						'label' => __('Arrows:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'options' => array(
							'none' => __('None','frontend-builder'),
							'squared' => __('Squared','frontend-builder'),
							'round' => __('Round','frontend-builder')
						)
					),
					'navigation_color' => array(
						'type' => 'color',
						'std' => '#ffffff',
						'label_width' => 0.25,
						'control_width' => 0.5,
						'label' => __('Color:','frontend-builder'),
						'hide_if' => array(
							'navigation' => array('none')
						)
					),
					'pagination' => array(
						'type' => 'checkbox',
						'std' => 'true',
						'label' => __('Pagination','frontend-builder')
					),
				)
			),
			'group_slides' => array(
				'type' => 'collapsible',
				'label' => __('Slides','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'sortable' => array(
						'type' => 'sortable',
						'label_width' => 0,
						'control_width' => 1,
						'desc' => __('Elements are sortable','frontend-builder'),
						'item_name' => __('slide','frontend-builder'),
						'std' => array(
							'items' => array(
								0 => array(
									'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
									'image' => $this->url.'images/image-default.jpg',
									'ctype' => 'image',
									'back_color' => '#000000',
									'text_color' => '#ffffff',
									'image_link' => '',
									'image_link_type' => 'standard'
								),
								1 => array(
									'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
									'image' => '',
									'ctype' => 'html',
									'back_color' => '#34495e',
									'text_color' => '#ffffff',
									'image_link' => '',
									'image_link_type' => 'standard'
								),
								2 => array(
									'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
									'image' => $this->url.'images/image-default.jpg',
									'ctype' => 'image',
									'back_color' => '#000000',
									'text_color' => '#ffffff',
									'image_link' => '',
									'image_link_type' => 'standard'
								)
							),
							'order' => array(
								0 => 0,
								1 => 1,
								2 => 2
							)
						),
						
						'options'=> array(
							'ctype' => array(
								'type' => 'select',
								'label' => __('Content Type:','frontend-builder'),
								'std' => 'image',
								'options' => array(
									'image' => __('Image','frontend-builder'),
									'html' => __('Text / Html','frontend-builder')
								)
							),
							'image' => array(
								'type' => 'image',
								'desc' => __('Add an image to tab content','frontend-builder'),
								'hide_if' => array(
									'sortable' => array(
										'ctype' => array('html')
									)
								)
							),
							'image_link' => array(
								'type' => 'input',
								'label_width' => 0.25,
								'control_width' => 0.75,
								'label' => __('Link:','frontend-builder'),
								'hide_if' => array(
									'sortable' => array(
										'ctype' => array('html')
									)
								)
							),
							'image_link_type' => array(
								'type' => 'select',
								'label_width' => 0.5,
								'control_width' => 0.5,
								'label' => __('Link Type:','frontend-builder'),
								'std' => 'standard',
								'desc' => __('open in new tab or lightbox','frontend-builder'),
								'options' => array(
									'standard' => __('Standard','frontend-builder'),
									'new-tab' => __('Open in new tab','frontend-builder'),
									'lightbox-image' => __('Lightbox image','frontend-builder'),
									'lightbox-iframe' => __('Lightbox iframe','frontend-builder')
								),
								'hide_if' => array(
									'sortable' => array(
										'ctype' => array('html')
									)
								)
							),
							'iframe_width' => array(
								'type' => 'number',
								'label' => __('Width:','frontend-builder'),
								'std' => 600,
								'min' => 0,
								'half_column' => 'true',
								'max' => 1200,
								'unit' => 'px',
								'hide_if' => array(
									'sortable' => array(
										'ctype' => array('html'),
										'image_link_type' => array('standard', 'new-tab', 'lightbox-image')
									)
								)
							),
							'iframe_height' => array(
								'type' => 'number',
								'label' => __('Height:','frontend-builder'),
								'std' => 300,
								'min' => 0,
								'half_column' => 'true',
								'max' => 1200,
								'unit' => 'px',
								'hide_if' => array(
									'sortable' => array(
										'ctype' => array('html'),
										'image_link_type' => array('standard', 'new-tab', 'lightbox-image')
									)
								)
							),
							'content' => array(
								'type' => 'textarea',
								'hide_if' => array(
									'sortable' => array(
										'ctype' => array('image')
									)
								)
							),
							'text_align' => array(
								'type' => 'select',
								'label' => __('Text alignment:','frontend-builder'),
								'std' => 'left',
								'options' => array(
									'left' => __('Left','frontend-builder'),
									'center' => __('Center','frontend-builder'),
									'right' => __('Right','frontend-builder')
								),
								'hide_if' => array(
									'sortable' => array(
										'ctype' => array('image')
									)
								)
							),
							'text_color' => array(
								'type' => 'color',
								'label_width' => 0.5,
								'control_width' => 0.5,
								'label' => __('Text:','frontend-builder'),
								'std' => $opts['text_color'],
								'hide_if' => array(
									'sortable' => array(
										'ctype' => array('image')
									)
								)
							),
							'back_color' => array(
								'type' => 'color',
								'label_width' => 0.5,
								'control_width' => 0.5,
								'label' => __('Background:','frontend-builder'),
								'std' => $opts['light_back_color'],
								'hide_if' => array(
									'sortable' => array(
										'ctype' => array('image')
									)
								)
							)
						)
					)
				)
			),
			'group_responsive' => array(
				'type' => 'collapsible',
				'label' => __('Responsive','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'responsive_layout' => array(
						'type' => 'checkbox',
						'std' => 'false',
						'label' => __('Responsive Layout','frontend-builder'),
						'hide_if' => array(
							'mode' => 'vertical'
						)
					),
					'min_slide_width' => array(
						'type' => 'number',
						'label' => __('Min. Slide Width:','frontend-builder'),
						'std' => 200,
						'min' => 0,
						'label_width' => 0.5,
						'control_width' => 0.5,
						'max' => 1200,
						'unit' => 'px',
						'hide_if' => array(
							'responsive_layout' => array('false')
						)
					),
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);


/* -------------------------------------------------------------------------------- */
/* CREATIVE POST SLIDER */
/* -------------------------------------------------------------------------------- */


$frb_creative_post_categories = get_categories(array('order'=>'desc')); 
$frb_creative_post_ready_categories = array(); 
if(is_object($frb_creative_post_categories[0])){
	foreach ( $frb_creative_post_categories as $category ) {
		$frb_creative_post_ready_categories = $frb_creative_post_ready_categories + array($category->term_id=>$category->name); 
	}

}
$creative_post_slider = array(
	'creative_post_slider' => array(
		'type' => 'draggable',
		'text' => __('Creative Post Slider','frontend-builder'),
		'icon' => $this->url.'images/icons/creative-post-slider.png',
		'function' => 'fbuilder_creative_post_slider',
		'group' => 'Content',
		'options'  => array_merge(
		 array(
			'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'slides_per_view' => array(
						'type' => 'number',
						'label' => __('Slides Per View:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'min' => 1,
						'max' => 15,
						'unit' => '',
						'std' => 3
					),
					'number_of_posts' => array(
						'type' => 'number',
						'label' => __('Number Of Posts:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'min' => 1,
						'max' => 50,
						'unit' => '',
						'std' => 5
					),
					'categories' => array(
						'type' => 'select',
						'multiselect' => 'true',
						'label' => __('Categories:','frontend-builder'),
						'std' => '1',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => $frb_creative_post_ready_categories
					),
					'category_order' => array(
						'type' => 'select',
						'label' => __('Category Order:','frontend-builder'),
						'std' => 'desc',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'asc' => 'Ascending',
							'desc' => 'Descending'
						)
					),
					'order_by' => array(
						'type' => 'select',
						'label' => __('Order by:','frontend-builder'),
						'std' => 'ID',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'ID' => 'ID',
							'author' => 'Author',
							'title' => 'Title',
							'name' => 'Name',
							'date' => 'Date',
							'comment_count' => 'Comments',
							'rand' => 'Random'
						)
					),
					'image_size' => array(
						'type' => 'select',
						'label' => __('Image Size:','frontend-builder'),
						'std' => 'full',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'full' => 'Full',
							'large' => 'Large',
							'creative_post_slider_medium' => 'Medium',
							'medium' => 'Small',
							'thumbnail' => 'Thumbnail'
						)
					),
					'aspect_ratio' => array(
						'type' => 'select',
						'label' => __('Aspect Ratio:','frontend-builder'),
						'std' => '16:9',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'1:1' => '1:1',
							'4:3' => '4:3',
							'16:9' => '16:9',
							'16:10' => '16:10',
							'1:2' => '1:2'
						)
					),
					'resize_reference' => array(
						'type' => 'number',
						'label' => __('Min. Slide Width:','frontend-builder'),
						'desc' => __('Reference for responsive layout calculations', 'frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'unit' => 'px',
						'std' => 200
					),
					'enable_custom_height' => array(
						'type' => 'checkbox',
						'std' => 'false',
						'label' => __('Custom Height','frontend-builder')
					),
					'custom_slider_height' => array(
						'type' => 'number',
						'label' => __('Slider Height:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'min' => 1,
						'max' => 1200,
						'unit' => 'px',
						'std' => 300,
						'hide_if' => array(
							'enable_custom_height' => array('false')
						)
					)
				)
			),	
			'group_hover' => array(
				'type' => 'collapsible',
				'label' => __('Hover Element','frontend-builder'),
				'open' => 'true',
				'options' => array(	
					'hover_background_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => '#ffffff',
						'label_width' => 0.5,
						'control_width' => 0.5
					),
					'hover_text_color' => array(
						'type' => 'color',
						'label' => __('Text:','frontend-builder'),
						'std' => $opts['text_color'],
						'label_width' => 0.5,
						'control_width' => 0.5
					),
					'link_type' => array(
						'type' => 'select',
						'label' => __('Link Type:','frontend-builder'),
						'std' => 'prettyphoto',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'prettyphoto' => 'Lightbox',
							'post' => 'Post'
						)
					),
					'open_link_in' => array(
						'type' => 'select',
						'label' => __('Open Link In:','frontend-builder'),
						'std' => 'default',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'options' => array(
							'default' => 'Same Tab',
							'post' => 'New Tab'
						),
						'hide_if' => array(
							'link_type' => array('prettyphoto')
						)
					),
					'enable_icon' => array(
						'type' => 'checkbox',
						'std' => 'true',
						'label' => __('Hover Icon','frontend-builder')
					),
					'hover_icon' => array(
						'type' => 'icon',
						'label' => __('Icon:','frontend-builder'),
						'std' => 'ba-search',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'enable_icon' => array('false')
						)
					),
					'hover_icon_size' => array(
						'type' => 'number',
						'label' => __('Hover icon size:','frontend-builder'),
						'std' => '30',
						'unit' => 'px',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'hide_if' => array(
							'enable_icon' => array('false')
						)
					),	
					'title_size' => array(
						'type' => 'number',
						'label' => __('Title Font Size:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'min' => 12,
						'std' => 20,
						'unit' => 'px'
					),
					'title_line_height' => array(
						'type' => 'number',
						'label' => __('Title Line Height:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'min' => 12,
						'std' => 28,
						'unit' => 'px'
					),
					'cat_size' => array(
						'type' => 'number',
						'label' => __('Cat. Font Size:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'min' => 12,
						'std' => 14,
						'unit' => 'px',
						'hide_if' => array(
							'category_show' => array('false')
						)
					),
					'cat_line_height' => array(
						'type' => 'number',
						'label' => __('Cat. Line Height:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'min' => 12,
						'std' => 18,
						'unit' => 'px',
						'hide_if' => array(
							'category_show' => array('false')
						)
					),
					'excerpt_size' => array(
						'type' => 'number',
						'label' => __('Excerpt Font Size:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'min' => 12,
						'std' => 14,
						'unit' => 'px',
						'hide_if' => array(
							'excerpt_show' => array('false')
						)
					),	
					'excerpt_line_height' => array(
						'type' => 'number',
						'label' => __('Excerpt Line Height:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'min' => 12,
						'std' => 18,
						'unit' => 'px',
						'hide_if' => array(
							'excerpt_show' => array('false')
						)
					),
					'excerpt_lenght' => array(
						'type' => 'number',
						'label' => __('Excerpt Lenght:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'min' => 0,
						'std' => 30,
						'unit' => '',
						'hide_if' => array(
							'excerpt_show' => array('false')
						)
					),
					'category_show' => array(
						'type' => 'checkbox',
						'half_column' => 'true',
						'std' => 'true',
						'label' => __('Category','frontend-builder')
					),
					'excerpt_show' => array(
						'type' => 'checkbox',
						'half_column' => 'true',
						'std' => 'false',
						'label' => __('Excerpt','frontend-builder')
					)	
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);


/* -------------------------------------------------------------------------------- */
/* FEATURES */
/* -------------------------------------------------------------------------------- */

$features = array(
	'features' => array(
		'type' => 'draggable',
		'text' => __('Features','frontend-builder'),
		'icon' => $this->url.'images/icons/features.png',
		'function' => 'fbuilder_features',
		'group' => 'Content',
		'options'  => array_merge(
		 array(
		 	'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'order' => array(
						'type' => 'select',
						'label' => __('Order:','frontend-builder'),
						'std' => 'icon-after-title',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'options' => array(
							'icon-left' => __('Icon left','frontend-builder'),
							'icon-right' => __('Icon right','frontend-builder'),
							'icon-after-title' => __('Icon after title','frontend-builder'),
							'icon-before-title' => __('Icon before title','frontend-builder')
						)
					),
					'style' => array(
						'type' => 'select',
						'label' => __('Style:','frontend-builder'),
						'std' => 'clean',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'options' => array(
							'clean' => __('Clean','frontend-builder'),
							'squared' => __('Squared','frontend-builder'),
							'rounded' => __('Rounded','frontend-builder'),
							'icon-squared' => __('Icon squared','frontend-builder'),
							'icon-rounded' => __('Icon rounded','frontend-builder')
						)
					),
					'link' => array(
						'type' => 'input',
						'label' => __('Link:','frontend-builder'),
						'std' => '',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'desc' => __('leave empty if you dont want the feature to be linked','frontend-builder')
					),
					'back_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['dark_back_color'],
						'label_width' => 0.25,
						'control_width' => 0.5,
						'hide_if' => array(
							'style' => array('clean')
						)
					),
					'back_hover_color' => array(
						'type' => 'color',
						'label' => __('Hover:','frontend-builder'),
						'std' => '',
						'label_width' => 0.25,
						'control_width' => 0.5,
						'hide_if' => array(
							'style' => array('clean')
						)
					)
				)
			),
			'group_title' => array(
				'type' => 'collapsible',
				'label' => __('Title','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'title' => array(
						'type' => 'input',
						'label' => __('Title:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'std' => 'Lorem ipsum'
					),
					'title_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['title_color']
					),
					'title_hover_color' => array(
						'type' => 'color',
						'label' => __('Hover:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['main_color']
					)
				)
			),
			'group_icon' => array(
				'type' => 'collapsible',
				'label' => __('Icon','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'icon' => array(
						'type' => 'icon',
						'label' => __('Icon Type:','frontend-builder'),
						'std' => 'na-svg23'
					),
					'icon_size' => array(
						'type' => 'number',
						'std' => 40,
						'unit' => 'px',
						'half_column' => 'true',
						'max' => 150,
						'label' => __('Size:','frontend-builder')
					),
					'icon_padding' => array(
						'type' => 'number',
						'std' => 0,
						'unit' => 'px',
						'half_column' => 'true',
						'max' => 100,
						'label' => __('Padding:','frontend-builder')
					),
					'icon_color' => array(
						'type' => 'color',
						'label' => __('Color','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['title_color']
					),
					'icon_hover_color' => array(
						'type' => 'color',
						'label' => __('Hover:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['main_color']
					),
					'icon_border' => array(
						'type' => 'checkbox',
						'std' => 'false',
						'label' => __('Icon border','frontend-builder')
					)	
				)
			),
			'group_text' => array(
				'type' => 'collapsible',
				'label' => __('Text','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'content' => array(
						'type' => 'textarea',
						'label' => __('Content','frontend-builder'),
						'std' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.'
					),
					'text_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['text_color']
					),
					'text_hover_color' => array(
						'type' => 'color',
						'label' => __('Hover:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['main_color']
					)			
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* CONTACT FORM */
/* -------------------------------------------------------------------------------- */

$contact_form = array(
	'contact_form' => array(
		'type' => 'draggable',
		'text' => __('Contact Form','frontend-builder'),
		'icon' => $this->url.'images/icons/contact-form.png',
		'function' => 'fbuilder_contact_form',
		'group' => 'Content',
		'options'  => array_merge(
		 array(
		 	'group_colors' => array(
				'type' => 'collapsible',
				'label' => __('Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'text_color' => array(
						'type' => 'color',
						'label' => __('Text:','frontend-builder'),
						'std' => '#333333',
						'label_width' => 0.5,
						'control_width' => 0.50
					),
					'active_text_color' => array(
						'type' => 'color',
						'label' => __('Active Text:','frontend-builder'),
						'std' => '#cccccc',
						'label_width' => 0.5,
						'control_width' => 0.50
					),
					'background_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => 'transparent',
						'label_width' => 0.5,
						'control_width' => 0.50
					),
					'active_background_color' => array(
						'type' => 'color',
						'label' => __('Active Background:','frontend-builder'),
						'std' => 'transparent',
						'label_width' => 0.5,
						'control_width' => 0.50
					),
					'border_color' => array(
						'type' => 'color',
						'label' => __('Border:','frontend-builder'),
						'std' => '#e7e7e7',
						'label_width' => 0.5,
						'control_width' => 0.50
					),
					'active_border_color' => array(
						'type' => 'color',
						'label' => __('Active Border:','frontend-builder'),
						'std' => '#cccccc',
						'label_width' => 0.5,
						'control_width' => 0.50
					)
				)	
			),
			'group_form' => array(
				'type' => 'collapsible',
				'label' => __('Form','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'email_ph' => array(
						'type' => 'input',
						'label' => __('Email Field Text:','frontend-builder'),
						'label_width' => 0.5,
						'std' => __('E-mail Address','frontend-builder'),
						'control_width' => 0.5,
						'desc' => 'Text for the email field'
					),
					'name_ph' => array(
						'type' => 'input',
						'label' => __('Name Field Text:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'std' => __('Name','frontend-builder'),
						'desc' => 'Text for the name field'
					),
					'website_ph' => array(
						'type' => 'input',
						'label' => __('Website Field Text:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'desc' => 'Text for the website field',
						'std' => __('Website','frontend-builder')
					),
					'textarea_ph' => array(
						'type' => 'input',
						'label' => __('Message Field Text:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'desc' => 'Text for the textarea field',
						'std' => __('Message goes here','frontend-builder')
					),
					'custom_field' => array(
						'type' => 'checkbox',
						'half_column' => 'true',
						'label' => __('Custom Field','frontend-builder'),
						'std' => 'false'
					),
					'datepicker' => array(
						'type' => 'checkbox',
						'half_column' => 'true',
						'label' => __('Date picker','frontend-builder'),
						'std' => 'false',
						'hide_if' => array(
							'custom_field' => array('false')
						)
					),
					'custom_ph' => array(
						'type' => 'input',
						'label' => __('Custom Field Text:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'desc' => 'Text for the custom field',
						'hide_if' => array(
							'custom_field' => array('false')
						)
					),
					'recipient_name' => array(
						'type' => 'input',
						'label' => __('Recipient Name:','frontend-builder'),
						'label_width' => 1,
						'control_width' => 1,
						'std' => '',
						'desc' => __('Name to be shown in auto-response message','frontend-builder')
					),
					'recipient_email' => array(
						'type' => 'input',
						'label' => __('Recipient E-mail:','frontend-builder'),
						'label_width' => 1,
						'std' => '',
						'control_width' => 1
					), 
					'required' => array(
						'type' => 'select',
						'multiselect' => 'true',
						'label' => __('Required Fields:','frontend-builder'),
						'std' => 'name,email,textarea',
						'label_width' => 1,
						'control_width' => 1,
						'options' => array(
							'email' => 'E-mail',
							'name' => 'Name',
							'website' => 'Website',
							'custom' => 'Custom Field',
							'textarea' => 'Message',
						)
					),
					'send_response_delay' => array(
						'type' => 'number',
						'label' => __('Response Delay:','frontend-builder'),
						'max' => 10000,
						'label_width' => 0.5,
						'control_width' => 0.5,
						'std' => 1500,
						'unit' => 'ms'
					),
					'response_message' => array(
						'type' => 'textarea',
						'label' => __('Response Message:','frontend-builder'),
						'label_width' => 1,
						'control_width' => 1,
						'std' => __('Message Successfully Sent!','frontend-builder'),
					)
				)
			),
			'group_button' => array(
				'type' => 'collapsible',
				'label' => __('Button','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'submit_ph' => array(
						'type' => 'input',
						'label' => __('Submit Field Text:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'desc' => 'Text for the submit field',
						'std' => __('Submit','frontend-builder')
					),
					'button_text_color' => array(
						'type' => 'color',
						'label' => __('Text:','frontend-builder'),
						'std' => '#ffffff',
						'label_width' => 0.5,
						'control_width' => 0.50
					),
					'active_button_text_color' => array(
						'type' => 'color',
						'label' => __('Active Text:','frontend-builder'),
						'std' => '#ffffff',
						'label_width' => 0.5,
						'control_width' => 0.50
					),
					'button_background_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => '#555555',
						'label_width' => 0.5,
						'control_width' => 0.50
					),
					'active_button_background_color' => array(
						'type' => 'color',
						'label' => __('Active Background:','frontend-builder'),
						'std' => '#222222',
						'label_width' => 0.5,
						'control_width' => 0.50
					),
					'button_border_color' => array(
						'type' => 'color',
						'label' => __('Border:','frontend-builder'),
						'std' => '#555555',
						'label_width' => 0.5,
						'control_width' => 0.50
					),
					'active_button_border_color' => array(
						'type' => 'color',
						'label' => __('Active Border:','frontend-builder'),
						'std' => '#222222',
						'label_width' => 0.5,
						'control_width' => 0.50
					),
					'button_align' => array(
						'type' => 'select',
						'label' => __('Button Alignment:','frontend-builder'),
						'std' => 'right',
						'options' => array(
							'center' => 'Center',
							'left' => 'Left',
							'right' => 'Right'
						)
					),
					'button_fullwidth' => array(
						'type' => 'checkbox',
						'label' => __('Button Fullwidth','frontend-builder'),
						'std' => 'false'
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);


/* -------------------------------------------------------------------------------- */
/* TESTIMONIALS */
/* -------------------------------------------------------------------------------- */

$testimonials = array(
	'testimonials' => array(
		'type' => 'draggable',
		'text' => __('Testimonials','frontend-builder'),
		'icon' => $this->url.'images/icons/testimonials.png',
		'function' => 'fbuilder_testimonials',
		'group' => 'Basic',
		'options'  => array_merge(
		 array(
		 	'group_name' => array(
				'type' => 'collapsible',
				'label' => __('Name','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'name' => array(
						'type' => __('input','frontend-builder'),
						'label' => __('Name:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'std' => __('John Dough','frontend-builder')
					),
					'profession' => array(
						'type' => 'input',
						'label' => __('Title:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'std' => __('photographer / fashion interactive','frontend-builder'),
					),
					'name_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['title_color']
					),
					'url' => array(
						'type' => 'input',
						'label' => __('Link:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'desc' => 'Type if you want to link the testimonial'			
					),
					'main_color' => array(
						'type' => 'color',
						'label' => __('Line:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['main_color']
					)
				)
			),
			'group_quote' => array(
				'type' => 'collapsible',
				'label' => __('Quote','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'quote' => array(
						'type' => 'input',
						'label' => __('Quote:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'std' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
					),
					'style' => array(
						'type' => 'select',
						'label' => __('Style:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'std' => 'default',
						'options' => array(
							'default' => __('Default','frontend-builder'),
							'clean' => __('Clean','frontend-builder'),
							'squared' => __('Squared','frontend-builder'),
							'rounded' => __('Rounded','frontend-builder')
						)
					),
					'quote_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['text_color']
					),
					'italic' => array(
						'type' => 'checkbox',
						'std' => 'true',
						'label' => __('Italic','frontend-builder'),
						'desc' => __('Enabling this option makes the text italic','frontend-builder')
					)	
				)
			),
			'group_image' => array(
				'type' => 'collapsible',
				'open' => 'true',
				'label' => __('Image','frontend-builder'),
				'options' => array(
					'image' => array(
						'type' => 'image',
						'std' => $this->url.'images/testimonials-image.jpg',
						'desc' => '80x80'			
					)
				)
			),
			'group_background' => array(
				'type' => 'collapsible',
				'label' => __('Background','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'back_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['dark_back_color']
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	
	)
);


/* -------------------------------------------------------------------------------- */
/* TABS */
/* -------------------------------------------------------------------------------- */

$tabs = array(
	'tabs' => array(
		'type' => 'draggable',
		'text' => __('Tabs','frontend-builder'),
		'icon' => $this->url.'images/icons/tabs.png',
		'function' => 'fbuilder_tabs',
		'group' => 'Basic',
		'options'  => array_merge(
		 array(
		 	'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'style' => array(
						'type' => 'select',
						'label' => __('Style','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'std' => 'default',
						'options' => array(
							'default' => __('Default', 'frontend-builder'),
							'clean' => __('Clean','frontend-builder'),
							'squared' => __('Squared','frontend-builder'),
							'rounded' => __('Rounded','frontend-builder')
						)
					)
				)
			),
			'group_tab_colors' => array(
				'type' => 'collapsible',
				'label' => __('Tab Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'title_color' => array(
						'type' => 'color',
						'label' => __('Title:','frontend-builder'),
						'std' => $opts['title_color']
					),
					'active_tab_title_color' => array(
						'type' => 'color',
						'label' => __('Active title:','frontend-builder'),
						'std' => $opts['title_color'],
						'hide_if' => array(
							'style' => array('clean', 'default')
						)
					),
					'tab_back_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => $opts['dark_back_color'],
						'hide_if' => array(
							'style' => array('clean')
						)
					),
					'active_tab_border_color' => array(
						'type' => 'color',
						'label' => __('Active border:','frontend-builder'),
						'std' => $opts['main_color']
					)
				)
			),
			'group_content_colors' => array(
				'type' => 'collapsible',
				'label' => __('Content Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(	
						'text_color' => array(
						'type' => 'color',
						'label' => __('Text:','frontend-builder'),
						'std' => $opts['text_color']
					),
					'border_color' => array(
						'type' => 'color',
						'label' => __('Border:','frontend-builder'),
						'std' => $opts['light_border_color'],
						'hide_if' => array(
							'style' => array('default')
						)
					),
					'back_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => $opts['light_back_color'],
						'hide_if' => array(
							'style' => array('clean', 'default')
						)
					)
				)
			),
			'group_tabs' => array(
				'type' => 'collapsible',
				'label' => __('Tabs','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'sortable' => array(
						'type' => 'sortable',
						'desc' => __('Elements are sortable','frontend-builder'),
						'item_name' => __('tab item','frontend-builder'),
						'label_width' => 0,
						'control_width' => 1,
						'std' => array(
							'items' => array(
								0 => array(
									'title' => 'Lorem ipsum',
									'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
									'image' => '',
									'active' => 'true'
								),
								1 => array(
									'title' => 'Lorem ipsum',
									'content' => 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.',
									'image' => '',
									'active' => 'false'
								),
								2 => array(
									'title' => 'Lorem ipsum',
									'content' => 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
									'image' => '',
									'active' => 'false'
								)
							),
							'order' => array(
								0 => 0,
								1 => 1,
								2 => 2
							)
						),
						
						'options'=> array(
							'title' => array(
								'type' => 'input',
								'label_width' => 0.25,
								'control_width' => 0.75,
								'label' => __('Title:','frontend-builder')
							),
							'content' => array(
								'type' => 'textarea'
							),
							'image' => array(
								'type' => 'image',
								'label' => __('Image:','frontend-builder'),
								'label_width' => 0.25,
								'control_width' => 0.75,
								'desc' => __('Add an image to tab content','frontend-builder')
							),
							'active' => array(
								'type' => 'checkbox',
								'label' => __('Mark as Default','frontend-builder'),
								'desc' => __('Only one panel can be active at a time, so be sure to uncheck the others for it to work properly','frontend-builder')
							)
						)
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* TOUR */
/* -------------------------------------------------------------------------------- */

$tour = array(
	'tour' => array(
		'type' => 'draggable',
		'text' => __('Tour','frontend-builder'),
		'icon' => $this->url.'images/icons/tour.png',
		'function' => 'fbuilder_tour',
		'group' => 'Basic',
		'options'  => array_merge(
		 array(
		 	'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'tab_position' => array(
						'type' => 'select',
						'label' => __('Tab position:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'std' => 'left',
						'options' => array(
							'left' => __('Left','frontend-builder'),
							'right' => __('Right','frontend-builder')
						)
					),
					'tab_text_align' => array(
						'type' => 'select',
						'label' => __('Tab header align:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'std' => 'left',
						'options' => array(
							'left' => __('Left','frontend-builder'),
							'right' => __('Right','frontend-builder'),
							'center' => __('Center','frontend-builder')
						)
					),
					'round' => array(
								'type' => 'checkbox',
								'label' => __('Round','frontend-builder'),
								'label_width' => 0.5,
								'control_width' => 0.5,
								'desc' => __('Adds border-radius to "Tour" shortcode','frontend-builder'),
								'std' => 'false'
							),
					'border_position' => array(
								'type' => 'checkbox',
								'label' => __('Full tab border','frontend-builder'),
								'label_width' => 0.5,
								'control_width' => 0.5,
								'desc' => __('If this option isn\'t set, only left/right border will be applied to tabs. Otherwise, full border will be set.','frontend-builder'),
								'std' => 'false'
							)
				)
			),
			'group_tab_colors' => array(
				'type' => 'collapsible',
				'label' => __('Tour Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'title_color' => array(
						'type' => 'color',
						'label' => __('Title:','frontend-builder'),
						'std' => $opts['title_color']
					),
					'active_tab_title_color' => array(
						'type' => 'color',
						'label' => __('Active title:','frontend-builder'),
						'std' => $opts['title_color'],
					),
					'tab_back_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => $opts['dark_back_color'],
					),
					'tab_border_color' => array(
						'type' => 'color',
						'label' => __('Border:','frontend-builder'),
						'std' => $opts['title_color']
					),
					'active_tab_border_color' => array(
						'type' => 'color',
						'label' => __('Active border:','frontend-builder'),
						'std' => $opts['main_color']
					)
				)
			),
			'group_content_colors' => array(
				'type' => 'collapsible',
				'label' => __('Content Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(	
						'text_color' => array(
						'type' => 'color',
						'label' => __('Text:','frontend-builder'),
						'std' => $opts['text_color']
					),
					'border_color' => array(
						'type' => 'color',
						'label' => __('Border:','frontend-builder'),
						'std' => $opts['light_border_color']
					),
					'back_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => $opts['light_back_color']
					)
				)
			),
			'group_tabs' => array(
				'type' => 'collapsible',
				'label' => __('Tour','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'sortable' => array(
						'type' => 'sortable',
						'desc' => __('Elements are sortable','frontend-builder'),
						'item_name' => __('Tour item','frontend-builder'),
						'label_width' => 0,
						'control_width' => 1,
						'std' => array(
							'items' => array(
								0 => array(
									'title' => 'Lorem ipsum',
									'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
									'image' => '',
									'active' => 'true'
								),
								1 => array(
									'title' => 'Lorem ipsum',
									'content' => 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.',
									'image' => '',
									'active' => 'false'
								),
								2 => array(
									'title' => 'Lorem ipsum',
									'content' => 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
									'image' => '',
									'active' => 'false'
								)
							),
							'order' => array(
								0 => 0,
								1 => 1,
								2 => 2
							)
						),
						
						'options'=> array(
							'title' => array(
								'type' => 'input',
								'label_width' => 0.25,
								'control_width' => 0.75,
								'label' => __('Title:','frontend-builder')
							),
							'content' => array(
								'type' => 'textarea'
							),
							'image' => array(
								'type' => 'image',
								'label' => __('Image:','frontend-builder'),
								'label_width' => 0.25,
								'control_width' => 0.75,
								'desc' => __('Add an image to tour content','frontend-builder')
							),
							'active' => array(
								'type' => 'checkbox',
								'label' => __('Mark as Default','frontend-builder'),
								'desc' => __('Only one panel can be active at a time, so be sure to uncheck the others for it to work properly','frontend-builder')
							)
						)
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* ACCORDION */
/* -------------------------------------------------------------------------------- */
$accordion = array(
	'accordion' => array(
		'type' => 'draggable',
		'text' => __('Accordion','frontend-builder'),
		'icon' => $this->url.'images/icons/accordion.png',
		'function' => 'fbuilder_accordion',
		'group' => 'Basic',
		'options'  => array_merge(
		 array(
		 	'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'style' => array(
						'type' => 'select',
						'label' => __('Style:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'std' => 'default',
						'options' => array(
							'default' => __('Default','frontend-builder'),
							'clean-right' => __('Clean right','frontend-builder'),
							'squared-right' => __('Squared right','frontend-builder'),
							'rounded-right' => __('Rounded right','frontend-builder'),
							'clean-left' => __('Clean left','frontend-builder'),
							'squared-left' => __('Squared left','frontend-builder'),
							'rounded-left' => __('Rounded left','frontend-builder')
						)
					),
					'text_color' => array(
						'type' => 'color',
						'label' => __('Text:','frontend-builder'),
						'std' => $opts['text_color']
					),
					'main_color' => array(
						'type' => 'color',
						'label' => __('Main:','frontend-builder'),
						'std' => $opts['main_color'],
						'hide_if' => array(
							'style' => array('default')
						)
					),
					'fixed_height' => array(
						'type' => 'checkbox',
						'label' => __('Fixed height','frontend-builder'),
						'desc' => __('if desabled height will vary due to content height','frontend-builder'),
						'std' => 'true'
					)
				)
			),
			'group_title_colors' => array(
				'type' => 'collapsible',
				'label' => __('Title Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'title_color' => array(
						'type' => 'color',
						'label' => __('Title','frontend-builder'),
						'std' => $opts['title_color']
					),
					'title_active_color' => array(
						'type' => 'color',
						'label' => __('Active Title:','frontend-builder'),
						'std' => $opts['title_color'],
						'hide_if' => array(
							'style' => array('clean-right', 'clean-left', 'squared-left', 'rounded-left')
						)
					)
				)
			),
			'group_trigger_colors' => array(
				'type' => 'collapsible',
				'label' => __('Trigger Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'trigger_color' => array(
						'type' => 'color',
						'label' => __('Trigger:','frontend-builder'),
						'std' => $opts['title_color']
					),
					'trigger_active_color' => array(
						'type' => 'color',
						'label' => __('Trigger active:','frontend-builder'),
						'std' => $opts['title_color']
					)
				)
			),
			'group_background_colors' => array(
				'type' => 'collapsible',
				'label' => __('Background Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(	
					'back_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => ''
					),
					'border_color' => array(
						'type' => 'color',
						'label' => __('Border:','frontend-builder'),
						'std' => $opts['light_border_color']
					)
				)
			),
			'group_accordion_elements' => array(
				'type' => 'collapsible',
				'label' => __('Accordion Elements','frontend-builder'),
				'open' => 'true',
				
				'options' => array(
					'sortable' => array(
						'type' => 'sortable',
						'desc' => __('Elements are sortable','frontend-builder'),
						'item_name' => __('accordion item','frontend-builder'),
						'label_width' => 0,
						'control_width' => 1,
						'std' => array(
							'items' => array(
								0 => array(
									'title' => 'Lorem ipsum',
									'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
									'image' => '',
									'active' => 'true'
								),
								1 => array(
									'title' => 'Lorem ipsum',
									'content' => 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.',
									'image' => '',
									'active' => 'false'
								),
								2 => array(
									'title' => 'Lorem ipsum',
									'content' => 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
									'image' => '',
									'active' => 'false'
								)
							),
							'order' => array(
								0 => 0,
								1 => 1,
								2 => 2
							)
						),
						'options'=> array(
							'title' => array(
								'type' => 'input',
								'label_width' => 0.25,
								'control_width' => 0.75,
								'label' => __('Title:','frontend-builder')
							),
							'content' => array(
								'type' => 'textarea'
							),
							'image' => array(
								'type' => 'image',
								'label_width' => 0.25,
								'control_width' => 0.75,
								'label' => __('Image:','frontend-builder'),
								'desc' => __('Add an image to accordion content','frontend-builder')
							),
							'active' => array(
								'type' => 'checkbox',
								'label' => __('Active','frontend-builder'),
								'desc' => __('Only one panel can be active at a time, so be sure to uncheck the others for it to work properly','frontend-builder')
							)
						)
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* TOGGLE */
/* -------------------------------------------------------------------------------- */
$toggle = array(
	'toggle' => array(
		'type' => 'draggable',
		'text' => __('Toggle','frontend-builder'),
		'icon' => $this->url.'images/icons/toggle.png',
		'function' => 'fbuilder_toggle',
		'group' => 'Basic',
		'options'  => array_merge(
		 array(
		 	'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'style' => array(
						'type' => 'select',
						'label' => __('Style:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'std' => 'clean-right',
						'options' => array(
							'clean-right' => __('Clean right','frontend-builder'),
							'squared-right' => __('Squared right','frontend-builder'),
							'rounded-right' => __('Rounded right','frontend-builder'),
							'clean-left' => __('Clean left','frontend-builder'),
							'squared-left' => __('Squared left','frontend-builder'),
							'rounded-left' => __('Rounded left','frontend-builder')
						)
					),
					'text_color' => array(
						'type' => 'color',
						'label' => __('Text:','frontend-builder'),
						'std' => $opts['text_color']
					),
					'main_color' => array(
						'type' => 'color',
						'label' => __('Main:','frontend-builder'),
						'std' => $opts['main_color']
					),
					'fixed_height' => array(
						'type' => 'checkbox',
						'label' => __('Fixed height','frontend-builder'),
						'desc' => __('if desabled height will vary due to content height','frontend-builder'),
						'std' => 'false'
					)
				)
			),
			'group_title_colors' => array(
				'type' => 'collapsible',
				'label' => __('Title Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'title_color' => array(
						'type' => 'color',
						'label' => __('Title','frontend-builder'),
						'std' => $opts['title_color']
					),
					'title_active_color' => array(
						'type' => 'color',
						'label' => __('Active Title:','frontend-builder'),
						'std' => $opts['title_color'],
						'hide_if' => array(
							'style' => array('clean-right', 'clean-left', 'squared-left', 'rounded-left')
						)
					)
				)
			),
			'group_trigger_colors' => array(
				'type' => 'collapsible',
				'label' => __('Trigger Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'trigger_color' => array(
						'type' => 'color',
						'label' => __('Trigger:','frontend-builder'),
						'std' => $opts['title_color']
					),
					'trigger_active_color' => array(
						'type' => 'color',
						'label' => __('Trigger active:','frontend-builder'),
						'std' => $opts['title_color']
					)
				)
			),
			'group_background_colors' => array(
				'type' => 'collapsible',
				'label' => __('Background Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(	
					'back_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => ''
					),
					'border_color' => array(
						'type' => 'color',
						'label' => __('Border:','frontend-builder'),
						'std' => $opts['light_border_color']
					),
					'active_border_color' => array(
						'type' => 'color',
						'label' => __('Border active:','frontend-builder'),
						'std' => '',
						'desc' => 'Active item\'s border'
					)
				)
			),
			'group_accordion_elements' => array(
				'type' => 'collapsible',
				'label' => __('Trigger Elements','frontend-builder'),
				'open' => 'true',
				
				'options' => array(
					'sortable' => array(
						'type' => 'sortable',
						'desc' => __('Elements are sortable','frontend-builder'),
						'item_name' => __('Trigger item','frontend-builder'),
						'label_width' => 0,
						'control_width' => 1,
						'std' => array(
							'items' => array(
								0 => array(
									'title' => 'Lorem ipsum',
									'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
									'image' => '',
									'active' => 'true'
								),
								1 => array(
									'title' => 'Lorem ipsum',
									'content' => 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.',
									'image' => '',
									'active' => 'false'
								),
								2 => array(
									'title' => 'Lorem ipsum',
									'content' => 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
									'image' => '',
									'active' => 'false'
								)
							),
							'order' => array(
								0 => 0,
								1 => 1,
								2 => 2
							)
						),
						'options'=> array(
							'title' => array(
								'type' => 'input',
								'label_width' => 0.25,
								'control_width' => 0.75,
								'label' => __('Title:','frontend-builder')
							),
							'content' => array(
								'type' => 'textarea'
							),
							'image' => array(
								'type' => 'image',
								'label_width' => 0.25,
								'control_width' => 0.75,
								'label' => __('Image:','frontend-builder'),
								'desc' => __('Add an image to trigger item content','frontend-builder')
							),
							'active' => array(
								'type' => 'checkbox',
								'label' => __('Active','frontend-builder'),
								'desc' => __('Only one panel can be active at a time, so be sure to uncheck the others for it to work properly','frontend-builder')
							)
						)
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);


/* -------------------------------------------------------------------------------- */
/* COUNTER */
/* -------------------------------------------------------------------------------- */
$counter = array(
	'counter' => array(
		'type' => 'draggable',
		'text' => __('Counter','frontend-builder'),
		'icon' => $this->url.'images/icons/counter.png',
		'function' => 'fbuilder_counter',
		'group' => 'Charts, Bars, Counters',
		'options'  => array_merge(
		 array(
		 	'group_counter' => array(
				'type' => 'collapsible',
				'label' => __('Counter','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'start_val' => array(
						'type' => 'input',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'label' => __('Start No:','frontend-builder'),
						'std' => 9999
					),
					'end_val' => array(
						'type' => 'input',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'label' => __('End No:','frontend-builder'),
						'std' => 8847
					),
					'direction' => array(
						'type' => 'select',
						'label' => __('Direction:','frontend-builder'),
						'std' => 'auto',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'options' => array(
							'auto' => 'Auto',
							'upward' => 'Upward',
							'downward' => 'Downward'
						)
					),
					'color' => array(
						'type' => 'color',
						'label_width' => 0.25,
						'control_width' => 0.5,
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['text_color']
					),	
					'font_size' => array(
						'type' => 'number',
						'label' => __('Size:','frontend-builder'),
						'std' => 60,
						'min' => 10,
						'half_column' => 'true',
						'max' => 350,
						'unit' => 'px'
					),
					'item_align' => array(
						'type' => 'select',
						'label' => __('Item Alignment:','frontend-builder'),
						'std' => 'center',
						'options' => array(
							'center' => 'Center',
							'left' => 'Left',
							'right' => 'Right'
						)
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* PERCENTAGE BARS */
/* -------------------------------------------------------------------------------- */
$percentage_bars = array(
	'percentage_bars' => array(
		'type' => 'draggable',
		'text' => __('Percentage Bars','frontend-builder'),
		'icon' => $this->url.'images/icons/percentage-bar.png',
		'function' => 'fbuilder_percentage_bars',
		'group' => 'Charts, Bars, Counters',
		'options'  => array_merge(
		 array(
		 	'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'bar_style' => array(
						'type' => 'select',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'label' => __('Bar Style:','frontend-builder'),
						'std' => 'sharp',
						'options' => array(
							'sharp' => 'Sharp',
							'round' => 'Round',
						)
					),
					'element_spacing' => array(
						'type' => 'number',
						'label' => __('Bar Margin:','frontend-builder'),
						'min' => 0,
						'max'=> 100,
						'std' => 20,
						'unit' => 'px'
					),
					'custom_height' => array(
						'type' => 'number',
						'label' => __('Bar Height:','frontend-builder'),
						'min' => 1,
						'max'=> 100,
						'std' => 5,
						'unit' => 'px'
					),
					'headline_color' => array(
						'type' => 'color',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'label' => __('Headline Color:','frontend-builder'),
						'std' => $opts['title_color']
					),
					'line_background' => array(
						'type' => 'color',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'label' => __('Bar Bg. Color:','frontend-builder'),
						'std' => '#eeeeee'
					),
					'percent_pin' => array(
						'type' => 'checkbox',
						'half_column' => 'true',
						'label' => __('Percentage Pin','frontend-builder'),
						'std' => 'true'
					),
					'headline_inside' => array(
						'type' => 'checkbox',
						'half_column' => 'true',
						'label' => __('Headline Inside','frontend-builder'),
						'std' => 'false'
					),
					'headline_top_margin' => array(
						'type' => 'number',
						'label' => __('H-Tag Top Margin:','frontend-builder'),
						'min' => -50,
						'max'=> 50,
						'std' => 0,
						'label_width' => 0.5,
						'control_width' => 0.5,
						'unit' => 'px',
						'hide_if' => array(
							'headline_inside' => array('false')
						)
					)
				)
			),
			'group_sortable' => array(
				'type' => 'collapsible',
				'label' => __('Sortable','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'sortable' => array(
						'type' => 'sortable',
						'label' => __('Bars','frontend-builder'),
						'item_name' => 'bar',
						'label_width' => 0,
						'control_width' => 1,
						'std' => array(
							'items' => array(
								0 => array(
									'direction' => 'ltr',
									'headline_content' => 'Percentage Bar',
									'line_color' => $opts['main_color'],
									'pin_color' => '#222222',
									'pin_text_color' => '#ffffff',
									'percentage' => 89,
									'an_speed' => 1000,
									'pattern_url' => ' '
								)
							),
							'order' => array(
								0 => 0
							)
						),
						'options' => array(
							'headline_content' => array(
								'type' => 'input',
								'label_width' => 1,
								'control_width' => 1,
								'label' => __('Headline Content:','frontend-builder'),
								'std' => 'Percentage Bar'
							),
							'pattern_url' => array(
								'type' => 'image',
								'label' =>  __('Image/Pattern:','frontend-builder'),
								'std' => ' '
							),
							'direction' => array(
								'type' => 'select',
								'label_width' => 0.5,
								'control_width' => 0.5,
								'label' => __('Direction:','frontend-builder'),
								'std' => 'ltr',
								'options' => array(
									'ltr' => 'Left to Right',
									'rtl' => 'Right to Left',
								)
							),
							'line_color' => array(
								'type' => 'color',
								'label_width' => 0.5,
								'control_width' => 0.5,
								'label' => __('Bar Active Color:','frontend-builder'),
								'std' => $opts['main_color']
							),
							'percentage' => array(
								'type' => 'number',
								'label' => __('Percentage:','frontend-builder'),
								'min' => 0,
								'max'=> 100,
								'std' => 100,
								'unit' => '%'
							),
							'an_speed' => array(
								'type' => 'number',
								'label' => __('Animation Speed:','frontend-builder'),
								'min' => 100,
								'max'=> 10000,
								'std' => 1000,
								'unit' => 'ms'
							),
							'pin_color' => array(
								'type' => 'color',
								'label_width' => 0.5,
								'control_width' => 0.5,
								'label' => __('Pin Color:','frontend-builder'),
								'std' => '#222222',
								'hide_if' => array(
									'percent_pin' => array('false')
								)
							),
							'pin_text_color' => array(
								'type' => 'color',
								'label_width' => 0.5,
								'control_width' => 0.5,
								'label' => __('Pin Text Color:','frontend-builder'),
								'std' => '#ffffff',
								'hide_if' => array(
									'percent_pin' => array('false')
								)
							)
						)
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* PERCENTAGE CHART */
/* -------------------------------------------------------------------------------- */
$percentage_chart = array(
	'percentage_chart' => array(
		'type' => 'draggable',
		'text' => __('Percentage Chart','frontend-builder'),
		'icon' => $this->url.'images/icons/percentage-cart.png',
		'function' => 'fbuilder_percentage_chart',
		'group' => 'Charts, Bars, Counters',
		'options'  => array_merge(
		 array(
		 	'group_number' => array(
				'type' => 'collapsible',
				'label' => __('Number','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'font_size' => array(
						'type' => 'number',
						'label' => __('Size:','frontend-builder'),
						'std' => 36,
						'min' => 10,
						'half_column' => 'true',
						'max' => 200,
						'unit' => 'px'
					),
					'percentage' => array(
						'type' => 'number',
						'label' => __('Number:','frontend-builder'),
						'min' => 0,
						'half_column' => 'true',
						'max' => 100,
						'std' => 73
					),
					'color' => array(
						'type' => 'color',
						'label_width' => 0.25,
						'control_width' => 0.5,
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['text_color']
					),
					'item_align' => array(
						'type' => 'select',
						'label' => __('Item Alignment:','frontend-builder'),
						'std' => 'center',
						'options' => array(
							'center' => 'Center',
							'left' => 'Left',
							'right' => 'Right'
						)
					),
					'percent_char' => array(
						'type' => 'checkbox',
						'label' => __('Percentage Character','frontend-builder'),
						'std' => 'true'
					)
				)
			),
			'group_line' => array(
				'type' => 'collapsible',
				'label' => __('Line','frontend-builder'),
				'open' => 'true',
				'options' => array(	
					/*'line_style' => array(
						'type' => 'select',
						'label' => __('Cap:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'std' => 'square',
						'options' => array(
							'square' => 'Square',
							'round' => 'Round'
						)
					),	*/
					'line_width' => array(
						'type' => 'number',
						'label' => __('Width:','frontend-builder'),
						'min' => 1,
						'half_column' => 'true',
						'max' => 20,
						'std' => 3
					),
					'radius' => array(
						'type' => 'number',
						'label' => __('Radius:','frontend-builder'),
						'min' => 50,
						'half_column' => 'true',
						'max' => 280,
						'std' => 200
					),
					'line_color' => array(
						'type' => 'color',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'label' => __('Active Color:','frontend-builder'),
						'std' => $opts['main_color']
					),
					'background_line_color' => array(
						'type' => 'color',
						'label_width' => 0.5,
						'control_width' => 0.5,
						'label' => __('Passive Color:','frontend-builder'),
						'std' => '#ffffff'
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* GRAPH */
/* -------------------------------------------------------------------------------- */
$graph = array(
	'graph' => array(
		'type' => 'draggable',
		'text' => __('Graph','frontend-builder'),
		'icon' => $this->url.'images/icons/graph.png',
		'function' => 'fbuilder_graph',
		'group' => 'Charts, Bars, Counters',
		'options'  => array_merge(
		 array(
		 	'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'item_align' => array(
						'type' => 'select',
						'label' => __('Item alignment:','frontend-builder'),
						'std' => 'center',
						'options' => array(
							'center' => 'Center',
							'left' => 'Left',
							'right' => 'Right'
						)
					),
					'item_height' => array(
						'type' => 'number',
						'label' => __('Height:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'max' => 1200,
						'min' => 100,
						'std' => 300
					),
					'item_width' => array(
						'type' => 'number',
						'label' => __('Width:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'max' => 1200,
						'min' => 100,
						'std' => 1000
					)
				)
			),
			'group_legend' => array(
				'type' => 'collapsible',
				'label' => __('Legend','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'legend_position' => array(
						'type' => 'select',
						'label' => __('Position:','frontend-builder'),
						'std' => 'right',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'options' => array(
							'bottom' => 'Bottom',
							'left' => 'Left',
							'right' => 'Right'
						)
					),
					'legend_shape' => array(
						'type' => 'select',
						'label' => __('Shape:','frontend-builder'),
						'std' => 'circle',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'options' => array(
							'square' => 'Square',
							'round' => 'Round',
							'circle' => 'Circle'
						)
					),
					'legend_font_color' => array(
						'type' => 'color',
						'label' => __('Text:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['text_color']
					),
					'legend_font_size' => array(
						'type' => 'number',
						'label' => __('Size:','frontend-builder'),
						'half_column' => 'true',
						'min' => 10,
						'max' => 100,
						'std' => 14,
						'unit' => 'px'
					)
				)
			),
			'group_graph' => array(
				'type' => 'collapsible',
				'label' => __('Graph','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'graph_style' => array(
						'type' => 'select',
						'label' => __('Style:','frontend-builder'),
						'std' => 'bar',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'options' => array(
							'line' => 'Line',
							'bar' => 'Bar'
						)
					),
					'scale_font_color' => array(
						'type' => 'color',
						'label' => __('Text:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['text_color']
					),
					'labels' => array(
						'type' => 'input',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'label' => __('Labels:','frontend-builder'),
						'desc' =>__('Words divided by comma'),
						'std' => 'January,February,March,April,May,June,July'
					),
					'fill' => array(
						'type' => 'checkbox',
						'std' => 'true',
						'half_column' => 'true',
						'label' => __('Fill','frontend-builder'),
						'hide_if' => array(
							'graph_style' => array('bar'),
						)
					),	
					'curve' => array(
						'type' => 'checkbox',
						'std' => 'true',
						'half_column' => 'true',
						'label' => __('Curve','frontend-builder'),
						'hide_if' => array(
							'graph_style' => array('bar'),
						)
					),	
					'bar_stroke' => array(
						'type' => 'checkbox',
						'std' => 'true',
						'label' => __('Stroke','frontend-builder'),
						'hide_if' => array(
							'graph_style' => array('line'),
						)
					)
				)
			),	
			'group_items' => array(
				'type' => 'collapsible',
				'label' => __('Items','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'sortable' => array(
						'type' => 'sortable',
						'label_width' => 0,
						'control_width' => 1,
						'desc' => __('Elements are sortable','frontend-builder'),
						'item_name' => __('Chart','frontend-builder'),
						'std' => array(
							'items' => array(
								0 => array(
									'data_value' => '15,20,25,30,50,60,80',
									'data_color' => '#44bdd6',
									'data_caption' => 'Lorem ipsum'
								)
							),
							'order' => array(
								0 => 0
							)
						
						),
						'options'=> array(
							'data_value' => array(
								'type' => 'input',
								'label_width' => 0.25,
								'control_width' => 0.75,
								'label' => __('Values:','frontend-builder'),
								'desc' =>__('Numbers divided by comma'),
								'std' => '1,34,53,22,13,2,3'
							),
							'data_caption' => array(
								'type' => 'input',
								'label_width' => 0.25,
								'control_width' => 0.75,
								'label' => __('Desc.:','frontend-builder'),
								'desc' =>__('Descriptive legend text'),
								'std' => 'Lorem ipsum'
							),
							'data_color' => array(
								'type' => 'color',
								'label_width' => 0.25,
								'control_width' => 0.5,
								'label' => __('Color:','frontend-builder'),
								'std' => '#000000'
							)
						)
					)	
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);


/* -------------------------------------------------------------------------------- */
/* GAUGE CHART */
/* -------------------------------------------------------------------------------- */

$gauge_chart = array(
	'gauge' => array(
		'type' => 'draggable',
		'text' => __('Gauge','frontend-builder'),
		'icon' => $this->url.'images/icons/gauge.png',
		'function' => 'fbuilder_gauge_chart',
		'group' => 'Charts, Bars, Counters',
		'options'  => array_merge(
		 array(
		 	'group_chart' => array(
				'type' => 'collapsible',
				'label' => __('Gauge','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'item_align' => array(
						'type' => 'select',
						'label' => __('Item alignment:','frontend-builder'),
						'std' => 'center',
						'options' => array(
							'center' => 'Center',
							'left' => 'Left',
							'right' => 'Right'
						)
					),
					'radius' => array(
						'type' => 'number',
						'label' => __('Radius:','frontend-builder'),
						'min' => 20,
						'max' => 1000,
						'std' => 400
					),
					'value' => array(
						'type' => 'number',
						'label' => __('Value:','frontend-builder'),
						'std' => 80,
						'min' => 0,
						'max' => 100,
						'desc' => __('Default value of the gauge chart','frontend-builder')
					),
					'min_value' => array(
						'type' => 'number',
						'label' => __('Min value:','frontend-builder'),
						'min' => 0,
						'max' => 10000,
						'std' => 0,
						'desc' => __('Minimum value of the gauge chart','frontend-builder')
					),
					'max_value' => array(
						'type' => 'number',
						'label' => __('Max value:','frontend-builder'),
						'min' => 0,
						'max' => 10000,
						'std' => 100,
						'desc' => __('Maximum value of the gauge chart','frontend-builder')
					),
					'unit' => array(
						'type' => 'input',
						'label' => __('Unit:','frontend-builder'),
						'std' => '%',
						'desc' =>  __('Gauge value unit','frontend-builder')
					),
					'show_min_max' => array(
						'type' => 'checkbox',
						'label' => __('Show min and max labels','frontend-builder'),
						'std' => 'true'
					),
					'show_inner_shadow' => array(
						'type' => 'checkbox',
						'label' => __('Show inner shadow','frontend-builder'),
						'std' => 'false'
					),
					'animation_length' => array(
						'type' => 'number',
						'label' => __('Animation length:','frontend-builder'),
						'min' => 0,
						'max' => 10000,
						'std' => 2500,
						'desc' => __('Length of the gauge animation in milliseconds','frontend-builder'),
						'unit' => 'ms'
					)
				)
			),
			'group_items' => array(
				'type' => 'collapsible',
				'label' => __('Gauge gradient colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'gauge_gradient_1' => array(
						'type' => 'color',
						'label' => __('Gradient color 1:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'std' => '#1abc9c'
					),
					'gauge_gradient_2' => array(
						'type' => 'color',
						'label' => __('Gradient color 2:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'std' => '#9b59b6'
					),
					'gauge_gradient_3' => array(
						'type' => 'color',
						'label' => __('Gradient color 3:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'std' => '#2c3e50'
					),
					'gauge_gradient_4' => array(
						'type' => 'color',
						'label' => __('Gradient color 4:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'std' => '#d35400'
					),
					'gauge_gradient_5' => array(
						'type' => 'color',
						'label' => __('Gradient color 5:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'std' => '#c0392b'
					),
				)
			),
			'group_legend' => array(
				'type' => 'collapsible',
				'label' => __('Style','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'value_color' => array(
						'type' => 'color',
						'label' => __('Value text color:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'std' => $opts['text_color']
					),
					'unit_color' => array(
						'type' => 'color',
						'label' => __('Unit text color:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'std' => $opts['text_color']
					),
					'gauge_color' => array(
						'type' => 'color',
						'label' => __('Gauge color:','frontend-builder'),
						'label_width' => 0.5,
						'control_width' => 0.5,
						'std' => '#bfbfbf'
					),
					'gauge_thickness' => array(
						'type' => 'number',
						'label' => __('Gauge thickness:','frontend-builder'),
						'min' => 0,
						'max' => 20,
						'std' => 2,
						'desc' => __('Thickness of the gauge chart','frontend-builder')
					),
					'shadow_opacity' => array(
						'type' => 'number',
						'label' => __('Shadow opacity:','frontend-builder'),
						'min' => 0,
						'max' => 10,
						'std' => 5,
						'desc' => __('Thickness of the gauge chart','frontend-builder'),
						'hide_if' => array(
							'show_inner_shadow' => array('false')
						)
					),
					'shadow_size' => array(
						'type' => 'number',
						'label' => __('Shadow size:','frontend-builder'),
						'min' => 0,
						'max' => 100,
						'std' => 20,
						'desc' => __('Size of the gauge shadow','frontend-builder'),
						'hide_if' => array(
							'show_inner_shadow' => array('false')
						)
					),
					'shadow_v_offset' => array(
						'type' => 'number',
						'label' => __('Shadow vertical offset:','frontend-builder'),
						'min' => 0,
						'max' => 50,
						'std' => 5,
						'desc' => __('Vertical offset of the gauge shadow','frontend-builder'),
						'hide_if' => array(
							'show_inner_shadow' => array('false')
						)
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* PIECHART */
/* -------------------------------------------------------------------------------- */
$piechart = array(
	'piechart' => array(
		'type' => 'draggable',
		'text' => __('Piechart','frontend-builder'),
		'icon' => $this->url.'images/icons/piecart.png',
		'function' => 'fbuilder_piechart',
		'group' => 'Charts, Bars, Counters',
		'options'  => array_merge(
		 array(
		 	'group_chart' => array(
				'type' => 'collapsible',
				'label' => __('Chart','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'item_align' => array(
						'type' => 'select',
						'label' => __('Item alignment:','frontend-builder'),
						'std' => 'center',
						'options' => array(
							'center' => 'Center',
							'left' => 'Left',
							'right' => 'Right'
						)
					),
					'radius' => array(
						'type' => 'number',
						'label' => __('Radius:','frontend-builder'),
						'min' => 20,
						'half_column' => 'true',
						'max' => 1000,
						'std' => 220
					),
					'inner_cut' => array(
						'type' => 'number',
						'label' => __('Mid Cut:','frontend-builder'),
						'min' => 0,
						'half_column' => 'true',
						'max' => 100,
						'std' => 0
					),
					'stroke_width' => array(
						'type' => 'number',
						'label' => __('Split Line Width:','frontend-builder'),
						'min' => 0,
						'max' => 20,
						'std' => 15
					),
					'stroke_color' => array(
						'type' => 'color',
						'label' => __('Split Line Color:','frontend-builder'),
						'std' => '#ffffff'
					)
				)
			),
			'group_legend' => array(
				'type' => 'collapsible',
				'label' => __('Legend','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'legend_position' => array(
						'type' => 'select',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'label' => __('Position:','frontend-builder'),
						'std' => 'bottom',
						'options' => array(
							'bottom' => 'Bottom',
							'left' => 'Left',
							'right' => 'Right'
						)
					),
					'color' => array(
						'type' => 'color',
						'label' => __('Text:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.5,
						'std' => $opts['text_color']
					),
					'font_size' => array(
						'type' => 'number',
						'half_column' => 'true',
						'label' => __('Size:','frontend-builder'),
						'min' => 10,
						'max' => 100,
						'std' => 16,
						'unit' => 'px'
					)
				)
			),
			'group_items' => array(
				'type' => 'collapsible',
				'label' => __('Items','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'sortable' => array(
						'type' => 'sortable',
						'label_width' => 0,
						'control_width' => 1,
						'desc' => __('Elements are sortable','frontend-builder'),
						'item_name' => __('Chart','frontend-builder'),
						'std' => array(
							'items' => array(
								0 => array(
									'data_value' => '15',
									'data_color' => '#6b58cd',
									'data_caption' => 'Lorem ipsum'
								),
								1 => array(
									'data_value' => '14',
									'data_color' => '#8677d4',
									'data_caption' => 'Lorem ipsum'
								),
								2 => array(
									'data_value' => '13',
									'data_color' => '#9c8ddc',
									'data_caption' => 'Lorem ipsum'
								)
							),
							'order' => array(
								0 => 0, 
								1 => 1,
								2 => 2
							)
						),
						'options'=> array(
							'data_value' => array(
								'type' => 'input',
								'label_width' => 0.25,
								'control_width' => 0.75,
								'label' => __('Value:','frontend-builder'),
								'desc' =>__('Numeric Value'),
								'std' => 1
							),
							'data_caption' => array(
								'type' => 'input',
								'label_width' => 0.25,
								'control_width' => 0.75,
								'label' => __('Desc.:','frontend-builder'),
								'desc' =>__('Descriptive text'),
								'std' => 'Lorem ipsum'
							),
							'data_color' => array(
								'type' => 'color',
								'label_width' => 0.25,
								'control_width' => 0.5,
								'label' => __('Color:','frontend-builder'),
								'std' => '#000000'
							)
						)
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);



/* -------------------------------------------------------------------------------- */
/* PRICING TABLE */
/* -------------------------------------------------------------------------------- */
$pricing = array(
	'pricing' => array(
		'type' => 'draggable',
		'text' => __('Pricing table','frontend-builder'),
		'icon' => $this->url.'images/icons/pricing-table-V2.png',
		'function' => 'fbuilder_pricing',
		'group' => 'Charts, Bars, Counters',
		'options'  => array_merge(
		 array(
		 	'group_pricing' => array(
				'type' => 'collapsible',
				'label' => __('Pricing','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'colnum' => array(
						'type' => 'select',
						'label' => __('Number of columns','frontend-builder'),
						'std' => '1',
						'options' => array(
							'1' => __('One column','frontend-builder'),
							'2' => __('Two columns','frontend-builder'),
							'3' => __('Three columns','frontend-builder'),
							'4' => __('Four columns','frontend-builder'),
							'5' => __('Five columns','frontend-builder')
						)
					),
					'services_sidebar' => array(
						'type' => 'checkbox',
						'label' => __('Show services sidebar', 'frontend-builder'),
						'std' => 'false'
					),
					'sortable' => array(
						'type' => 'sortable',
						'label_width' => 0,
								'control_width' => 1,
						'desc' => __('Rows are sortable','frontend-builder'),
						'item_name' => __('row','frontend-builder'),
						'std' => array(
							'items' => array(
								0 => array(
									'row_type' => 'heading',
									'bot_border' => 'true',
									'service_label' => 'Lorem Ipsum',
									'service_icon' => 'ba-star',
									'column_1_icon' => 'ba-check',
									'column_1_text' => '<br><STRONG>REGULAR LICENSE</STRONG><BR><BR>',
									'column_1_price' => '40',
									'column_1_interval' => '/dolor sit amet',
									'column_1_button_text' => 'Lorem ipsum',
									'column_1_button_link' => '#',
									'column_2_icon' => 'ba-check',
									'column_2_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_2_price' => '40',
									'column_2_interval' => '/dolor sit amet',
									'column_2_button_text' => 'Lorem ipsum',
									'column_2_button_link' => '#',
									'column_3_icon' => 'ba-check',
									'column_3_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_3_price' => '40',
									'column_3_interval' => '/dolor sit amet',
									'column_3_button_text' => 'Lorem ipsum',
									'column_3_button_link' => '#',
									'column_4_icon' => 'ba-check',
									'column_4_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_4_price' => '40',
									'column_4_interval' => '/dolor sit amet',
									'column_4_button_text' => 'Lorem ipsum',
									'column_4_button_link' => '#',
									'column_5_icon' => 'ba-check',
									'column_5_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_5_price' => '40',
									'column_5_interval' => '/dolor sit amet',
									'column_5_button_text' => 'Lorem ipsum',
									'column_5_button_link' => '#'
								),
								1 => array(
									'row_type' => 'price',
									'bot_border' => 'true',
									'service_label' => 'Lorem Ipsum',
									'service_icon' => 'ba-star',
									'column_1_icon' => 'ba-check',
									'column_1_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_1_price' => '40',
									'column_1_interval' => '/single use',
									'column_1_button_text' => 'Lorem ipsum',
									'column_1_button_link' => '#',
									'column_2_icon' => 'ba-check',
									'column_2_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_2_price' => '40',
									'column_2_interval' => '/dolor sit amet',
									'column_2_button_text' => 'Lorem ipsum',
									'column_2_button_link' => '#',
									'column_3_icon' => 'ba-check',
									'column_3_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_3_price' => '40',
									'column_3_interval' => '/dolor sit amet',
									'column_3_button_text' => 'Lorem ipsum',
									'column_3_button_link' => '#',
									'column_4_icon' => 'ba-check',
									'column_4_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_4_price' => '40',
									'column_4_interval' => '/dolor sit amet',
									'column_4_button_text' => 'Lorem ipsum',
									'column_4_button_link' => '#',
									'column_5_icon' => 'ba-check',
									'column_5_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_5_price' => '40',
									'column_5_interval' => '/dolor sit amet',
									'column_5_button_text' => 'Lorem ipsum',
									'column_5_button_link' => '#'
								 ),
								2 => array(
									'row_type' => 'service',
									'bot_border' => 'true',
									'service_label' => 'Lorem Ipsum',
									'service_icon' => 'ba-star',
									'column_1_icon' => 'no-icon',
									'column_1_text' => '<br>Premium Support Included<br><br>',
									'column_1_price' => '40',
									'column_1_interval' => '/dolor sit amet',
									'column_1_button_text' => 'Lorem ipsum',
									'column_1_button_link' => '#',
									'column_2_icon' => 'ba-check',
									'column_2_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_2_price' => '40',
									'column_2_interval' => '/dolor sit amet',
									'column_2_button_text' => 'Lorem ipsum',
									'column_2_button_link' => '#',
									'column_3_icon' => 'ba-check',
									'column_3_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_3_price' => '40',
									'column_3_interval' => '/dolor sit amet',
									'column_3_button_text' => 'Lorem ipsum',
									'column_3_button_link' => '#',
									'column_4_icon' => 'ba-check',
									'column_4_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_4_price' => '40',
									'column_4_interval' => '/dolor sit amet',
									'column_4_button_text' => 'Lorem ipsum',
									'column_4_button_link' => '#',
									'column_5_icon' => 'ba-check',
									'column_5_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_5_price' => '40',
									'column_5_interval' => '/dolor sit amet',
									'column_5_button_text' => 'Lorem ipsum',
									'column_5_button_link' => '#'
								),
								3 => array(
									'row_type' => 'service',
									'bot_border' => 'true',
									'service_label' => 'Lorem Ipsum',
									'service_icon' => 'ba-star',
									'column_1_icon' => 'no-icon',
									'column_1_text' => '<br>Free Modification of the Template<br><br>',
									'column_1_price' => '40',
									'column_1_interval' => '/dolor sit amet',
									'column_1_button_text' => 'Lorem ipsum',
									'column_1_button_link' => '#',
									'column_2_icon' => 'ba-check',
									'column_2_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_2_price' => '40',
									'column_2_interval' => '/dolor sit amet',
									'column_2_button_text' => 'Lorem ipsum',
									'column_2_button_link' => '#',
									'column_3_icon' => 'ba-check',
									'column_3_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_3_price' => '40',
									'column_3_interval' => '/dolor sit amet',
									'column_3_button_text' => 'Lorem ipsum',
									'column_3_button_link' => '#',
									'column_4_icon' => 'ba-check',
									'column_4_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_4_price' => '40',
									'column_4_interval' => '/dolor sit amet',
									'column_4_button_text' => 'Lorem ipsum',
									'column_4_button_link' => '#',
									'column_5_icon' => 'ba-check',
									'column_5_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_5_price' => '40',
									'column_5_interval' => '/dolor sit amet',
									'column_5_button_text' => 'Lorem ipsum',
									'column_5_button_link' => '#'
								),
								4 => array(
									'row_type' => 'service',
									'bot_border' => 'true',
									'service_label' => 'Lorem Ipsum',
									'service_icon' => 'ba-star',
									'column_1_icon' => 'no-icon',
									'column_1_text' => '<br>3 Premium Plugins Included<br><br>',
									'column_1_price' => '40',
									'column_1_interval' => '/dolor sit amet',
									'column_1_button_text' => 'Lorem ipsum',
									'column_1_button_link' => '#',
									'column_2_icon' => 'ba-check',
									'column_2_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_2_price' => '40',
									'column_2_interval' => '/dolor sit amet',
									'column_2_button_text' => 'Lorem ipsum',
									'column_2_button_link' => '#',
									'column_3_icon' => 'ba-check',
									'column_3_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_3_price' => '40',
									'column_3_interval' => '/dolor sit amet',
									'column_3_button_text' => 'Lorem ipsum',
									'column_3_button_link' => '#',
									'column_4_icon' => 'ba-check',
									'column_4_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_4_price' => '40',
									'column_4_interval' => '/dolor sit amet',
									'column_4_button_text' => 'Lorem ipsum',
									'column_4_button_link' => '#',
									'column_5_icon' => 'ba-check',
									'column_5_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_5_price' => '40',
									'column_5_interval' => '/dolor sit amet',
									'column_5_button_text' => 'Lorem ipsum',
									'column_5_button_link' => '#'
								),
								5 => array(
									'row_type' => 'service',
									'bot_border' => 'true',
									'service_label' => 'Lorem Ipsum',
									'service_icon' => 'ba-star',
									'column_1_icon' => 'no-icon',
									'column_1_text' => '<br>Drag & Drop Content Editing<br><br>',
									'column_1_price' => '40',
									'column_1_interval' => '/dolor sit amet',
									'column_1_button_text' => 'Lorem ipsum',
									'column_1_button_link' => '#',
									'column_2_icon' => 'ba-check',
									'column_2_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_2_price' => '40',
									'column_2_interval' => '/dolor sit amet',
									'column_2_button_text' => 'Lorem ipsum',
									'column_2_button_link' => '#',
									'column_3_icon' => 'ba-check',
									'column_3_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_3_price' => '40',
									'column_3_interval' => '/dolor sit amet',
									'column_3_button_text' => 'Lorem ipsum',
									'column_3_button_link' => '#',
									'column_4_icon' => 'ba-check',
									'column_4_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_4_price' => '40',
									'column_4_interval' => '/dolor sit amet',
									'column_4_button_text' => 'Lorem ipsum',
									'column_4_button_link' => '#',
									'column_5_icon' => 'ba-check',
									'column_5_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_5_price' => '40',
									'column_5_interval' => '/dolor sit amet',
									'column_5_button_text' => 'Lorem ipsum',
									'column_5_button_link' => '#'
								),
								6 => array(
									'row_type' => 'service',
									'bot_border' => 'true',
									'service_label' => 'Lorem Ipsum',
									'service_icon' => 'ba-star',
									'column_1_icon' => 'no-icon',
									'column_1_text' => '<br>You Can Not Resell the Item<br><br>',
									'column_1_price' => '40',
									'column_1_interval' => '/dolor sit amet',
									'column_1_button_text' => 'Lorem ipsum',
									'column_1_button_link' => '#',
									'column_2_icon' => 'ba-check',
									'column_2_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_2_price' => '40',
									'column_2_interval' => '/dolor sit amet',
									'column_2_button_text' => 'Lorem ipsum',
									'column_2_button_link' => '#',
									'column_3_icon' => 'ba-check',
									'column_3_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_3_price' => '40',
									'column_3_interval' => '/dolor sit amet',
									'column_3_button_text' => 'Lorem ipsum',
									'column_3_button_link' => '#',
									'column_4_icon' => 'ba-check',
									'column_4_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_4_price' => '40',
									'column_4_interval' => '/dolor sit amet',
									'column_4_button_text' => 'Lorem ipsum',
									'column_4_button_link' => '#',
									'column_5_icon' => 'ba-check',
									'column_5_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_5_price' => '40',
									'column_5_interval' => '/dolor sit amet',
									'column_5_button_text' => 'Lorem ipsum',
									'column_5_button_link' => '#'
								),
								7 => array(
									'row_type' => 'service',
									'bot_border' => 'false',
									'service_label' => 'Lorem Ipsum',
									'service_icon' => 'ba-star',
									'column_1_icon' => 'no-icon',
									'column_1_text' => '',
									'column_1_price' => '40',
									'column_1_interval' => '/dolor sit amet',
									'column_1_button_text' => 'Lorem ipsum',
									'column_1_button_link' => '#',
									'column_2_icon' => 'ba-check',
									'column_2_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_2_price' => '40',
									'column_2_interval' => '/dolor sit amet',
									'column_2_button_text' => 'Lorem ipsum',
									'column_2_button_link' => '#',
									'column_3_icon' => 'ba-check',
									'column_3_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_3_price' => '40',
									'column_3_interval' => '/dolor sit amet',
									'column_3_button_text' => 'Lorem ipsum',
									'column_3_button_link' => '#',
									'column_4_icon' => 'ba-check',
									'column_4_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_4_price' => '40',
									'column_4_interval' => '/dolor sit amet',
									'column_4_button_text' => 'Lorem ipsum',
									'column_4_button_link' => '#',
									'column_5_icon' => 'ba-check',
									'column_5_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_5_price' => '40',
									'column_5_interval' => '/dolor sit amet',
									'column_5_button_text' => 'Lorem ipsum',
									'column_5_button_link' => '#'
								),
								8 => array(
									'row_type' => 'button',
									'bot_border' => 'false',
									'service_label' => 'Lorem Ipsum',
									'service_icon' => 'ba-star',
									'column_1_icon' => 'no-icon',
									'column_1_text' => '<br>3 Premium Plugins Included<br><br>',
									'column_1_price' => '40',
									'column_1_interval' => '/dolor sit amet',
									'column_1_button_text' => '<br><strong>Purchase Now</strong><br><br>',
									'column_1_button_link' => '#',
									'column_2_icon' => 'ba-check',
									'column_2_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_2_price' => '40',
									'column_2_interval' => '/dolor sit amet',
									'column_2_button_text' => 'Lorem ipsum',
									'column_2_button_link' => '#',
									'column_3_icon' => 'ba-check',
									'column_3_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_3_price' => '40',
									'column_3_interval' => '/dolor sit amet',
									'column_3_button_text' => 'Lorem ipsum',
									'column_3_button_link' => '#',
									'column_4_icon' => 'ba-check',
									'column_4_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_4_price' => '40',
									'column_4_interval' => '/dolor sit amet',
									'column_4_button_text' => 'Lorem ipsum',
									'column_4_button_link' => '#',
									'column_5_icon' => 'ba-check',
									'column_5_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_5_price' => '40',
									'column_5_interval' => '/dolor sit amet',
									'column_5_button_text' => 'Lorem ipsum',
									'column_5_button_link' => '#'
								),
								9 => array(
									'row_type' => 'border',
									'bot_border' => 'true',
									'service_label' => 'Lorem Ipsum',
									'service_icon' => 'ba-star',
									'column_1_icon' => 'no-icon',
									'column_1_text' => '<br>3 Premium Plugins Included<br><br>',
									'column_1_price' => '40',
									'column_1_interval' => '/dolor sit amet',
									'column_1_button_text' => '<br><strong>Purchase Now</strong><br><br>',
									'column_1_button_link' => '#',
									'column_2_icon' => 'ba-check',
									'column_2_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_2_price' => '40',
									'column_2_interval' => '/dolor sit amet',
									'column_2_button_text' => 'Lorem ipsum',
									'column_2_button_link' => '#',
									'column_3_icon' => 'ba-check',
									'column_3_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_3_price' => '40',
									'column_3_interval' => '/dolor sit amet',
									'column_3_button_text' => 'Lorem ipsum',
									'column_3_button_link' => '#',
									'column_4_icon' => 'ba-check',
									'column_4_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_4_price' => '40',
									'column_4_interval' => '/dolor sit amet',
									'column_4_button_text' => 'Lorem ipsum',
									'column_4_button_link' => '#',
									'column_5_icon' => 'ba-check',
									'column_5_text' => '<br><STRONG>LOREM IPSUM</STRONG><BR><BR>',
									'column_5_price' => '40',
									'column_5_interval' => '/dolor sit amet',
									'column_5_button_text' => 'Lorem ipsum',
									'column_5_button_link' => '#'
								)
							),
							'order' => array(
								0 => 0,
								1 => 1,
								2 => 2,
								3 => 3,
								4 => 4,
								5 => 5,
								6 => 6,
								7 => 7,
								8 => 8,
								9 => 9
							)
						),
						'options'=> array(
							'row_type' => array(
								'type' => 'select',
								'label' => __('Row type','frontend-builder'),
								'std' => 'service',
								'options' => array(
									'heading' => __('Heading','frontend-builder'),
									'price' => __('Price','frontend-builder'),
									'button' => __('Button','frontend-builder'),
									'text-button' => __('Text with button','frontend-builder'),
									'section' => __('Section','frontend-builder'),
									'service' => __('Service','frontend-builder'),
									'border' => __('Border','frontend-builder')
								)
							),
							'bot_border' => array(
								'type' => 'checkbox',
								'label' => __('Bottom border','frontend-builder'),
								'std' => 'true'
							),
							// sidebar
							'service_label' => array(
								'type' => 'input',
								'label' => __('Service label','frontend-builder'),
								'std' => 'Lorem ipsum',
								'hide_if' => array(
									'services_sidebar' => array('false'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'button', 'border')
									)
								)
							),
							'service_icon' => array(
								'type' => 'icon',
								'label' => __('Icon','frontend-builder'),
								'std' => 'ba-star',
								'notNull' => false,
								'hide_if' => array(
									'services_sidebar' => array('false'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'button', 'text-button', 'service', 'border')
									)
								)
							),
							// column 1
							'column_1_icon' => array(
								'type' => 'icon',
								'label' => __('Column 1 icon','frontend-builder'),
								'std' => 'ba-check',
								'notNull' => false,
								'hide_if' => array(
									'sortable' => array(
										'row_type' => array('heading', 'price', 'button','text-button', 'section', 'border')
									)
								)
							),
							'column_1_text' => array(
								'type' => 'input',
								'label' => __('Column 1 text','frontend-builder'),
								'std' => 'Lorem ipstum',
								'hide_if' => array(
									'sortable' => array(
										'row_type' => array('price', 'button', 'border', 'section')
									)
								)
							),
							'column_1_price' => array(
								'type' => 'input',
								'label' => __('Column 1 price','frontend-builder'),
								'std' => '42',
								'hide_if' => array(
									'sortable' => array(
										'row_type' => array('heading', 'button', 'text-button', 'section', 'service', 'border')
									)
								)
							),
							'column_1_interval' => array(
								'type' => 'input',
								'label' => __('Column 1 interval','frontend-builder'),
								'std' => '/per month',
								'hide_if' => array(
									'sortable' => array(
										'row_type' => array('heading', 'button', 'text-button', 'section', 'service', 'border')
									)
								)
							),
							'column_1_button_text' => array(
								'type' => 'input',
								'label' => __('Column 1 button text','frontend-builder'),
								'std' => 'Lorem ipsum',
								'hide_if' => array(
									'sortable' => array(
										'row_type' => array('heading', 'price', 'section', 'service', 'border')
									)
								)
							),
							'column_1_button_link' => array(
								'type' => 'input',
								'label' => __('Column 1 button link','frontend-builder'),
								'std' => '#',
								'hide_if' => array(
									'sortable' => array(
										'row_type' => array('heading', 'price', 'section', 'service', 'border')
									)
								)
							),
							// column 2
							'column_2_icon' => array(
								'type' => 'icon',
								'label' => __('Column 2 icon','frontend-builder'),
								'std' => 'ba-times',
								'notNull' => false,
								'hide_if' => array(
									'colnum' => array('1'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'button','text-button', 'section', 'border')
									)
								)
							),
							'column_2_text' => array(
								'type' => 'input',
								'label' => __('Column 2 text','frontend-builder'),
								'std' => 'Lorem ipstum',
								'hide_if' => array(
									'colnum' => array('1'),
									'sortable' => array(
										'row_type' => array('price', 'button', 'border', 'section')
									)
								)
							),
							'column_2_price' => array(
								'type' => 'input',
								'label' => __('Column 2 price','frontend-builder'),
								'std' => '42',
								'hide_if' => array(
									'colnum' => array('1'),
									'sortable' => array(
										'row_type' => array('heading', 'button', 'text-button', 'section', 'service', 'border')
									)
								)
							),
							'column_2_interval' => array(
								'type' => 'input',
								'label' => __('Column 2 interval','frontend-builder'),
								'std' => '/per month',
								'hide_if' => array(
									'colnum' => array('1'),
									'sortable' => array(
										'row_type' => array('heading', 'button', 'text-button', 'section', 'service', 'border')
									)
								)
							),
							'column_2_button_text' => array(
								'type' => 'input',
								'label' => __('Column 2 button text','frontend-builder'),
								'std' => 'Lorem ipstum',
								'hide_if' => array(
									'colnum' => array('1'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'section', 'service', 'border')
									)
								)
							),
							'column_2_button_link' => array(
								'type' => 'input',
								'label' => __('Column 2 button link','frontend-builder'),
								'std' => '#',
								'hide_if' => array(
									'colnum' => array('1'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'section', 'service', 'border')
									)
								)
							),
							// column 3
							'column_3_icon' => array(
								'type' => 'icon',
								'label' => __('Column 3 icon','frontend-builder'),
								'std' => 'no-icon',
								'notNull' => false,
								'hide_if' => array(
									'colnum' => array('1','2'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'button','text-button', 'section', 'border')
									)
								)
							),
							'column_3_text' => array(
								'type' => 'input',
								'label' => __('Column 3 text','frontend-builder'),
								'std' => 'Lorem ipstum',
								'hide_if' => array(
									'colnum' => array('1','2'),
									'sortable' => array(
										'row_type' => array('price', 'button', 'section', 'border')
									)
								)
							),
							'column_3_price' => array(
								'type' => 'input',
								'label' => __('Column 3 price','frontend-builder'),
								'std' => '42',
								'hide_if' => array(
									'colnum' => array('1','2'),
									'sortable' => array(
										'row_type' => array('heading', 'button', 'text-button', 'section', 'service', 'border')
									)
								)
							),
							'column_3_interval' => array(
								'type' => 'input',
								'label' => __('Column 3 interval','frontend-builder'),
								'std' => '/per month',
								'hide_if' => array(
									'colnum' => array('1','2'),
									'sortable' => array(
										'row_type' => array('heading', 'button', 'text-button', 'section', 'service', 'border')
									)
								)
							),
							'column_3_button_text' => array(
								'type' => 'input',
								'label' => __('Column 3 button text','frontend-builder'),
								'std' => 'Lorem ipstum',
								'hide_if' => array(
									'colnum' => array('1','2'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'section', 'service', 'border')
									)
								)
							),
							'column_3_button_link' => array(
								'type' => 'input',
								'label' => __('Column 3 button link','frontend-builder'),
								'std' => '#',
								'hide_if' => array(
									'colnum' => array('1','2'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'section', 'service', 'border')
									)
								)
							),
							// column 4
							'column_4_icon' => array(
								'type' => 'icon',
								'label' => __('Column 4 icon','frontend-builder'),
								'std' => 'no-icon',
								'notNull' => false,
								'hide_if' => array(
									'colnum' => array('1','2','3'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'button','text-button', 'section', 'border')
									)
								)
							),
							'column_4_text' => array(
								'type' => 'input',
								'label' => __('Column 4 text','frontend-builder'),
								'std' => 'Lorem ipstum',
								'hide_if' => array(
									'colnum' => array('1','2','3'),
									'sortable' => array(
										'row_type' => array('price', 'button', 'section', 'border')
									)
								)
							),
							'column_4_price' => array(
								'type' => 'input',
								'label' => __('Column 4 price','frontend-builder'),
								'std' => '42',
								'hide_if' => array(
									'colnum' => array('1','2','3'),
									'sortable' => array(
										'row_type' => array('heading', 'button', 'text-button', 'section', 'service', 'border')
									)
								)
							),
							'column_4_interval' => array(
								'type' => 'input',
								'label' => __('Column 4 interval','frontend-builder'),
								'std' => '/per month',
								'hide_if' => array(
									'colnum' => array('1','2','3'),
									'sortable' => array(
										'row_type' => array('heading', 'button', 'text-button', 'section', 'service', 'border')
									)
								)
							),
							'column_4_button_text' => array(
								'type' => 'input',
								'label' => __('Column 4 button text','frontend-builder'),
								'std' => 'Lorem ipstum',
								'hide_if' => array(
									'colnum' => array('1','2','3'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'section', 'service', 'border')
									)
								)
							),
							'column_4_button_link' => array(
								'type' => 'input',
								'label' => __('Column 4 button link','frontend-builder'),
								'std' => '#',
								'hide_if' => array(
									'colnum' => array('1','2','3'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'section', 'service', 'border')
									)
								)
							),
							// column 5
							'column_5_icon' => array(
								'type' => 'icon',
								'label' => __('Column 5 icon','frontend-builder'),
								'std' => 'no-icon',
								'notNull' => false,
								'hide_if' => array(
									'colnum' => array('1','2','3','4'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'button','text-button', 'section', 'border')
									)
								)
							),
							'column_5_text' => array(
								'type' => 'input',
								'label' => __('Column 5 text','frontend-builder'),
								'std' => 'Lorem ipstum',
								'hide_if' => array(
									'colnum' => array('1','2','3','4'),
									'sortable' => array(
										'row_type' => array('price', 'button', 'section', 'border')
									)
								)
							),
							'column_5_price' => array(
								'type' => 'input',
								'label' => __('Column 5 price','frontend-builder'),
								'std' => '42',
								'hide_if' => array(
									'colnum' => array('1','2','3','4'),
									'sortable' => array(
										'row_type' => array('heading', 'button', 'text-button', 'section', 'service', 'border')
									)
								)
							),
							'column_5_interval' => array(
								'type' => 'input',
								'label' => __('Column 5 interval','frontend-builder'),
								'std' => '/per month',
								'hide_if' => array(
									'colnum' => array('1','2','3','4'),
									'sortable' => array(
										'row_type' => array('heading', 'button', 'text-button', 'section', 'service', 'border')
									)
								)
							),
							'column_5_button_text' => array(
								'type' => 'input',
								'label' => __('Column 5 button text','frontend-builder'),
								'std' => 'Lorem ipstum',
								'hide_if' => array(
									'colnum' => array('1','2','3','4'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'section', 'service', 'border')
									)
								)
							),
							'column_5_button_link' => array(
								'type' => 'input',
								'label' => __('Column 5 button link','frontend-builder'),
								'std' => '#',
								'hide_if' => array(
									'colnum' => array('1','2','3','4'),
									'sortable' => array(
										'row_type' => array('heading', 'price', 'section', 'service', 'border')
									)
								)
							)
						)
					),
					'currency' => array(
						'type' => 'input',
						'label' => __('Currency','frontend-builder'),
						'std' => '$'
					),
					'text_color' => array(
						'type' => 'color',
						'label' => __('Text color','frontend-builder'),
						'std' => $opts['text_color']
					),
					'back_color' => array(
						'type' => 'color',
						'label' => __('Background color','frontend-builder'),
						'std' => $opts['light_back_color']
					),
					'column_1_main_color' => array(
						'type' => 'color',
						'label' => __('Column 1 main color','frontend-builder'),
						'std' => '#445a68'
					),
					'column_1_hover_color' => array(
						'type' => 'color',
						'label' => __('Column 1 hover color','frontend-builder'),
						'std' => '#5b798c'
					),
					'column_1_button_text_color' => array(
						'type' => 'color',
						'label' => __('Column 1 button text color','frontend-builder'),
						'std' => '#ffffff'
					),
					'column_2_main_color' => array(
						'type' => 'color',
						'label' => __('Column 2 main color','frontend-builder'),
						'std' => '#ed4c3a',
						'hide_if' => array(
							'colnum' => array('1')
						)
					),
					'column_2_hover_color' => array(
						'type' => 'color',
						'label' => __('Column 2 hover color','frontend-builder'),
						'std' => '#f17163',
						'hide_if' => array(
							'colnum' => array('1')
						)
					),
					'column_2_button_text_color' => array(
						'type' => 'color',
						'label' => __('Column 2 button text color','frontend-builder'),
						'std' => '#ffffff',
						'hide_if' => array(
							'colnum' => array('1')
						)
					),
					'column_3_main_color' => array(
						'type' => 'color',
						'label' => __('Column 3 main color','frontend-builder'),
						'std' => '#2b98d3',
						'hide_if' => array(
							'colnum' => array('1','2')
						)
					),
					'column_3_hover_color' => array(
						'type' => 'color',
						'label' => __('Column 3 hover color','frontend-builder'),
						'std' => '#54acdc',
						'hide_if' => array(
							'colnum' => array('1','2')
						)
					),
					'column_3_button_text_color' => array(
						'type' => 'color',
						'label' => __('Column 3 button text color','frontend-builder'),
						'std' => '#ffffff',
						'hide_if' => array(
							'colnum' => array('1','2')
						)
					),
					'column_4_main_color' => array(
						'type' => 'color',
						'label' => __('Column 4 main color','frontend-builder'),
						'std' => '#16a085',
						'hide_if' => array(
							'colnum' => array('1','2','3')
						)
					),
					'column_4_hover_color' => array(
						'type' => 'color',
						'label' => __('Column 4 hover color','frontend-builder'),
						'std' => '#1abc9c',
						'hide_if' => array(
							'colnum' => array('1','2','3')
						)
					),
					'column_4_button_text_color' => array(
						'type' => 'color',
						'label' => __('Column 4 button text color','frontend-builder'),
						'std' => '#ffffff',
						'hide_if' => array(
							'colnum' => array('1','2','3')
						)
					),
					'column_5_main_color' => array(
						'type' => 'color',
						'label' => __('Column 5 main color','frontend-builder'),
						'std' => '#f39c12',
						'hide_if' => array(
							'colnum' => array('1','2','3','4')
						)
					),
					'column_5_hover_color' => array(
						'type' => 'color',
						'label' => __('Column 5 hover color','frontend-builder'),
						'std' => '#f1c40f',
						'hide_if' => array(
							'colnum' => array('1','2','3','4')
						)
					),
					'column_5_button_text_color' => array(
						'type' => 'color',
						'label' => __('Column 5 button text color','frontend-builder'),
						'std' => '#ffffff',
						'hide_if' => array(
							'colnum' => array('1','2','3','4')
						)
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* CODE */
/* -------------------------------------------------------------------------------- */
$code = array(
	'code' => array(
		'type' => 'draggable',
		'text' => 'Code',
		'icon' => $this->url.'images/icons/code.png',
		'function' => 'fbuilder_code',
		'group' => 'Basic',
		'options'  => array_merge(
		 array(
		 	'group_code' => array(
				'type' => 'collapsible',
				'open' => 'true',
				'label' => __('Code','frontend-builder'),
				'options' => array(
					'content' => array(
						'type' => 'textarea',
						'std' => 'function Start(){// do something}'
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* ALERT */
/* -------------------------------------------------------------------------------- */
$alert = array(
	'alert' => array(
		'type' => 'draggable',
		'text' => __('Alert box','frontend-builder'),
		'icon' => $this->url.'images/icons/alert-box.png',
		'function' => 'fbuilder_alert',
		'group' => 'Basic',
		'options'  => array_merge(
		array(
			'group_alert_box' => array(
				'type' => 'collapsible',
				'label' => __('Alert Box','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'style' => array(
						'type' => 'select',
						'label' => __('Style:','frontend-builder'),
						'std' => 'clean',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'options' => array(
							'clean' => __('Clean','frontend-builder'),
							'squared' => __('Squared','frontend-builder'),
							'rounded' => __('Rounded','frontend-builder')
						)
					),
					'type' => array(
						'type' => 'select',
						'label' => __('Type:','frontend-builder'),
						'std' => 'info',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'options' => array(
							'info' => __('Info','frontend-builder'),
							'success' => __('Success','frontend-builder'),
							'notice' => __('Notice','frontend-builder'),
							'warning' => __('Warning','frontend-builder'),
							'custom' => __('Custom','frontend-builder')
						)
					),
					'text' => array(
						'type' => 'textarea',
						'std' => __('This is an alert','frontend-builder')
					)
				)
			),
			'group_custom' => array(
				'type' => 'collapsible',
				'label' => __('Custom','frontend-builder'),
				'desc' => 'select "Custom" type',
				'open' => 'true',
				'options' => array(
					'icon' => array(
						'type' => 'icon',
						'label' => __('Icon Type:','frontend-builder'),
						'std' => 'ba-warning',
						'hide_if' => array(
							'type' => array('info', 'success', 'notice', 'warning')
						)
					),
					'main_color' => array(
						'type' => 'color',
						'label' => __('Main:','frontend-builder'),
						'std' => $opts['main_color'],
						'hide_if' => array(
							'type' => array('info', 'success', 'notice', 'warning')
						)
					),
					'text_color' => array(
						'type' => 'color',
						'label' => __('Text:','frontend-builder'),
						'std' => $opts['text_color'],
						'hide_if' => array(
							'type' => array('info', 'success', 'notice', 'warning')
						)
					),
					'icon_color' => array(
						'type' => 'color',
						'label' => __('Icon:','frontend-builder'),
						'std' => $opts['main_color'],
						'hide_if' => array(
							'type' => array('info', 'success', 'notice', 'warning')
						)
					),
					'back_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => '',
						'hide_if' => array(
							'type' => array('info', 'success', 'notice', 'warning')
						)
					)
				)		
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* MENU */
/* -------------------------------------------------------------------------------- */

$menu = array(
	'menu' => array(
		'type' => 'draggable',
		'text' => __('Nav menu','frontend-builder'),
		'icon' => $this->url.'images/icons/nav-menu.png',
		'function' => 'fbuilder_nav_menu',
		'group' => 'Basic',
		'options'  => array_merge(
		array(
			'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'wp_menu' => array(
						'type' => 'select',
						'label' => __('Menu:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'desc' => __('Select wordpress nav menu that you want to display','frontend-builder'),
						'options'=> $fbuilder_menus,
						'std' => $fbuilder_menu_std
					),
					'type' => array(
						'type' => 'select',
						'label' => __('Type:','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'std' => 'horizontal-clean',
						'options' => array(
							'horizontal-clean' => __('Clean horizontal menu','frontend-builder'),
							'horizontal-squared' => __('Squared horizontal menu','frontend-builder'),
							'horizontal-rounded' => __('Rounded horizontal menu','frontend-builder'),
							'vertical-clean' => __('Clean vertical menu','frontend-builder'),
							'vertical-squared' => __('Squared vertical menu','frontend-builder'),
							'vertical-rounded' => __('Rounded vertical menu','frontend-builder'),
							'dropdown-clean' => __('Clean dropdown menu','frontend-builder'),
							'dropdown-squared' => __('Squared dropdown menu','frontend-builder'),
							'dropdown-rounded' => __('Rounded dropdown menu','frontend-builder')
						)
					),
					'menu_title' => array(
						'type' => 'input',
						'label' => __('Title:','frontend-builder'),
						'std' => __('Nav menu','frontend-builder'),
						'label_width' => 0.25,
						'control_width' => 0.75,
						'hide_if' => array(
							'type' => array('horizontal-clean', 'horizontal-squared','horizontal-rounded')
						)
					)
				)
			),
			'group_text' => array(
				'type' => 'collapsible',
				'label' => __('Text','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'text_color' => array(
						'type' => 'color',
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['title_color']
					),
					'hover_text_color' => array(
						'type' => 'color',
						'label' => __('Hover:','frontend-builder'),
						'std' => $opts['main_back_text_color']
					)
				)
			),
			'group_sub_menu' => array(
				'type' => 'collapsible',
				'label' => __('Sub-menu','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'sub_text_color' => array(
						'type' => 'color',
						'label' => __('Text','frontend-builder'),
						'std' => $opts['text_color']
					),
					'separator_color' => array(
						'type' => 'color',
						'label' => __('Separator:','frontend-builder'),
						'std' => $opts['light_border_color'],
						'hide_if' => array(
							'type' => array('horizontal-squared', 'horizontal-rounded', 'dropdown-squared', 'dropdown-rounded')
						)
					),
					'sub_back_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => $opts['light_back_color']
					),
					
				)
			),
			'group_advanced' => array(
				'type' => 'collapsible',
				'label' => __('Advanced','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'back_color' => array(
						'type' => 'color',
						'label' => __('Background:','frontend-builder'),
						'std' => ''
					),
					'hover_color' => array(
						'type' => 'color',
						'label' => __('Hover:','frontend-builder'),
						'std' => $opts['main_color']
					)	
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* ICON MENU */
/* -------------------------------------------------------------------------------- */
$icon_menu = array(
	'icon_menu' => array(
		'type' => 'draggable',
		'text' => __('Icon menu','frontend-builder'),
		'icon' => $this->url.'images/icons/icon-menu.png',
		'function' => 'fbuilder_icon_menu',
		'group' => 'Basic',
		'options'  => array_merge(
		array(
			'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'align' => array(
						'type' => 'select',
						'std' => 'left',
						'label' => __('Icon alignment','frontend-builder'),
						'options' => array(
							'left' => __('Left','frontend-builder'),
							'right' => __('Right','frontend-builder'),
							'center' => __('Center','frontend-builder')
						)
					),
					'icon_padding' => array(
						'type' => 'number',
						'std' => 5,
						'label' => __('Icon spacing:','frontend-builder'),
						'unit' => 'px',
						'desc' => __('Spacing between menu icons','frontend-builder'), 
					),
					'icon_size' => array(
						'type' => 'number',
						'std' => 24,
						'label' => __('Size:','frontend-builder'),
						'half_column' => 'true',
						'unit' => 'px'
					),
					'round' => array(
						'type' => 'checkbox',
						'std' => 'false',
						'half_column' => 'true',
						'label' => __('Round edges','frontend-builder')
					)
				)
			),
			'group_colors' => array(
				'type' => 'collapsible',
				'label' => __('Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'icon_color' => array(
						'type' => 'color',
						'std' => $opts['title_color'],
						'label' => __('Icon:','frontend-builder')
					),
					'icon_hover_color' => array(
						'type' => 'color',
						'std' => $opts['main_color'],
						'label' => __('Hover:','frontend-builder')
					),
					'back_color' => array(
						'type' => 'color',
						'std' => '',
						'label' => __('Background:','frontend-builder')
					),
					'back_hover_color' => array(
						'type' => 'color',
						'std' => '',
						'label' => __('Hover:','frontend-builder')
					)
				)	
			),
			'group_icons' => array(
				'type' => 'collapsible',
				'label' => __('Icons','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'sortable' => array(
						'type' => 'sortable',
						'label' => __('Icons','frontend-builder'),
						'item_name' => 'icon',
						'label_width' => 0,
						'control_width' => 1,
						'std' => array(
							'items' => array(
								0 => array(
									'icon' => 'ba-gears',
									'url' => '#',
									'link_type' => 'standard'
								),
								1 => array(
									'icon' => 'ba-key',
									'url' => '#',
									'link_type' => 'standard'
								),
								2 => array(
									'icon' => 'ba-group',
									'url' => '#',
									'link_type' => 'standard'
								),
							),
							'order' => array(
								0 => 0,
								1 => 1,
								2 => 2
							)
						),
						'options' => array(
							'icon' => array(
								'type' => 'icon',
								'label' => __('Icon:','frontend-builder'),
								'label_width' => 0.25,
								'control_width' => 0.75,
								'std' => 'ba-check-square-o'
							),
							'url' => array(
								'type' => 'input',
								'label_width' => 0.25,
								'control_width' => 0.75,
								'label' => __('Link:','frontend-builder')
							),
							'link_type' => array(
								'type' => 'select',
								'label' => __('Type:','frontend-builder'),
								'label_width' => 0.25,
								'control_width' => 0.75,
								'std' => 'standard',
								'desc' => __('open in new tab or lightbox','frontend-builder'),
								'options' => array(
									'standard' => __('Standard','frontend-builder'),
									'new-tab' => __('Open in new tab','frontend-builder'),
									'lightbox-image' => __('Lightbox image','frontend-builder'),
									'lightbox-iframe' => __('Lightbox iframe','frontend-builder')
								)
							),
							'iframe_width' => array(
								'type' => 'number',
								'label' => __('Width:','frontend-builder'),
								'std' => 600,
								'min' => 0,
								'max' => 1200,
								'unit' => 'px',
								'half_column' => 'true',
								'hide_if' => array(
									'sortable' => array(
										'link_type' => array('standard', 'new-tab', 'lightbx-image')
									)
								)
							),
							'iframe_height' => array(
								'type' => 'number',
								'label' => __('Height:','frontend-builder'),
								'std' => 300,
								'min' => 0,
								'max' => 1200,
								'unit' => 'px',
								'half_column' => 'true',
								'hide_if' => array(
									'sortable' => array(
										'link_type' => array('standard', 'new-tab', 'lightbx-image')
									)
								)
							)
						)
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* SIDEBAR */
/* -------------------------------------------------------------------------------- */
$sidebar = array(
	'sidebar' => array(
		'type' => 'draggable',
		'text' => __('Sidebar','frontend-builder'),
		'icon' => $this->url.'images/icons/sidebar.png',
		'function' => 'fbuilder_sidebar',
		'group' => 'Basic',
		'options'  => array_merge(
		array(
			'group_sidebar' => array(
				'type' => 'collapsible',
				'label' => __('Sidebar','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'name' => array(
						'type' => 'select',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'label' => __('WP Sidebar:','frontend-builder'),
						'desc' => __('Select wordpress sidebar that you want to display','frontend-builder'),
						'options'=> '$fbuilder_sidebars',
						'std' => '$fbuilder_sidebar_std'
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* SEARCH */
/* -------------------------------------------------------------------------------- */
$search = array(
	'search' => array(
		'type' => 'draggable',
		'text' => __('Search box','frontend-builder'),
		'icon' => $this->url.'images/icons/search-box.png',
		'function' => 'fbuilder_search',
		'group' => 'Basic',
		'options'  => array_merge(
		array(
			'group_basic' => array(
				'type' => 'collapsible',
				'label' => __('Basic','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'text' => array(
						'type' => 'input',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'label' => __('Text','frontend-builder'),
						'std' => __('Search','frontend-builder')
					),
					'round' => array(
						'type' => 'checkbox',
						'label' => __('Round edges','frontend-builder'),
						'std' => 'false'
					)
				)
			),
			'group_text_colors' => array(
				'type' => 'collapsible',
				'label' => __('Text Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'text_color' => array(
						'type' => 'color',
						'label_width' => 0.25,
						'control_width' => 0.5,
						'label' => __('Main:','frontend-builder'),
						'std' => $opts['title_color']
					),
					'text_focus_color' => array(
						'type' => 'color',
						'label_width' => 0.25,
						'control_width' => 0.5,
						'label' => __('Focus:','frontend-builder'),
						'std' => $opts['dark_border_color']
					)
				)
			),
			'group_border_colors' => array(
				'type' => 'collapsible',
				'label' => __('Border Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'border_color' => array(
						'type' => 'color',
						'label_width' => 0.25,
						'control_width' => 0.5,
						'label' => __('Main:','frontend-builder'),
						'std' => $opts['light_border_color']
					),
					'border_focus_color' => array(
						'type' => 'color',
						'label_width' => 0.25,
						'control_width' => 0.5,
						'label' => __('Focus:','frontend-builder'),
						'std' => $opts['dark_border_color']
					)
				)
			),
			'group_background_colors' => array(
				'type' => 'collapsible',
				'label' => __('Background Colors','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'back_color' => array(
						'type' => 'color',
						'label_width' => 0.25,
						'control_width' => 0.5,
						'label' => __('Main:','frontend-builder'),
						'std' => ''
					),
					'back_focus_color' => array(
						'type' => 'color',
						'label_width' => 0.25,
						'control_width' => 0.5,
						'label' => __('Focus:','frontend-builder'),
						'std' => ''
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* SEPARATOR */
/* -------------------------------------------------------------------------------- */
$separator = array(
	'separator' => array(
		'type' => 'draggable',
		'text' => __('Separator','frontend-builder'),
		'icon' => $this->url.'images/icons/separator.png',
		'function' => 'fbuilder_separator',
		'group' => 'Basic',
		'options'  => array_merge(
		array(
			'group_separator' => array(
				'type' => 'collapsible',
				'label' => __('Separator','frontend-builder'),
				'open' => 'true',
				'options' => array(
					'style' => array(
						'type' => 'select',
						'label_width' => 0.25,
						'control_width' => 0.75,
						'label' => __('Type:','frontend-builder'),
						'options' => array(
							'solid' => __('Solid','frontend-builder'),
							'dashed' => __('Dashed','frontend-builder'),
							'dotted' => __('Dotted','frontend-builder'),
							'double' => __('Double','frontend-builder')
						)
					),
					'width' => array(
						'type' => 'number',
						'label' => __('Width:','frontend-builder'),
						'std' => 1,
						'max' => 20,
						'half_column' => 'true',
						'unit' => 'px',
					),
					'color' => array(
						'type' => 'color',
						'label_width' => 0.25,
						'control_width' => 0.5,
						'label' => __('Color:','frontend-builder'),
						'std' => $opts['main_color']
					)
				)
			)
		),
		$classControl,
		array(
			'group_general' => array(
				'type' => 'collapsible',
				'label' => __('General','frontend-builder'),
				'options' => array(
					'bot_margin' => array(
						'type' => 'number',
						'label' => __('Bottom margin:','frontend-builder'),
						'std' => $opts['bottom_margin'],
						'unit' => 'px'
					)
				)
			)
		),
		$animationControl
		)
	)
);

/* -------------------------------------------------------------------------------- */
/* READ MORE */
/* -------------------------------------------------------------------------------- */
$read_more = array(
	'read_more' => array(
		'type' => 'draggable',
		'text' => __('More tag','frontend-builder'),
		'desc' => __('This module has no options','frontend-builder'),
		'icon' => $this->url.'images/icons/more-tags.png',
		'function' => 'fbuilder_more',
		'group' => 'Basic',
		'options'  => array()
	)
);



$fbuilder_shortcodes = array_merge($heading, $text, $bulletlist, $button, $image, $video, $audio, $features, $contact_form, $slider, $creative_post_slider, $testimonials, $tabs, $tour, $accordion, $toggle, $counter, $percentage_bars, $percentage_chart, $graph, $gauge_chart, $piechart, $pricing, $code, $alert, $menu, $icon_menu, $sidebar, $search, $separator, $read_more, $post, $recent_post, $gallery);





$output = $fbuilder_shortcodes;
?>