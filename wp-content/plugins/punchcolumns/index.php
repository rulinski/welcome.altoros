<?php
/* Plugin Name: punchColumns
Plugin URI: http://www.themepunch.com
Description: Content Columns Shortcodes for WordPress
Version: 1.0
Author: ThemePunch
Author URI: http://www.themepunch.com
License: GPLv2 or later
*/

define( 'THUNDERCOLUMNS_PATH', plugin_dir_path(__FILE__) );
define( 'THUNDERCOLUMNS_URL',str_replace("index.php","",plugins_url( 'index.php', __FILE__ )));

if ( ! defined( 'ABSPATH' ) )
	die( "Can't load this file directly" );

class ThunderColumns
{
	function __construct() {
		add_action( 'admin_init', array( $this, 'action_admin_init' ) );
	}
	
	function action_admin_init() {
		if ( current_user_can( 'edit_posts' ) && current_user_can( 'edit_pages' ) ) {
			add_filter( 'mce_buttons', array( $this, 'filter_mce_button' ) );
			add_filter( 'mce_external_plugins', array( $this, 'filter_mce_plugin' ) );
		}
	}
	
	function filter_mce_button( $buttons ) {
		array_push( $buttons, '|', 'thundercolumns_button' );
		return $buttons;
	}
	
	function filter_mce_plugin( $plugins ) {
		$plugins['thundercolumns'] = THUNDERCOLUMNS_URL . 'editor/thundercolumns_plugin.js';
		return $plugins;
	}
}
$thunderCode = new ThunderColumns();


//SHORTCODES COLUMNS

	if (!function_exists('tp_row_shortcode')) {
		function tp_row_shortcode($atts, $content = null){
			global $post;
			$box=get_post_meta($post->ID,"goodweb_show_box",true);
			/*if(empty($box)) $fluid="";
			else */$fluid = "-fluid";
		
			return '<section class="row'.$fluid.'">'.do_shortcode($content).'</section><div style="clear:both"></div>';
		}
		add_shortcode('tp_row', 'tp_row_shortcode');
	}
	
	if (!function_exists('tp_span_shortcode')) {
		function tp_span_shortcode($atts, $content = null){
			$span = empty($atts["cols"]) ? 12 : $atts["cols"];
			$html = '<div class="span' . $span . '">' . do_shortcode($content) . '</div>';
			return $html;
		}
		add_shortcode('tp_span', 'tp_span_shortcode');
	}
	
	
function columnsCSS() {
	if (!is_admin()) wp_enqueue_style( 'thunderColumns',THUNDERCOLUMNS_URL . 'editor/columns.css',array('bootstrap'));
}
add_action('wp_enqueue_scripts', 'columnsCSS');
?>