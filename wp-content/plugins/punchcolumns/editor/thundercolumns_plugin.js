(function(){
		tinymce.create('tinymce.plugins.thundercolumns', {
	    init : function(ed, url) {
		    thundercolumnsurl = url;
		},
		createControl: function(n, cm) {
	        switch (n) {
	            case 'thundercolumns_button':
	                var c = cm.createSplitButton('thundercolumns_button', {
	                    title : 'punchColumns',
	                    image : thundercolumnsurl+'/columns.png',
	                    
	                });
	
	                c.onRenderMenu.add(function(c, m) {
	                    m.add({title : 'punchColumns', 'class' : 'mceMenuItemTitle'}).setDisabled(1);
	
	                    m.add({title : 'Full', onclick : function() {
	                    	tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[tp_row][tp_span]YOUR_TEXT_HERE[/tp_span][/tp_row]');                    
	                    }});
	                    
	                    m.add({title : '1/2 1/2', onclick : function() {
	                    	tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[tp_row][tp_span cols=6]YOUR_TEXT_HERE[/tp_span][tp_span cols=6]YOUR_TEXT_HERE[/tp_span][/tp_row]');                    
	                    }});
	                    
	                    m.add({title : '1/3 1/3 1/3', onclick : function() {
	                    	tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[tp_row][tp_span cols=4]YOUR_TEXT_HERE[/tp_span][tp_span cols=4]YOUR_TEXT_HERE[/tp_span][tp_span cols=4]YOUR_TEXT_HERE[/tp_span][/tp_row]');                    
	                    }});
	                    
	                    m.add({title : '1/3 2/3', onclick : function() {
	                    	tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[tp_row][tp_span cols=4]YOUR_TEXT_HERE[/tp_span][tp_span cols=8]YOUR_TEXT_HERE[/tp_span][/tp_row]');                    
	                    }});
	                    
	                    m.add({title : '2/3 1/3', onclick : function() {
	                    	tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[tp_row][tp_span cols=8]YOUR_TEXT_HERE[/tp_span][tp_span cols=4]YOUR_TEXT_HERE[/tp_span][/tp_row]');                    
	                    }});
	                    
	                    m.add({title : '1/4 1/4 1/4 1/4', onclick : function() {
	                    	tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[tp_row][tp_span cols=3]YOUR_TEXT_HERE[/tp_span][tp_span cols=3]YOUR_TEXT_HERE[/tp_span][tp_span cols=3]YOUR_TEXT_HERE[/tp_span][tp_span cols=3]YOUR_TEXT_HERE[/tp_span][/tp_row]');                    
	                    }});
	                    
	                    m.add({title : '1/4 3/4', onclick : function() {
	                    	tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[tp_row][tp_span cols=3]YOUR_TEXT_HERE[/tp_span][tp_span cols=9]YOUR_TEXT_HERE[/tp_span][/tp_row]');                    
	                    }});
	                    
	                     m.add({title : '3/4 1/4', onclick : function() {
	                    	tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[tp_row][tp_span cols=9]YOUR_TEXT_HERE[/tp_span][tp_span cols=3]YOUR_TEXT_HERE[/tp_span][/tp_row]');                    
	                    }});
	                    
	                     m.add({title : '1/4 1/4 1/2', onclick : function() {
	                    	tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[tp_row][tp_span cols=3]YOUR_TEXT_HERE[/tp_span][tp_span cols=3]YOUR_TEXT_HERE[/tp_span][tp_span cols=6]YOUR_TEXT_HERE[/tp_span][/tp_row]');                    
	                    }});
	                    
	                    m.add({title : '1/2 1/4 1/4', onclick : function() {
	                    	tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[tp_row][tp_span cols=6]YOUR_TEXT_HERE[/tp_span][tp_span cols=3]YOUR_TEXT_HERE[/tp_span][tp_span cols=3]YOUR_TEXT_HERE[/tp_span][/tp_row]');                    
	                    }});
	                    
	                    m.add({title : '1/5 1/5 1/5 1/5 1/5', onclick : function() {
	                    	tinyMCE.activeEditor.execCommand('mceInsertContent', 0, '[tp_row][tp_span cols=2]YOUR_TEXT_HERE[/tp_span][tp_span cols=2]YOUR_TEXT_HERE[/tp_span][tp_span cols=2]YOUR_TEXT_HERE[/tp_span][tp_span cols=2]YOUR_TEXT_HERE[/tp_span][tp_span cols=4]YOUR_TEXT_HERE[/tp_span][/tp_row]');                    
	                    }});
	                });
	
	              // Return the new splitbutton instance
	              return c;
	        }
	
	        return null;
	    }
	});	
	tinymce.PluginManager.add('thundercolumns', tinymce.plugins.thundercolumns);
})()