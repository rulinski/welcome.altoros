<?php

/**
 * Template Name: About Page
 */

get_header(); ?>
<?php
//Page Options
$pageoptions = getOptions($post->ID);
//debug($pageoptions);
$page_gallery        = isset($pageoptions["welcome_gallery"]) ? $pageoptions["welcome_gallery"] : "";
?>
	<div class="site">
		<div id="primary" class="content-area content-left-block">
			<main id="main" class="site-main" role="main">

				<?php while (have_posts()) : the_post(); ?>

					<?php get_template_part('template-parts/content', 'page'); ?>

				<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- #primary -->
		<?php get_sidebar(); ?>
		<br clear="all"/>
	</div>


<?php if ($page_gallery) { ?>
	<div class="site" id="life"><h3>ЖИЗНЬ В АЛЬТОРОС</h3></div>
	<?=do_shortcode('[nggallery id=1]')?>
	<!--<ul class="gallery">-->
	<!--	<li><img src="/wp-content/themes/welcome/image/test-gallery.jpg" alt=""></li>-->
	<!--	<li><img src="/wp-content/themes/welcome/image/test-gallery.jpg" alt=""></li>-->
	<!--	<li><img src="/wp-content/themes/welcome/image/test-gallery.jpg" alt=""></li>-->
	<!--	<li><img src="/wp-content/themes/welcome/image/test-gallery.jpg" alt=""></li>-->
	<!--	<li><img src="/wp-content/themes/welcome/image/test-gallery.jpg" alt=""></li>-->
	<!--	<li><img src="/wp-content/themes/welcome/image/test-gallery.jpg" alt=""></li>-->
	<!--	<li><img src="/wp-content/themes/welcome/image/test-gallery.jpg" alt=""></li>-->
	<!--	<li><img src="/wp-content/themes/welcome/image/test-gallery.jpg" alt=""></li>-->
	<!--	<li><img src="/wp-content/themes/welcome/image/test-gallery.jpg" alt=""></li>-->
	<!--	<li><img src="/wp-content/themes/welcome/image/test-gallery.jpg" alt=""></li>-->
	<!--	<li><img src="/wp-content/themes/welcome/image/test-gallery.jpg" alt=""></li>-->
	<!--	<li><img src="/wp-content/themes/welcome/image/test-gallery.jpg" alt=""></li>-->
	<!--</ul>-->
<?php } ?>

	<div class="site">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<h3 id="success">Success Story</h3>
				<div class="success-story" id="success">
					<?php
					$args=array(
						'cat' => 10,
						'post_type' => 'post',
						'post_status' => 'publish',
						'posts_per_page' => -1,
						'caller_get_posts'=> 1,
						'orderby'          => 'date',
						'order'            => 'ASC',
					);
					$my_query = null;
					$my_query = new WP_Query($args);

					if( $my_query->have_posts() ) :
						$year = '';
						$derecttion = 'right';
						while ($my_query->have_posts()) : $my_query->the_post();
							$node_year = date('Y',strtotime($post->post_date));
							$derecttion = ($derecttion == 'left') ? 'right' : 'left';
							$image = get_the_post_thumbnail( $post->ID, 'thumbnail', array( 'class' => 'alignleft' ) );
							if (!$year) {
								$out = "
								<div class=\"posts-year first-year\">
									<div class=\"date-year\">{$node_year}</div>";
								$out .= "
								<div class=\"post-wrp post-{$derecttion}\">
									<div class=\"post-cnt\">
										{$image}
										<p>{$post->post_title}</p>
									</div>
								</div>";
							} elseif ($year == $node_year) {
								$out .= "
								<div class=\"post-wrp post-{$derecttion}\">
									<div class=\"post-cnt\">
										{$image}
										<p>{$post->post_title}</p>
									</div>
								</div>";
							} elseif ($year && ($year < $node_year)) {
								$out .= "
								</div>
								<div class=\"posts-year\">
									<div class=\"date-year\">{$node_year}</div>";
								$out .= "
								<div class=\"post-wrp post-{$derecttion}\">
									<div class=\"post-cnt\">
										{$image}
										<p>{$post->post_title}</p>
									</div>
								</div>";
							}
							$year = $node_year;
						endwhile;
						$out .= "</div>";
						echo $out;

					endif; ?>

				</div>

			</main><!-- #main -->
		</div><!-- #primary -->
	</div>

<?php get_footer(); ?>