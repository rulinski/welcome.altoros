<?php

/**
 * Template Name: Full Width Page
 *
 * @package kikirt
 */

get_header();
//Page Options
$pageoptions         = getOptions($post->ID);
$page_bottom_content = isset($pageoptions["welcome_bottom_content"]) ? $pageoptions["welcome_bottom_content"] : "";
?>
	<div class="site">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

				<?php
				if (strpos($_SERVER['REQUEST_URI'], 'jobs/', 0)) {
					$args  = array('taxonomy' => 'jobman_category', 'select_name' => 'All', 'name__like' => 'Tech_',);
					$terms = get_terms($args);
					if (function_exists('welcome_tax_dropdown'))
						echo '<div class="filters"><div class="selectable"><label>Технология:</label> ' . welcome_tax_dropdown($terms, 'tech_list') . '</div>';

					$args  = array('taxonomy' => 'jobman_category', 'select_name' => 'All', 'name__like' => 'Sphere_',);
					$terms = get_terms($args);
					if (function_exists('welcome_tax_dropdown'))
						echo '<div class="selectable"><label>Сфера:</label> ' . welcome_tax_dropdown($terms, 'sphere_list') . '</div>';
					echo '<label>Поиск:</label>
						<div id="filter-search" class="filter-search">
							<input type="text" value="" onkeyup="showJobsByText()">
							<a class="search" onklick="showJobsByText()"><i class="welcome-altoros-sprite-008"></i></a>
						</div>
					</div>';
				} ?>

				<?php
				if (have_posts()) :

					/* Start the Loop */
					while (have_posts()) : the_post();

						get_template_part('template-parts/content', 'page');

					endwhile;

				else :

					get_template_part('template-parts/content', 'none');

				endif; ?>

			</main><!-- #main -->
		</div><!-- #primary -->
		<?php if ($page_bottom_content) { ?>
			<!-- bottom block -->
			<?= $page_bottom_content ?>
		<?php } ?>
	</div>
<?php

get_footer();