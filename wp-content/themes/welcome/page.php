<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package SoSimple
 */

get_header(); ?>
<?php
//Page Options
$pageoptions = getOptions($post->ID);
//debug($pageoptions);
$page_bottom_content = isset($pageoptions["welcome_bottom_content"]) ? $pageoptions["welcome_bottom_content"] : "";
$page_gallery        = isset($pageoptions["welcome_gallery"]) ? $pageoptions["welcome_gallery"] : "";
?>
<div class="site">
	<div id="primary" class="content-area content-left-block">
		<main id="main" class="site-main" role="main">

			<?php while (have_posts()) : the_post(); ?>

				<?php get_template_part('template-parts/content', 'page'); ?>

				<?php
				// If comments are open or we have at least one comment, load up the comment template.
				//if (comments_open() || get_comments_number()) :
				//	comments_template();
				//endif;
				?>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->


	</div><!-- #primary -->
	<?php get_sidebar(); ?>
	<br clear="all"/>
	<?php if ($page_bottom_content) { ?>
		<!-- bottom block -->
		<?= $page_bottom_content ?>
	<?php } ?>
</div>

<?php get_footer(); ?>
