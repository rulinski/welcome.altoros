/**
 * Created by rulinski on 26.06.16.
 */
jQuery(function () {
    jQuery('#header_search').slideUp(200);
    jQuery('.accordion .element-title').on('click', function () {
        jQuery(this).closest('.element').toggleClass('active');
    });
    jQuery('.job-div .element-title a').click(function(event) {
        if (jQuery(this).closest('.job-div').hasClass('active') == false){
            event.preventDefault();
        }
    });
    jQuery('.social .search').on('click', function () {
        jQuery('#header_search').slideDown(200);
    });
    jQuery(document).mouseup(function (e) {
        var div = jQuery("#header_search");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            div.slideUp(200);
        }
    });

    jQuery(".post-right").boxLoader({
        direction: "x",
        position: "50%",
        effect: "fadeIn",
        duration: "1s",
        windowarea: "90%"
    });

    jQuery(".post-left").boxLoader({
        direction: "x",
        position: "-50%",
        effect: "fadeIn",
        duration: "1s",
        windowarea: "90%"
    });

    /** buttons for jobs page*/
    jQuery('.links-btn a').on('click', function () {
        jQuery(this).closest('.f-right').find('.form_place').hide();
        jQuery(this).closest('.f-right').find('.links-btn>a').hide();

        var form = jQuery(this).attr("data-form");
        jQuery(this).closest('.links-btn').find('.form_content').show();
        jQuery(this).closest('.links-btn').find('.form_content .' + form).show();
        jQuery(this).closest('.links-btn').find('form input[name="purl"]').val(jQuery(this).closest('.element-block').find('.page_url').text());
        jQuery(this).closest('.links-btn').find('form input[name="send_to"]').val(jQuery(this).closest('.element-block').find('.page_send_to').text());
    });

    jQuery('.links-btn .close_form').on('click', function () {
        jQuery('.form_content').hide();
        jQuery('.form_content .form_place').hide();
        jQuery('.links-btn>a').show();
        jQuery('.wpcf7-response-output').text('');
    });

    /** submit_button_ok click */
    jQuery('.submit_button_ok').on('click', function () {
        console.log('asd');
        console.log(jQuery(this).closest('form').serialize());
    });

    /** Show all sidebar on click*/
    if (jQuery('#secondary .widget_jobmanhighlightedjobswidget').length) {
        jQuery('#secondary').append('<div onclick="showSidebar(\'widget_jobmanhighlightedjobswidget\')">>></div>');
        jQuery('#secondary .widget_jobmanhighlightedjobswidget').toggleClass('short');
    }


});

function showSidebar(name) {
    jQuery('#secondary .' + name).toggleClass('short');
}

jQuery(document).ready(function ($) {
    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 300,
    //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1200,
    //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
    //grab the "back to top" link
        $back_to_top = jQuery('.cd-top');

    //hide or show the "back to top" link
    jQuery(window).scroll(function () {
        ( jQuery(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if (jQuery(this).scrollTop() > offset_opacity) {
            $back_to_top.addClass('cd-fade-out');
        }
    });

    //smooth scroll to top
    $back_to_top.on('click', function (event) {
        event.preventDefault();
        jQuery('body,html').animate({
                scrollTop: 0,
            }, scroll_top_duration
        );
    });
});

//function scroll(){
//
//    jQuery("html, body").animate({
//        scrollTop: jQuery(".boxes").offset().top
//    }, {
//        queue: false,
//        duration: 1000});
//
//}

/** Share buttons */
Share = {
    vkontakte: function (purl, ptitle, pimg, text) {
        url = 'http://vkontakte.ru/share.php?';
        url += 'url=' + encodeURIComponent(purl);
        url += '&title=' + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&image=' + encodeURIComponent(pimg);
        url += '&noparse=true';
        Share.popup(url);
    },
    linkedin: function (purl, ptitle, pimg, text) {
        url = 'http://www.linkedin.com/shareArticle?mini=true';
        url += '&url=' + encodeURIComponent(purl);
        url += '&title=' + encodeURIComponent(ptitle);
        Share.popup(url);
    },
    facebook: function (purl, ptitle, pimg, text) {
        url = 'http://www.facebook.com/sharer.php?s=100';
        url += '&p[title]=' + encodeURIComponent(ptitle);
        url += '&p[summary]=' + encodeURIComponent(text);
        url += '&p[url]=' + encodeURIComponent(purl);
        url += '&p[images][0]=' + encodeURIComponent(pimg);
        Share.popup(url);
    },
    twitter: function (purl, ptitle) {
        url = 'http://twitter.com/share?';
        url += 'text=' + encodeURIComponent(ptitle);
        url += '&url=' + encodeURIComponent(purl);
        url += '&counturl=' + encodeURIComponent(purl);
        Share.popup(url);
    },

    popup: function (url) {
        window.open(url, '', 'toolbar=yes,scrollbars=yes,resizable=yes,width=626,height=436');
    }
};

/** linkedIn authorise */
function checkIfLinkedINAutorized(el) {
    jQuery('form.linkedIn_list_form button').removeClass('active');
    jQuery(el).addClass('active');
    if (IN.User.isAuthorized()) {
        onLinkedInLogin(el);
    } else {
        IN.User.authorize(onLinkedInLogin);
    }
}
function onLinkedInLogin(button) {
    $button = jQuery('form.linkedIn_list_form button.active');
    // we pass field selectors as a single parameter (array of strings)
    IN.API.Profile("me").fields([
        'id',
        'first-name',
        'last-name',
        'date-of-birth',
        'email-address',
        'phone-numbers',
        'picture-url',
        'main-address',
        'location',
        'im-accounts',
        'primary-twitter-account',
        'public-profile-url',
        'headline',
        'positions',
        'skills',
        'educations',
        'languages',
        'three-current-positions',
        'three-past-positions',
        'interests',
        'volunteer',
        'proposal-comments',
        'recommendations-received',
        'num-recommenders',
        'patents',
        'specialties',
        'associations',
        'publications',
        'certifications',
        'courses',
        'honors-awards',
        'summary',
        'last-modified-timestamp'

    ]).result(function (result) {
        var data = new FormData();
        // data.append('csrfmiddlewaretoken', 'LUCZ1AHo5AMbsvtNJHUb3B9QqtcUnAsL');
        data.append('linfedInProfile', JSON.stringify(result));
        data.append('action', 'linkedin_js');
        var _url = '/ajaxhandler.php';
        jQuery.ajax({
            url: _url,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
                $button.closest('form').prepend(data);
                $button.children('span').html("Отправить профиль");
                $button.attr('onclick', '').unbind('click');
                // $button.addClass('submit_button_ok');
                $button.attr('type', 'submit');
                // $button.siblings('span.IN-widget').fadeOut().remove();
            },
            error: function (resp) {
                console.log('ajax failed!');
            }
        });

    }).error(function (err) {
        console.log(err);
    });
}
jQuery(document).ready(function () {
    /*right block fixed*/

    console.log('testr');
    function rightBlockFixed() {
        console.log('rightBlockFixed');
        if (jQuery('#secondary').is(':visible')) {

            var offset = jQuery('#secondary').offset();

            if (jQuery('body').scrollTop() >= offset.top) {
                jQuery('#secondary').addClass('fix-right-block').css('left', offset.left);
            } else {
                jQuery('#secondary').removeClass('fix-right-block');
            }
        }
    }

    //var offset = jQuery('#secondary').offset();
    //    console.log(offset.top);
    //console.log(offset.left);
    //console.log(jQuery('#primary').height()-jQuery('#secondary').height());
    //console.log(jQuery('body').scrollTop());
    jQuery(window).scroll(function () {
        rightBlockFixed();
    });

    rightBlockFixed();
});

function show_jobs(id, selectName) {
    if (id != 'All') {
        jQuery('.job-div').hide();
        jQuery('.job-div').each(function () {
            if (jQuery(this).find('span.cat').text().indexOf(id) != -1) {
                jQuery(this).show();
            }
        });
    } else {
        jQuery('.job-div').show();
    }
    isVacancyExist();
}

function showJobsByText() {
    var search = jQuery('#filter-search input').val().toLowerCase();
    jQuery('.job-div').hide();
    jQuery('.job-div').each(function () {
        if (jQuery(this).text().toLowerCase().indexOf(search) != -1) {
            jQuery(this).show();
        }
    });
    isVacancyExist();
}

function isVacancyExist() {
    jQuery('.jobs-warning').remove();
    if (!jQuery('.job-div').is(":visible")) {
        jQuery('.accordion').append('<div class="jobs-warning warning">Ой, такой вакансии на данный момент в компании не обнаружено</div>');
        jQuery('.jobs-warning').show();
    }
}