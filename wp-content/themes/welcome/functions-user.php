<?php
//User profile
add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

function my_show_extra_profile_fields( $user ) { ?>

	<h3>Extra profile information</h3>

	<table class="form-table">
		<tr>
			<th><label for="twitter">Skype</label></th>
			<td>
				<input type="text" name="skype" id="skype" value="<?php echo esc_attr( get_the_author_meta( 'skype', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Skype username.</span>
			</td>
		</tr>
		<tr>
			<th><label for="twitter">Linkedin</label></th>
			<td>
				<input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Linkedin user link.</span>
			</td>
		</tr>
		<tr>
			<th><label for="twitter">Phone</label></th>
			<td>
				<input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your Phone number.</span>
			</td>
		</tr>

	</table>
<?php }

add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	/* Copy and paste this line for additional fields. Make sure to change 'twitter' to the field ID. */
	update_user_meta( $user_id, 'linkedin', $_POST['linkedin'] );
	update_user_meta( $user_id, 'skype', $_POST['skype'] );
	update_user_meta( $user_id, 'phone', $_POST['phone'] );
}

add_shortcode( 'jobs_hr', 'jobs_hr_shortcode' );
function jobs_hr_shortcode( $atts, $content, $tag ) {
	$userdata = get_userdata( $atts['id'] );
	$user_image = str_replace('-150x150.','.',get_avatar( $atts['id'], 220 ));

	$post_3 = get_post( $atts['job_id'] );
	$title = $post_3->post_title;
	$pdescription = substr(strip_tags(do_shortcode('[job_field5]')), 0, 100);
	$purl = get_permalink($post_3->ID);
	$img_path = urldecode('http://www.altoros.com/blog/wp-content/uploads/2015/04/AltorosLogo_mini1.png');

	include 'template-parts/hr-block.php';

	return $out;
}