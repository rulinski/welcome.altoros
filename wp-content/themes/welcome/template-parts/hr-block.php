<?php
$form = do_shortcode('[contact-form-7 id="1177" title="vacancy"]');
$out=<<<TEXT
<div class="wrp-img">
	<!--img src="/wp-content/themes/welcome/image/hr.png" alt=""-->
	{$user_image}
</div>
<h4>{$userdata->display_name}</h4>
<div class="links">
	<a href="mailto:{$userdata->user_email}"><i class="welcome-altoros-sprite-012"></i>{$userdata->user_email}</a><br/>
	<a href="skype:{$userdata->skype}?chat"><i class="welcome-altoros-sprite-013"></i>{$userdata->skype}</a><br/>
	<a><i class="welcome-altoros-sprite-014"></i>{$userdata->phone}</a><br/>
	<a href="https://www.linkedin.com/in/{$userdata->linkedin}" target="_blank"><i class="welcome-altoros-sprite-015"></i>{$userdata->linkedin}</a>
</div>
<div class='hidden'>
	<p class='page_url'>{$purl}</p>
	<p class='page_send_to'>{$userdata->user_email}</p>
</div>
<div class="links-btn">
	<a data-form="application_form"><span><i class="welcome-altoros-sprite-016"></i></span>Отправить резюме</a>
	<a data-form="linkedin_form"><span><i class="welcome-altoros-sprite-017"></i></span>Откликнуться с LinkledIn</a>
	<div class="form_content" style="display: none;">
		<div class="form_place application_form" style="display: none;">
			<h3 class="form_title">Форма заявки</h3>
			<button type="button" class="close_form">x</button>
			<!-- Vacancy application form -->
			{$form}
			<!-- end of Vacancy application form -->
		</div>

		<div class="form_place linkedin_form" style="display: none;">
			<h3 class="form_title">Отправить с LinkedIn</h3>
			<button type="button" class="close_form">x</button>
			
			<!-- LinkedIn application form -->
			<div role="form" class="wpcf7" id="wpcf7-f1177-o1" lang="en-US" dir="ltr">
			<div class="screen-reader-response" role="alert"></div>
			<form action="/jobs/?p={$atts['job_id']}#wpcf7-1419-o11" method="post" class="linkedIn_list_form wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
				<div style="display: none;">
					<input type="hidden" name="_wpcf7" value="1419">
					<input type="hidden" name="_wpcf7_version" value="4.4.2">
					<input type="hidden" name="_wpcf7_locale" value="en_US">
					<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1419-o11">
					<input type="hidden" name="_wpnonce" value="6c90f52eee">
				</div>
				<div style="display: none;">
					<span class="wpcf7-form-control-wrap purl"><input type="text" name="purl" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span><br>
					<span class="wpcf7-form-control-wrap send_to"><input type="email" name="send_to" value="{$userdata->user_email}" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email" aria-invalid="false"></span>
				</div>
	
				<button type="button" class="submit_button wpcf7-form-control wpcf7-submit" onclick="checkIfLinkedINAutorized(this)">
					<i></i><span>Получить профиль</span>
				</button>
				<div class="wpcf7-response-output wpcf7-display-none"></div>
			</form>
			</div>
			<!-- end of LinkedIn application form -->
		</div>
	</div>
</div>

<div class="links-social">
	<label>Поделиться:</label>
	<a onclick="Share.facebook('{$purl}','{$title}','{$img_path}','{$pdescription}')"><i class="welcome-altoros-sprite-002"></i></a>
	<a onclick="Share.twitter('{$purl}','{$title}','{$img_path}','{$pdescription}')"><i class="welcome-altoros-sprite-003"></i></a>
	<a onclick="Share.vkontakte('{$purl}','{$title}','{$img_path}','{$pdescription}')"><i class="welcome-altoros-sprite-005"></i></a>
	<a onclick="Share.linkedin('{$purl}','{$title}','{$img_path}','{$pdescription}')"><i class="welcome-altoros-sprite-004"></i></a>
</div>
TEXT;
