<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package SoSimple
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php wp_head(); ?>

	<?php
	//Page Options
	$pageoptions         = getOptions($post->ID);
	$page_header_links   = isset($pageoptions["welcome_page_header_links"]) ? $pageoptions["welcome_page_header_links"] : "";
	$page_header_title   = isset($pageoptions["welcome_page_header_title"]) ? $pageoptions["welcome_page_header_title"] : "";
	$page_description    = isset($pageoptions["welcome_page_description"]) ? $pageoptions["welcome_page_description"] : "";
	?>
	<script type="text/javascript" src="//platform.linkedin.com/in.js">
		api_key:7785hjbjxfo25u
		scope: r_emailaddress r_basicprofile
		authorize: false
	</script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed">

	<section class="header">
		<div class="header-top">
			<div class="site">
				<a class="logo" href="http://welcome.altoros.com/"><i class="welcome-altoros-sprite-001"></i></a>
				<nav id="site-navigation" class="main-navigation" role="navigation">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'sosimple' ); ?></button>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
				</nav><!-- #site-navigation -->
				<div class="social">
					<a target="_blank" href="https://www.facebook.com/AltorosDevelopment/"><i class="welcome-altoros-sprite-002"></i></a>
					<a target="_blank" href="https://twitter.com/altoros"><i class="welcome-altoros-sprite-003"></i></a>
					<a target="_blank" href="https://www.linkedin.com/company/altoros-systems"><i class="welcome-altoros-sprite-004"></i></a>
					<a target="_blank" href="http://vk.com/altorosdevelopment"><i class="welcome-altoros-sprite-005"></i></a>
					<a target="_blank" href="https://www.youtube.com/channel/UCzVZ70hdQxYqzxavQL6x8pw"><i class="welcome-altoros-sprite-006"></i></a>
					<!--<a class="enter" href="/wp-login.php">Войти</a>-->
					<a class="search" href="#"><i class="welcome-altoros-sprite-007"></i></a>
					<!-- !SEARCH FORM -->
					<article id="header_search" class="pull-right">
						<?php get_search_form(); ?>
					</article>
					<!-- END OF SEARCH FORM -->
					<!--<a class="search" href="#"><i class="welcome-altoros-sprite-007"></i></a>-->
				</div>
			</div>
		</div>
		<?php if ($page_header_links) { ?>
			<div class="site">
				<div class="header-right">
					<?= $page_header_links ?>
				</div>
			</div>
		<?php } ?>
	</section>
	<?php if ($page_header_title) { ?>
		<div class="header-title">
			<div class="site">
				<div class="title full-width">
					<?= $page_header_title ?>
				</div>
			</div>
		</div>
	<?php } ?>
	<?php if ($page_description) { ?>
		<section class="description">
			<div class="site">
				<?= $page_description ?>
			</div>
		</section>
	<?php } ?>

	<div id="content" class="site-content content">
